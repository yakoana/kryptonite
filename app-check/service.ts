/* * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Zip extension service test app
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import * as express from "express";
import * as parser from "body-parser";
import * as fs from "fs";
import * as path from "path";
import { spawnSync } from "child_process";

let PKI: string;
let OPENSSL: string;
function new_file(ext: string): string | null
{
	let filename: string = (new Date()).getTime().toString() + ext;
	try
	{
		fs.accessSync(filename, fs.constants.F_OK);
		return null;
	}
	catch (e) { return filename; }
}
function write(path: string, body: Buffer): Promise<null>
{
	return new Promise((resolve, reject) =>
	{
		fs.writeFile(path, body, { encoding: "utf8" }, (err) =>
		{
			console.log("Wrinting file " + path + "...")
			if (!err) resolve(); else reject(err);
		}); 
	});
}
function sign_callback(request: express.Request , response: express.Response): void
{
	let newName: string | null = null;
	do { newName = new_file(".req"); }
	while (!newName);
	try
	{
		let target: string = newName ? path.join(__dirname, newName) : "";
		write(target, request.body)
		.then(()  =>
		{
			let cmd: string = path.resolve(PKI, "issue");
			let arg: ReadonlyArray<string> = [target, OPENSSL];
			console.log("Sending to sign by " + cmd + " with params: " + arg);
			let ret = spawnSync(cmd, arg, { cwd: path.resolve(__dirname), shell: true, windowsHide: true });
			if (ret.status != 0) throw ret;
			fs.unlinkSync(target);

			target = target.replace(".req", ".p7b");
			console.log("Will read:" + target);
			let pem: string = fs.readFileSync(target, { encoding: "UTF-8" });
			response.status(200).send(pem);
		})
		.catch((reason: any) =>
		{
			console.log("Could not write file due to error %s\n", JSON.stringify(reason));
			response.status(500).send(JSON.stringify(reason));
		});
	}
	catch (e)
	{
		console.log("Could process request due to error %s\n", JSON.stringify(e));
		response.status(500).send(JSON.stringify(e));
	}
}
function main(): void
{
	process.argv.forEach((value) =>
	{
		if (value.startsWith("--pki"))
		{
			let parts = value.split("=");
			if (parts.length != 2) throw new Error("Invalid argument");
			PKI = path.resolve(parts[1], "bin");
		}
		else if (value.startsWith("--ssl"))
		{
			let parts = value.split("=");
			if (parts.length != 2) throw new Error("Invalid argument");
			OPENSSL = parts[1];
		}
	});
	if (!PKI) throw new Error("--pki=[path] argument required");
	if (!OPENSSL && !process.env.OPENSSL) throw new Error("--ssl=[path] argument required")

	let app = express();
	app.use("/", express.static(__dirname));
	app.use(parser.text({ type: "text/plain" }));
	app.post("/issuecertificate", sign_callback);
	app.listen(8888, "localhost", () => { console.log("Listening at port 8888...\n"); });
}	main();
