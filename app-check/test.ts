/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Kryptonite regression extension client test app
 * Copyleft (C) 2018/2019 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';

function __findOutput(): HTMLTextAreaElement
{
	let output: HTMLElement | null = document.getElementById("test-output");
	if (!output || !(output instanceof HTMLTextAreaElement)) throw new Error("Could not find test output HTMLTextAreaElement. Aborting...");
	else return output;
}
function __devices(): Promise<string[]>
{
	return new kptaenroll.Enroll()
	.enumerateDevices()
	.then((value: kptaenroll.Response) =>
	{
		if (value.result != kptaenroll.KPTAError.KPTA_OK) return Promise.reject(new Error("Response reason " + value.reason.toString()));
		return Promise.resolve(value.payload);
	});
}
function __generateCSR(device: string, cn: string): Promise<string>
{
	return new Promise((resolve, reject) =>
	{
		return new kptaenroll.Enroll()
		.generateCSR(device, cn, true)
		.then((value: kptaenroll.Response) =>
		{
			if (value.result != kptaenroll.KPTAError.KPTA_OK) reject(new Error("Response reason " + value.reason.toString()));
			resolve(value.payload);
		});
	});
}
function __issue_certificate(csr: string): Promise<string>
{
	return new Promise((resolve, reject) =>
	{
		let client: XMLHttpRequest = new XMLHttpRequest();
		client.addEventListener("load", (e: Event) =>
		{
			if (client.status != 200) return reject(new Error("Response page: " + client.response));
			return resolve(client.responseText);
		});
		client.addEventListener("error", (e) => { return reject(e); });
		client.open("POST", "/issuecertificate");
		client.setRequestHeader("Content-Type", "text/plain");
		client.send(csr);
	});
}
function __install(cms: string): Promise<boolean>
{
	return new kptaenroll.Enroll()
	.installCertificate(cms)
	.then((value: kptaenroll.Response) =>
	{
		if (value.result != kptaenroll.KPTAError.KPTA_OK) return Promise.reject(new Error("Response reason " + value.reason.toString()));
		return Promise.resolve(true);
	});
}
function __certificates(): Promise<kptasign.Certificate[]>
{
	return new kptasign.Sign().enumerateCerts()
	.then((value: kptasign.Response) =>
	{
		if (value.reason != kptasign.KPTAError.KPTA_OK) return Promise.reject(new Error("Response reason " + value.reason.toString()));
		return Promise.resolve(value.payload.certificates);
	});
}
function __sign(cert: kptasign.Certificate, contents: string): Promise<Uint8Array>
{
	return new kptasign.Sign().sign(cert, contents, true)
	.then((value: kptasign.Response) =>
	{
		if (value.reason != kptasign.KPTAError.KPTA_OK) return Promise.reject(new Error("Response reason " + value.reason.toString()));
		return Promise.resolve(value.payload);
	});
}
function __zip(cms: Uint8Array, out: HTMLTextAreaElement): Promise<Uint8Array>
{
	return new Promise((resolve, reject) =>
	{
		let zip: kptazip.Deflate = new kptazip.Deflate();
		let handle: number;
		return zip.create().then((value: kptazip.Response) =>
		{
			if (value.reason != kptasign.KPTAError.KPTA_OK) reject(new Error("Response reason " + value.reason.toString()));
			handle = value.payload;
			out.value += "Zip file created with handle " + handle.toString() + "\n";
			return zip.add(handle, cms, "My entry");
		})
		.then((response: kptazip.Response) =>
		{
			if (response.reason != kptasign.KPTAError.KPTA_OK) reject(new Error("Response reason " + response.reason.toString()));
			out.value += "CMS Signed Data document added to zip file\n";
			return Promise.resolve();
		})
		.then(() => { return zip.close(handle); })
		.then((ret: kptazip.Response) =>
		{
			if (ret.reason != kptasign.KPTAError.KPTA_OK) reject(new Error("Response reason " + ret.reason.toString()));
			out.value += "Zip file closed\n";
			resolve(ret.payload);
		});
	});
}
function __unzip(zip: Uint8Array, out: HTMLTextAreaElement): Promise<Uint8Array>
{
	return new Promise((resolve, reject) =>
	{
		let unzip: kptazip.Inflate = new kptazip.Inflate();
		let handle: number;
		return unzip.open(zip)
		.then((value: kptazip.Response) =>
		{
			if (value.reason != kptasign.KPTAError.KPTA_OK) reject(new Error("Response reason " + value.reason.toString()));
			handle = value.payload;
			out.value += "Zipped file opened with handle " + handle.toString() + "\n";
			return unzip.list(handle);
		})
		.then((response: kptazip.Response) =>
		{
			if (response.reason != kptasign.KPTAError.KPTA_OK) reject(new Error("Response reason " + response.reason.toString()));
			let entry: string = response.payload[0];
			out.value += "Found entry named " + entry + " in zip file\n";
			return unzip.inflate(handle, entry);
		})
		.then((entry: kptazip.Response) =>
		{
			if (entry.reason != kptasign.KPTAError.KPTA_OK) reject(new Error("Response reason " + entry.reason.toString()));
			let cms: Uint8Array = entry.payload;
			out.value += "Zip entry inflated\n";
			unzip.close(handle).then((ret: kptazip.Response) =>
			{
				if (ret.reason != kptasign.KPTAError.KPTA_OK) reject(new Error("Response reason " + ret.reason.toString()));
				out.value += "Zipped file closed\n";
				resolve(cms);
			});
		});
	});
}
function __verify(cms: Uint8Array, out: HTMLTextAreaElement): Promise<Uint8Array>
{
	return new Promise((resolve, reject) =>
	{
		let vrfy: kptaverify.Verify = new kptaverify.Verify();
		return vrfy.verify(cms.buffer)
		.then((value: kptaverify.Response) =>
		{
			if (value.reason != kptasign.KPTAError.KPTA_OK) reject(new Error("Response reason " + value.reason.toString()));
			out.value += "CMS Signed Data document verified\n";
			return vrfy.getContent(cms.buffer);
		})
		.then((response: kptaverify.Response) =>
		{
			if (response.reason != kptasign.KPTAError.KPTA_OK) reject(new Error("Response reason " + response.reason.toString()));
			resolve(response.payload);
		});
	});
}

const PROVIDER: string = "Microsoft Enhanced RSA and AES Cryptographic Provider";
export function test_main()
{
	let cn: string = "Francisvaldo Genevaldo das Torres " + new Date().getTime().toString();
	let out: HTMLTextAreaElement = __findOutput();
	out.value = "Kryptonite regression test...\n";
	__devices().then((devices: string[]) =>
	{
		let found = devices.find((value: string) => { return value ===  PROVIDER; });
		if (!found) return Promise.reject(new Error("Required cryptographic device not found"));
		out.value += "Proper cryptographic device has been selected\n";
		return __generateCSR(found, cn);
	})
	.then((csr: string) =>
	{
		out.value += "Certificate request has been generated\n";
		return __issue_certificate(csr);
	})
	.then((response: string) =>
	{
		out.value += "Certificate has been issued by CA\n";
		return __install(response);
	})
	.then((accept: boolean) =>
	{
		if (!accept) return Promise.reject(new Error("Issued certificate installation failure"));
		out.value += "Issued certificate has been installed\n";
		return __certificates();
	})
	.then((certs: kptasign.Certificate[]) =>
	{
		let cert: kptasign.Certificate | undefined = certs.find((value: kptasign.Certificate) => { return (value && value.subject === "CN=" + cn); });
		if (!cert) return Promise.reject(new Error("Issued certificate not found in repository"));
		out.value += "Issued certificate selected to signature\n";
		return __sign(cert, PROVIDER);
	})
	.then((cms: Uint8Array) =>
	{
		out.value += "Issued certificate has signed its first document\n";
		return __zip(cms, out);
	})
	.then((zip: Uint8Array) =>
	{
		out.value += "CMS Signed Data document zipped\n";
		return __unzip(zip, out);
	})
	.then((cms: Uint8Array) =>
	{
		out.value += "CMS Signed Data document unzipped\n";
		return __verify(cms, out);
	})
	.then((contents: Uint8Array) =>
	{
		out.value += "Signed contents extracted from CMS Signed Data document\n";
		
		if (PROVIDER.localeCompare((new TextDecoder("utf-8")).decode(contents.buffer).valueOf()) == 0) out.value += "Signed contents validated\n";
		else out.value += "Signed contents validation failed\n";
	})
	.catch((reason: any) => { out.value += "Error: " + reason.message;; });
}