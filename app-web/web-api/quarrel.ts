/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Web communication engine
 * Copyleft (C) 2018/2019 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import { resolve } from "dns";
import {
	Chatter,
	Command,
	Response,
	Envelope,
	DelayedPromise,
	InvalidResponse,
	Logger,
	Transmitter,
	Payload,
	Receiver,
	KPTAError
} from "../kryptonite";

export class Quarrel extends Chatter
{
	private request: string;
	private response: string;
	private listening: boolean;
	private receivers: Map<number, Receiver>;
	constructor(evt: string, res: string)
	{
		super();
		this.request = evt;
		this.response = res;
		this.listening = false;
		this.receivers = new Map<number, Receiver>();
	}
	protected __onEvent(evt: Event): void
	{
		Logger.debug(Logger.QUARREL_EVENT_ARRIVED, JSON.stringify(evt));
		let target: EventTarget | null = evt.target;
		if (target && (target instanceof Element))
		{
			let value: string | null = target.getAttribute("response");
			if (value)
			{
				let ob: any = JSON.parse(value);
				if (Envelope.typeOf(ob))
				{
					let env: Envelope = ob as Envelope;
					let promise: DelayedPromise | undefined = this.getPromise(env);
					if (promise)
					{
						let ret: Response | undefined;
						if (Response.typeOf(env.payload))
						{
							let response: Response = env.payload as Response;
							if (Payload.typeOf(response.payload))
							{
								let payload: Payload = response.payload;
								let receiver: Receiver | undefined = this.receivers.get(env.nonce);
								if (typeof receiver === 'undefined')
								{
									receiver = new Receiver();
									this.receivers.set(env.nonce, receiver);
								}
								if (receiver.receive(payload))
								{
									this.receivers.delete(env.nonce);
									this.unregister(env);
									this.listening = false;
									ret = new Response(receiver.retrieve(), response.result, response.reason);
								}
							}
							else ret = new InvalidResponse();
						}
						else ret = new InvalidResponse();
						if (typeof ret !== 'undefined')
						{
							if (ret.result != KPTAError.KPTA_FAILED) promise.resolve(ret);
							else promise.reject(ret);
						}
					}
					else Logger.warn(Logger.QUARREL_NO_PROMISE);
				}
				else Logger.warn(Logger.QUARREL_INVALID_CONTENT);
			}
			else Logger.warn(Logger.QUARREL_MISSING_EVT_ATTR);
		}
		else Logger.warn(Logger.QUARREL_EVT_DISCARDED);
	}
	post(cmd: Command): Promise<Response>
	{
		let that = this;
		return new Promise((resolve, reject) =>
		{
			let elem: Element = document.createElement("MrPostman");
			document.documentElement!.appendChild(elem);
			elem.addEventListener(this.response, function handler(evt: Event): void
			{
				that.__onEvent.bind(that)(evt);
				if (!that.listening)
				{
					elem.removeEventListener(that.response, handler);
					document.documentElement!.removeChild(elem);
				}
			});
			that.listening = true;
			let registered: Envelope = this.register(cmd, new DelayedPromise(resolve, reject));
			let transport: Transmitter = new Transmitter(typeof cmd.payload === 'undefined' || cmd.payload == null ? '' : cmd.payload);
			let payload: Payload | null;
			while ((payload = transport.next()))
			{
				let pack: Command = new Command(cmd.commandID, payload);
				let env: Envelope = new Envelope(pack, registered.nonce);
				elem.setAttribute("request", JSON.stringify(env));
				elem.dispatchEvent(new Event(that.request, { "bubbles": true, "cancelable": false }));
			}
		});
	}
	release(): void {}
}