/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Web communication engine (Verify extension)
 * Copyleft (C) 2018/2019 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import { inject } from "./contents";
import { Protocol } from "../kryptonite";

inject(Protocol.VERIFY_EXTENSION_ID, Protocol.VERIFY_REQUEST_EVENT, Protocol.VERIFY_RESPONSE_EVENT);