/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Zip web API
 * Copyleft (C) 2018/2019 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import { Quarrel } from "./quarrel";
import { Response, Command, Protocol, AddZipPayload, InflatePayload, KPTAError, Base64 } from "../kryptonite";



interface IDeflate								// Compress files in PKWare format
{
	create										// Initializes a new zip file
	():	Promise<Response>;						// Payload returns a handle to new zip
	add											// Adds a new zip entry to current zip file
	(
		handle: number,							// Handle to zip file (returned by create)
		entry: ArrayBuffer | Uint8Array,		// Entry to zip
		name: string,							// Entry name
		date?: number,							// Entry date. Default value: current timestamp
		compress?: boolean						// Compression level indicator. Default value: true (level 8)
	):	Promise<Response>;
	close										// Finishes zipping
	(
		handle: number,							// Handle to zip file (returned by create)
		preserve?: boolean						// False if response payload must be converted to base 64
	):	Promise<Response>;						// Payload returns an Uint8Array with zip file.
}
interface IInflate								// Uncompress files in PKWare format
{
	open										// Opens a zip file
	(
		zip: ArrayBuffer | Uint8Array			// Zip file
	): Promise<Response>;						// Payload returns a handle to zip file
	list										// List all entries of zip files
	(
		handle: number							// Handle to zip file (returned by open)
	): Promise<Response>;						// Payload returns an Array of string
	inflate										// Gets zip entry specified by its name
	(
		handle: number,							// Handle to zip file (returned by open)
		name: string,							// Entry name (returned by list)
		preserve?: boolean						// False if response payload must be converted to base 64
	): Promise<Response>;						// Payload contais an Uint8Array
	close										// Closes zip entry
	(
		handle: number							// Handle to zip file (returned by open)
	): Promise<Response>;
}

/**
 * Interfaces implementation
 */
let gossip: Quarrel = new Quarrel(Protocol.ZIP_REQUEST_EVENT, Protocol.ZIP_RESPONSE_EVENT);
class Deflate implements IDeflate
{
	create(): Promise<Response> { return gossip.post(new Command(Protocol.CREATE_ZIP_CMD)); }
	add(handle: number, entry: ArrayBuffer | Uint8Array, name: string, date?: number, compress?: boolean): Promise<Response>
	{
		let param: Uint8Array;
		if (entry instanceof Uint8Array) param = entry;
		else param = new Uint8Array(entry);
		return gossip.post(new Command(Protocol.ADD_ENTRY_CMD, new AddZipPayload(handle, Base64.btoa(param), name, date ? date : (new Date()).getTime(), compress ? compress : true)));
	}
	close(handle: number, preserve?: boolean):	Promise<Response>
	{
		return gossip.post(new Command(Protocol.CLOSE_ZIP_CMD, handle))
		.then((value: Response) =>
		{
			if (value.result === KPTAError.KPTA_OK && value.reason === KPTAError.KPTA_NONE) return Promise.resolve(new Response(preserve ? value.payload : Base64.atob(value.payload)));
			return Promise.resolve(value);
		});
	}
}
class Inflate implements IInflate
{
	open(zip: ArrayBuffer | Uint8Array ): Promise<Response>
	{
		let param: Uint8Array;
		if (zip instanceof Uint8Array) param = zip;
		else param = new Uint8Array(zip);
		return gossip.post(new Command(Protocol.OPEN_ZIP_CMD, Base64.btoa(param)));
	}
	list(handle: number): Promise<Response> { return gossip.post(new Command(Protocol.LIST_ZIP_CMD, handle)); }
	inflate(handle: number, name: string, preserve?: boolean): Promise<Response>
	{
		return gossip.post(new Command(Protocol.INFLATE_ENTRY_CMD, new InflatePayload(handle, name)))
		.then((value: Response) =>
		{
			if (value.result === KPTAError.KPTA_OK && value.reason === KPTAError.KPTA_NONE) return Promise.resolve(new Response(preserve ? value.payload : Base64.atob(value.payload)));
			return Promise.resolve(value);
		});
	}
	close(handle: number): Promise<Response> { return gossip.post(new Command(Protocol.CLOSE_CMD, handle)); }
}
class KPTAZip
{
	Deflate = Deflate;
	Inflate = Inflate;
	KPTAError = KPTAError;
	Base64 = Base64;
}
export var kptazip: KPTAZip = new KPTAZip();	// Zip/unzip magic keyword
