/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Web communication bridge
 * Copyleft (C) 2018/2019 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import { krypton } from "../browserdef";
import { Envelope, Protocol, Logger, ContextInvalidated, Payload, Command, Response } from "../kryptonite";

class PayloadController
{
	private parts: Array<boolean> | undefined;
	hasDone(part: Payload): boolean
	{
		if (typeof this.parts === 'undefined')
		{
			this.parts = new Array<boolean>(part.parts);
			this.parts.fill(false);
		}
		this.parts[part.part - 1] = true;
		for (let i = 0; i < this.parts.length; i++) if (!this.parts[i]) return false;
		return true;
	}
}
class Bridge
{
	private request: string;
	private response: string;
	private port: chrome.runtime.Port | undefined;
	private requests: Map<number, PayloadController>;
	private responses: Map<number, PayloadController>;
	constructor(requestEvt: string, responseEvt: string)
	{
		this.request = requestEvt;
		this.response = responseEvt;
		document.addEventListener(this.request, { handleEvent: this.__onEvent.bind(this) });
		Logger.debug(Logger.BRIDGE_LISTEN_REQUEST, requestEvt);
		this.requests = new Map<number, PayloadController>();
		this.responses = new Map<number, PayloadController>();
	}
	protected __onEvent(evt: Event): void
	{
		Logger.debug(Logger.BRIDGE_EVENT_ARRIVED, JSON.stringify(evt));
		let target: EventTarget | null = evt.target;
		if (target && (target instanceof Element))
		{
			let tgEvt: Element = target as Element;
			let value: string | null = tgEvt.getAttribute("request");
			if (value)
			{
				let ob: any = JSON.parse(value);
				if (Envelope.typeOf(ob) && Command.typeOf(ob.payload) && Payload.typeOf(ob.payload.payload))
				{
					let env: Envelope = ob as Envelope;
					try
					{
						let port: chrome.runtime.Port = this.getPort();
						let requestPart: Payload = env.payload.payload;
						let controlRequest: PayloadController | undefined = this.requests.get(env.nonce);
						if (typeof controlRequest === 'undefined')
						{
							controlRequest = new PayloadController();
							this.requests.set(env.nonce, controlRequest);
							let response = this.response;
							let that: Bridge = this;
							port.onMessage.addListener(function handler(message: any, port: chrome.runtime.Port)
							{
								Logger.debug(Logger.BRIDGE_RESPONSE_RECEIVED, JSON.stringify(message));
								if (Envelope.typeOf(message) && Response.typeOf(message.payload) && Payload.typeOf(message.payload.payload))
								{
									let ret: Envelope = message as Envelope;
									Logger.debug(Logger.BRIDGE_RESPONSE_NONCE, ret.nonce);
									if (ret.nonce == env.nonce)
									{
										let responsePart: Payload = ret.payload.payload;
										let responseRequest: PayloadController | undefined = that.responses.get(ret.nonce);
										if (typeof responseRequest === 'undefined')
										{
											responseRequest = new PayloadController();
											that.responses.set(ret.nonce, responseRequest);
										}
										tgEvt.setAttribute("response", JSON.stringify(ret));
										tgEvt.dispatchEvent(new Event(response, { "bubbles": true, "cancelable": false }));
										if (responseRequest.hasDone(responsePart))
										{
											port.onMessage.removeListener(handler);
											that.responses.delete(ret.nonce);
										}
										Logger.debug(Logger.BRIDGE_RESPONSE_SENT);
									}
								}
								else Logger.warn(Logger.BRIDGE_MISSING_ENV);
							});
						}
						if (controlRequest.hasDone(requestPart)) this.requests.delete(env.nonce);
						port.postMessage(env);
						Logger.debug(Logger.BRIDGE_MSG_DISPATCHED);
					}
					catch (e)
					{
						Logger.error(Logger.CTX_INVALIDATED_MESSAGE, JSON.stringify(e));
						tgEvt.setAttribute("response", JSON.stringify(new ContextInvalidated()));
						tgEvt.dispatchEvent(new Event(this.response, { "bubbles": true, "cancelable": false }));
					}
				}
				else Logger.warn(Logger.BRIDGE_INVALID_MESSAGE);
			}
			else Logger.warn(Logger.BRIDGE_MISSING_EVT_ATTR);
		}
		else Logger.warn(Logger.BRIDGE_EVT_DISCARDED);
	}
	protected getPort(): chrome.runtime.Port
	{
		if(!this.port) this.port = krypton.runtime.connect();
		return this.port;
	}
}

var bridge: Bridge;
export function inject(id: string, requestEvt: string, responseEvt: string): void
{
	bridge = new Bridge(requestEvt, responseEvt);
	let script: HTMLScriptElement = document.createElement('script');
	script.src = krypton.extension.getURL('global.js');
	if (document.head) document.head.appendChild(script);
	else if (document.documentElement) document.documentElement.appendChild(script);
	let signal: HTMLElement | null = document.getElementById(Protocol.HTML_ELEMENT_ID);
	if (signal) signal.textContent = signal.textContent + ";" + id;
}