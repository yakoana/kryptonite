/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Web communication engine (Signature extension)
 * Copyleft (C) 2018/2019 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import { inject } from "./contents";
import { Protocol } from "../kryptonite";

inject(Protocol.SIGN_EXTENSION_ID, Protocol.SIGN_REQUEST_EVENT, Protocol.SIGN_RESPONSE_EVENT);