/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Verify signature web API
 * Copyleft (C) 2018/2019 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import { Quarrel } from "./quarrel";
import { Response, Protocol, Command, KPTAError, VerifyPayload, Base64 } from "../kryptonite";


interface IVerify
{
	getSignerId(cms: string): Promise<Response>;
	getSignerId(cms: ArrayBuffer): Promise<Response>;
	getSignerCert(cms: string): Promise<Response>;
	getSignerCert(cms: ArrayBuffer): Promise<Response>;
	getSignerCert(cms: string, preserve: boolean): Promise<Response>;
	getSignerCert(cms: ArrayBuffer, preserve: boolean): Promise<Response>;
	getContent(cms: string): Promise<Response>;
	getContent(cms: ArrayBuffer): Promise<Response>;
	getContent(cms: string, preserve: boolean): Promise<Response>;
	getContent(cms: ArrayBuffer, preserve: boolean): Promise<Response>;
	isTrusted(certificate: string): Promise<Response>;
	isTrusted(certificate: ArrayBuffer): Promise<Response>;
	verify(cms: string, content: string, certificate: string): Promise<Response>;
	verify(cms: ArrayBuffer, content: ArrayBuffer, certificate: ArrayBuffer): Promise<Response>;
	verify(cms: string): Promise<Response>;
	verify(cms: ArrayBuffer): Promise<Response>;
}
class Verify implements IVerify
{
	private gossip: Quarrel;
	constructor() { this.gossip = new Quarrel(Protocol.VERIFY_REQUEST_EVENT, Protocol.VERIFY_RESPONSE_EVENT); }
	getSignerId(cms: string): Promise<Response>;
	getSignerId(cms: ArrayBuffer): Promise<Response>;
	getSignerId(cms: string | ArrayBuffer): Promise<Response> { return this.gossip.post(new Command(Protocol.VRFY_SIGNER_ID_CMD, typeof cms === "string" ? cms : Base64.btoa(new Uint8Array(cms)))); }
	getSignerCert(cms: string): Promise<Response>;
	getSignerCert(cms: ArrayBuffer): Promise<Response>;
	getSignerCert(cms: string, preserve: boolean): Promise<Response>;
	getSignerCert(cms: ArrayBuffer, preserve: boolean): Promise<Response>;
	getSignerCert(cms: string | ArrayBuffer, preserve?: boolean): Promise<Response>
	{
		return this.gossip.post(new Command(Protocol.VRFY_SIGNER_CERT_CMD, typeof cms === "string" ? cms : Base64.btoa(new Uint8Array(cms))))
		.then((value: Response) =>
		{
			if (value.result === KPTAError.KPTA_OK) return Promise.resolve(new Response(preserve ? value.payload : Base64.atob(value.payload)));
			return Promise.resolve(value);
		});
	}
	getContent(cms: string): Promise<Response>;
	getContent(cms: ArrayBuffer): Promise<Response>;
	getContent(cms: string, preserve: boolean): Promise<Response>;
	getContent(cms: ArrayBuffer, preserve: boolean): Promise<Response>;
	getContent(cms: string | ArrayBuffer, preserve?: boolean): Promise<Response>
	{
		return this.gossip.post(new Command(Protocol.VRFY_CONTENT_CMD, typeof cms === "string" ? cms : Base64.btoa(new Uint8Array(cms))))
		.then((value: Response) =>
		{
			if (value.result === KPTAError.KPTA_OK) return Promise.resolve(new Response(preserve ? value.payload : Base64.atob(value.payload)));
			return Promise.resolve(value);
		});
	}
	isTrusted(certificate: string): Promise<Response>;
	isTrusted(certificate: ArrayBuffer): Promise<Response>;
	isTrusted(certificate: string | ArrayBuffer): Promise<Response> { return this.gossip.post(new Command(Protocol.VRFY_IS_TRUSTED_CMD, typeof certificate === "string" ? certificate : Base64.btoa(new Uint8Array(certificate)))); }
	verify(cms: string, content: string, certificate: string): Promise<Response>;
	verify(cms: ArrayBuffer, content: ArrayBuffer, certificate: ArrayBuffer): Promise<Response>;
	verify(cms: string): Promise<Response>;
	verify(cms: ArrayBuffer): Promise<Response>;
	verify(cms: string | ArrayBuffer, content?: string | ArrayBuffer, certificate?: string | ArrayBuffer): Promise<Response>
	{
		let pkcs7: string = typeof cms === "string" ? cms : Base64.btoa(new Uint8Array(cms));
		if (content && certificate)
		{
			let document: string;
			let isBase64: boolean = false;
			if (typeof content === "string") document = content;
			else
			{
				document = Base64.btoa(new Uint8Array(content));
				isBase64 = true;
			}
			let cert: string = typeof certificate === "string" ? certificate : Base64.btoa(new Uint8Array(certificate));
			return this.gossip.post(new Command(Protocol.VRFY_VERIFY_CMD, new VerifyPayload(pkcs7, document, isBase64, cert)));
		}
		else return this.gossip.post(new Command(Protocol.VRFY_VERIFY_ALL_CMD, pkcs7));
	}
}
class KPTAVerify
{
	Verify = Verify;
	KPTAError = KPTAError;
	Base64 = Base64;
}
export var kptaverify: KPTAVerify = new KPTAVerify();		// Verify magic object
