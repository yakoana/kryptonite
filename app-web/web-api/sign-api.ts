/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Signing web API
 * Copyleft (C) 2018/2019 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import { Quarrel } from "./quarrel";
import { Response, Certificate, Command, Protocol, SignPayload, KPTAError, Base64 } from "../kryptonite";


document.addEventListener("unload", (evt: Event) =>
{
	if (evt.target === document)
	{
		let gossip: Quarrel = new Quarrel(Protocol.SIGN_REQUEST_EVENT, Protocol.SIGN_RESPONSE_EVENT);
		gossip.post(new Command(Protocol.SIGN_RELINQUISH_CMD));
	}
});

interface ISign									// Sign documents and transactions
{
	enumerateCerts								// Enumerates installed certificates which have private key
	(): Promise<Response>;						// Payload returns a (possibly empty) array of certificate objects.
	sign										// Signs specified document
	(
		certificate: Certificate,				// Signing certificate
		document: string,						// Document (or transaction) to sign
		attach: boolean,						// True if transaction must be attached to generated CMS Signed Data
		preserve?: boolean						// False if response payload must be converted to base 64
	): Promise<Response>;						// Payload should return CMS Signed data envelope as an Uint8Array object
	sign										// Signs specified document
	(
		certificate: Certificate,				// Signing certificate
		document: ArrayBuffer,					// Document to sign
		attach: boolean,						// True if document must be attached to generated CMS Signed Data
		preserve?: boolean						// False if response payload must be converted to base 64
	): Promise<Response>;						// Payload should return CMS Signed data envelope as an Uint8Array object
}

/**
 * Interface implementation
 */
class Sign implements ISign
{
	private gossip: Quarrel;
	constructor() { this.gossip = new Quarrel(Protocol.SIGN_REQUEST_EVENT, Protocol.SIGN_RESPONSE_EVENT); }
	enumerateCerts(): Promise<Response> { return this.gossip.post(new Command(Protocol.ENUM_KEYS_CMD)); }
	sign(certificate: Certificate, document: string, attach: boolean, preserve?: boolean): Promise<Response>;
	sign(certificate: Certificate, document: ArrayBuffer, attach: boolean, preserve?: boolean): Promise<Response>;
	sign(certificate: Certificate, document: string | ArrayBuffer, attach: boolean, preserve?: boolean): Promise<Response>
	{
		let isBase64: boolean = false;
		let transaction: string;
		if (typeof document === "string") transaction = document;
		else
		{
			let param: Uint8Array;
			if (document instanceof Uint8Array) param = document;
			else param = new Uint8Array(document);
			transaction = Base64.btoa(param);
			isBase64 = true;
		}
		return this.gossip.post(new Command(Protocol.SIGN_CMD, new SignPayload(certificate, transaction, isBase64, attach)))
		.then((value: Response) =>
		{
			if (value.result === KPTAError.KPTA_OK && value.reason === KPTAError.KPTA_NONE) return Promise.resolve(new Response(preserve ? value.payload : Base64.atob(value.payload)));
			return Promise.resolve(value);
		});
	}
}
class KPTASign
{
	Sign = Sign;
	KPTAError = KPTAError;
	Base64 = Base64;
}
export var kptasign: KPTASign = new KPTASign();		// Signature magic object
