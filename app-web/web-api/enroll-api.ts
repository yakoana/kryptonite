/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Enrollment web API
 * Copyleft (C) 2018/2019 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import { Quarrel } from "./quarrel";
import { Response, Command, Protocol, KPTAError, GenerateCSRPayload, Base64 } from "../kryptonite";


document.addEventListener("unload", (evt: Event) =>
{
	if (evt.target === document)
	{
		let gossip: Quarrel = new Quarrel(Protocol.ENROLL_REQUEST_EVENT, Protocol.ENROLL_RESPONSE_EVENT);
		gossip.post(new Command(Protocol.ENROLL_RELINQUISH_CMD));
	}
});

interface IEnroll
{
	enumerateDevices(): Promise<Response>;
	generateCSR(dev: string, cn: string, preserve?: boolean): Promise<Response>;
	installCertificate(cms: string): Promise<Response>;
	installCertificate(cms: ArrayBuffer): Promise<Response>;
}

class Enroll implements IEnroll
{
	private gossip: Quarrel;
	constructor() { this.gossip = new Quarrel(Protocol.ENROLL_REQUEST_EVENT, Protocol.ENROLL_RESPONSE_EVENT); }
	enumerateDevices(): Promise<Response> { return this.gossip.post(new Command(Protocol.ENROLL_ENUM_CMD)); }
	generateCSR(dev: string, cn: string, preserve?: boolean): Promise<Response>
	{
		return this.gossip.post(new Command(Protocol.ENROLL_GENCSR_CMD, new GenerateCSRPayload(dev, cn)))
		.then((value: Response) =>
		{
			if (value.result === KPTAError.KPTA_OK && value.reason === KPTAError.KPTA_NONE) return Promise.resolve(new Response(preserve ? value.payload : Base64.atob(value.payload)));
			return Promise.resolve(value);
		});
	}
	installCertificate(cms: string): Promise<Response>;
	installCertificate(cms: ArrayBuffer): Promise<Response>;
	installCertificate(cms: string | ArrayBuffer): Promise<Response>
	{
		let pkcs7: string;
		if (typeof cms === "string") pkcs7 = cms;
		else
		{
			let param: Uint8Array;
			if (cms instanceof Uint8Array) param = cms;
			else param = new Uint8Array(cms);
			pkcs7 = Base64.btoa(param);
		}
		return this.gossip.post(new Command(Protocol.ENROLL_INSTALL_CMD, pkcs7));
	}
}

class KPTAEnroll
{
	Enroll = Enroll;
	KPTAError = KPTAError;
	Base64 = Base64;
}
export var kptaenroll: KPTAEnroll = new KPTAEnroll();		// Enrollment magic object
