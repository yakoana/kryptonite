/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Web communication engine (Zip extension)
 * Copyleft (C) 2018/2019 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import { inject } from "./contents";
import { Protocol } from "../kryptonite";

inject(Protocol.ZIP_EXTENSION_ID, Protocol.ZIP_REQUEST_EVENT, Protocol.ZIP_RESPONSE_EVENT);