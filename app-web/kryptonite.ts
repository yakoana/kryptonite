/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Base web API
 * Copyleft (C) 2018/2019 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import internal = require("assert");

/**
 * Error definitions
 * New Javascript errors should be added to KPTA_SCRIPT_ERROR section
 * All others are shared with native code
 */
export class KPTAError extends Error
{
	/**
	 * Operation results
	 */
	static readonly KPTA_OK						= 0;							// Successful operation
	static readonly KPTA_FAILED					= 1;							// Operation failure
	static readonly KPTA_UNKNOWN					= 2;							// Unsupported operation

	/**
	 * Reason codes
	 */
	static readonly KPTA_NONE					= 0;							// Success
	static readonly KPTA_INVALID_ARG				= 1;							// Invalid arguments
	static readonly KPTA_INVALID_RESPONSE			= 2;							// Invalid response from extensions

	static readonly KPTA_BASE_ERROR				= KPTAError.KPTA_INVALID_RESPONSE + 1;
	static readonly KPTA_MEMORY_ERROR				= (KPTAError.KPTA_BASE_ERROR + 1);		// Out of memory error
	static readonly KPTA_MSG_SIZE_ERROR				= (KPTAError.KPTA_BASE_ERROR + 2);		// JSON message size error
	static readonly KPTA_IO_FILE_ERROR				= (KPTAError.KPTA_BASE_ERROR + 3);		// JSON file I/O error
	static readonly KPTA_STDIO_ERROR				= (KPTAError.KPTA_BASE_ERROR + 4);		// JSON document I/O error
	static readonly KPTA_INVALID_JSON				= (KPTAError.KPTA_BASE_ERROR + 5);		// JSON file parsing error
	static readonly KPTA_TEMPLATE_ERROR				= (KPTAError.KPTA_BASE_ERROR + 6);		// JSON template error
	static readonly KPTA_INVALID_STATE_OBJECT			= (KPTAError.KPTA_BASE_ERROR + 7);		// Cannot execute method due to object invalid state
	static readonly KPTA_COMPONENT_NOT_FOUND			= (KPTAError.KPTA_BASE_ERROR + 8);		// Required component not found in configuration
	static readonly KPTA_BUFFER_TOO_SMALL			= (KPTAError.KPTA_BASE_ERROR + 9);		// Output buffer too small
	static readonly KPTA_PROPERTY_NOT_FOUND			= (KPTAError.KPTA_BASE_ERROR + 10);		// Requested property not found in configuration
	static readonly KPTA_INVALID_PROPERTY			= (KPTAError.KPTA_BASE_ERROR + 11);		// Requested property has another type
	static readonly KPTA_INVALID_LOGLEVEL			= (KPTAError.KPTA_BASE_ERROR + 12);		// JS log level value invalid
	static readonly KPTA_LOGFILE_ERROR				= (KPTAError.KPTA_BASE_ERROR + 13);		// Windows log file opening error
	static readonly KPTA_JSON_ERROR				= (KPTAError.KPTA_BASE_ERROR + 14);		// Invalid JSON document
	static readonly KPTA_COMPONENT_MAP_ERROR			= (KPTAError.KPTA_BASE_ERROR + 15);		// Components hash map access unexpected error
	static readonly KPTA_PUT_MAP_ERROR				= (KPTAError.KPTA_BASE_ERROR + 16);		// Hash map put error
	static readonly KPTA_SLOT_NOT_FOUND_ERROR			= (KPTAError.KPTA_BASE_ERROR + 17);		// Could not find slot name
	static readonly KPTA_CERTIFICATES_NOT_FOUND		= (KPTAError.KPTA_BASE_ERROR + 18);		// Could not find certificates in PKCS #7
	static readonly KPTA_CERT_DN_NOT_FOUND			= (KPTAError.KPTA_BASE_ERROR + 20);		// Selected certificate DN not found in repository
	static readonly KPTA_UNSUPPORTED_MECHANISM		= (KPTAError.KPTA_BASE_ERROR + 21);		// Specified cryptographic mechanism is not supported
	static readonly KPTA_JSON_PRINTER_ERROR			= (KPTAError.KPTA_BASE_ERROR + 22);		// JSON builder facility error indicator (use G_SYSERROR for details)
	static readonly KPTA_JSON_PRINTER_END			= (KPTAError.KPTA_BASE_ERROR + 23);		// JSON builder facility has ended writing

	static readonly KPTA_WINDOWS_API_ERROR			= (KPTAError.KPTA_BASE_ERROR + 64);		// Windows API error (use G_SYSERROR for details)
	static readonly KPTA_WINREGISTRY_ERROR			= (KPTAError.KPTA_BASE_ERROR + 65);		// Windows Registry access error
	static readonly KPTA_LOADDLL_ERROR				= (KPTAError.KPTA_BASE_ERROR + 66);		// Load Windows DLL error
	static readonly KPTA_CRYPTO_ENROLL_ERROR			= (KPTAError.KPTA_BASE_ERROR + 67);		// Windows Cryptographic API error under enrollment context
	static readonly KPTA_CRYPTO_ENUM_CERTS_ERROR		= (KPTAError.KPTA_BASE_ERROR + 68);		// Windows Cryptographic API error under enumerate certificates context
	static readonly KPTA_CRYPTO_SIGN_ERROR			= (KPTAError.KPTA_BASE_ERROR + 69);		// Windows Cryptographic API error under signing context
	static readonly KPTA_CRYPTO_VERIFY_ERROR			= (KPTAError.KPTA_BASE_ERROR + 70);		// Windows Cryptographic API error under verification context
	static readonly KPTA_TRY_NEXT_STORE				= (KPTAError.KPTA_BASE_ERROR + 71);		// Could not validate certificate in this store

	static readonly KPTA_LINUX_API_ERROR			= (KPTAError.KPTA_BASE_ERROR + 128);	// Linux system libraries error (use G_SYSERROR for details)
	static readonly KPTA_LOADSO_ERROR				= (KPTAError.KPTA_BASE_ERROR + 129);	// Load Linux shared object error

	static readonly KPTA_NHARU_ERROR				= (KPTAError.KPTA_BASE_ERROR + 192);	// Nharu Library error indicator
	static readonly KPTA_PARSE_CERT_ERROR			= (KPTAError.KPTA_BASE_ERROR + 193);	// Certificate parser error
	static readonly KPTA_ENCODE_CMS_ERROR			= (KPTAError.KPTA_BASE_ERROR + 194);	// CMS encoder error
	static readonly KPTA_CMS_SD_NOECONTENT_ERROR		= (KPTAError.KPTA_BASE_ERROR + 195);	// Unattached CMS error

	/**
	 * Javascript only errors
	 */
	static readonly KPTA_SCRIPT_ERROR				= (KPTAError.KPTA_BASE_ERROR + 224);	// Non-shared errors
	static readonly KPTA_UNEXPECTED_ERROR			= (KPTAError.KPTA_BASE_ERROR + 225);	// Unknown error
	static readonly KPTA_INVALID_HANDLE_ERROR			= (KPTAError.KPTA_BASE_ERROR + 226);	// Invalid zip handle
	static readonly KPTA_ADD_ZIP_ERROR				= (KPTAError.KPTA_BASE_ERROR + 227);	// Zip add error
	static readonly KPTA_UNZIP_ENTRY_ERROR			= (KPTAError.KPTA_BASE_ERROR + 228);	// Unzip entry error
	static readonly KPTA_REJECTED_BY_USER			= (KPTAError.KPTA_BASE_ERROR + 229);	// Command rejected by user
	static readonly KPTA_GET_CONFIG_ERROR			= (KPTAError.KPTA_BASE_ERROR + 230);	// Failure on get config from native app
	static readonly KPTA_CONNECTION_ERROR			= (KPTAError.KPTA_BASE_ERROR + 231);	// Extension connection failure
	static readonly KPTA_WIN_BRIDGE_ERROR			= (KPTAError.KPTA_BASE_ERROR + 232);	// MS Edge bridge error
	static readonly KPTA_CTX_INVALIDATED			= (KPTAError.KPTA_BASE_ERROR + 233);	// Context tab invalidated by browser

	/**
	 * Error messages
	 */
	static readonly UNDEFINED_EXTENSION_MSG			= "Extensão não definida";
	static readonly OBJECT_STATE_ERROR_MSG			= "Objeto já finalizado";
	static readonly MALFORMED_ZIP_MSG				= "Arquivo zip mal formado";
	static readonly UNEXPECTED_ERROR_MSG			= "Ocorreu um erro inesperado";
	static readonly INVALID_LOGGER_INIT_MSG			= "O componente de log não foi inicializado apropriadamente";
	static readonly UNSUPPORTED_NAVIGATOR_MSG			= "Navegador web não suportado";
	static readonly INVALID_BASE64_MSG				= "A codificação recebida como base64 é inválida";
	static readonly INVALID_CONFIG_ARG_MSG			= "Argumento deve ser uma representação JSON válida do objeto Config";
	static readonly HTML_ELEMENT_NOT_FOUND_MSG		= "Não foi possível encontrar o elemento HTML ";
	static readonly UPDATE_STATE_ERROR_MSG			= "Nenhum componente encontrado para atualização";
	static readonly GET_CONFIG_ERROR_MSG			= "Não foi possível obter a configuração em vista do erro ";
	static readonly STORAGE_ERROR_MSG				= "Não foi possível atualizar o armazenamento do navegador em vista do erro ";

	constructor(m: string) { super(m); }
}
/**
 * This error should not occurs...
 */
export class EpicError extends KPTAError { constructor() { super(KPTAError.UNDEFINED_EXTENSION_MSG); }}

/**
 * Implements a simple logger
 */
enum LogLevel { error = 3, warn, notice, debug = 7 }
export class Logger
{
	private static	noop			(message?: any, ...optionalParams: any[]) {};
	private static	notInitialized	(message?: any, ...optionalParams: any[]) { console.error(KPTAError.INVALID_LOGGER_INIT_MSG); };
	public  static	debug:		(message?: any, ...optionalParams: any[]) => void;
	public  static	notice:		(message?: any, ...optionalParams: any[]) => void;
	public  static	warn:			(message?: any, ...optionalParams: any[]) => void;
	public  static	error:		(message?: any, ...optionalParams: any[]) => void;
	public  static	init			(level: LogLevel): void
	{
		Logger.debug	= Logger.noop;
		Logger.notice	= Logger.noop;
		Logger.warn		= Logger.noop;
		Logger.error	= Logger.noop;
		switch (level)
		{
		case LogLevel.debug:	Logger.debug	= console.debug;
		case LogLevel.notice:	Logger.notice	= console.info;
		case LogLevel.warn:	Logger.warn		= console.warn;
		case LogLevel.error:	Logger.error	= console.error; break;
		default:
			Logger.debug	= Logger.notInitialized;
			Logger.notice	= Logger.notInitialized;
			Logger.warn		= Logger.notInitialized;
			Logger.error	= Logger.notInitialized;
		}
	}

	/**
	 * Log messages
	 */
	static readonly CONNECTION_ACCEPTED_MSG	= "Conexão aceita originada na URL %s";
	static readonly COMMAND_PROCESSED_MSG	= "Comando processado com o resultado %d e razão %d";
	static readonly COMMAND_FAILED_MSG		= "Processamento do comando com a falha %s";
	static readonly UNKNOWN_COMMAND_MSG		= "Processador de comando desconhecido para a mensagem [%s]";
	static readonly UNKNOWN_RESPONSE_MSG	= "Resposta %s recebida da conexão nativa desconhecida";
	static readonly REQUEST_NOT_FOUND_MSG	= "Requisição feita não encontrada";
	static readonly PAYLOAD_REJECTED_MSG	= "Payload da mensagem [%s] rejeitado";
	static readonly COMMAND_ACCEPTED_MSG	= "Comando [%s] aceito e enviado para o componente %s para processamento";
	static readonly ACCEPT_DIALOG_ERROR_MSG	= "Falha inesperada na execução do diálogo de aceite: %s";
	static readonly CONFIG_SAVED_MSG		= "Operação de salvar configuração realizada com o reason code %d";
	static readonly CONFIG_SAVE_ERROR_MSG	= "Falha inesperada na operação de salvar a configuração: %s";
	static readonly GET_CONFIG_MSG		= "Obtida a configuração com seguinte conteúdo: [%s]";
	static readonly EXTENSION_START_MSG		= "Inicialização de extensão Kryptonita";
	static readonly PROCESSOR_ADDED_MSG		= "Processador de comando %s adicionado";
	static readonly UNKNOWN_MESSAGE_MSG		= "Mensagem [%s] recebida e não processada";
	static readonly EXIT_REQUESTED_MSG		= "Mensagem de saída recebida e repassada";
	static readonly REJECTED_BY_USER_MSG	= "Comando [%s] rejeitado pelo usuário";
	static readonly ACCEPTED_BY_USER_MSG	= "Comando [%s] aceito pelo usuário e enviado para processamento";
	static readonly CONFIG_UPDATED_MSG		= "Configuração atualizada com os valores [%s]";
	static readonly VERIFY_ERROR_MSG		= "Falha inesperada na validação do payload: %s";
	static readonly UPDATE_LOAD_ERROR_MSG	= "Não foi possível carregar uma nova atualização em vista do erro HTTP %s";
	static readonly UPDATE_ERROR_MSG		= "Impossível atualizar o produto em vista do erro: %s";
	static readonly UPDATE_INIT_MSG		= "Notificação de atualização recebida com o valor [%s]";
	static readonly UPDATE_DONE_MSG		= "Atualização concluída";
	static readonly COMPONENT_UPDATED_MSG	= "Componente atualizado com sucesso";
	static readonly STORAGE_UPDATED_MSG		= "Operação de atualização do armazenamento do navegador bem sucedida";

	static readonly LOG_INITED			= "Logger inicializado no nível %i";
	static readonly MESSAGE_ARRIVED		= "A seguinte mensagem foi recebida: [%s]";
	static readonly INVALID_MESSAGE_CMD		= "A mensagem recebida não é um comando reconhecido";
	static readonly INVALID_PAYLOAD		= "Payload da mensagem inválido";
	static readonly BRIDGE_LISTEN_REQUEST	= "Bridge: mensagens do tipo %s subscritas";
	static readonly BRIDGE_EVENT_ARRIVED	= "Bridge: evento [%s] recebido";
	static readonly BRIDGE_EVT_DISCARDED	= "Bridge: evento descartado por EventTarget desconhecido";
	static readonly BRIDGE_MISSING_EVT_ATTR	= "Bridge: evento descartado por falta do atributo request";
	static readonly BRIDGE_INVALID_MESSAGE	= "Bridge: evento descartado por falta do conteúdo adequado";
	static readonly BRIDGE_MSG_DISPATCHED	= "Bridge: mensagem despachada ao destino";
	static readonly BRIDGE_RESPONSE_RECEIVED	= "Bridge: resposta [%s] à mensagem recebida";
	static readonly BRIDGE_MISSING_ENV		= "Bridge: resposta descartada por falta do envelope adequado";
	static readonly BRIDGE_RESPONSE_NONCE	= "Bridge: recebida resposta à mensagem com o nonce %i";
	static readonly BRIDGE_RESPONSE_SENT	= "Bridge: resposta enviada ao destinatário original";
	static readonly QUARREL_EVENT_ARRIVED	= "Quarrel: evento [%s] recebido";
	static readonly QUARREL_EVT_DISCARDED	= "Quarrel: evento descartado por EventTarget desconhecido";
	static readonly QUARREL_MISSING_EVT_ATTR	= "Quarrel: evento descartado por falta do atributo response";
	static readonly QUARREL_INVALID_CONTENT	= "Quarrel: evento descartado por tipo inválido";
	static readonly QUARREL_INVALID_MESSAGE	= "Quarrel: evento descartado por falta do conteúdo adequado";
	static readonly QUARREL_NO_PROMISE		= "Quarrel: impossível responder a evento não registrado";
	static readonly CTX_INVALIDATED_MESSAGE	= "Contexto corrente invalidado: [%s]";
}
// WARNING: Requires cond_compile Webpack loader
//IFDEF(DEBUG)
Logger.init(LogLevel.debug);
//ELSE
Logger.init(LogLevel.error);
//ENDIF

/**
 * Communication protocol
 */
export class Protocol
{
	/**
	 * Install signals
	 */
	static readonly HTML_ELEMENT_ID			= "kryptonite_install_signal";
	static readonly ZIP_EXTENSION_ID			= "zip@kryptonite";
	static readonly ENROLL_EXTENSION_ID			= "enroll@kryptonite";
	static readonly SIGN_EXTENSION_ID			= "sign@kryptonite";
	static readonly VERIFY_EXTENSION_ID			= "verify@kryptonite";

	/**
	 * Web events
	 */
	static readonly ZIP_REQUEST_EVENT			= "request@kryptonite.zip";
	static readonly ZIP_RESPONSE_EVENT			= "response@kryptonite.zip";
	static readonly SIGN_REQUEST_EVENT			= "request@kryptonite.signing";
	static readonly SIGN_RESPONSE_EVENT			= "response@kryptonite.signing";
	static readonly VERIFY_REQUEST_EVENT		= "request@kryptonite.verify";
	static readonly VERIFY_RESPONSE_EVENT		= "response@kryptonite.verify";
	static readonly ENROLL_REQUEST_EVENT		= "request@kryptonite.enrollment";
	static readonly ENROLL_RESPONSE_EVENT		= "response@kryptonite.enrollment";

	/**
	 * Web commands
	 */
	static readonly EXIT_CMD				= "Exit@kryptonite";
	static readonly CREATE_ZIP_CMD			= "CreateZip@kryptonite.deflate";
	static readonly ADD_ENTRY_CMD				= "AddEntry@kryptonite.deflate";
	static readonly CLOSE_ZIP_CMD				= "CloseZip@kryptonite.deflate";
	static readonly OPEN_ZIP_CMD				= "OpenZip@kryptonite.inflate";
	static readonly LIST_ZIP_CMD				= "ListZip@kryptonite.inflate";
	static readonly INFLATE_ENTRY_CMD			= "InflateEntry@kryptonite.inflate";
	static readonly CLOSE_CMD				= "Close@kryptonite.inflate";
	static readonly ENUM_KEYS_CMD				= "EnumerateCerts@kryptonite.signing";
	static readonly SIGN_CMD				= "Sign@kryptonite.signing";
	static readonly SIGN_RELINQUISH_CMD			= "Relinquish@kryptonite.signing";				// MS Edge Log rotate command
	static readonly VRFY_SIGNER_ID_CMD			= "GetSignerID@kryptonite.verify";
	static readonly VRFY_SIGNER_CERT_CMD		= "GetSignerCert@kryptonite.verify";
	static readonly VRFY_CONTENT_CMD			= "GetContent@kryptonite.verify";
	static readonly VRFY_IS_TRUSTED_CMD			= "IsTrusted@kryptonite.verify";
	static readonly VRFY_VERIFY_CMD			= "Verify@kryptonite.verify";
	static readonly VRFY_VERIFY_ALL_CMD			= "VerifyAll@kryptonite.verify";
	static readonly ENROLL_ENUM_CMD			= "EnumerateDevices@kryptonite.enrollment";
	static readonly ENROLL_GENCSR_CMD			= "GenerateCSR@kryptonite.enrollment";
	static readonly ENROLL_INSTALL_CMD			= "InstallCertificate@kryptonite.enrollment";
	static readonly ENROLL_RELINQUISH_CMD		= "Relinquish@kryptonite.enrollment";			// MS Edge Log rotate command

	/**
	 * Native commands
	 */
	static readonly NATIVE_HOST_NAME			= "org.crypthing.kryptonite";					// Native app manifest.json name property
	static readonly NATIVE_SIGNING_ID			= "72499c1f-93ba-4b7d-8cb2-76048f8b31b5";		// Signing native component
	static readonly NATIVE_ENUM_KEYS_CMD		= "a35bc7b0-9fca-4350-8f6f-b915bf23be85";		// EnumerateCerts native command
	static readonly NATIVE_SIGN_CMD			= "e14f5034-b3a8-4400-bdae-715c0981df2a";		// Sign native command
	static readonly NATIVE_VERSIONING_ID		= "e203869a-9a1c-4958-afe1-68c8315bc3fd";		// Version controll component
	static readonly NATIVE_GET_CONFIG_CMD		= "7b040131-6ba2-42e4-b886-b134d8090ed7";		// GetConfig native command
	static readonly NATIVE_SET_CONFIG_CMD		= "dda968c3-8683-4d2e-b944-6cdfeb1f17c3";		// SetConfig native command
	static readonly NATIVE_REQ_UPDTE_CMD		= "7093fe99-0f69-46e5-ba87-5418db398ecf";		// RequiresUpdate native command
	static readonly NATIVE_UPDATE_CMD			= "0b8578e3-a69a-4d98-a097-5d8edabd5c7f";		// Update command
	static readonly NATIVE_GETLOGPATH_CMD		= "78643b10-4d47-4b34-bfb5-e1a5bbb897db";		// GetLogPath command
	static readonly NATIVE_RELINQUISH_CMD		= "0b15b3cf-9e71-4fc0-b7f2-0d9876908b87";		// MS Edge rotation log command
	static readonly NATIVE_VERIFY_ID			= "51d003c8-a028-450d-8c51-41b4ea3bafb0";		// Signature validation component
	static readonly NATIVE_GET_SIGNER_ID_CMD		= "cf18e965-ddec-4a15-beb4-567744052cd7";		// GetSignerID native command
	static readonly NATIVE_GET_SIGNER_CERT_CMD	= "9fe3492a-5a85-45b9-b586-6ca535249fd5";		// GetSignerCert native command
	static readonly NATIVE_GET_CONTENT_CMD		= "f2112590-0868-4872-b112-c0c00f339f78";		// GetContent native command
	static readonly NATIVE_IS_TRUSTED_CMD		= "2e420e7e-be8e-403a-9ced-674e0467b12f";		// IsTrusted native command
	static readonly NATIVE_VERIFY_CMD			= "308e30e3-cf68-48a1-9476-47319ca72560";		// Verify native command
	static readonly NATIVE_VERIFY_ALL_CMD		= "83d9121c-7913-414e-a590-6794c5be2ff3";		// VerifyAll native command
	static readonly NATIVE_ENROLLMENT_ID		= "f258091d-bd65-49f0-ba7b-a18f182ce6e0";		// Enrollment native component
	static readonly NATIVE_ENUM_CMD			= "aae1b8ce-9c85-4b52-a80b-578e36a3a775";		// EnumerateDevices native command
	static readonly NATIVE_GENCSR_CMD			= "6c17f198-385c-4ba6-af2b-59b40c2950f9";		// GenerateCSR native command
	static readonly NATIVE_INSTALL_CMD			= "8565fa47-b184-417f-a7a7-62b145a3b260";		// InstallCertificate native command

	payload: any | undefined;
	/**
	 * Creates a new protocol object
	 * @param payload: a possibly empty message payload
	 */
	protected constructor(payload?: any) { this.payload = payload; }
}
let counter: number = 0;
export class Envelope
{
	nonce: number;
	payload: Protocol;
	/**
	 * Greates a new message envelope
	 * @param payload message payload
	 * @param nonce protocol nonce
	 */
	constructor(payload: Protocol, nonce?: number)
	{
		if (nonce) this.nonce = nonce;
		else this.nonce = ++counter;
		this.payload = payload;
	}
	static typeOf(ob: any | null): boolean { return (ob && !isNaN(ob.nonce) && ob.payload); }
}
export class ResponseEnvelope extends Envelope
{
	/**
	 * Multipart response envelope (to exceed 1MB native message limit)
	 */
	part: number = 1;		// Current message part
	parts: number = 1;		// Total parts
	static typeOf(ob: any | null): boolean { return Envelope.typeOf(ob) && !isNaN(ob.part) && !isNaN(ob.parts); }
}
export class DelayedPromise
{
	resolve: (value: Response | PromiseLike<Response>) => void;
	reject: (reason: any) => void;
	slot: string = "";
	receiver: Receiver;
	/**
	 * Creates a new promise (to be fulfilled later)
	 * @param resolve promise resolve callback
	 * @param reject promise reject callback
	 */
	constructor(resolve: (value: Response | PromiseLike<Response>) => void, reject: (reason?: any) => void)
	{
		this.resolve = resolve;
		this.reject = reject;
		this.receiver = new Receiver();
	}
}
export interface IMessageCheck { check: (ob: any | null) => Promise<boolean>; }				// Message sintax verifier
export class Command extends Protocol
{
	commandID: string;
	/**
	 * Creates a new protocol command
	 * @param id command identifier
	 * @param payload message payload, if any
	 */
	constructor(id: string, payload?: any)
	{
		super(payload);
		this.commandID = id;
	}
	static typeOf(ob: any | null): boolean { return (ob && ob.commandID); }
}
export class Response extends Protocol implements IMessageCheck
{
	result: number = KPTAError.KPTA_OK;
	reason: number = KPTAError.KPTA_NONE;
	/**
	 * Creates a new protocol response object
	 * @param payload message payload, if any
	 * @param result KPTAError result code. Optional. Default: KPTA_OK
	 * @param reason KPTAError reason code. Optional. Default: KPTA_NONE
	 */
	constructor(payload?: any, result?: number, reason?: number)
	{
		super(payload);
		if (result) this.result = result;
		if (reason) this.reason	= reason;
	}
	check(ob: any | null): Promise<boolean> { return Promise.resolve(Response.typeOf(ob)); }
	static typeOf(ob: any | null): boolean { return (ob && !isNaN(ob.result) && !isNaN(ob.reason)); }
}
/**
 * Thrown if an unrecognized response is received
 */
export class InvalidResponse extends Response { constructor() { super(null, KPTAError.KPTA_FAILED, KPTAError.KPTA_INVALID_RESPONSE); }}
export class ContextInvalidated extends Response { constructor() { super(null, KPTAError.KPTA_FAILED, KPTAError.KPTA_CTX_INVALIDATED); } }

export abstract class Chatter
{
	private map: Map<number, DelayedPromise>;
	/**
	 * Creates a new communication engine
	 */
	protected constructor() { this.map = new Map<number, DelayedPromise>(); }
	protected register(cmd: Command, promise: DelayedPromise): Envelope
	{
		let env: Envelope = new Envelope(cmd);
		this.map.set(env.nonce, promise);
		return env;
	}
	protected unregister(env: Envelope): void { this.map.delete(env.nonce); }
	protected getPromise(env: Envelope): DelayedPromise | undefined { return this.map.get(env.nonce); }
	/**
	 * Sends a message to current target of communication channel
	 * @param cmd message to send
	 */
	abstract post(cmd: Command): Promise<Response>;
	/**
	 * Releases the communication channel
	 * Used for short term communications
	 */
	abstract release(): void;
}

/**
 * Message payloads (only those of a complex type)
 */
export class AddZipPayload implements IMessageCheck
{
	static i: AddZipPayload = new AddZipPayload(0, "", "");
	handle: number;
	entry: string;
	name: string;
	date: number;
	compress: boolean;
	/**
	 * Creates a new payload to AddEntry@kryptonite.deflate command
	 * @param handle Handle to zip file
	 * @param entry Base64 encoded from Uint8Array entry
	 * @param name Entry name
	 * @param date Entry date and time. Optional. Default current date and time
	 * @param compress Compression indicator. Optional. Default true
	 */
	constructor(handle: number, entry: string, name: string, date?: number, compress?: boolean)
	{
		this.handle = handle;
		this.entry = entry;
		this.name = name;
		this.date = (typeof date != "undefined") ? date : (new Date()).getTime();
		this.compress = (typeof compress != "undefined") ? compress : true;
	}
	check(ob: any | null): Promise<boolean> { return Promise.resolve(ob && !isNaN(ob.handle) && ob.entry && ob.name && ob.date && typeof ob.compress == "boolean"); }
}
export class InflatePayload implements IMessageCheck
{
	static i: InflatePayload = new InflatePayload(0, "");
	handle: number;
	name: string;
	/**
	 * Creates a new payload to InflateEntry@kryptonite.inflate command
	 * @param handle handle to zip file
	 * @param name entry name
	 */
	constructor(handle: number, name: string)
	{
		this.handle = handle;
		this.name = name;
	}
	check(ob: any | null): Promise<boolean> { return Promise.resolve(ob && !isNaN(ob.handle) && ob.name); }
}
export class Certificate implements IMessageCheck
{
	static i: Certificate = new Certificate("", "", "");
	subject: string;
	issuer: string;
	serial: string;
	company_id: string | undefined;
	sponsor_id: string | undefined;
	subject_id: string | undefined;
	/**
	 * Identifies an X.509 certificate 
	 * @param subject Certificate subject common name
	 * @param issuer Certificate issuer common name
	 * @param serial Certificate serial number as an hexadecimal string
	 */
	constructor(subject: string, issuer: string, serial: string)
	{
		this.subject = subject;
		this.issuer = issuer;
		this.serial = serial;
	}
	check(ob: any | null): Promise<boolean> { return Promise.resolve(ob && ob.subject && ob.issuer && ob.serial); }
}
export class SignPayload implements IMessageCheck				// Sign command payload
{
	static i: SignPayload = new SignPayload(new Certificate("", "", ""), "", false, true);
	certificate: Certificate;
	transaction: string;
	isBase64: boolean;
	attach: boolean
	/**
	 * Creates a new payload to Sign@kryptonite.signing command
	 * @param cert Signing certificate
	 * @param transaction Content to sign
	 * @param isBase64 True if transaction is base64 encoded
	 * @param attach True if transaction should be attached to CMS
	 */
	constructor(cert: Certificate, transaction: string, isBase64: boolean, attach: boolean)
	{
		this.certificate = cert;
		this.transaction = transaction;
		this.isBase64 = isBase64;
		this.attach = attach;
	}
	check(ob: any | null): Promise<boolean>
	{
		return Promise.resolve(ob && ob.certificate && ob.transaction && typeof ob.isBase64 == "boolean" && typeof ob.attach == "boolean")
		.then((value: boolean) =>
		{
			if (value) return Certificate.i.check(ob.certificate);
			else return Promise.resolve(false);
		});
	}
}
export class RequiresUpdatePayload
{
	component: string;
	hash: string;
	constructor(comp: string, hash: string)
	{
		this.component = comp;
		this.hash = hash;
	}
}
export class UpdatePayload extends RequiresUpdatePayload
{
	file: string;
	constructor(comp: string, hash: string, file: string)
	{
		super(comp, hash);
		this.file = file;
	}
}
export class SignerIdentifier implements IMessageCheck
{
	static i: SignerIdentifier = new SignerIdentifier();
	issser: string | undefined;
	serial: string | undefined;
	subjectKeyIdentifier: string | undefined;
	check(ob: any): Promise<boolean> { return Promise.resolve(ob && ((ob.issuer && ob.serial) || (ob.subjectKeyIdentifier))); }
}
export class VerifyPayload implements IMessageCheck
{
	static i: VerifyPayload = new VerifyPayload("", "", false, "");
	cms: string;
	content: string;
	isBase64: boolean;
	certificate: string;
	constructor(cms: string, content: string, isBase64: boolean, certificate: string)
	{
		this.cms = cms;
		this.content = content;
		this.isBase64 = isBase64;
		this.certificate = certificate;
	}
	check(ob: any): Promise<boolean> { return Promise.resolve(ob && ob.cms && ob.content && typeof ob.isBase64 == "boolean" && ob.certificate); }
}
export class GenerateCSRPayload implements IMessageCheck
{
	static i: GenerateCSRPayload = new GenerateCSRPayload("", "");
	device: string;
	commonName: string;
	constructor(dev: string, cn: string)
	{
		this.device = dev;
		this.commonName = cn;
	}
	check(ob: any): Promise<boolean> { return Promise.resolve(ob && ob.device && ob.commonName); }
}


/**
 * Base64 conversion
 */
const encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
const equals = "=".charCodeAt(0);
const dash = "-".charCodeAt(0);
const decodings = [
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,62,-1,-1,-1,63,
	52,53,54,55,56,57,58,59,60,61,-1,-1,-1,-1,-1,-1,
	-1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,
	15,16,17,18,19,20,21,22,23,24,25,-1,-1,-1,-1,-1,
	-1,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,
	41,42,43,44,45,46,47,48,49,50,51,-1,-1,-1,-1,-1,
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
	-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
];
const equals_value	= -2;
const dash_value 	= -3;
decodings[equals]	= equals_value;
decodings[dash] 	= dash_value;
const pre = 0;
const content_1 = 1;
const content_2 = 2;
const content_3 = 3;
const content_4 = 4;
export class Base64
{
	// Encodes specified binary data
	static btoa(bytes: Uint8Array) : string
	{
		var base64        = '';
		var byteLength    = bytes.byteLength;
		var byteRemainder = byteLength % 3;
		var mainLength    = byteLength - byteRemainder;
		var a, b, c, d;
		var chunk;
		for (var i = 0; i < mainLength; i = i + 3)
		{
			chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];
			a = (chunk & 16515072) >> 18;
			b = (chunk & 258048)   >> 12;
			c = (chunk & 4032)     >>  6;
			d = chunk & 63;
			base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
		}
		if (byteRemainder == 1)
		{
			chunk = bytes[mainLength];
			a = (chunk & 252) >> 2;
			b = (chunk & 3)   << 4;
			base64 += encodings[a] + encodings[b] + '==';
		}
		else if (byteRemainder == 2)
		{
			chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];
			a = (chunk & 64512) >> 10;
			b = (chunk & 1008)  >>  4;
			c = (chunk & 15)    <<  2;
			base64 += encodings[a] + encodings[b] + encodings[c] + '=';
		}
		return base64;
	}
	// Decodes specified Base64 value
	static atob(base64: string): Uint8Array
	{
		var charlen = base64.length;
		var byteoff = 0;
		var byteLength = Math.round(((charlen) / 4 * 3)+1);
		var bytes = new Uint8Array(byteLength)
		var chunk = 0;
		var i = 0;
		var code;
		code = decodings[base64.charCodeAt(i)];
		if (code == dash_value)
		{
			while (code == dash_value && i < charlen) code = decodings[base64.charCodeAt(++i)];
			if (i!=0)
			{
				while(code != dash_value && i < charlen) code=decodings[base64.charCodeAt(++i)];
				while(code == dash_value && i < charlen) code=decodings[base64.charCodeAt(++i)];
			}
		}
		while(code<0 && code != dash_value && i < charlen) code=decodings[base64.charCodeAt(++i)];
		if(code == dash_value || i >= charlen) throw new Error("A codificação recebida como base64 é inválida");
		var stage = pre; 
		while(i < charlen && code != dash_value) {
			while(i < charlen && stage != content_4 && code != dash_value)
			{
				stage++;
				switch(stage)
				{
					case content_1:
						chunk = code << 18;
						break;
					case content_2:
						chunk |= code << 12;
						break;
					case content_3:
						chunk |= code << 6;
						break;
					case content_4:
						chunk |= code;
						break;
				}
				code = decodings[base64.charCodeAt(++i)];
				while(code < 0 && code != dash_value && i < charlen) code = decodings[base64.charCodeAt(++i)];
			}
			switch(stage)
			{
				case content_1: throw new Error("A codificação recebida como base64 é inválida");
				case content_4:	bytes[byteoff + 2] = chunk &  255;
				case content_3:	bytes[byteoff + 1] = chunk >> 8;
				case content_2:	bytes[byteoff    ] = chunk >> 16;
			}
			byteoff += stage-1;
			stage = pre;
		}
		return bytes.subarray(0,byteoff);
	}

}

/**
 * Workaroud for IPC 120 MB message length limitation
 */
export class Payload
{
	static readonly KPTA_MAXIMUM_LENGTH = 16777216;
	part: number;
	parts: number;
	carrier: any;
	isObject: boolean;
	constructor(data: any, isObject: boolean, part?: number, parts?: number)
	{
		this.carrier = data;
		this.part  = typeof part  != 'undefined' ? part  : 1;
		this.parts = typeof parts != 'undefined' ? parts : 1;
		this.isObject = isObject;
	}
	static typeOf(ob: any | null): boolean { return ob && ob.part && ob.parts && (typeof ob.carrier !== 'undefined'); }
}
export class Transmitter
{
	private parts: number;
	private payload: Payload[];
	private index: number;
	constructor(data: any)
	{
		let isObject: boolean = typeof data === 'object';
		let local: any = isObject ? JSON.stringify(data) : data;
		if (typeof local === 'string')
		{
			this.parts = Math.ceil(local.length / Payload.KPTA_MAXIMUM_LENGTH);
			if (this.parts == 0) this.parts = 1;
			this.payload = new Array(this.parts);
			let pos: number = 0;
			for (let i: number = 1; i <= this.parts; i++)
			{
				let current: string = local.substr(pos, Payload.KPTA_MAXIMUM_LENGTH);
				pos += current.length;
				this.payload[i] = new Payload(current, isObject, i, this.parts);
			}
		}
		else
		{
			this.parts = 1;
			this.payload = new Array(this.parts);
			this.payload[1] = new Payload(local, isObject, 1, this.parts);
		}
		this.index = 1;
	}
	next() : Payload | null { return this.index <= this.parts ? this.payload[this.index++] : null; }
	reset() : void { this.index = 1; }
}
export class Receiver
{
	private parts: number = 0;
	private payload: Payload[] | undefined;
	private isObject:boolean | undefined;
	receive(message: Payload) : boolean
	{
		if (this.parts == 0 || typeof this.payload === 'undefined')
		{
			this.parts = message.parts;
			this.payload = new Array(this.parts);
			this.isObject = message.isObject;
		}
		this.payload[message.part] = message;
		for (let i = 1; i <= this.parts; i++) if (typeof this.payload[i] === 'undefined') return false;
		return true;
	}
	retrieve() : any
	{
		let value: string = '';
		let ret: any;
		for (let i = 1; i <= this.parts; i++)
		{
			if (typeof this.payload === 'undefined' || typeof this.payload[i] === 'undefined') throw 'Incomplete message';
			if (typeof this.payload[i].carrier === 'string') value += this.payload[i].carrier;
			else return this.payload[i].carrier;
		}
		return this.isObject ? JSON.parse(value) : value;
	}
}