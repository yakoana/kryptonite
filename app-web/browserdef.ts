/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Base web API
 * Copyleft (C) 2018/2019 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';

/**
 * Ensures browser compatibility
 */
export class krypton
{
	static __get_host()
	{
		if (typeof browser != "undefined") return browser;
		if (typeof chrome != "undefined") return chrome;
		throw new Error("Navegador web não suportado");
	}
	static runtime		= krypton.__get_host().runtime;
	static extension		= krypton.__get_host().extension;
	static windows		= krypton.__get_host().windows;
	static storage		= krypton.__get_host().storage;
}
