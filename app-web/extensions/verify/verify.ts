/* * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Verify extension implementation
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import { Gossip } from "../krypton";
import { BridgeProcessor, TypeOfTrue, IProcessor, UpdatableExtension } from "../evtprocessor";
import { Protocol, VerifyPayload } from "../../kryptonite";


/**
 * Initializes extension
 */
function main()
{
	let gossip: Gossip = new Gossip(Protocol.NATIVE_HOST_NAME);
	let signerID: BridgeProcessor = new BridgeProcessor(gossip, new TypeOfTrue(), Protocol.NATIVE_VERIFY_ID, Protocol.NATIVE_GET_SIGNER_ID_CMD);
	let signerCert: BridgeProcessor = new BridgeProcessor(gossip, new TypeOfTrue(), Protocol.NATIVE_VERIFY_ID, Protocol.NATIVE_GET_SIGNER_CERT_CMD);
	let content: BridgeProcessor = new BridgeProcessor(gossip, new TypeOfTrue(), Protocol.NATIVE_VERIFY_ID, Protocol.NATIVE_GET_CONTENT_CMD);
	let isTrusted: BridgeProcessor = new BridgeProcessor(gossip, new TypeOfTrue(), Protocol.NATIVE_VERIFY_ID, Protocol.NATIVE_IS_TRUSTED_CMD);
	let verify: BridgeProcessor = new BridgeProcessor(gossip, VerifyPayload.i, Protocol.NATIVE_VERIFY_ID, Protocol.NATIVE_VERIFY_CMD);
	let verifyAll: BridgeProcessor = new BridgeProcessor(gossip, new TypeOfTrue(), Protocol.NATIVE_VERIFY_ID, Protocol.NATIVE_VERIFY_ALL_CMD);

	let map: Map<string, IProcessor> = new Map<string, IProcessor>();
	map.set(Protocol.VRFY_SIGNER_ID_CMD, signerID);
	map.set(Protocol.VRFY_SIGNER_CERT_CMD, signerCert);
	map.set(Protocol.VRFY_CONTENT_CMD, content);
	map.set(Protocol.VRFY_IS_TRUSTED_CMD, isTrusted);
	map.set(Protocol.VRFY_VERIFY_CMD, verify);
	map.set(Protocol.VRFY_VERIFY_ALL_CMD, verifyAll);

	let ext: UpdatableExtension = new UpdatableExtension(map);
	ext.init();
}	main();
