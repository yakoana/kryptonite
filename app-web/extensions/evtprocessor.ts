/* * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Chrome extension implementation
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import { Gossip, Config, ObjectHandler, AppComponent, AppVersion } from "./krypton";
import {
	IMessageCheck,
	Response,
	Command,
	KPTAError,
	EpicError,
	Protocol,
	Envelope,
	RequiresUpdatePayload,
	UpdatePayload,
	Logger,
	Receiver,
	Payload,
	Transmitter
} from "../kryptonite";


/**
 * Extension update engine
 */
class Component extends AppComponent
{
	constructor() { super(); }
	retrieve(): Promise<string>
	{
		if (this.__binary || this.__binary == "") return Promise.resolve(this.__binary);
		return fetch(chrome.extension.getURL(this.file))
		.then((value) =>
		{
			if (value.ok) return value.arrayBuffer()
			.then((data: ArrayBuffer) =>
			{
				let decode: TextDecoder = new TextDecoder("UTF-8");
				this.__binary = decode.decode(data);
				return Promise.resolve(this.__binary);
			});
			else
			{
				this.__binary = "";
				return Promise.resolve(this.__binary);
			}
		});
	}
	static typed(jsonOb: any): Component
	{
		let o = new Component();
		o.extension = jsonOb.extension;
		o.version = jsonOb.version;
		o.component = jsonOb.component;
		o.hash = jsonOb.hash;
		o.file = jsonOb.file;
		return o;
	}
}
class Version extends AppVersion
{
	constructor() { super(); }
	static load(): Promise<Version>
	{
		return new Promise((resolve, reject) =>
		{
			let cur: Version = new Version();
			return fetch(chrome.runtime.getURL(AppVersion.getURI()))
			.then((value) =>
			{
				if (value.ok) return value.json();
				else return reject(new Error(value.status.toString()));
			})
			.then((ob: any) =>
			{
				if (AppVersion.typeOf(ob)) return resolve(Version.typed(ob));
				return resolve(cur);
			})
			.catch((reason) => { return resolve(cur); });
		});
	}
	static typed(jsonOb: any): Version
	{
		let o = new Version();
		o.date = jsonOb.date;
		o.config = jsonOb.config;
		if (jsonOb.components) jsonOb.components.forEach((element: any) => { o.components.push(Component.typed(element)); });
		return o;
	}
}


// Returned when an invalid payload is received
export class InvalidPayload extends Response { constructor() { super(null, KPTAError.KPTA_FAILED, KPTAError.KPTA_INVALID_ARG); }}
// Returned when an unknown command is received
class UnknownCommand extends Response { constructor() { super(null, KPTAError.KPTA_UNKNOWN, KPTAError.KPTA_INVALID_ARG); }}
// Returned if user did not accept command
class RejectedByUser extends Response { constructor() { super(null, KPTAError.KPTA_FAILED, KPTAError.KPTA_REJECTED_BY_USER); }}
// Returned if configuration file could not be load
class GetConfigFailure extends Response { constructor() { super(null, KPTAError.KPTA_FAILED, KPTAError.KPTA_GET_CONFIG_ERROR); }}
class ResponseError extends Response
{
	/**
	 * Returned if an error has occurred during processing
	 * @param response failure response
	 */
	constructor(response: any)
	{
		let reason: number = KPTAError.KPTA_UNEXPECTED_ERROR;
		if (response instanceof Response) reason = response.reason;
		super(null, KPTAError.KPTA_FAILED, reason);
	}
}
export interface IProcessor								// Message processor
{
	process												// Do your job, my son
	(
		message: Command,								// Web API command
		origin: string,									// Origin URL of the message
		config: Promise<Config>
	):	Promise<Response>;
	verify												// Sanity check of message payload
	(
		message:	any,								// Web API command
		typeCheck:	IMessageCheck						// Type checker
	):	Promise<boolean>;
}
abstract class Extension
{
	private processors: Map<string, IProcessor>	= new Map<string, IProcessor>();
	private uri : string = "";
	protected config: Config | null = null;
	private semaphor: Promise<boolean> = Promise.resolve(true);
	private receivers: Map<number, Receiver>;
	/**
	 * Creates a new extension
	 * Warning: init() and update() methods must be implemented
	 * @param props commands processors
	 * @param requires true if processing commands requires loading configuration file.
	 */
	protected constructor(props: Map<string, IProcessor>, requires?: boolean)
	{
		Logger.notice(Logger.EXTENSION_START_MSG);
		if (!requires) this.config = new Config();
		props.forEach((value: IProcessor, key: string, map: Map<string, IProcessor>) =>
		{
			this.processors.set(key, value);
			Logger.debug(Logger.PROCESSOR_ADDED_MSG, key);
		});
		this.receivers = new Map<number, Receiver>();
	}
	protected getConfig(): Promise<Config>
	{
		if (this.config) return Promise.resolve(this.config);
		let gossip: Gossip = new Gossip(Protocol.NATIVE_HOST_NAME);
		return Config.load(gossip)
		.then((value: Config) =>
		{
			this.config = value;
			gossip.release();
			Logger.init(value.logLevel);
			Logger.notice(Logger.LOG_INITED, value.logLevel);
			return Promise.resolve(this.config);
		})
		.catch((reason) =>
		{
			let msg: string = KPTAError.GET_CONFIG_ERROR_MSG + (reason.message || JSON.stringify(reason));
			gossip.release();
			Logger.error(msg);
			return Promise.reject(new Error(msg));
		});
	}
	/**
	 * Must be implemented to initialize extension
	 */
	abstract init(): void;
	/**
	 * Must be implemented to update extension
	 */
	protected abstract update(): void;
	/**
	 * onInstalled default listener
	 * @param details kind of update
	 */
	protected __onInstalled(details: chrome.runtime.InstalledDetails): void
	{ 
		this.semaphor = new Promise<boolean>((resolve, reject) =>
		{
			this.update();
			return resolve(true);
		});
	}
	/**
	 * onConnect default listener
	 * @param port communication port
	 */
	protected __onConnect(port: chrome.runtime.Port): void
	{
		if (port.sender && port.sender.url) this.uri = port.sender.url;
		Logger.notice(Logger.CONNECTION_ACCEPTED_MSG, this.uri);
		port.onMessage.addListener(this.__onMessage.bind(this));
	}
	/**
	 * onMessage default listener
	 * @param message incoming message
	 * @param port incoming port
	 */
	protected __onMessage(message: any, port: chrome.runtime.Port): void
	{
		this.semaphor.then((value: boolean) =>
		{
			Logger.debug(Logger.MESSAGE_ARRIVED, JSON.stringify(message));
			if (Envelope.typeOf(message))
			{
				let nonce: number = message.nonce;
				if (Command.typeOf(message.payload))
				{
					let cmd: Command = message.payload;
					if (Payload.typeOf(cmd.payload))
					{
						let payload: Payload = cmd.payload;
						let receiver: Receiver | undefined = this.receivers.get(nonce);
						if (typeof receiver === 'undefined')
						{
							receiver = new Receiver();
							this.receivers.set(nonce, receiver);
						}
						if (receiver.receive(payload))
						{
							let processor: IProcessor | undefined = this.processors.get(cmd.commandID);
							if (processor)
							{
								let restored: Command = new Command(cmd.commandID, receiver.retrieve());
								processor.process(restored, this.uri, this.getConfig())
								.then((value: Response) =>
								{
									let transport: Transmitter = new Transmitter(typeof value.payload === 'undefined' || value.payload == null ? '' : value.payload);
									let payload: Payload | null;
									while ((payload = transport.next()))
									{
										let ret: Response = new Response(payload, value.result, value.reason);
										port.postMessage(new Envelope(ret, nonce));
									}
								})
								.catch((reason: any) =>
								{
									let err = JSON.stringify(reason);
									Logger.error(Logger.COMMAND_FAILED_MSG, err);
									port.postMessage(new Envelope(new ResponseError(new Payload(err, false)), nonce));
								});
							}
							else
							{
								Logger.error(Logger.UNKNOWN_COMMAND_MSG, JSON.stringify(message));
								port.postMessage(new Envelope(new Response(new Payload('', false), KPTAError.KPTA_UNKNOWN, KPTAError.KPTA_INVALID_ARG), nonce));
							}
							this.receivers.delete(nonce);
						}
					}
					else Logger.notice(Logger.INVALID_PAYLOAD);
				}
				else Logger.notice(Logger.INVALID_MESSAGE_CMD);
			}
			else Logger.error(Logger.UNKNOWN_MESSAGE_MSG, JSON.stringify(message));
		});
	}
}
export abstract class DefaultExtension extends Extension
{
	/**
	 * Creates a new extension using default listeners
	 * Warning: update() method must be implemented
	 * @param props commands processors
	 * @param requires true if processing commands requires loading configuration file.
	 */
	protected constructor(props: Map<string, IProcessor>, requires?: boolean) { super(props, requires); }
	init(): void
	{
		chrome.storage.local.clear(() =>
		{
			if (chrome.runtime.lastError) Logger.error(KPTAError.STORAGE_ERROR_MSG + JSON.stringify(chrome.runtime.lastError));
			else Logger.debug(Logger.STORAGE_UPDATED_MSG);
		});
		chrome.runtime.onInstalled.addListener(this.__onInstalled.bind(this))
		chrome.runtime.onConnect.addListener(this.__onConnect.bind(this));
	}
}
export class UpdatableExtension extends DefaultExtension
{
	/**
	 * Implements an updatable extension, e.g., an extension tied to a native component that requires update.
	 * @param props commands to process
	 */
	constructor(props: Map<string, IProcessor>) { super(props, true); }
	private updateConfig(value: Version): Promise<Response>
	{
		return this.getConfig().then((cfg: Config) =>
		{
			let newCfg: Config = Object.assign(new Config(), ObjectHandler.merge(ObjectHandler.deepCopy(cfg), value.config));
			let gossip: Gossip = new Gossip(Protocol.NATIVE_HOST_NAME);
			return newCfg.store(gossip)
			.then((resp: Response) =>
			{
				if (resp.result == KPTAError.KPTA_OK) this.config = newCfg;
				gossip.release();
				return Promise.resolve(resp);
			})
			.catch((reason: any) => { gossip.release(); return Promise.reject(reason); });
		});
	}
	private doUpdate(gossip: Gossip, elem: Component): Promise<Response>
	{
		return gossip.post(new Command(Protocol.NATIVE_VERSIONING_ID, new Command(Protocol.NATIVE_REQ_UPDTE_CMD, new RequiresUpdatePayload(elem.component, elem.hash))))
		.then((response: Response) =>
		{
			if (response.result == KPTAError.KPTA_OK && response.payload)
			{
				return elem.retrieve()
				.then((bin: string) =>
				{
					return gossip.post
					(
						new Command
						(
							Protocol.NATIVE_VERSIONING_ID,
							new Command
							(
								Protocol.NATIVE_UPDATE_CMD,
								new UpdatePayload(elem.component, elem.hash, bin)
							)
						)
					);
				});
			}
			else return Promise.resolve(response);
		});
	}
	private updateComponent(components: Array<AppComponent>, gossip: Gossip, resolve: (value?: {} | PromiseLike<{}> | undefined) => void, reject: (reason?: any) => void): void
	{
		let component = components.pop();
		if (component instanceof Component)
		{
			this.doUpdate(gossip, component).then((value: Response) =>
			{
				if (value.result == KPTAError.KPTA_OK) this.updateComponent(components, gossip, resolve, reject);
				else reject(new Error(JSON.stringify(value)));
			})
			.catch((reason: any) => { reject(reason); });
		}
		else resolve();
	}
	private updateComponents(value: Version):  Promise<any>
	{
		let gossip: Gossip = new Gossip(Protocol.NATIVE_HOST_NAME);
		return new Promise((resolve, reject) =>
		{
			try { this.updateComponent(value.components, gossip, resolve, reject); }
			catch (e) { reject(e); }
		})
		.then(() => { gossip.release(); })
		.catch((e) => { gossip.release(); return Promise.reject(e) });
	}
	protected update(): void
	{
		Version.load().then((value: Version) =>
		{
			Logger.debug(Logger.UPDATE_INIT_MSG, value.toString());
			if (value.config) this.updateConfig(value)
			.then((ret: Response) =>
			{
				if (ret.result == KPTAError.KPTA_OK) Logger.notice(Logger.CONFIG_SAVED_MSG, JSON.stringify(value.config));
				else { Logger.error(Logger.UPDATE_ERROR_MSG, ret.reason.toString()); }
			})
			.catch((reason: any) => { Logger.error(Logger.CONFIG_SAVE_ERROR_MSG, JSON.stringify(reason)); });
			
			if (Array.isArray(value.components) && value.components.length > 0) this.updateComponents(value)
			.then(() => { Logger.notice(Logger.COMPONENT_UPDATED_MSG); })
			.catch((reason: any) => { Logger.notice(Logger.UPDATE_ERROR_MSG, JSON.stringify(reason)); });
		})
		.catch((reason: any) => { Logger.error(Logger.UPDATE_ERROR_MSG, (reason.message | reason)); });
	}
}
export abstract class GenericProcessor implements IProcessor
{
	/**
	 * Implements a basic message processor
	 * @param message message to process
	 * @param origin message URL
	 * @param config current configuration
	 */
	abstract process(message: Command, origin: string, cfg: Promise<Config>): Promise<Response>;
	verify(payload: any, typeCheck: IMessageCheck): Promise<boolean> { return typeCheck.check(payload); }
}
export class BridgeProcessor extends GenericProcessor
{
	protected gossip: Gossip;
	protected checker: IMessageCheck;
	protected component: string;
	protected cmdId: string;
	/**
	 * Implements a processor that forwards its commands to a native application
	 * @param gossip native conector
	 * @param checker payload checker
	 * @param component native component ID
	 * @param cmdId command identifier
	 */
	constructor(gossip: Gossip, checker: IMessageCheck, component: string, cmdId: string)
	{
		super();
		this.gossip = gossip;
		this.checker = checker;
		this.component = component;
		this.cmdId = cmdId;
	}
	process(message: Command, origin: string, cfg: Promise<Config>): Promise<Response>
	{
		return this.verify(message.payload, this.checker)
		.then((value: boolean) =>
		{
			if (value)
			{
				Logger.debug(Logger.COMMAND_ACCEPTED_MSG, JSON.stringify(message), this.component);
				return this.gossip.post(new Command(this.component, new Command(this.cmdId, message.payload)));
			}
			else
			{
				Logger.error(Logger.PAYLOAD_REJECTED_MSG, JSON.stringify(message));
				return Promise.reject(new InvalidPayload());
			}
		})
		.catch((reason) =>
		{
			Logger.error(Logger.VERIFY_ERROR_MSG, (reason.message || JSON.stringify(reason)));
			return Promise.reject(new EpicError());
		});
	}
}
export class ExitProcessor extends GenericProcessor
{
	private _gossip: Gossip;
	constructor(gossip: Gossip) { super(); this._gossip = gossip; }
	/**
	 * Implements a processor that terminates native application
	 * @param message exit comand (from web)
	 * @param origin ignored
	 * @param config ignored
	 */
	process(message: Command, origin: string, cfg: Promise<Config>): Promise<Response>
	{
		Logger.notice(Logger.EXIT_REQUESTED_MSG);
		return this._gossip.post(new Command("exit"));
	}
}
export abstract class AccordanceProcessor extends BridgeProcessor
{
	/**
	 * A native processor that requires user acceptance to continue
	 * @param gossip native conector
	 * @param checker payload checker
	 * @param component native component ID
	 * @param cmdId command identifier
	 */
	protected constructor(gossip: Gossip, checker: IMessageCheck, component: string, cmdId: string) { super(gossip, checker, component, cmdId); }
	process(message: Command, origin: string,  cfg: Promise<Config>): Promise<Response>
	{
		return cfg.then((config: Config) => 
		{
			return this.verify(message.payload, this.checker)
			.then((value: boolean) =>
			{
				if (value) return this.validate(message, origin, config).then((accept) =>
				{
					if (accept)
					{
						Logger.debug(Logger.ACCEPTED_BY_USER_MSG, JSON.stringify(message));
						return this.gossip.post(new Command(this.component, new Command(this.cmdId, message.payload)));
					}
					throw new RejectedByUser();
				});
				throw new InvalidPayload();
			})
		})
		.catch((reason) =>
		{
			Logger.error(Logger.VERIFY_ERROR_MSG, (reason.message || JSON.stringify(reason)));
			return Promise.reject(reason);
		});
	}
	abstract validate(message: Command, origin: string, config: Config): Promise<boolean>;
}
/**
 * An implementation to commands without payload
 */
export class TypeOfTrue implements IMessageCheck { check(ob: any | null): Promise<boolean> { return Promise.resolve(true); }}
