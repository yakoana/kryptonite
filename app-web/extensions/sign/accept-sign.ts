/* * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Signature extension implementation
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * 
 *  * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import { KPTAError, Base64, Logger, Payload } from "../../kryptonite";


function __getParagraph(id: string): HTMLParagraphElement
{
	let elem : HTMLElement | null = document.getElementById(id);
	if (elem && elem instanceof HTMLParagraphElement) return elem;
	else
	{
		Logger.error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG + id);
		throw new Error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG + id);
	}
}
function __getInput(id: string): HTMLInputElement
{
	let elem : HTMLElement | null = document.getElementById(id);
	if (elem && elem instanceof HTMLInputElement) return elem;
	else
	{
		Logger.error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG + id);
		throw new Error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG + id);
	}
}
function __getAnchor(id: string): HTMLAnchorElement
{
	let elem = document.getElementById(id);
	if (elem && elem instanceof HTMLAnchorElement) return elem;
	else
	{
		Logger.error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG + id);
		throw new Error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG + id);
	}
}
function __getLocation(): Location
{
	if (document.location) return document.location;
	else
	{
		Logger.error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG + "document.location");
		throw new Error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG + "document.location");
	}
}
function __getUUID(): string
{
	let uuid = (new URL(__getLocation().toString())).searchParams.get('id');
	if (uuid) return uuid;
	else
	{
		Logger.error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG);
		throw new Error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG);
	}
}
let __isResolved: boolean = false;
function __resolve(uuid: string, response: boolean)
{
	__isResolved = true;
	chrome.runtime.sendMessage({ type: uuid, response: response,  remember: __getInput("forget").checked });
	chrome.windows.getCurrent((window: chrome.windows.Window): void => { chrome.windows.remove(window.id); });
}
function __onBeforeUnload()
{
	if (!__isResolved) __resolve(__getUUID(), false);
	window.removeEventListener("load",__onLoad);
	window.removeEventListener("beforeunload", __onBeforeUnload);
}
const TEXT_LEGEND = "Atenção: O aplicativo no endereço __URL__ enviou o documento a seguir para ser assinado. Você concorda em realizar essa operação?";
const BIN_LEGEND =  "O documento recebido não parece ser do tipo texto plano ou é muito grande para vizualização. Se desejar inspeção mais detalhada, clique no botão apropriado para baixar o arquivo.";
function __onLoad()
{
	let uuid: string = __getUUID();
	chrome.storage.local.get([uuid + "-origin", uuid + "-contents", uuid + "-isBase64"], (items: { [key: string]: any }) =>
	{
		if (chrome.runtime.lastError)
		{
			Logger.error(KPTAError.STORAGE_ERROR_MSG + chrome.runtime.lastError);
			__resolve(uuid, false);
		}
		else
		{
			let message: any = { origin: items[uuid + "-origin"], contents: items[uuid + "-contents"], isBase64: items[uuid + "-isBase64"] };
			if (typeof message.origin != "undefined" && typeof message.contents != "undefined" && typeof message.isBase64 != "undefined")
			{
				let doNotDisplay: boolean = message.isBase64 || message.contents.length >= Payload.KPTA_MAXIMUM_LENGTH;
				let display: string = doNotDisplay  ? BIN_LEGEND : message.contents;
				__getParagraph("legend").appendChild(document.createTextNode(TEXT_LEGEND.replace(/__URL__/, message.origin)));
				__getParagraph("contents").appendChild(document.createTextNode(display));
				if (doNotDisplay)
				{
					let parts: BlobPart[];
					if (message.isBase64) parts = [Base64.atob(message.contents)];
					else parts = [message.contents];
					let blob: Blob = new Blob(parts, { type: "application/octet-stream" });
					let url: string = URL.createObjectURL(blob);
					let elem: HTMLAnchorElement = document.createElement("a");
					elem.setAttribute("href", url);
					elem.setAttribute("download", "contents.bin");
					elem.setAttribute("id", "link");
					document.body.appendChild(elem);
					let download: HTMLInputElement = __getInput("download");
					download.addEventListener("click", () => { __getAnchor("link").click(); });
					download.style.display = "inline";
				}
				__getInput("accept").addEventListener("click", () => { __resolve(uuid, true); });
				__getInput("refuse").addEventListener("click", () => { __resolve(uuid, false); });
			}
			else
			{
				Logger.error(KPTAError.UNEXPECTED_ERROR_MSG);
				__resolve(uuid, false);
			}
		}
	});
}
window.addEventListener("load", __onLoad);
window.addEventListener("beforeunload", __onBeforeUnload);
