/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Signature extension implementation
 * Copyleft (C) 2018/2019 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import { IProcessor, BridgeProcessor, TypeOfTrue, ExitProcessor, AccordanceProcessor, UpdatableExtension } from "../evtprocessor";
import { Gossip, Config, Sampler, UUID } from "../krypton";
import { Protocol, SignPayload, Command, Response, Base64, Logger } from "../../kryptonite";


class SignatureProcessor extends AccordanceProcessor
{
	/**
	 * Creates a new Sign@kryptonite.signing command processor
	 * @param gossip native connection manager
	 */
	constructor(gossip: Gossip) { super(gossip, SignPayload.i, Protocol.NATIVE_SIGNING_ID, Protocol.NATIVE_SIGN_CMD); }
	/**
	 * Verifies if user accepts the command
	 * @param message signature command
	 * @param origin origin URL
	 * @param config current configuration
	 */
	validate(message: Command, origin: string, config: Config): Promise<boolean>
	{
		return new Promise((resolve, reject) =>
		{
			if (config.sign.blackList.some((value: string) => { return value === origin; })) return resolve(false);
			if (config.sign.whiteList.some((value: string) => { return value === origin; })) return resolve(true);
			try
			{
				let payload: SignPayload = message.payload;
				let contents: string = payload.transaction;
				let isBase64: boolean = false;
				if (payload.isBase64)
				{
					let buffer: Uint8Array = Base64.atob(payload.transaction);
					let sampler: Sampler = new Sampler();
					if (sampler.check(buffer))
					{
						let coder = new TextDecoder("utf-8");
						contents = coder.decode(buffer);
					}
					else isBase64 = true;
				}
				let uuid: string = UUID.generate();
				let param: { [k: string]: any } = {};
				param[uuid + "-origin"] = origin;
				param[uuid + "-contents"] = contents;
				param[uuid + "-isBase64"] = isBase64;
				let that: SignatureProcessor = this;
				function callback(message: any)
				{
					if (message.type == uuid)
					{
						if (message.remember)
						{
							if (message.response) config.sign.whiteList.push(origin);
							else config.sign.blackList.push(origin);
							config.store(that.gossip)
							.then((value: Response) => { Logger.notice(Logger.CONFIG_SAVED_MSG, value.reason); })
							.catch((reason) => { Logger.error(Logger.CONFIG_SAVE_ERROR_MSG, (reason.message || JSON.stringify(reason)))});
						}
						resolve(message.response);
						chrome.runtime.onMessage.removeListener(callback);
					}
				}
				chrome.storage.local.set(param, () =>
				{
					if (chrome.runtime.lastError) reject(chrome.runtime.lastError);
					else chrome.windows.create({ url: chrome.extension.getURL("sign-contents.html?id=" + uuid), type: "popup", width: 800, height: 500 }, (() =>
					{
						chrome.runtime.onMessage.addListener(callback);
					}));
				});
			}
			catch (e) { reject(e); }
		});
	}
}

/**
 * Initializes extension
 */
function main()
{
	let gossip: Gossip = new Gossip(Protocol.NATIVE_HOST_NAME);
	let bridgeProcessor: BridgeProcessor = new BridgeProcessor(gossip, new TypeOfTrue(), Protocol.NATIVE_SIGNING_ID, Protocol.NATIVE_ENUM_KEYS_CMD);
	let signProcessor: SignatureProcessor = new SignatureProcessor(gossip);
	let byeProcessor: BridgeProcessor = new BridgeProcessor(gossip, new TypeOfTrue(), Protocol.NATIVE_VERSIONING_ID, Protocol.NATIVE_RELINQUISH_CMD);
	let map: Map<string, IProcessor> = new Map<string, IProcessor>();
	map.set(Protocol.ENUM_KEYS_CMD, bridgeProcessor);
	map.set(Protocol.SIGN_RELINQUISH_CMD, byeProcessor);
	map.set(Protocol.SIGN_CMD, signProcessor);
	map.set(Protocol.EXIT_CMD, new ExitProcessor(gossip));
	let ext: UpdatableExtension = new UpdatableExtension(map);
	ext.init();
}	main();