/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Extension facilities
 * Copyleft (C) 2018/2019 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import { krypton } from "../browserdef";
import { DelayedPromise, Envelope, ResponseEnvelope, Response, InvalidResponse, Command, Chatter, Protocol, KPTAError } from "../kryptonite";

/**
 * Configuration object
 */

class ControllableResources			// Controllable URL
{
	blackList: Array<string>;		// Resources rejected a priori
	whiteList: Array<string>;		// Resources accepted a priori
	constructor()
	{
		this.blackList = [];
		this.whiteList = [];
	}
	static typeOf(ob: any): boolean { return Array.isArray(ob.blackList) && Array.isArray(ob.whiteList); }
}
class IssueProfile					// Profile to certificate issuing
{
	keySize: number;				// Generated RSA key size
	signAlgOID: string;				// Certificate request signature algorithm object identifier
	constructor()
	{
		this.keySize = 2048;
		this.signAlgOID = "1.2.840.113549.1.1.11";
	}
	static typeOf(ob: any): boolean { return ob && !isNaN(ob.keySize) && ob.signAlgOID; }
}
class EnrollConfig extends ControllableResources	// Configurations to enroll component
{
	issueProfile: IssueProfile;
	constructor()
	{
		super();
		this.issueProfile = new IssueProfile();
	}
	static typeOf(ob: any): boolean { return ob && IssueProfile.typeOf(ob.issueProfile) && ControllableResources.typeOf(ob); }
}
class SignProfile									// Profile to document signing
{
	signAlg: number;								// Signature algorithm (as a PKCS #11 constant value)
	policy: string;									// CMS signature policy
	constructor()
	{
		this.signAlg = 64;
		this.policy = "CAdES-BES";
	}
	static typeOf(ob: any): boolean { return ob && !isNaN(ob.signAlg) && ob.policy; }
}
class SignConfig extends ControllableResources		// Configuration to signature component
{
	signProfile:  SignProfile;
	constructor()
	{
		super();
		this.signProfile = new SignProfile();
	}
	static typeOf(ob: any): boolean { return ob && SignProfile.typeOf(ob.signProfile) && ControllableResources.typeOf(ob);; }
}
export class AppConfig								// Configuration object
{
	version: number;										// current version of this configuration
	logLevel: number;										// Log level (error = 3, warn, notice, debug = 7)
	maxResponseSize: number;								// Maximum size of a native response
	components: object;										// Installed native components hash table
	enroll: EnrollConfig;									// Enrollment configuration
	sign: SignConfig;										// Signature configuration
	constructor(size: number | null)
	{
		this.version = 16777216;
		this.logLevel = 3;
		this.maxResponseSize = size ? size : 1024000;
		this.components = {
			"f258091d-bd65-49f0-ba7b-a18f182ce6e0": "kptaroll",
			"72499c1f-93ba-4b7d-8cb2-76048f8b31b5": "kptasign",
			"51d003c8-a028-450d-8c51-41b4ea3bafb0": "kptavrfy"
		};
		this.enroll = new EnrollConfig();
		this.sign = new SignConfig();
	}
	toString(): string { return JSON.stringify(this); }
	static typeOf(ob: any): boolean { return ob && !isNaN(ob.logLevel) && !isNaN(ob.maxResponseSize) && ob.components && EnrollConfig.typeOf(ob.enroll) && SignConfig.typeOf(ob.sign); }
}


/**
 * Version definition file: a stringified instance of Version object with name current-version.json.
 * If no such file exists, a new Version instance is returned.
 * In this case, only date property is set with current date and time.
 */
export class AppComponent									// Native component to be updated
{
	extension: string;										// Parent extension
	version: string;										// Current version
	component: string;										// Component ID
	hash: string;											// File hash
	file: string;											// Filename of binary
	protected __binary: string | null;						// Base64 encoding of file to be updated
	constructor()
	{
		this.extension = "";
		this.version = "";
		this.component = "";
		this.hash = "";
		this.file = "";
		this.__binary = null;
	}
	static typeOf(ob: any): boolean { return ob && ob.extension && ob.version && ob.component && ob.hash && ob.file; }
}
export class AppVersion										// Update declaration
{
	date: number;											// Update instant
	config: any | null;										// Config object update, if any
	components: Array<AppComponent>;						// Native components to be updated, if any
	constructor()
	{
		this.date = (new Date()).getTime();
		this.config = null;
		this.components = [];
	}
	toString(): string { return JSON.stringify(this); }
	static typeOf(ob: any): boolean { return ob && ob.date && Array.isArray(ob.components); }
	static getURI(): string { return "./current-version.json"; }
}

export class UUID
{
	static generate(): string
	{
		let d = new Date().getTime();
		if (typeof performance !== 'undefined' && typeof performance.now === 'function') d += performance.now();
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c)
		{
			let r = (d + Math.random() * 16) % 16 | 0;
			d = Math.floor(d / 16);
			return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
		});
	}
}


/**
 * Native communication engine
 */
export class Gossip extends Chatter
{
	private port: chrome.runtime.Port | null;
	private id: string;
	/**
	 * Creates a new communication channel with native host
	 * @param id native target identifier
	 */
	constructor(id: string)
	{
		super();
		this.port = null;
		this.id = id;
	}
	protected onMessage(msg: any, port: chrome.runtime.Port): void
	{
		let rcv = typeof msg === "string" ? JSON.parse(msg) : msg;	// WARNING: Required by M$ Edge...
		if (Envelope.typeOf(rcv))
		{
			let ob: DelayedPromise | undefined = this.getPromise(rcv);
			if (ob && ResponseEnvelope.typeOf(rcv))
			{
				let ret;
				if (rcv.parts > 1) ob.slot += rcv.payload;
				else ret = rcv.payload;
				if (rcv.part == rcv.parts)
				{
					if (rcv.parts > 1) ret = JSON.parse(atob(ob.slot));
					this.unregister(rcv);
					if (Response.typeOf(ret)) ob.resolve(new Response(ret.payload, ret.result, ret.reason));
					else ob.reject(new InvalidResponse());
				}
			}
		}
	}
	private connect(): void
	{
		if (!this.port)
		{
			this.port = krypton.runtime.connectNative(this.id);
			this.port.onDisconnect.addListener(() => { this.port = null; });
			this.port.onMessage.addListener(this.onMessage.bind(this));
		}
	}
	post(cmd: Command): Promise<Response>
	{
		this.connect();
		return new Promise((resolve, reject) => { if (this.port) this.port.postMessage(this.register(cmd, new DelayedPromise(resolve, reject))); });
	}
	release(): void { if (this.port) this.port.disconnect(); }
}

export class Config	extends AppConfig
{
	/**
	 * Implements an application config kept by native host
	 */
	constructor() { super(null); }
	/**
	 * Stores current configuration
	 * @param gossip native application communication engine
	 */
	store(gossip: Gossip) : Promise<Response> { return gossip.post(new Command(Protocol.NATIVE_VERSIONING_ID, new Command(Protocol.NATIVE_SET_CONFIG_CMD, this))); }
	/**
	 * Loads current version of configuration file
	 * @param gossip native application communication engine
	 */
	static load(gossip: Gossip) : Promise<Config>
	{
		return gossip.post(new Command(Protocol.NATIVE_VERSIONING_ID, new Command(Protocol.NATIVE_GET_CONFIG_CMD)))
		.then((value: Response) =>
		{
			if (value.result == KPTAError.KPTA_OK)
			{
				let ob: object = value.payload;
				if (!Config.typeOf(ob)) return Promise.reject(new Error(KPTAError.INVALID_CONFIG_ARG_MSG));
				return Promise.resolve(Object.assign(new Config(), ob));
			}
			else return Promise.reject(value);
		});
	}
}

export class ObjectHandler
{
	static merge(obj1: any, obj2: any): any
	{
		for (var p in obj2)
		{
			if (obj2.hasOwnProperty(p))
			{
				if (obj2[p].constructor == Object)
				{
					if (!obj1.hasOwnProperty(p)) obj1[p]= {};
					if (!obj1[p]) obj1[p]= {};
					obj1[p] = ObjectHandler.merge(obj1[p], obj2[p]);
				}
				else obj1[p] = obj2[p];
			}
		}
		return obj1;
	}
	static deepCopy(ob: any): any { return ObjectHandler.merge({}, ob);	}
}

/**
 * Checks by samplings if an array of bytes is UTF-8 encoded
 * The implementation takes three samples from the ends and middle of the array.
 * If input is less than 2048 bytes, it is completely checked
 */
export class Sampler
{
	private count_chars(byte: number): number
	{
		let j = 0;
		while (j < 6 && (byte >> (7 - j) & 1)) j++;
		return (j - 1);
	}
	private sample(buffer: Uint8Array, pos: number, len: number): boolean
	{
		let i = pos;
		if (buffer.length < pos + len) throw new RangeError("Out of range arguments");
		while (i < len)
		{
			let c = buffer[i];
			if ((c > 31 && c < 128) || c == 9 || c == 10 || c == 13) i++;
			else if (c > 191 && c < 254)
			{
				i++;
				for (let j = 0, k = this.count_chars(c); j < k; j++, i++)
				{
					c = buffer[i];
					if (c < 128 && c > 191) return false;
				}
			}
			else return false;
		}
		return true;
	}
	private get_sample(buffer: Uint8Array, pos: number): number
	{
		let i = pos;
		while (i > 0)
		{
			let c = buffer[i];
			if (c < 128 || (c > 191 && c < 254)) return i;
			i--;
		}
		return i;
	}

	/**
	 * Checks if input is UTF-8 encoded
	 * @param buffer the array of bytes to be checked
	 */
	check(buffer: Uint8Array)
	{
		let pos = 0;
		let len = 128;
		let ret = false;
		if (buffer.length < 2048) return this.sample(buffer, 0, buffer.length);
		ret = this.sample(buffer, pos, len);
		if (ret)
		{
			pos = this.get_sample(buffer, buffer.length - len);
			ret = this.sample(buffer, pos, len);
			if (ret)
			{
				pos = this.get_sample(buffer, Math.trunc(buffer.length / 2));
				ret = this.sample(buffer, pos, len);
			}
		}
		return ret;
	}
}