/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Enrollment extension implementation
 * Copyleft (C) 2018/2019 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * 
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';
import { KPTAError, Logger } from "../../kryptonite";

function __getParagraph(id: string): HTMLParagraphElement
{
	let elem : HTMLElement | null = document.getElementById(id);
	if (elem && elem instanceof HTMLParagraphElement) return elem;
	else
	{
		Logger.error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG + id);
		throw new Error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG + id);
	}
}
function __getButton(id: string): HTMLButtonElement
{
	let elem : HTMLElement | null = document.getElementById(id);
	if (elem && elem instanceof HTMLButtonElement) return elem;
	else
	{
		Logger.error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG + id);
		throw new Error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG + id);
	}
}
function __getInput(id: string): HTMLInputElement
{
	let elem : HTMLElement | null = document.getElementById(id);
	if (elem && elem instanceof HTMLInputElement) return elem;
	else
	{
		Logger.error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG + id);
		throw new Error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG + id);
	}
}
function __getLocation(): Location
{
	if (document.location) return document.location;
	else
	{
		Logger.error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG + "document.location");
		throw new Error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG + "document.location");
	}
}
function __getUUID(): string
{
	let uuid = (new URL(__getLocation().toString())).searchParams.get('id');
	if (uuid) return uuid;
	else
	{
		Logger.error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG);
		throw new Error(KPTAError.HTML_ELEMENT_NOT_FOUND_MSG);
	}
}
let __isResolved: boolean = false;
function __resolve(uuid: string, response: boolean)
{
	__isResolved = true;
	chrome.runtime.sendMessage({ type: uuid, response: response,  remember: __getInput("forget").checked });
	chrome.windows.getCurrent((window: chrome.windows.Window): void => { chrome.windows.remove(window.id); });
}
function __onBeforeUnload()
{
	if (!__isResolved) __resolve(__getUUID(), false);
	window.removeEventListener("load",__onLoad);
	window.removeEventListener("beforeunload", __onBeforeUnload);
}
const TEXT_LEGEND = "Atenção: O aplicativo no endereço __URL__ enviou um certificado digital para ser instalado no seu computador. Você concorda em realizar essa operação?";
function __onLoad()
{
	let uuid: string = __getUUID();
	chrome.storage.local.get([uuid + "-origin"], (items: { [key: string]: any }) =>
	{
		if (chrome.runtime.lastError)
		{
			Logger.error(KPTAError.STORAGE_ERROR_MSG + chrome.runtime.lastError);
			__resolve(uuid, false);
		}
		else
		{
			let message: any = { origin: items[uuid + "-origin"] };
			if (typeof message.origin != "undefined")
			{
				__getParagraph("legend").appendChild(document.createTextNode(TEXT_LEGEND.replace(/__URL__/, message.origin)));
				__getButton("accept").addEventListener("click", () => { __resolve(uuid, true); });
				__getButton("refuse").addEventListener("click", () => { __resolve(uuid, false); });
			}
			else
			{
				Logger.error(KPTAError.UNEXPECTED_ERROR_MSG);
				__resolve(uuid, false);
			}
		}
	});
	document.getElementById("myModal")!.style.display = "block";
}
window.addEventListener("load", __onLoad);
window.addEventListener("beforeunload", __onBeforeUnload);
