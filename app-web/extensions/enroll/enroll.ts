/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Enrollment extension implementation
 * Copyleft (C) 2018/2019 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import { IProcessor, BridgeProcessor, TypeOfTrue, ExitProcessor, AccordanceProcessor, UpdatableExtension } from "../evtprocessor";
import { Gossip, Config, UUID } from "../krypton";
import { Protocol, Command, Response, GenerateCSRPayload, Logger } from "../../kryptonite";


class EnrollmentProcessor extends AccordanceProcessor
{
	/**
	 * Creates a new Enroll@kryptonite command processor
	 * @param gossip native connection manager
	 */
	constructor(gossip: Gossip)
	{
		super(gossip, new TypeOfTrue(), Protocol.NATIVE_ENROLLMENT_ID, Protocol.NATIVE_INSTALL_CMD);
		console.log('Enrollment extension initializing...');
	}
	/**
	 * Verifies if user accepts the command
	 * @param message signature command
	 * @param origin origin URL
	 * @param config current configuration
	 */
	validate(msg: Command, origin: string, config: Config): Promise<boolean>
	{
		return new Promise((resolve, reject) =>
		{
			if (config.enroll.blackList.some((value: string) => { return value === origin; })) return resolve(false);
			if (config.enroll.whiteList.some((value: string) => { return value === origin; })) return resolve(true);
			try
			{
				let uuid: string = UUID.generate();
				let param: { [k: string]: any } = {};
				param[uuid + "-origin"] = origin;
				let that: EnrollmentProcessor = this;
				function callback(message: any)
				{
					if (message.type == uuid)
					{
						if (message.remember)
						{
							if (message.response) config.enroll.whiteList.push(origin);
							else config.enroll.blackList.push(origin);
							config.store(that.gossip)
							.then((value: Response) => { Logger.notice(Logger.CONFIG_SAVED_MSG, value.reason); })
							.catch((reason) => { Logger.error(Logger.CONFIG_SAVE_ERROR_MSG, (reason.message || JSON.stringify(reason)))});
						}
						resolve(message.response);
						chrome.runtime.onMessage.removeListener(callback);
					}
				};
				chrome.storage.local.set(param, () =>
				{
					if (chrome.runtime.lastError) reject(chrome.runtime.lastError);
					else chrome.windows.create({ url: chrome.extension.getURL("install.html?id=" + uuid), type: "popup", width: 560, height: 300 }, (() =>
					{
						chrome.runtime.onMessage.addListener(callback);
					}));
				});
			}
			catch (e) { reject(e); }
		});
	}
}

/**
 * Initializes extension
 */
function main()
{
	let gossip: Gossip = new Gossip(Protocol.NATIVE_HOST_NAME);
	let enumProcessor: BridgeProcessor = new BridgeProcessor(gossip, new TypeOfTrue(), Protocol.NATIVE_ENROLLMENT_ID, Protocol.NATIVE_ENUM_CMD);
	let genProcessor: BridgeProcessor = new BridgeProcessor(gossip, GenerateCSRPayload.i, Protocol.NATIVE_ENROLLMENT_ID, Protocol.NATIVE_GENCSR_CMD);
	let installProcessor: EnrollmentProcessor = new EnrollmentProcessor(gossip);
	let byeProcessor: BridgeProcessor = new BridgeProcessor(gossip, new TypeOfTrue(), Protocol.NATIVE_VERSIONING_ID, Protocol.NATIVE_RELINQUISH_CMD);
	let map: Map<string, IProcessor> = new Map<string, IProcessor>();
	map.set(Protocol.ENROLL_ENUM_CMD, enumProcessor);
	map.set(Protocol.ENROLL_GENCSR_CMD, genProcessor);
	map.set(Protocol.ENROLL_INSTALL_CMD, installProcessor);
	map.set(Protocol.ENROLL_RELINQUISH_CMD, byeProcessor);
	map.set(Protocol.EXIT_CMD, new ExitProcessor(gossip));
	let ext: UpdatableExtension = new UpdatableExtension(map);
	ext.init();
}	main();