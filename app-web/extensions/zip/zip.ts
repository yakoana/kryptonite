/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Zip extension implementation
 * Copyleft (C) 2018/2019 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';

import { Writable, Readable } from "stream";
import { ZipFile } from "yazl";
import { ZipFile as UnzipFile, Entry, fromBuffer } from "yauzl";
import { Config } from "../krypton";
import { GenericProcessor, InvalidPayload, IProcessor, DefaultExtension } from "../evtprocessor";
import { Command, Response, KPTAError, AddZipPayload, Protocol, InflatePayload, Base64 } from "../../kryptonite";


// Thrown if MemoryStream _write memory is called after object has completed its job
class ObjectStateError extends Error { constructor(m: string) { super(m); }}
// Generic zip error
class ZipError extends Error { constructor(m: string) { super(m); }}
// Thrown if inflate() method (of Inflater) is called with a nonexistent entry 
class EntryNotFoundError extends ZipError { constructor(m: string) { super(m); }}
// Thrown if deflater could not compress an entry
class ZipErrorResponse extends Response { constructor(e: ZipError) { super(e, KPTAError.KPTA_FAILED, KPTAError.KPTA_ADD_ZIP_ERROR); }}
// Throw if could not inflate
class UnzipErrorResponse extends Response { constructor(e: ZipError) { super(e, KPTAError.KPTA_FAILED, KPTAError.KPTA_UNZIP_ENTRY_ERROR); }}
// Throw if an invalid handle is received from web API
class InvalidHandle extends Response { constructor() { super(null, KPTAError.KPTA_FAILED, KPTAError.KPTA_INVALID_HANDLE_ERROR); }}

/**
 * Implements a memory stream writable
 */
class MemoryStream extends Writable
{
	private _buffer: Array<Buffer> = new Array<Buffer>();
	private _stream: Buffer | null = null;
	private __getEncoding(value: string) : BufferEncoding
	{
		let ret: BufferEncoding ="utf-8";
		if (value === "ascii" || value ===  "utf8" || value ===  "utf-8" || value ===  "utf16le" || value ===  "ucs2" || value ===  "ucs-2" || value ===  "base64" || value ===  "latin1" || value ===  "binary" || value ===  "hex") ret = value;
		return ret;
	}
	_write(chunk: any, encoding: string, callback: (error?: Error | null) => void): void
	{
		if (this._stream) callback(new ObjectStateError(KPTAError.OBJECT_STATE_ERROR_MSG));
		else
		{
			let err: Error | null = null;
			let data: Buffer;
			try
			{
				if (typeof chunk === "string") data = Buffer.from(chunk, this.__getEncoding(encoding));
				else data = chunk;
				this._buffer.push(data);
			}
			catch (e) { err = e; }
			callback(err);
		}
	}
	_final(callback: (error?: Error | null) => void): void { this.emit("finish", this.toBuffer()); }
	/**
	 * Convert Buffer array buffer into an array of bytes and finishes the stream
	 */
	toBuffer(): Uint8Array
	{
		if (!this._stream) this._stream = Buffer.concat(this._buffer);
		return new Uint8Array(this._stream.buffer);
	}
}

/**
 * Deflation implementation
 */
const REGULAR_FILE = parseInt("0100664", 8); /* (0100000 => POSIX S_IFREG: regular file) + 664 => -rw-rw-r-- equiv chown */
let dCounter: number = 0;
class Deflater
{ 
	hHandle: number;
	private zip: ZipFile;
	private writer: MemoryStream;
	/**
	 * Creates a new deflater for a zip file
	 */
	constructor()
	{
		this.zip = new ZipFile();
		this.writer = new MemoryStream();
		this.zip.outputStream.pipe(this.writer);
		this.hHandle = ++dCounter;
	}
	/**
	 * Adds an entry to zip file
	 * @param entry entry to add to zip
	 */
	add(entry: AddZipPayload): void
	{
		try { this.zip.addBuffer(Buffer.from(new Uint8Array(Base64.atob(entry.entry)).buffer), entry.name, { mtime: new Date(entry.date), mode: REGULAR_FILE, compress: entry.compress });}
		catch (e) { throw new ZipError(e); }
	}
	/**
	 * Finishis zipping and returns an array buffer of generated zip file
	 */
	close(): Promise<Uint8Array>
	{
		return new Promise((resolve, reject) => 
		{
			this.writer.on("finish", () => { resolve(this.writer.toBuffer()); });
			this.zip.end();
		});
	}
}
class DeflateProcessor extends GenericProcessor
{
	private map: Map<number, Deflater> = new Map<number, Deflater>();
	/**
	 * Processes a deflate command
	 * @param message command
	 * @param origin origin URL. Ignored
	 * @param config current config. Ignored.
	 */
	process(message: Command, origin: string, cfg: Promise<Config>): Promise<Response>
	{
		return new Promise((resolve, reject) =>
		{
			if (message.commandID === Protocol.CREATE_ZIP_CMD)
			{
				let deflater: Deflater = new Deflater();
				this.map.set(deflater.hHandle, deflater);
				resolve(new Response(deflater.hHandle));
			}

			else if (message.commandID === Protocol.ADD_ENTRY_CMD)
			{
				AddZipPayload.i.check(message.payload).then((value: boolean) =>
				{
					if (value)
					{
						let payload: AddZipPayload = message.payload;
						let deflater: Deflater | undefined = this.map.get(payload.handle);
						if (deflater)
						{
							try
							{
								deflater.add(payload);
								resolve(new Response());
							}
							catch (e) { reject(new ZipErrorResponse(e)); }
						}
						else reject(new InvalidHandle());
					}
					else reject(new InvalidPayload());
				});
			}

			else if (message.commandID === Protocol.CLOSE_ZIP_CMD)
			{
				let deflater: Deflater | undefined = this.map.get(message.payload);
				if (deflater)
				{
					this.map.delete(message.payload);
					deflater.close().then((value: Uint8Array) => { resolve(new Response(Base64.btoa(value))); });
				}
				else reject(new InvalidHandle());
			}
		});
	}
}

/**
 * Inflation implementaiton
 */
let iCounter: number = 0;
class Inflater
{
	/**
	 * Opens a zip file to inflation
	 * @param buffer zip file
	 */
	static open(buffer: ArrayBuffer): Promise<Inflater>
	{
		return new Promise((resolve, reject) =>
		{
			var ret = new Inflater(buffer);
			fromBuffer(ret.buffer, {}, (err?: Error | undefined, zipFile?: UnzipFile | undefined) =>
			{
				if (err) reject(new ZipError(err.message));
				else if (zipFile)
				{
					ret.zip = zipFile;
					ret.zip.on("entry", (entry: Entry) => { ret.entries.set(entry.fileName, entry); });
					ret.zip.on("end", () => { resolve(ret) });
				}
				else reject(new ZipError(KPTAError.MALFORMED_ZIP_MSG));
			});
		});
	}
	handle: number = ++iCounter;;
	private buffer: Buffer;
	private zip: UnzipFile | null = null;
	private entries: Map<string, Entry> = new Map<string, Entry>();
	constructor(buffer: ArrayBuffer) { this.buffer = Buffer.from(buffer); }
	/**
	 * List all entries
	 */
	list(): Set<string> { return new Set(this.entries.keys()); }
	/**
	 * Inflates specified entry
	 * @param name entry name
	 */
	inflate(name: string): Promise<Uint8Array>
	{
		return new Promise((resolve, reject) =>
		{
			let entry: Entry | undefined = this.entries.get(name);
			if (!entry) reject(new EntryNotFoundError(name));
			else
			{
				if (!this.zip) reject(new ZipError(KPTAError.UNEXPECTED_ERROR_MSG));
				else
				{
					this.zip.openReadStream(entry, (err: Error | undefined, stream?: Readable | undefined) =>
					{
						if (err) reject(err);
						else if (stream)
						{
							let writer: MemoryStream = new MemoryStream();
							stream.on("end", () => { resolve(writer.toBuffer()); });
							stream.pipe(writer);
						}
						else reject(new ZipError(KPTAError.UNEXPECTED_ERROR_MSG));
					});
				}
			}
		});
	}
}
class InflateProcessor extends GenericProcessor
{
	private map: Map<number, Inflater> = new Map<number, Inflater>();
	/**
	 * Processes an inflate command
	 * @param message command to process
	 * @param origin origin URL. Ignored
	 * @param config current configuration. Ignored
	 */
	process(message: Command, origin: string, cfg: Promise<Config>): Promise<Response>
	{
		return new Promise((resolve, reject) =>
		{
			if (message.commandID === Protocol.OPEN_ZIP_CMD)
			{
				if (message.payload)
				{
					Inflater.open(new Uint8Array(Base64.atob(message.payload)).buffer)
					.then((inflater: Inflater) =>
					{
						this.map.set(inflater.handle, inflater);
						resolve(new Response(inflater.handle));
					})
					.catch((e) => { reject(new UnzipErrorResponse(e)); });
				}
				else reject(new InvalidPayload());
			}

			else if (message.commandID === Protocol.LIST_ZIP_CMD)
			{
				let inflater: Inflater | undefined = this.map.get(message.payload);
				if (inflater) resolve(new Response(Array.from(inflater.list())));
				else reject(new InvalidHandle());
			}

			else if (message.commandID === Protocol.INFLATE_ENTRY_CMD)
			{
				InflatePayload.i.check(message.payload)
				.then((value: boolean) =>
				{
					if (value)
					{
						let payload: InflatePayload = message.payload;
						let inflater: Inflater | undefined = this.map.get(payload.handle);
						if (inflater)
						{
							inflater.inflate(payload.name)
							.then((file: Uint8Array) => { resolve(new Response(Base64.btoa(file))); })
							.catch((e) => { reject(new UnzipErrorResponse(e)); });
						}
						else reject(new InvalidHandle());
					}
					else reject(new InvalidPayload());
				});
			}

			else if (message.commandID === Protocol.CLOSE_CMD)
			{
				let inflater: Inflater | undefined = this.map.get(message.payload);
				if (inflater)
				{
					this.map.delete(message.payload);
					resolve(new Response());
				}
				else reject(new InvalidHandle());
			}
		});
	}
}

class ZipExtension extends DefaultExtension
{
	/**
	 * Creates a new instance of zip extension
	 * @param props command processors
	 */
	constructor(props: Map<string, IProcessor>) { super(props); }
	protected update(): void {}
}

/**
 * Initializes extension
 */
function main()
{
	let dProcessor: DeflateProcessor = new DeflateProcessor();
	let iProcessor: InflateProcessor = new InflateProcessor();
	let map: Map<string, IProcessor> = new Map<string, IProcessor>();
	map.set(Protocol.CREATE_ZIP_CMD, dProcessor);
	map.set(Protocol.ADD_ENTRY_CMD, dProcessor);
	map.set(Protocol.CLOSE_ZIP_CMD, dProcessor);
	map.set(Protocol.OPEN_ZIP_CMD, iProcessor);
	map.set(Protocol.LIST_ZIP_CMD, iProcessor);
	map.set(Protocol.INFLATE_ENTRY_CMD, iProcessor);
	map.set(Protocol.CLOSE_CMD, iProcessor)
	let ext: ZipExtension = new ZipExtension(map);
	ext.init();
}	main();