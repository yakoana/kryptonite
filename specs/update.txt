{
  "date": 1564151078527,
  "config": {
    "version": 16777216,
    "logLevel": 3,
    "maxResponseSize": 1024000,
    "components": {
      "f258091d-bd65-49f0-ba7b-a18f182ce6e0": "kptaroll",
      "72499c1f-93ba-4b7d-8cb2-76048f8b31b5": "kptasign",
      "51d003c8-a028-450d-8c51-41b4ea3bafb0": "kptavrfy"
    },
    "enroll": {
      "blackList": [],
      "whiteList": [],
      "issueProfile": {
        "keySize": 2048,
        "signAlgOID": "1.2.840.113549.1.1.11"
      }
    },
    "sign": {
      "blackList": [],
      "whiteList": [],
      "signProfile": {
        "signAlg": 64,
        "policy": "CAdES-BES"
      }
    }
  },
  "components": [
    {
      "extension": "sign",
      "version": "1.0.1",
      "component": "72499c1f-93ba-4b7d-8cb2-76048f8b31b5",
      "hash": "50c2dc6ed8a58c448fbd43aecd455af359ffcd54c3ada7ed49529e826c8d703b",
      "file": "sign.dat",
      "__binary": null
    }
  ]
}