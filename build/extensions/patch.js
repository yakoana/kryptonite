/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Build components
 * Copyleft (C) 2018/2020 by The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';
const OS		= require("os");
const WIN32		= (OS.platform().valueOf() === "win32");
const PATH		= WIN32 ? require("path").win32 : require("path").posix;
const FSYS		= require("fs");
const HASH		= require("hash.js");
const CP_TEMPLATE	= WIN32 ? "__NAME__.dll" : "lib__NAME__.so";
const accessSync 	= FSYS.accessSync;
function __accessSync(target, access) { try { accessSync(target, access); return true; } catch(e) { return false; }}
FSYS.accessSync	= __accessSync;
const COLORS	= require('colors/safe');


class NativeComponents
{
	constructor()
	{
		this.sign = null;
		this.verify = null;
		this.enroll = null;
	}
	static load(file)
	{
		return JSON.parse(FSYS.readFileSync(PATH.resolve(__dirname, file)));
	}
}
class AppUpdate
{
	constructor()
	{
		this.date = (new Date()).getTime();
		this.config = null;
		this.components = [];
	}
	toString()
	{
		return JSON.stringify(this);
	}
}
class AppConfig
{
	constructor()
	{
		this.oldVersion = 16777216;
		this.version = 16777216;
		this.logLevel = 3;
		this.maxResponseSize = 1024000;
		this.components = {
			"f258091d-bd65-49f0-ba7b-a18f182ce6e0": "kptaroll",
			"72499c1f-93ba-4b7d-8cb2-76048f8b31b5": "kptasign",
			"51d003c8-a028-450d-8c51-41b4ea3bafb0": "kptavrfy"
		};
		this.enroll = {
			blackList: [],
			whiteList: [],
			issueProfile: {
				keySize: 2048,
				signAlgOID: "1.2.840.113549.1.1.11"
			}
		};
		this.sign = {
			blackList: [],
			whiteList: [],
			signProfile: {
				signAlg: 64,
				policy: "CAdES-BES"
			}
		};
	}
	toString() { return JSON.stringify(this); }
	static load()
	{
		return JSON.parse(FSYS.readFileSync(PATH.resolve(__dirname, "config-upd.json")));
	}
}
class Version
{
	constructor()
	{
		this.major = 1;
		this.minor = 0;
		this.release = 0;
	}
	toString() { return this.major.toString() + "." + this.minor.toString() + "." + this.release.toString(); }
	static load(ext, file)
	{
		let ob = JSON.parse(FSYS.readFileSync(PATH.resolve(__dirname, file)));
		let ret = new Version();
		ret.major = ob[ext].major;
		ret.minor = ob[ext].minor;
		ret.release = ob[ext].release;
		return ret;
	}
}
class NativeVersion extends Version
{
	constructor()
	{
		super();
		this.extID = "";
		this.file = "";
		this.id = "";
		this.hash = "";
	}
	static load(ext, file)
	{
		let ob = NativeComponents.load(file);
		let ret = new NativeVersion();
		ret.major = ob[ext].major;
		ret.minor = ob[ext].minor;
		ret.release = ob[ext].release;
		ret.file = ob[ext].file;
		ret.id = ob[ext].id;
		ret.hash = ob[ext].hash;
		return ret;
	}
}
class AppComponent
{
	constructor()
	{
		this.extension = "";
		this.version = "";
		this.component = "";
		this.hash = "";
		this.file = "";
		this.__binary = null;
	}
}
class TaskResult
{
	constructor(name, output, warnings, errors)
	{
		this.__name = name;
		this.__output = output;
		this.__warnings = warnings;
		this.__errors = errors;
	}
	format(callback)
	{
		callback.log("Task " + !this.__errors ? COLORS.green.bold(this.__name) : COLORS.red.bold(this.__name) + " has been executed with following results:");
		if (this.__output) callback.log("Output: " + this.__output);
		if (this.__warnings) callback.warn(COLORS.yellow.bold("Warnings: ") + this.__warnings);
		if (this.__errors) callback.error(COLORS.red.bold("Errors: ") + this.__errors);
	}
}
class Patch
{
	constructor(targetDir, isDebug)
	{
		this.name = "Create native app patch";
		this.__source = PATH.resolve
		(
			__dirname,
			"..",
			"..",
			targetDir,
			isDebug ? "Debug" : "Release",
			"dist",
			WIN32 && process.env.PLATFORM == "x86" ? "Win32" : process.env.PLATFORM,
			"kryptonite",
			"App"
		);
		this.__target = PATH.resolve
		(
			__dirname,
			"..",
			"..",
			targetDir,
			isDebug ? "Debug" : "Release",
			"bin",
			"javascript"
		);
	}
	execute()
	{
		return new Promise((resolve, reject) =>
		{
			try
			{
				let json = new NativeComponents();
				let upd = new AppUpdate();
				let cfg = AppConfig.load();
				let signVer = NativeVersion.load("sign", "native-ver.json");
				let vrfyVer = NativeVersion.load("verify", "native-ver.json");
				let rollVer = NativeVersion.load("enroll", "native-ver.json");
				let signExt = PATH.resolve(this.__target, "sign-extension");
				let vrfyExt = PATH.resolve(this.__target, "verify-extension");
				let rollExt = PATH.resolve(this.__target, "enroll-extension");
				if (cfg.oldVersion != cfg.version) upd.config = cfg;
				let lib = FSYS.readFileSync(PATH.resolve(this.__source, CP_TEMPLATE.replace(/__NAME__/, signVer.file)));
				let hash = HASH.sha256().update(lib).digest('hex');
				if (hash.toUpperCase() != signVer.hash.toUpperCase())
				{
					let app = new AppComponent();
					app.extension = "sign";
					app.version = signVer.toString();
					app.component = signVer.id;
					app.hash = hash;
					app.file = signVer.file + ".dat";
					FSYS.writeFileSync(PATH.resolve(signExt, app.file), this.__btoa(lib));
					upd.components.push(app);
					signVer.hash = hash;
					json.sign = signVer;
				}
				lib = FSYS.readFileSync(PATH.resolve(this.__source, CP_TEMPLATE.replace(/__NAME__/, vrfyVer.file)));
				hash = HASH.sha256().update(lib).digest('hex');
				if (hash.toUpperCase() != vrfyVer.hash.toUpperCase())
				{
					let app = new AppComponent();
					app.extension = "sign";
					app.version = vrfyVer.toString();
					app.component = vrfyVer.id;
					app.hash = vrfyVer.hash;
					app.file = vrfyVer.file + ".dat";
					FSYS.writeFileSync(PATH.resolve(vrfyExt, app.file), this.__btoa(lib));
					upd.components.push(app);
					vrfyVer.hash = hash;
					json.verify = vrfyVer;
				}
				lib = FSYS.readFileSync(PATH.resolve(this.__source, CP_TEMPLATE.replace(/__NAME__/, rollVer.file)));
				hash = HASH.sha256().update(lib).digest('hex');
				if (hash.toUpperCase() != rollVer.hash.toUpperCase())
				{
					let app = new AppComponent();
					app.extension = "sign";
					app.version = rollVer.toString();
					app.component = rollVer.id;
					app.hash = rollVer.hash;
					app.file = rollVer.file + ".dat";
					FSYS.writeFileSync(PATH.resolve(rollExt, app.file), this.__btoa(lib));
					upd.components.push(app);
					rollVer.hash = hash;
					json.enroll = rollVer;
				}
				let hasUpdate = upd.config || upd.components.length > 0;
				if (hasUpdate)
				{
					let dat = JSON.stringify(upd);
					FSYS.writeFileSync(PATH.resolve(signExt, "current-version.json"), dat);
					FSYS.writeFileSync(PATH.resolve(vrfyExt, "current-version.json"), dat);
					FSYS.writeFileSync(PATH.resolve(rollExt, "current-version.json"), dat);
					FSYS.writeFileSync(PATH.resolve(__dirname, "native-ver.json"), JSON.stringify(json));
				}
				return resolve(new TaskResult(this.name, hasUpdate ? "A new patch has been created" : "No patch is required"));
			}
			catch (e) { return reject(new TaskResult(this.name, "Task failed", "", JSON.stringify(e))); }
		});
	}
	__btoa(bytes)
	{
		const encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
		var base64        = '';
		var byteLength    = bytes.byteLength;
		var byteRemainder = byteLength % 3;
		var mainLength    = byteLength - byteRemainder;
		var a, b, c, d;
		var chunk;
		for (var i = 0; i < mainLength; i = i + 3)
		{
			chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];
			a = (chunk & 16515072) >> 18;
			b = (chunk & 258048)   >> 12;
			c = (chunk & 4032)     >>  6;
			d = chunk & 63;
			base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
		}
		if (byteRemainder == 1)
		{
			chunk = bytes[mainLength];
			a = (chunk & 252) >> 2;
			b = (chunk & 3)   << 4;
			base64 += encodings[a] + encodings[b] + '==';
		}
		else if (byteRemainder == 2)
		{
			chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];
			a = (chunk & 64512) >> 10;
			b = (chunk & 1008)  >>  4;
			c = (chunk & 15)    <<  2;
			base64 += encodings[a] + encodings[b] + encodings[c] + '=';
		}
		return base64;
	}
}
/**
 * Command line arguments:
 * 	--debug: debug options. If not set, only Test task will be build with debugging optionh
 * 	--output=[path]: build directory, relative to project home. Optional; default value: ./output
 */
function Main()
{
	let isDebug = false;
	let out = "output";
	let xtors = [];
	process.argv.forEach((value) =>
	{
		if (value.startsWith("--"))
		{
			if (value.startsWith("--debug")) isDebug = true;
			else if (value.startsWith("--output"))
			{
				let parts = value.split("=");
				if (parts.length == 2) out = parts[1];
			}
		}
	});
	let patch = new Patch(out, isDebug);
	console.info("Executing task " + patch.name);
	patch.execute()
	.then(() =>
	{
		console.info(COLORS.green.bold("Task done!"));
	})
	.catch((reason) =>
	{
		console.error(COLORS.red.bold("Execution failure with reason: " + JSON.stringify(reason)));
	});
} Main();