/* * * * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Build components
 * Copyleft (C) 2018/2020 by The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * * * * * * * * * * * * * * * * * * * * * * * * * */
'use strict';
const OS		= require("os");
const WIN32		= (OS.platform().valueOf() === "win32");
const PATH		= WIN32 ? require("path").win32 : require("path").posix;
const Progress	= require("progress-bar-webpack-plugin");
const COLORS	= require('colors/safe');
const WEBPACK	= require("webpack");

class WebPackConfig
{
	constructor(source, isDebug, isWeb)
	{
		this.context = PATH.resolve(__dirname, "..", "..");
		this.resolve = { extensions: [ ".tsx", ".ts", ".js" ] };
		this.bail = true;
		this.plugins = [ new Progress({ summary: false }) ];
		this.module = {
			rules: [
			{
				test: /\.tsx?$/,
				use: [
					{ loader: "ts-loader" },
					{
						loader: "cond_compile",
						options: { DEBUG: isDebug }
					}
				],
				exclude: /node_modules/
			}]
		};
		this.entry = PATH.resolve(this.context, source);
		this.mode = isDebug ? "development" : "production";
		this.devtool = isDebug ? "inline-source-map" : "";
		if (isWeb)
		{
			this.target = "web";
			this.node = { fs: "empty", Buffer : true };
		}
		else
		{
			this.target = "node";
			this.node = { fs: "empty", Buffer : true, __dirname: false, __filename: false };
		}
	}
	publish() { return Object.assign(this); }
}
class ExtensionWebPackConfig extends WebPackConfig
{
	constructor(extID, source, targetDir, target, libName, isDebug)
	{
		super(source, isDebug, true);
		this.output = {
			filename: target,
			path: PATH.resolve(this.context, targetDir, isDebug ? "Debug" : "Release", "bin", "javascript", extID + "-extension"),
			library: libName ? libName : "",
			libraryExport: libName ? libName : ""
		};
	}
}
class EnrollBundleWebPackConfig extends ExtensionWebPackConfig
{
	constructor(targetDir, isDebug)
	{
		super("enroll", "app-web/extensions/enroll/enroll.ts", targetDir, "bundle.js", "", isDebug);
	}
}
class EnrollContentsWebPackConfig extends ExtensionWebPackConfig
{
	constructor(targetDir, isDebug)
	{
		super("enroll", "app-web/web-api/enroll-contents.ts", targetDir, "contents.js", "", isDebug);
	}
}
class EnrollAPIWebPackConfig extends ExtensionWebPackConfig
{
	constructor(targetDir, isDebug)
	{
		super("enroll", "app-web/web-api/enroll-api.ts", targetDir, "global.js", "kptaenroll", isDebug);
	}
}
class EnrollAcceptWebPackConfig extends ExtensionWebPackConfig
{
	constructor(targetDir, isDebug)
	{
		super("enroll", "app-web/extensions/enroll/accept-install.ts", targetDir, "accept-install.js", "", isDebug);
	}
}
class SignBundleWebPackConfig extends ExtensionWebPackConfig
{
	constructor(targetDir, isDebug)
	{
		super("sign", "app-web/extensions/sign/sign.ts", targetDir, "bundle.js", "", isDebug);
	}
}
class SignContentsWebPackConfig extends ExtensionWebPackConfig
{
	constructor(targetDir, isDebug)
	{
		super("sign", "app-web/web-api/sign-contents.ts", targetDir, "contents.js", "", isDebug);
	}
}
class SignAPIWebPackConfig extends ExtensionWebPackConfig
{
	constructor(targetDir, isDebug)
	{
		super("sign", "app-web/web-api/sign-api.ts", targetDir, "global.js", "kptasign", isDebug);
	}
}
class SignAcceptWebPackConfig extends ExtensionWebPackConfig
{
	constructor(targetDir, isDebug)
	{
		super("sign", "app-web/extensions/sign/accept-sign.ts", targetDir, "accept-sign.js", "", isDebug);
	}
}
class VerifyBundleWebPackConfig extends ExtensionWebPackConfig
{
	constructor(targetDir, isDebug)
	{
		super("verify", "app-web/extensions/verify/verify.ts", targetDir, "bundle.js", "", isDebug);
	}
}
class VerifyContentsWebPackConfig extends ExtensionWebPackConfig
{
	constructor(targetDir, isDebug)
	{
		super("verify", "app-web/web-api/verify-contents.ts", targetDir, "contents.js", "", isDebug);
	}
}
class VerifyAPIWebPackConfig extends ExtensionWebPackConfig
{
	constructor(targetDir, isDebug)
	{
		super("verify", "app-web/web-api/verify-api.ts", targetDir, "global.js", "kptaverify", isDebug);
	}
}
class ZipBundleWebPackConfig extends ExtensionWebPackConfig
{
	constructor(targetDir, isDebug)
	{
		super("zip", "app-web/extensions/zip/zip.ts", targetDir, "bundle.js", "", isDebug);
	}
}
class ZipContentsWebPackConfig extends ExtensionWebPackConfig
{
	constructor(targetDir, isDebug)
	{
		super("zip", "app-web/web-api/zip-contents.ts", targetDir, "contents.js", "", isDebug);
	}
}
class ZipAPIWebPackConfig extends ExtensionWebPackConfig
{
	constructor(targetDir, isDebug)
	{
		super("zip", "app-web/web-api/zip-api.ts", targetDir, "global.js", "kptazip", isDebug);
	}
}
class TestWebPackConfig extends WebPackConfig
{
	constructor(source, targetDir, target, libName, isWeb)
	{
		super(source, true, isWeb);
		this.output = {
			filename: target,
			path: PATH.resolve(this.context, targetDir, "Debug", "bin", "javascript", "check"),
			library: libName ? libName : "",
			libraryExport: libName ? libName : ""
		};
	}
}
class ServiceTestWebPackConfig extends TestWebPackConfig
{
	constructor(targetDir)
	{
		super("app-check/service.ts", targetDir, "service.js", "", false);
	}
}
class WebTestWebPackConfig extends TestWebPackConfig
{
	constructor(targetDir)
	{
		super("app-check/test.ts", targetDir, "test.js", "test_main", false);
	}
}
class TaskResult
{
	constructor(name, output, warnings, errors)
	{
		this.__name = name;
		this.__output = output;
		this.__warnings = warnings;
		this.__errors = errors;
	}
	format(callback)
	{
		callback.log("Task " + !this.__errors ? COLORS.green.bold(this.__name) : COLORS.red.bold(this.__name) + " has been executed with following results:");
		if (this.__output) callback.log("Output: " + this.__output);
		if (this.__warnings) callback.warn(COLORS.yellow.bold("Warnings: ") + this.__warnings);
		if (this.__errors) callback.error(COLORS.red.bold("Errors: ") + this.__errors);
	}
}
class WebPackTask
{
	constructor(name, cfg)
	{
		this.name = name;
		this.__config = cfg;
	}
	execute()
	{
		return new Promise((resolve, reject) =>
		{
			WEBPACK(this.__config.publish(), (err, stats) =>
			{
				if (err) reject(new TaskResult(this.name, "Failed to publish Webpack object", JSON.stringify(this.__config), err.message + " " + err.details));
				else
				{
					let output = stats.toString({ chunks: false, colors: true });
					let info = stats.toJson();
					let warns = stats.hasWarnings() ? info.warnings : "";
					let errs = stats.hasErrors() ? info.errors : "";
					resolve(new TaskResult(this.name, output, warns, errs));
				}
			});
		});
	}
}
class Task
{
	constructor(name, isDebug)
	{
		this.name = name;
		this.__isDebug = isDebug;
		this.__tasks = [];
	}
	pop()
	{
		return this.__tasks.pop();
	}
}
class EnrollExtension extends Task
{
	constructor(targetDir, isDebug)
	{
		super("Kriptonite Enrollment", isDebug);
		this.__tasks.push(new WebPackTask
		(
			"Building extension bundle",
			new EnrollBundleWebPackConfig(targetDir, isDebug))
		);
		this.__tasks.push(new WebPackTask
		(
			"Building extension content script",
			new EnrollContentsWebPackConfig(targetDir, isDebug))
		);
		this.__tasks.push(new WebPackTask
		(
			"Building extension API",
			new EnrollAPIWebPackConfig(targetDir, isDebug))
		);
		this.__tasks.push(new WebPackTask
		(
			"Building extension accept dialog",
			new EnrollAcceptWebPackConfig(targetDir, isDebug))
		);
	}
}
class SignExtension extends Task
{
	constructor(targetDir, isDebug)
	{
		super("Kriptonite Digital Signature", isDebug);
		this.__tasks.push(new WebPackTask
		(
			"Building extension bundle",
			new SignBundleWebPackConfig(targetDir, isDebug))
		);
		this.__tasks.push(new WebPackTask
		(
			"Building extension content script",
			new SignContentsWebPackConfig(targetDir, isDebug))
		);
		this.__tasks.push(new WebPackTask
		(
			"Building extension API",
			new SignAPIWebPackConfig(targetDir, isDebug))
		);
		this.__tasks.push(new WebPackTask
		(
			"Building extension accept dialog",
			new SignAcceptWebPackConfig(targetDir, isDebug))
		);
	}
}
class VerifyExtension extends Task
{
	constructor(targetDir, isDebug)
	{
		super("Kriptonite Digital Signature Validation", isDebug);
		this.__tasks.push(new WebPackTask
		(
			"Building extension bundle",
			new VerifyBundleWebPackConfig(targetDir, isDebug))
		);
		this.__tasks.push(new WebPackTask
		(
			"Building extension content script",
			new VerifyContentsWebPackConfig(targetDir, isDebug))
		);
		this.__tasks.push(new WebPackTask
		(
			"Building extension API",
			new VerifyAPIWebPackConfig(targetDir, isDebug))
		);
	}
}
class ZipExtension extends Task
{
	constructor(targetDir, isDebug)
	{
		super("Kriptonite Deflate and Inflate", isDebug);
		this.__tasks.push(new WebPackTask
		(
			"Building extension bundle",
			new ZipBundleWebPackConfig(targetDir, isDebug))
		);
		this.__tasks.push(new WebPackTask
		(
			"Building extension content script",
			new ZipContentsWebPackConfig(targetDir, isDebug))
		);
		this.__tasks.push(new WebPackTask
		(
			"Building extension API",
			new ZipAPIWebPackConfig(targetDir, isDebug))
		);
	}
}
class Test extends Task
{
	constructor(targetDir)
	{
		super("Kryptonite test web application", true);
		this.__tasks.push(new WebPackTask
		(
			"Building test service",
			new ServiceTestWebPackConfig(targetDir))
		);
		this.__tasks.push(new WebPackTask
		(
			"Build test web application",
			new WebTestWebPackConfig(targetDir))
		);
	}
}
class Executor
{
	constructor(task)
	{
		this.__path = task;
		this.__console = console;
	}
	__chain(task, resolve, reject)
	{
		this.__console.log("Executing task " + task.name);
		task.execute().then((value) =>
		{
			value.format(this.__console);
			let next = this.__path.pop();
			if (next) this.__chain(next, resolve, reject);
			else resolve();
		})
		.catch((reason) =>
		{
			if (reason.format) reason.format(this.__console);
			this.__console.error("The following tasks will not execute:");
			let next;
			do
			{
				next = this.__path.pop();
				if (next) this.__console.error(next.name);
			}
			while (next);
			reject(reason);
		});
	}
	execute(resolve, reject)
	{
		let task = this.__path.pop();
		if (task) this.__chain(task, resolve, reject);
	}
}

/**
 * Command line arguments:
 * 	--debug: debug options. If not set, only Test task will be build with debugging optionh
 * 	--output=[path]: build directory, relative to project home. Optional; default value: ./output
 */
function Main()
{
	let isDebug = false;
	let out = "output";
	let xtors = [];
	process.argv.forEach((value) =>
	{
		if (value.startsWith("--"))
		{
			if (value.startsWith("--debug")) isDebug = true;
			else if (value.startsWith("--output"))
			{
				let parts = value.split("=");
				if (parts.length == 2) out = parts[1];
			}
		}
	});
	xtors.push(new Executor(new EnrollExtension(out, isDebug)));
	xtors.push(new Executor(new SignExtension(out, isDebug)));
	xtors.push(new Executor(new VerifyExtension(out, isDebug)));
	xtors.push(new Executor(new ZipExtension(out, isDebug)));
	xtors.push(new Executor(new Test(out)));
	Promise.all(xtors.map((value) =>
	{
		return new Promise((resolve, reject) =>
		{
			value.execute(resolve, reject);
		});
	}))
	.then(() =>
	{
		console.info(COLORS.green.bold("All tasks done!"));
	})
	.catch((reason) =>
	{
		console.error(COLORS.red.bold("Execution failure with reason: " + JSON.stringify(reason)));
	});
} Main();