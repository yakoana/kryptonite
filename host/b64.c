/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Base64/PEM encoder facility
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "kryptonite.h"
#include "b64/cencode.h"
#include "b64/cdecode.h"

#define BEGIN_P10_ARMOUR			"-----BEGIN CERTIFICATE REQUEST-----\n"
#define END_P10_ARMOUR				"-----END CERTIFICATE REQUEST-----"
#define BEGIN_CERT_ARMOUR			"-----BEGIN CERTIFICATE -----\n"
#define END_CERT_ARMOUR				"-----END CERTIFICATE -----"
#define BEGIN_P7_ARMOUR				"-----BEGIN PKCS7-----\n"
#define END_P7_ARMOUR				"-----END PKCS7-----"
KP_IMPLEMENTATION(size_t,  __remove_PEM_armour)(KP_STRING bPEM, KP_STRING *start, size_t *newlen)
{
	size_t idx = 0, nlen = 0, armourlen, ulSize;
	ulSize = strlen(bPEM);
	while (bPEM[idx] == 0x2D && idx < ulSize) idx++;	/* Remove ---- */
	if (idx == 0)										/* PEM without armour? Just a base64 encoding... */
	{
		*start = bPEM;
		*newlen = ulSize;
		return 0;
	}
	while (bPEM[idx] != 0x2D && idx < ulSize) idx++;	/* Remove BEGIN XXXX */
	while (bPEM[idx] == 0x2D && idx < ulSize) idx++;	/* Remove ---- */
	if (idx == 0 || idx == ulSize) return (size_t) -1;
	*start = &bPEM[idx];
	armourlen = idx;
	while (bPEM[idx] != 0x2D && idx < ulSize)
	{
		idx++;
		nlen++;
	}
	*newlen = nlen;
	return armourlen;
}
KP_IMPLEMENTATION(KP_RV, __encode64)(KP_IN KP_BYTE *buffer, KP_IN size_t size, KP_OUT KP_STRING *szOut)
{
    KP_RV rv;
    size_t ulLen;
	KP_STRING out;
	base64_encodestate state_in;
	ulLen = (((size + ((size % 3) ? (3 - (size % 3)) : 0)) / 3) * 4);
	ulLen += (ulLen / 72) + 2;
    if (KP_SUCCESS(rv = (out = (KP_STRING) malloc(ulLen)) ? KPTA_OK : KPTA_MEMORY_ERROR))
    {
		base64_init_encodestate(&state_in);
		ulLen = base64_encode_block((char*) buffer, size, out, &state_in);
		ulLen += base64_encode_blockend((out + ulLen), &state_in);
		out[ulLen] = 0;
		*szOut = out;
    }
    return rv;
}
KP_IMPLEMENTATION(KP_RV, __decodeb64)(KP_IN KP_STRING szText, KP_OUT KP_BYTE **out, KP_OUT size_t *outSize)
{
	KP_RV rv;
	KP_BYTE *buffer;
	size_t ulLen;
	base64_decodestate state_in;
	if (KP_SUCCESS(rv = (buffer = (KP_BYTE*) malloc(strlen(szText) + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR))
	{
		base64_init_decodestate(&state_in);
		ulLen = base64_decode_block(szText, strlen(szText), (char*)buffer, &state_in);
		*out = buffer;
		*outSize = ulLen;
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __toPEM)(KP_IN KP_BYTE *buffer, KP_IN size_t size, KP_IN KP_STRING szBeginArmour, KP_IN KP_STRING szEndArmour, KP_OUT KP_STRING *szOut)
{
	KP_RV rv;
	unsigned long ulLen;
	char *out;
	base64_encodestate state_in;
	ulLen = (((size + ((size % 3) ? (3 - (size % 3)) : 0)) / 3) * 4);
	ulLen += (ulLen / 72) + 3 + strlen(szBeginArmour) + strlen(szEndArmour) + 2;
	if (KP_SUCCESS(rv = (out = (char*) malloc(ulLen)) ? KPTA_OK : KPTA_MEMORY_ERROR))
	{
		memcpy(out, szBeginArmour, strlen(szBeginArmour));
		base64_init_encodestate(&state_in);
		ulLen = base64_encode_block((char*) buffer, size, (out + strlen(szBeginArmour)), &state_in);
		ulLen += base64_encode_blockend((out + strlen(szBeginArmour) + ulLen), &state_in);
		memcpy((out + strlen(szBeginArmour) + ulLen), szEndArmour, strlen(szEndArmour));
		ulLen += strlen(szBeginArmour) + strlen(szEndArmour);
		out[ulLen] = 0;
		*szOut = out;
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __topkcs10)(KP_IN KP_BYTE *buffer, KP_IN size_t size, KP_OUT KP_STRING *szOut)
{
	return __toPEM(buffer, size, BEGIN_P10_ARMOUR, END_P10_ARMOUR, szOut);
}
KP_IMPLEMENTATION(KP_RV, __fromPEM)(KP_IN KP_STRING szText, KP_OUT KP_BYTE **out, KP_OUT size_t *outSize)
{
	KP_RV rv;
	KP_STRING start;
	KP_BYTE *buffer;
	size_t ulLen, ulNewLen;
	base64_decodestate state_in;
	__remove_PEM_armour(szText, &start, &ulNewLen);
	if (KP_SUCCESS(rv = (buffer = (KP_BYTE*) malloc(ulNewLen)) ? KPTA_OK : KPTA_MEMORY_ERROR))
	{
		base64_init_decodestate(&state_in);
		ulLen = base64_decode_block(start, ulNewLen, (KP_STRING) buffer, &state_in);
		*out = buffer;
		*outSize = ulLen;
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __topkcs7)(KP_IN KP_BYTE *buffer, KP_IN size_t size, KP_OUT KP_STRING *szOut)
{
	return __toPEM(buffer, size, BEGIN_P7_ARMOUR, END_P7_ARMOUR, szOut);
}
KP_IMPLEMENTATION(KP_RV, __tocert)(KP_IN KP_BYTE *buffer, KP_IN size_t size, KP_OUT KP_STRING *szOut)
{
	return __toPEM(buffer, size, BEGIN_CERT_ARMOUR, END_CERT_ARMOUR, szOut);
}

static const KP_BASE64_STR default_converter =
{
	__encode64,
	__decodeb64,
	__topkcs10,
	__fromPEM,
	__topkcs7,
	__fromPEM,
	__tocert,
	__fromPEM
};
KP_NEW(new_base64)(KP_OUT KP_BASE64 *hHandler)
{
	KP_RV rv;
	KP_BASE64 out;
	if (KP_SUCCESS(rv = (out = (KP_BASE64) malloc(sizeof(KP_BASE64_STR))) ? KPTA_OK : KPTA_MEMORY_ERROR))
	{
		memcpy(out, &default_converter, sizeof(KP_BASE64_STR));
		*hHandler = out;
	}
	return rv;
}
KP_DELETE(delete_base64)(KP_INOUT KP_BASE64 hHandler) { if (hHandler) free(hHandler); }