/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Signature shared pobject implementation
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "sign.h"
#include <string.h>


#if (defined(_WIN32))

#include <SDKDDKVer.h>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason, LPVOID lpReserved)
{
    UNREFERENCED_PARAMETER(hModule);
    UNREFERENCED_PARAMETER(lpReserved);
	switch (ul_reason)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

#endif


KP_IMPLEMENTATION(int, __to_bool)(char *szValue)
{
	const char* TEMPLATE[2] = { "false", "true" };
	char *c = szValue;
	int pValue = -1, pos = 1;
	while (*c && *c == ' ') c++;
	if (*c == 't') pValue = 1;
	else if (*c == 'f') pValue = 0;
	while (pValue != -1 && *c)
	{
		c++;
		if (TEMPLATE[pValue][pos])
		{
			if (TEMPLATE[pValue][pos] == *c) pos++;
			else pValue = -1;
		}
		else
		{
			while (*c && *c == ' ') c++;
			if (*c) pValue = -1;
		}
	}
	return pValue;
}

KP_IMPLEMENTATION(KP_RV, __add_pkibr_attributes)(KP_IN KP_JSON hFrom, const kson_node_t *p, KP_INOUT KP_JSON_BUILDER hTo)
{
	KP_RV rv = KPTA_OK;
	const kson_node_t *q;
	if ((q = hFrom->by_key(p, "company_id"))) rv = hTo->put_string(hTo, "company_id", q->v.str);
	if (KP_SUCCESS(rv) && (q = hFrom->by_key(p, "sponsor_id"))) rv = hTo->put_string(hTo, "sponsor_id", q->v.str);
	if (KP_SUCCESS(rv) && (q = hFrom->by_key(p, "subject_id"))) rv = hTo->put_string(hTo, "subject_id", q->v.str);
	return rv;
}

#define NATIVE_ENUM_CERTS_CMD				"a35bc7b0-9fca-4350-8f6f-b915bf23be85"
#define NATIVE_SIGN_CMD						"e14f5034-b3a8-4400-bdae-715c0981df2a"
KP_FUNCTION(KP_RV, KP_execute_command)(KP_IN KP_ENV env, KP_IN KP_JSON input, KP_OUT KP_JSON_BUILDER *output)
{
	KP_RV rv = KPTA_OK, ret;
	KP_PROCESSOR hProcessor = NULL;
	const kson_node_t *q, *p, *r;
	KP_JSON_BUILDER hBuilder;
	KP_JSON hParser;
	long idx = 0;
	KP_STRING szTransaction, szPEM;
	int isBase64, attach;

	env->hLogger->debug(env->hLogger, SIGNATURE_CATEGORY, KPL_OP_INIT, SIGN_CMD_OP, input->raw);
	if
	(
		KP_SUCCESS(ret = env->new_processor(&hProcessor)) &&
		KP_SUCCESS(ret = hProcessor->init(hProcessor, input)) &&
		KP_SUCCESS(ret = (q = hProcessor->command(hProcessor)) ? KPTA_OK : KPTA_INVALID_ARG)
	)
	{
		if (strcmp(q->v.str, NATIVE_ENUM_CERTS_CMD) == 0 && KP_SUCCESS(rv = KPTA_enumerate_certs(env, &hBuilder)))
		{
			if (KP_SUCCESS(ret = env->new_json(&hParser)))
			{
				if
				(
					KP_SUCCESS(ret = hParser->from_template(hParser, (KP_STRING) hBuilder->_buffer)) &&
					KP_SUCCESS(ret = hParser->parse(hParser)) &&
					KP_SUCCESS(ret = (p = (kson_node_t *) hParser->by_path(hParser, 1, "certificates")) ? KPTA_OK : KPTA_INVALID_JSON) &&
					KP_SUCCESS(ret = hProcessor->hOutput->begin_object(hProcessor->hOutput, "payload")) &&
					KP_SUCCESS(ret = hProcessor->hOutput->begin_array(hProcessor->hOutput, "certificates"))
				)
				{
					while (KP_SUCCESS(ret) && (q = (kson_node_t *) hParser->by_index(p, idx++)))
					{
						if
						(
							KP_SUCCESS(ret = hProcessor->hOutput->begin_object(hProcessor->hOutput, NULL)) &&
							KP_SUCCESS(ret = (r = hParser->by_key(q, "subject")) ? KPTA_OK : KPTA_INVALID_JSON) &&
							KP_SUCCESS(ret = hProcessor->hOutput->put_string(hProcessor->hOutput, "subject",r->v.str)) &&
							KP_SUCCESS(ret = (r = hParser->by_key(q, "issuer")) ? KPTA_OK : KPTA_INVALID_JSON) &&
							KP_SUCCESS(ret = hProcessor->hOutput->put_string(hProcessor->hOutput, "issuer",r->v.str)) &&
							KP_SUCCESS(ret = (r = hParser->by_key(q, "serial")) ? KPTA_OK : KPTA_INVALID_JSON) &&
							KP_SUCCESS(ret = hProcessor->hOutput->put_string(hProcessor->hOutput, "serial",r->v.str)) &&
							KP_SUCCESS(ret = __add_pkibr_attributes(hParser, q, hProcessor->hOutput))
						)	ret = hProcessor->hOutput->end_object(hProcessor->hOutput);
					}
					if (KP_SUCCESS(ret) && KP_SUCCESS(ret = hProcessor->hOutput->end_array(hProcessor->hOutput))) ret = hProcessor->hOutput->end_object(hProcessor->hOutput);
				}
				env->delete_json(hParser);
			}
			env->delete_json_builder(hBuilder);
		}
		else if (strcmp(q->v.str, NATIVE_SIGN_CMD) == 0)
		{
			if
			(
				KP_SUCCESS(rv = (p = input->by_path(input, 3, "payload", "payload", "payload")) ? KPTA_OK : KPTA_INVALID_ARG) &&
				KP_SUCCESS(rv = (q = input->by_key(p, "certificate")) ? KPTA_OK : KPTA_INVALID_ARG) &&
				KP_SUCCESS(ret = env->new_json_builder(&hBuilder))
			)
			{
				if
				(
					KP_SUCCESS(ret = hBuilder->begin_write(hBuilder)) &&
					KP_SUCCESS(ret = (r = input->by_key(q, "subject")) ? KPTA_OK : KPTA_INVALID_JSON) &&
					KP_SUCCESS(ret = hBuilder->put_string(hBuilder, "subject", r->v.str)) &&
					KP_SUCCESS(ret = (r = input->by_key(q, "issuer")) ? KPTA_OK : KPTA_INVALID_JSON) &&
					KP_SUCCESS(ret = hBuilder->put_string(hBuilder, "issuer", r->v.str)) &&
					KP_SUCCESS(ret = (r = input->by_key(q, "serial")) ? KPTA_OK : KPTA_INVALID_JSON) &&
					KP_SUCCESS(ret = hBuilder->put_string(hBuilder, "serial", r->v.str))
				)
				{
					hBuilder->end_write(hBuilder);
					if
					(
						KP_SUCCESS(rv = (q = input->by_key(p, "transaction")) ? KPTA_OK : KPTA_INVALID_ARG) &&
						KP_SUCCESS(ret = (szTransaction = (KP_STRING) malloc(strlen(q->v.str) + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR)
					)
					{
						strcpy(szTransaction, q->v.str);
						if
						(
							KP_SUCCESS(rv = (q = input->by_key(p, "isBase64")) ? KPTA_OK : KPTA_INVALID_ARG) &&
							KP_SUCCESS(rv = (isBase64 = __to_bool(q->v.str)) != -1 ? KPTA_OK : KPTA_INVALID_ARG) &&
							KP_SUCCESS(rv = (r = input->by_key(p, "attach")) ? KPTA_OK : KPTA_INVALID_ARG) &&
							KP_SUCCESS(rv = (attach = __to_bool(r->v.str)) != -1 ? KPTA_OK : KPTA_INVALID_ARG) &&
							KP_SUCCESS(rv = KPTA_sign(env,	(KP_STRING) hBuilder->_buffer, szTransaction, isBase64, attach, &szPEM))
						)
						{
							ret = hProcessor->hOutput->put_string(hProcessor->hOutput, "payload", szPEM);
							free(szPEM);
						}
						free(szTransaction);
					}
				}
				env->delete_json_builder(hBuilder);
			}
		}
		else rv = KPTA_INVALID_ARG;
	}
	if (KP_SUCCESS(ret) && KP_SUCCESS(ret = hProcessor->finish(hProcessor, rv)))
	{
		env->hLogger->info(env->hLogger, SIGNATURE_CATEGORY, KPL_OPERATION_SUCCEEDED, SIGN_CMD_OP);
		env->hLogger->debug(env->hLogger, SIGNATURE_CATEGORY, KPL_CMD_EXECUTED, (char*) hProcessor->hOutput->_buffer);
		*output = hProcessor->hOutput;
	}
	else
	{
		if (hProcessor->hOutput) env->delete_json_builder(hProcessor->hOutput);
		env->hLogger->error(env->hLogger, SIGNATURE_CATEGORY, KPL_OPERATION_FAILURE, ret, SIGN_CMD_OP);
	}
	if (hProcessor) env->delete_processor(hProcessor);
	return ret;
}