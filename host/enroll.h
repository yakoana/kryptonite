/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Enroll component implementation
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#ifndef __ENROLL_H__
#define __ENROLL_H__

#include "kryptonite.h"

#if defined(__cplusplus)
KP_EXTERN {
#endif

KP_UTILITY(KP_RV, KPTA_enumerate_slots) /* Enumerate present cryptographic slots */
(
    KP_INOUT KP_ENV,                    /* Environment */
    KP_OUT KP_STRING**,                 /* Output array of NULL terminated strings */
    KP_OUT int*                         /* Count of strings */
);
KP_UTILITY(KP_RV, KPTA_generate_CSR)    /* Generate RSA key pair, create a certificate request and sign it in PKCS #10 */
(
    KP_IN KP_ENV,                       /* Environment */
    KP_IN KP_STRING,                    /* Chosen cryptographic slot that should generate key pair */
    KP_IN KP_STRING,                    /* Certificate common name */
    KP_OUT KP_STRING*                   /* Output NULL terminated string (a PEM encoded PKCS #10) */
);
KP_UTILITY(KP_RV, KPTA_install_certificate)  /* Installs signed certificate in the slot that has generated associated key pair. If possible, it also installs embbeded CA chain */
(
    KP_IN KP_ENV,                       /* Environment */
    KP_IN KP_STRING                     /* PEM encoded PKCS #7 */
);

#if defined(__cplusplus)
}
#endif

#endif /* __ENROLL_H__ */