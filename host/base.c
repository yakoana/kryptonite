/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Native host main program
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "kryptonite.h"
#include <string.h>
#include <stdio.h>
#include <errno.h>

#if (defined(_WIN32))
	#include <fcntl.h>
	#include <io.h>
#endif

#define NATIVE_VERSION_COMPONENT_ID		"e203869a-9a1c-4958-afe1-68c8315bc3fd"
#define NATIVE_GET_CONFIG_CMD			"7b040131-6ba2-42e4-b886-b134d8090ed7"
#define NATIVE_SET_CONFIG_CMD			"dda968c3-8683-4d2e-b944-6cdfeb1f17c3"
#define NATIVE_REQUIRES_UPDATE_CMD		"7093fe99-0f69-46e5-ba87-5418db398ecf"
#define NATIVE_UPDATE_CMD				"0b8578e3-a69a-4d98-a097-5d8edabd5c7f"
#define NATIVE_GETLOGPATH_CMD			"78643b10-4d47-4b34-bfb5-e1a5bbb897db"
#define NATIVE_RELINQUISH_CMD			"0b15b3cf-9e71-4fc0-b7f2-0d9876908b87"

KP_IMPLEMENTATION(KP_RV, __clone)(KP_INOUT KP_JSON_BUILDER self, KP_IN KP_STRING raw)
{
	KP_RV rv = KPTA_OK;
	size_t size = strlen(raw);
	if (self->_i + size + 2 > self->_size)
	{
		self->_size =  self->_i + size + 2;
		rv = (self->_buffer =  (KP_BYTE*) realloc(self->_buffer, self->_size)) ? KPTA_OK : KPTA_MEMORY_ERROR;
	}
	if (KP_SUCCESS(rv))
	{
		memcpy(self->_buffer + self->_i, raw, size);
		self->_i += size;
		self->_buffer[self->_i] = 0x2C;
		self->_i++;
	}
	return rv;
}
#define ROUNDUP(x)	(--(x), (x)|=(x)>>1, (x)|=(x)>>2, (x)|=(x)>>4, (x)|=(x)>>8, (x)|=(x)>>16, ++(x))
KP_IMPLEMENTATION(KP_RV, __read_from_file)(KP_IN KP_STRING szFilename, KP_OUT KP_BYTE **bOut, KP_OUT size_t *size)
{
	KP_RV rv;
	KP_BYTE buf[512], *bData = NULL;
	size_t i, max = 0, len = 0;
	FILE *fp;
	if (KP_SUCCESS(rv = (fp = fopen(szFilename, "rb")) ? KPTA_OK : KP_SET_ERROR(errno, KPTA_IO_FILE_ERROR)))
	{
		while ((i = fread(buf, 1, sizeof(buf), fp)))
		{
			if (len + i + 1 > max)
			{
				max = len + i + 1;
				ROUNDUP(max);
				bData = (KP_BYTE*) realloc(bData, max);
			}
			memcpy(bData + len, buf, i);
			len += i;
		}
		if (feof(fp))
		{
			*size = len;
			*bOut = bData;
		}
		else
		{
			rv = KP_SET_ERROR(ferror(fp), KPTA_IO_FILE_ERROR);
			if (bData) free(bData);
		}
		fclose(fp);
	}
	return rv;
}
KP_IMPLEMENTATION(KP_STRING, __bin2hex)(KP_IN KP_BYTE *bin, KP_IN size_t len)
{
	KP_STRING out;
	size_t i;
	if (!bin || !len) return NULL;

	if ((out = malloc(len * 2 + 1)))
	{
		for (i = 0; i < len; i++)
		{
			out[i * 2    ] = "0123456789ABCDEF"[bin[i] >> 4  ];
			out[i * 2 + 1] = "0123456789ABCDEF"[bin[i] & 0x0F];
		}
		out[len * 2] = '\0';
	}
	return out;
}
KP_IMPLEMENTATION(KP_RV, __hash_of)(KP_IN KP_BYTE *bData, KP_IN size_t ulLen, KP_OUT KP_BYTE **bHash, KP_OUT size_t *hSize)
{
	KP_RV rv;
	NH_HASH_HANDLER hHash;
	size_t size;
	KP_BYTE *hash;
	if (KP_SUCCESS(rv = NH_new_hash(&hHash)))
	{
		if
		(
			KP_SUCCESS(rv = hHash->init(hHash, CKM_SHA256)) &&
			KP_SUCCESS(rv = hHash->update(hHash, bData, ulLen)) &&
			KP_SUCCESS(rv = hHash->finish(hHash, NULL, &size)) &&
			KP_SUCCESS(rv = (hash = (KP_BYTE*) malloc(size)) ? KPTA_OK : KPTA_MEMORY_ERROR)
		)
		{
			if (KP_SUCCESS(rv = hHash->finish(hHash, hash, &size)))
			{
				*bHash = hash;
				*hSize = size;
			}
			else free(hash);
		}
		NH_release_hash(hHash);
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __hash_component)(KP_IN KP_ENV env, KP_IN KP_STRING szID, KP_OUT KP_BYTE **bOut, KP_OUT size_t *size)
{
	KP_RV rv;
	KP_STRING szComponent;
	KP_BYTE *bData;
	size_t len;
	if (KP_SUCCESS(rv = env->get_component_path(env, szID, &szComponent)))
	{
		if (KP_SUCCESS(rv = __read_from_file(szComponent, &bData, &len)))
		{
			rv = __hash_of(bData, len, bOut, size);
			free(bData);
		}
		free(szComponent);
	}
	return rv;
}
KP_IMPLEMENTATION(unsigned char, __tolower)(unsigned char ch)
{
	if (ch >= 'A' && ch <= 'Z') ch = 'a' + (ch - 'A');
	return ch;
}
KP_IMPLEMENTATION(int, __strcasecmp)(const char *s1, const char *s2)
{
	const unsigned char *us1 = (const u_char *)s1, *us2 = (const u_char *)s2;
	while (__tolower(*us1) == __tolower(*us2++)) if (*us1++ == '\0') return (0);
	return (__tolower(*us1) - __tolower(*--us2));
}
KP_IMPLEMENTATION(KP_RV, __requires_update)(KP_IN KP_ENV env, KP_IN KP_STRING szID, KP_IN KP_STRING szHash, KP_OUT int *requires)
{
	KP_RV rv;
	KP_STRING szCalculated;
	KP_BYTE *hash;
	size_t hsize;
	if (KP_SUCCESS(rv = __hash_component(env, szID, &hash, &hsize)))
	{
		if (KP_SUCCESS(rv = (szCalculated = __bin2hex(hash, hsize)) ? KPTA_OK : KPTA_MEMORY_ERROR))
		{
			*requires = !(strlen(szHash) == strlen(szCalculated) && __strcasecmp(szCalculated, szHash) == 0);
			free(szCalculated);
		}
		free(hash);
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __safe_replace)(KP_IN KP_BYTE *bFile, KP_IN size_t len, KP_IN KP_STRING szComponent, KP_INOUT char *szTmp)
{
	KP_RV rv;
	FILE *fp;
	if
	(
		KP_SUCCESS(rv = tmpnam(szTmp) ? KPTA_OK : KPTA_IO_FILE_ERROR) &&
		KP_SUCCESS(rv = rename(szComponent, szTmp) ?  KP_SET_ERROR(errno, KPTA_IO_FILE_ERROR) : KPTA_OK) &&
		KP_SUCCESS(rv = (fp = fopen(szComponent, "wb")) ? KPTA_OK : KP_SET_ERROR(errno, KPTA_IO_FILE_ERROR))
	)
	{
		rv = fwrite(bFile, 1, len, fp) == len ? KPTA_OK : KP_SET_ERROR(ferror(fp), KPTA_IO_FILE_ERROR);
		fclose(fp);
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __update_component)(KP_IN KP_ENV env, KP_IN KP_STRING szID, KP_IN KP_STRING szHash, KP_IN KP_STRING szFile)
{
	KP_RV rv;
	int requires;
	KP_BYTE *bFile;
	size_t len;
	KP_STRING szComponent;
	char szTmp[FILENAME_MAX];
	memset(szTmp, 0, FILENAME_MAX);
	if
	(
		KP_SUCCESS(rv = __requires_update(env, szID, szHash, &requires)) &&
		KP_SUCCESS(rv = requires ? KPTA_OK : KPTA_INVALID_STATE_OBJECT) &&
		KP_SUCCESS(rv = env->hEncoder->decode(szFile, &bFile, &len))
	)
	{
		if (KP_SUCCESS(rv = env->stop_component(env, szID)))
		{
			if (KP_SUCCESS(rv = env->get_component_path(env, szID, &szComponent)))
			{
				if
				(
					KP_SUCCESS(__safe_replace(bFile, len, szComponent, szTmp)) &&
					KP_SUCCESS(rv = __requires_update(env, szID, szHash, &requires)) &&
					KP_SUCCESS(rv = requires ? KPTA_IO_FILE_ERROR : KPTA_OK)
				)	rv = remove(szTmp) ? KP_SET_ERROR(errno, KPTA_IO_FILE_ERROR) : KPTA_OK;
				else
				{
					remove(szComponent);
					rename(szTmp, szComponent);
				}
				free(szComponent);
			}
			if (KP_SUCCESS(rv)) rv = env->start_component(env, szID);
		}
		free(bFile);
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __execute_version_command)(KP_IN KP_ENV env, KP_IN KP_JSON input, KP_OUT KP_JSON_BUILDER *output)
{
	KP_RV rv = KPTA_OK, ret;
	KP_PROCESSOR hProcessor = NULL;
	const kson_node_t *q, *p, *r;
	KP_JSON_BUILDER hBuilder = NULL;
	KP_STRING szPath = NULL;
	int requires;

	env->hLogger->debug(env->hLogger, CONFIGURATION_CATEGORY, KPL_OP_INIT, CM_CMD_OP, input->raw);
	if
	(
		KP_SUCCESS(ret = env->new_processor(&hProcessor)) &&
		KP_SUCCESS(ret = hProcessor->init(hProcessor, input)) &&
		KP_SUCCESS(ret = (q = hProcessor->command(hProcessor)) ? KPTA_OK : KPTA_INVALID_ARG)
	)
	{
		if (strcmp(q->v.str, NATIVE_GET_CONFIG_CMD) == 0 && KP_SUCCESS(ret = hProcessor->hOutput->begin_object(hProcessor->hOutput, "payload")))
		{
			hProcessor->hOutput->_i--;
			rv = __clone(hProcessor->hOutput, env->hConfig->hJson->raw);
		}

		else if (strcmp(q->v.str, NATIVE_SET_CONFIG_CMD) == 0  && KP_SUCCESS(rv = (p = input->by_path(input, 3, "payload", "payload", "payload")) ? KPTA_OK : KPTA_INVALID_ARG))
		{
			if (KP_SUCCESS(rv = env->new_json_builder(&hBuilder)))
			{
				if
				(
					KP_SUCCESS(rv = hBuilder->embed(hBuilder, p, FALSE)) &&
					KP_SUCCESS(rv = env->hConfig->replace(env->hConfig, (KP_STRING) hBuilder->_buffer)) &&
					KP_SUCCESS(rv = env->hConfig->store(env->hConfig))
				)	env->hLogger->debug(env->hLogger, CONFIGURATION_CATEGORY, KPL_CMD_EXECUTED, (char*) hBuilder->_buffer);
				else env->hLogger->error(env->hLogger, CONFIGURATION_CATEGORY, KPL_REQUEST_FAILED, (char*) input->raw, rv);
				env->delete_json_builder(hBuilder);
			}
		}

		else if (strcmp(q->v.str, NATIVE_REQUIRES_UPDATE_CMD) == 0)	
		{
			if
			(
				KP_SUCCESS(rv = (p = input->by_path(input, 4, "payload", "payload", "payload", "component")) ? KPTA_OK : KPTA_INVALID_ARG) &&
				KP_SUCCESS(rv = (q = input->by_path(input, 4, "payload", "payload", "payload", "hash")) ? KPTA_OK : KPTA_INVALID_ARG) &&
				KP_SUCCESS(rv = __requires_update(env, p->v.str, q->v.str, &requires))
			)	ret = hProcessor->hOutput->put_boolean(hProcessor->hOutput, "payload", requires);
		}

		else if (strcmp(q->v.str, NATIVE_UPDATE_CMD) == 0)
		{
			if
			(
				KP_SUCCESS(rv = (p = input->by_path(input, 4, "payload", "payload", "payload", "component")) ? KPTA_OK : KPTA_INVALID_ARG) &&
				KP_SUCCESS(rv = (q = input->by_path(input, 4, "payload", "payload", "payload", "hash")) ? KPTA_OK : KPTA_INVALID_ARG) &&
				KP_SUCCESS(rv = (r = input->by_path(input, 4, "payload", "payload", "payload", "file")) ? KPTA_OK : KPTA_INVALID_ARG)
			)	rv = __update_component(env, p->v.str, q->v.str, r->v.str);
		}

		else if (strcmp(q->v.str, NATIVE_GETLOGPATH_CMD) == 0)
		{
			if (KP_SUCCESS(ret = env->hLogger->file_path(env->hLogger, &szPath)))
			{
				ret = hProcessor->hOutput->put_string(hProcessor->hOutput, "payload", szPath);
				free(szPath);
			}
		}
		else if (strcmp(q->v.str, NATIVE_RELINQUISH_CMD) == 0); /* Command for MS Edge rotation log. Ignored */

		else rv = KPTA_INVALID_ARG;
	}
	if (KP_SUCCESS(ret) && KP_SUCCESS(ret = hProcessor->finish(hProcessor, rv)))
	{
		env->hLogger->info(env->hLogger, CONFIGURATION_CATEGORY, KPL_OPERATION_SUCCEEDED, CM_CMD_OP);
		env->hLogger->debug(env->hLogger, CONFIGURATION_CATEGORY, KPL_CMD_EXECUTED, (char*) hProcessor->hOutput->_buffer);
		*output = hProcessor->hOutput;
	}
	else
	{
		if (hProcessor->hOutput) env->delete_json_builder(hProcessor->hOutput);
		env->hLogger->error(env->hLogger, CONFIGURATION_CATEGORY, KPL_OPERATION_FAILURE, ret, CM_CMD_OP);
	}
	if (hProcessor) env->delete_processor(hProcessor);
	return ret;
}


KP_IMPLEMENTATION(KP_RV, __minced_meat)(KP_IN KP_ENV env, KP_IN KP_STRING szMessage, KP_OUT int *iParts, KP_OUT KP_STRING **szParts)
{

	KP_RV rv;
	int part = 1, parts;
	size_t msize, remains, len, maxSize = 0;
	KP_STRING *szOut = NULL, szBuffer = NULL;

	if
	(
		KP_SUCCESS(rv = env->hConfig->get_responsesize(env->hConfig, (KP_ULONG*) &maxSize)) &&
		KP_SUCCESS(rv = maxSize > 0 ? KPTA_OK : KPTA_INVALID_PROPERTY) &&
		strlen(szMessage) > maxSize &&
		KP_SUCCESS(rv = env->hEncoder->encode((KP_BYTE*) szMessage, strlen(szMessage), &szBuffer))
	)
	{
		msize = strlen(szBuffer);
		parts = (msize / maxSize) + (msize % maxSize > 0 ? 1 : 0);
	}
	else
	{
		msize = strlen(szMessage);
		parts = 1;
		szBuffer = szMessage;
	}
	remains = msize;
	if (KP_SUCCESS(rv) && KP_SUCCESS(rv = (szOut = (KP_STRING*) malloc(parts * sizeof(KP_STRING))) ? KPTA_OK : KPTA_MEMORY_ERROR)) memset(szOut, 0, parts * sizeof(KP_STRING));
	while (KP_SUCCESS(rv) && part <= parts)
	{
		len = remains > maxSize ? maxSize : remains;
		if (KP_SUCCESS(rv = (szOut[part - 1] = (KP_STRING) malloc(len + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR))
		{
			memcpy(szOut[part - 1], szBuffer + msize - remains, len);
			szOut[part - 1][len] = 0;
			part++;
			remains -= len;
		}
	}
	if (parts > 1 && szBuffer) free(szBuffer);
	if (KP_SUCCESS(rv))
	{
		*szParts = szOut;
		*iParts = parts;
	}
	else
	{
		if (szOut)
		{
			for (part = 1; part <= parts; part++) if (szOut[part - 1]) free(szOut[part - 1]);
			free(szOut);
		}
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __send_response)(KP_IN KP_ENV env, KP_IN KP_STRING szNonce, KP_IN KP_JSON_BUILDER hResponse)
{
	KP_RV rv;
	KP_JSON hParser;
	KP_JSON_BUILDER hEnvelope;
	int part = 1, parts = 0;
	char sznPart[5], sznParts[5], **szParts = NULL;

	rv = __minced_meat(env, (KP_STRING) hResponse->_buffer, &parts, &szParts);
	sprintf(sznParts, "%d", parts);
	while (KP_SUCCESS(rv) && part <= parts)
	{
		sprintf(sznPart, "%d", part);
		if (KP_SUCCESS(rv = env->new_json_builder(&hEnvelope)))
		{
			if
			(
				KP_SUCCESS(rv = hEnvelope->begin_write(hEnvelope)) &&
				KP_SUCCESS(rv = hEnvelope->put_number(hEnvelope, "nonce", szNonce)) &&
				KP_SUCCESS(rv = hEnvelope->put_number(hEnvelope, "part", sznPart)) &&
				KP_SUCCESS(rv = hEnvelope->put_number(hEnvelope, "parts", sznParts))
			)
			{
				if (parts > 1) rv = hEnvelope->put_string(hEnvelope, "payload", szParts[part - 1]);
				else
				{
					rv = hEnvelope->begin_object(hEnvelope, "payload");
					hEnvelope->_i--;
					if (KP_SUCCESS(rv = env->new_json(&hParser)))
					{
						if
						(
							KP_SUCCESS(rv = hParser->from_template(hParser, szParts[part - 1])) &&
							KP_SUCCESS(rv = hParser->parse(hParser))
						)	rv = hEnvelope->embed(hEnvelope, hParser->hParser->root, TRUE);
						env->delete_json(hParser);
					}
				}
				if (KP_SUCCESS(rv))
				{
					hEnvelope->end_write(hEnvelope);
					rv = hEnvelope->unlade_stdout(hEnvelope);
				}
			}
			env->delete_json_builder(hEnvelope);
		}
		part++;
	}	
	if (szParts)
	{
		for (part = 1; part <= parts; part++) if (szParts[part - 1]) free(szParts[part - 1]);
		free(szParts);
	}
	return rv;
}


int main(int argc, char* argv[])
{
	KP_RV rv, ret;
	KP_ENV env;
	int i, bExit = FALSE;
	KP_JSON hIn = NULL;
	KP_JSON_BUILDER hOut = NULL;
	char nonce[32], *defaultNonce = "0", reason[32], result[32];
	const kson_node_t *p, *q;
	KP_COMPONENT hComponent;


	if (KP_FAIL(rv = new_environment(&env)))
	{
		fprintf(stderr,"Kryptonite init failure: %llu\n", rv);
		return -1;
	}
#if (defined(_DEBUG_))
	env->hLogger->set_level(env->hLogger, KP_LOG_DEBUG);
#endif

#if (defined(_WIN32))
	if (_setmode(_fileno(stdin), _O_BINARY)  == -1 || _setmode(_fileno(stdout), _O_BINARY) == -1)
	{
		env->hLogger->error(env->hLogger, ENVIRONMENT_CATEGORY, KPL_STDIO_SETMODE_ERROR, errno);
		delete_environment(env);
		return -1;
	}
#endif
	if (setvbuf(stdin, NULL, _IONBF, 0) || setvbuf(stdout, NULL, _IONBF, 0))
	{
		env->hLogger->error(env->hLogger, ENVIRONMENT_CATEGORY, KPL_STDIO_SETMODE_ERROR, errno);
		delete_environment(env);
		return -1;
	}
	for (i = 0; i < argc; i++) env->hLogger->debug(env->hLogger, ENVIRONMENT_CATEGORY, KPL_HOST_INIT, argv[i]);
	env->hLogger->info(env->hLogger, ENVIRONMENT_CATEGORY, KPL_INIT_FINISH);

	while (!bExit && KP_SUCCESS(rv) && KP_SUCCESS(rv = env->new_json(&hIn)) && KP_SUCCESS(rv = hIn->from_stdin(hIn)))
	{
		strcpy(nonce, defaultNonce);
		env->hLogger->debug(env->hLogger, ENVIRONMENT_CATEGORY, KPL_MESSAGE_RECEIVED, (char*) hIn->raw);
		if
		(
			KP_SUCCESS(ret = hIn->parse(hIn)) &&
			KP_SUCCESS(ret = (p = hIn->by_path(hIn, 2, "payload", "commandID")) ? KPTA_OK : KPTA_INVALID_ARG) &&
			KP_SUCCESS(ret = (q = hIn->by_path(hIn, 1, "nonce")) ? KPTA_OK : KPTA_INVALID_ARG)
		)
		{
			strcpy(nonce, q->v.str);
			if (!(bExit = strcmp(p->v.str, "exit") == 0))
			{
				if (strcmp(p->v.str, NATIVE_VERSION_COMPONENT_ID) == 0) rv = __execute_version_command(env, hIn, &hOut);
				else if (KP_SUCCESS(ret = (hComponent = env->get_component(env, p->v.str)) ? KPTA_OK : KPTA_INVALID_ARG)) rv = hComponent->hProc(env, hIn, &hOut);
				else
				{
					env->hLogger->error(env->hLogger, ENVIRONMENT_CATEGORY, KPL_GET_COMP_FAILURE, ret);
					sprintf(reason, "%llu", ret);
					ret = KPTA_UNKNOWN;
				}
			}
		}
		else
		{
			sprintf(reason, "%llu", ret);
			ret = KPTA_FAILED;
		}
		env->delete_json(hIn);
		hIn = NULL;
		if (!bExit && KP_SUCCESS(rv))
		{
			if
			(
				KP_FAIL(ret) &&
				KP_SUCCESS(rv = env->new_json_builder(&hOut)) &&
				KP_SUCCESS(rv = hOut->begin_write(hOut)) &&
				sprintf(result, "%llu", ret) > 0 &&
				KP_SUCCESS(rv = hOut->put_number(hOut, "result", result)) &&
				KP_SUCCESS(rv = hOut->put_number(hOut, "reason", reason))
			)	hOut->end_write(hOut);
		}
		if (!bExit && hOut && KP_SUCCESS(rv)) if (KP_SUCCESS(rv = __send_response(env, nonce, hOut))) env->hLogger->debug(env->hLogger, ENVIRONMENT_CATEGORY, KPL_RESPONSE_EXECUTED, (char*) hOut->_buffer);
		if (hOut) env->delete_json_builder(hOut);
		hOut = NULL;
	}
	if (KP_FAIL(rv)) env->hLogger->error(env->hLogger, ENVIRONMENT_CATEGORY, KPL_APP_FINISHED_BY_ERROR, rv);
	else env->hLogger->info(env->hLogger, ENVIRONMENT_CATEGORY, KPL_HOST_END);
	if (hIn) env->delete_json(hIn);
	delete_environment(env);
	return (int) rv;
}
