/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Signature verification component
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#ifndef __VERIFY_H__
#define __VERIFY_H__

#include "kryptonite.h"


#if defined(__cplusplus)
KP_EXTERN {
#endif

/**
 * KPTA_signer response JSON:
 * {
 *   "issuer": "C=BR, O=PKI Brazil, OU=PKI Ruler for All Cats, CN=Common Name for All Cats End User CA",
 *   "serial": "04",
 *   "subjectKeyIdentifier": "408bd51fdbbf65d1872315be964bd2eed98a70967297d9f6e80e02bf494e476a"
 * }
 */
KP_UTILITY(KP_RV, KPTA_signer_id)	/* Gets SignerIdentifier of CMS SignedData document */
(
	KP_IN KP_ENV,					/* env: Environment */
	KP_IN KP_STRING,				/* szCMS: PEM encoding of CMS SignedData document */
	KP_OUT KP_JSON_BUILDER*			/* hOut: JSON response */
);
KP_UTILITY(KP_RV, KPTA_signer_cert)	/* Gets signer certificate of CMS SignedData document, if present */
(
	KP_IN KP_ENV,					/* env: Environment */
	KP_IN KP_STRING,				/* szCMS: PEM encoding of CMS SignedData document */
	KP_OUT KP_STRING*				/* szCert: PEM encoding of certificate or NULL */
);
KP_UTILITY(KP_RV, KPTA_content)		/* Get encapsulated content info, if any */
(
	KP_IN KP_ENV,					/* env: Environment */
	KP_IN KP_STRING,				/* szCMS: PEM encoding of CMS SignedData documento */
	KP_OUT KP_STRING*				/* szContent: Base64 encoding of content or NULL */
);
KP_UTILITY(KP_RV, KPTA_trusted)		/* Checks if specified certificate is trusted */
(
	KP_IN KP_ENV,					/* env: Environment */
	KP_IN KP_STRING					/* szCert: PEM encoding of certificate */
);
KP_UTILITY(KP_RV, KPTA_verify)		/* Checks CMS SignedData document signature, using given certificate */
(
	KP_IN KP_ENV,					/* env: Environment */
	KP_IN KP_STRING,				/* szCMS: PEM encoding of CMS SignedData document */
	KP_IN KP_STRING,				/* szContent: signed content */
	KP_IN int,						/* Base64 encoding of szContent indicator */
	KP_IN KP_STRING					/* szCert: PEM encoding of signer certificate */
);
KP_UTILITY(KP_RV, KPTA_verify_all)	/* Checks attached CMS SignedData document signature using embbeded certificate and verify its trustworthy */
(
	KP_IN KP_ENV,					/* env: Environment */
	KP_IN KP_STRING					/* szCMS: PEM encoding of CMS SignedData document */
);



#if defined(__cplusplus)
}
#endif

#endif /* __VERIFY_H__ */