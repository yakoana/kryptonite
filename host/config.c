/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Configuration implementation
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "kryptonite.h"
#include <string.h>
#include <stdlib.h>


#define CONFIG_JSON				(PATH_SEPARATOR"config.json")

KP_IMPLEMENTATION(KP_RV, __load)(KP_INOUT KP_CONFIG_STR *self, KP_IN KP_STRING szLocation)
{
	KP_RV rv;
	char path[MAX_PATH];
	if
	(
		KP_SUCCESS(rv = szLocation ? KPTA_OK : KPTA_INVALID_ARG) &&
		KP_SUCCESS(rv = !self->hJson->raw && !self->szConfigFile ? KPTA_OK : KPTA_INVALID_STATE_OBJECT) &&
		strcpy(path, szLocation) &&
		strcat(path, CONFIG_JSON) &&
		KP_SUCCESS(rv = self->hJson->from_file(self->hJson, path)) &&
		KP_SUCCESS(rv = (self->szConfigFile = (KP_STRING) malloc(strlen(path) + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR) &&
		strcpy(self->szConfigFile, path)
	)	rv = self->hJson->parse(self->hJson);
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __store)(KP_IN KP_CONFIG_STR *self)
{
	KP_RV rv;
	if
	(
		KP_SUCCESS(rv = self->hJson && self->szConfigFile ? KPTA_OK : KPTA_INVALID_STATE_OBJECT)
	)	rv = self->hJson->unlade_disk(self->hJson, self->szConfigFile);
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __replace)(KP_INOUT KP_CONFIG_STR *self, KP_IN KP_STRING raw)
{
	KP_RV rv;
	delete_json(self->hJson);
	if
	(
		KP_SUCCESS(rv = new_json(&self->hJson)) &&
		KP_SUCCESS(rv = self->hJson->from_template(self->hJson, raw))
	)	rv = self->hJson->parse(self->hJson);
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __response_size)(KP_IN KP_CONFIG_STR *self, KP_OUT KP_ULONG *ulValue)
{
	KP_RV rv;
	const kson_node_t *p;
	if
	(
		KP_SUCCESS(rv = self->hJson->raw ? KPTA_OK : KPTA_INVALID_STATE_OBJECT) &&
		KP_SUCCESS(rv = (p = self->hJson->by_path(self->hJson, 1, "maxResponseSize")) ? KPTA_OK : KPTA_PROPERTY_NOT_FOUND)
	)	*ulValue = atol(p->v.str);
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __component)(KP_IN KP_CONFIG_STR *self, KP_IN KP_STRING szID, KP_OUT KP_STRING *szFilename)
{
	KP_RV rv;
	const kson_node_t *p;
	KP_STRING out;
	if
	(
		KP_SUCCESS(rv = self->hJson->raw ? KPTA_OK : KPTA_INVALID_STATE_OBJECT) &&
		KP_SUCCESS(rv = (p = self->hJson->by_path(self->hJson, 2, "components", szID)) ? KPTA_OK : KPTA_PROPERTY_NOT_FOUND) &&
		KP_SUCCESS(rv = (out = (KP_STRING) malloc(strlen(p->v.str) + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR)
	)
	{
		strcpy(out, p->v.str);
		*szFilename = out;
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __key_size)(KP_IN KP_CONFIG_STR *self, KP_OUT KP_ULONG *ulValue)
{
	KP_RV rv;
	const kson_node_t *p;
	if
	(
		KP_SUCCESS(rv = self->hJson->raw ? KPTA_OK : KPTA_INVALID_STATE_OBJECT) &&
		KP_SUCCESS(rv = (p = self->hJson->by_path(self->hJson, 3, "enroll", "issueProfile", "keySize")) ? KPTA_OK : KPTA_PROPERTY_NOT_FOUND)
	)	*ulValue = atol(p->v.str);
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __sigalgOID)(KP_IN KP_CONFIG_STR *self, KP_OUT KP_STRING *szOID)
{
	KP_RV rv;
	const kson_node_t *p;
	KP_STRING out;
	if
	(
		KP_SUCCESS(rv = self->hJson->raw ? KPTA_OK : KPTA_INVALID_STATE_OBJECT) &&
		KP_SUCCESS(rv = (p = self->hJson->by_path(self->hJson, 3, "enroll", "issueProfile", "signAlgOID")) ? KPTA_OK : KPTA_PROPERTY_NOT_FOUND) &&
		KP_SUCCESS(rv = (out = (KP_STRING) malloc(strlen(p->v.str) + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR)
	)
	{
		strcpy(out, p->v.str);
		*szOID = out;
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __sign_alg)(KP_IN KP_CONFIG_STR *self, KP_OUT KP_ULONG *ulValue)
{
	KP_RV rv;
	const kson_node_t *p;
	if
	(
		KP_SUCCESS(rv = self->hJson->raw ? KPTA_OK : KPTA_INVALID_STATE_OBJECT) &&
		KP_SUCCESS(rv = (p = self->hJson->by_path(self->hJson, 3, "sign", "signProfile", "signAlg")) ? KPTA_OK : KPTA_PROPERTY_NOT_FOUND)
	)	*ulValue = atol(p->v.str);
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __sign_policy)(KP_IN KP_CONFIG_STR *self, KP_OUT KP_STRING *szPolicy)
{
	KP_RV rv;
	const kson_node_t *p;
	KP_STRING out;
	if
	(
		KP_SUCCESS(rv = self->hJson->raw ? KPTA_OK : KPTA_INVALID_STATE_OBJECT) &&
		KP_SUCCESS(rv = (p = self->hJson->by_path(self->hJson, 3, "sign", "signProfile", "policy")) ? KPTA_OK : KPTA_PROPERTY_NOT_FOUND) &&
		KP_SUCCESS(rv = (out = (KP_STRING) malloc(strlen(p->v.str) + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR)
	)
	{
		strcpy(out, p->v.str);
		*szPolicy = out;
	}
	return rv;
}

const static KP_CONFIG_STR default_config =
{
	NULL,
	NULL,

	__load,
	__store,
	__replace,
	__response_size,
	__component,
	__key_size,
	__sigalgOID,
	__sign_alg,
	__sign_policy
};

KP_NEW(new_config)(KP_OUT KP_CONFIG *self)
{
	KP_RV rv;
	KP_CONFIG out;
	KP_JSON hJson;
	if (KP_SUCCESS(rv = (out = (KP_CONFIG) malloc(sizeof(KP_CONFIG_STR))) ? KPTA_OK : KPTA_MEMORY_ERROR))
	{
		memcpy(out, &default_config, sizeof(KP_CONFIG_STR));
		if (KP_SUCCESS(rv = new_json(&hJson)))
		{
			out->hJson = hJson;
			*self = out;
		} else free(out);
	}
	return rv;
}
KP_DELETE(delete_config)(KP_INOUT KP_CONFIG	self)
{
	if (self)
	{
		if (self->szConfigFile) free(self->szConfigFile);
		if (self->hJson) delete_json(self->hJson);
		free(self);
	}
}