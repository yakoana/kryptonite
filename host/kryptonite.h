/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Application definitions
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#ifndef __KRYPTONITE_H__
#define __KRYPTONITE_H__

#include <stdint.h>
#include <stdio.h>
#include "cms.h"								/* https://bitbucket.org/yakoana/nharu.git */
#include "klib/kson.h"							/* https://github.com/attractivechaos/klib.git */
#include "klib/khash.h"							/* Ibidem */
#include "libjson/json.h"						/* http://projects.snarc.org/libjson/ */

#if (defined(_WIN32))							/** Windows SDK 32 bits compilation required */

	#include <SDKDDKVer.h>
	#include <Windows.h>
	#include <winbase.h>
	#include <sal.h>

	#define KP_IN								_In_ const
	#define KP_OUT								_Out_
	#define KP_INOUT							_Inout_
	#define KP_CONSTRUCTOR	
	#define KP_DESTRUCTOR
	#define KP_UNUSED
	#define KP_EXPORT							__declspec(dllexport)
	#define KP_HIDDEN
	#define KP_EXTERNAL							extern
	#define KP_CALL_SPEC						__cdecl
	#define KP_INLINE							__inline
	#define KP_NOP								__asm nop

	#define PATH_SEPARATOR						"\\"
	#define COMPONENT_LOCATION					"%s\\%s.dll"

#else											/** Unix GCC 32 bits compilation required */

	#define KP_IN								const
	#define KP_OUT
	#define KP_INOUT
	#define KP_CONSTRUCTOR						__attribute__((constructor))
	#define KP_DESTRUCTOR						__attribute__((destructor))
	#define KP_UNUSED							__attribute__((unused))
	#define KP_EXPORT
	#define KP_HIDDEN							__attribute__((__visibility__("internal")))
	#define KP_EXTERNAL
	#define KP_CALL_SPEC						__attribute__((cdecl))
	#define KP_INLINE							__inline__
	#define KP_NOP								__asm__("nop")

	#define PATH_SEPARATOR						"/"
	#define COMPONENT_LOCATION					"%s/lib%s.so"

#endif
#if defined(__cplusplus)
	#define KP_EXTERN							extern "C"
#else
	#define KP_EXTERN							extern
#endif

#ifdef NULL
	#undef NULL
#endif
#if defined(__cplusplus)
	#if (__cplusplus >= 201100L)
		#define NULL							null_ptr
	#else
		#define NULL							((void *) 0)
	#endif
#else
	#define NULL								0
#endif
#ifndef TRUE
	#define TRUE								0xFF
#endif
#ifndef FALSE
	#define FALSE								0x00
#endif

typedef int64_t			 						KP_RV;
typedef unsigned char							KP_BYTE;
typedef int32_t									KP_INT32;
typedef NH_BLOB									KP_BLOB;
typedef char*									KP_STRING;

#define KP_FUNCTION(type, name)					KP_EXTERNAL type KP_EXPORT KP_CALL_SPEC name	/* Exportend functions */
#define KP_UTILITY(type, name)					KP_HIDDEN type KP_CALL_SPEC name				/* Internal functions */
#define KP_METHOD(type, name)					type (KP_CALL_SPEC *name)						/* Struct member fnctions */
#define KP_CALLBACK(type, name)					KP_METHOD(type, name)							/* Callback functions */
#define KP_NEW(name)							KP_UTILITY(KP_RV, name)							/* New object */
#define KP_DELETE(name)							KP_UTILITY(void, name)							/* Delete object */
#define KP_IMPLEMENTATION(type, name)			static KP_UTILITY(type, name)					/* Method implementation */


/**
 * Operation results (exposed to Javascript)
 */
#define KPTA_OK									(KP_RV)0				/* Successful operation */
#define KPTA_FAILED								(KP_RV)1				/* Operation failure */
#define KPTA_UNKNOWN							(KP_RV)2				/* Unsupported operation */

/**
 * Operation reasons (exposed to Javascript)
 */
#define KPTA_NONE								(KP_RV)0				/* Success */
#define KPTA_INVALID_ARG						(KPTA_OK + 1)			/* Invalid arguments */
#define KPTA_INVALID_RESPONSE					(KPTA_OK + 2)			/* Invalid response from extensions */
#define KPTA_EXIT_SIGNAL						(KPTA_OK + 3)			/* Exit signal received from browser */

#define KPTA_BASE_ERROR							KPTA_EXIT_SIGNAL
#define KPTA_MEMORY_ERROR						(KPTA_BASE_ERROR + 1)	/* Out of memory error */
#define KPTA_MSG_SIZE_ERROR						(KPTA_BASE_ERROR + 2)	/* JSON message size error */
#define KPTA_IO_FILE_ERROR						(KPTA_BASE_ERROR + 3)	/* JSON file I/O error */
#define KPTA_STDIO_ERROR						(KPTA_BASE_ERROR + 4)	/* JSON document I/O error */
#define KPTA_INVALID_JSON						(KPTA_BASE_ERROR + 5)	/* JSON file parsing error */
#define KPTA_TEMPLATE_ERROR						(KPTA_BASE_ERROR + 6)	/* JSON template error */
#define KPTA_INVALID_STATE_OBJECT				(KPTA_BASE_ERROR + 7)	/* Cannot execute method due to object invalid state */
#define KPTA_COMPONENT_NOT_FOUND				(KPTA_BASE_ERROR + 8)	/* Required component not found in configuration */
#define KPTA_BUFFER_TOO_SMALL					(KPTA_BASE_ERROR + 9)	/* Output buffer too small */
#define KPTA_PROPERTY_NOT_FOUND					(KPTA_BASE_ERROR + 10)	/* Requested property not found in configuration */
#define KPTA_INVALID_PROPERTY					(KPTA_BASE_ERROR + 11)	/* Requested property has another type */
#define KPTA_INVALID_LOGLEVEL					(KPTA_BASE_ERROR + 12)	/* JS log level value invalid */
#define KPTA_LOGFILE_ERROR						(KPTA_BASE_ERROR + 13)	/* Windows log file opening error */
#define KPTA_JSON_ERROR							(KPTA_BASE_ERROR + 14)	/* Invalid JSON document */
#define KPTA_COMPONENT_MAP_ERROR				(KPTA_BASE_ERROR + 15)	/* Components hash map access unexpected error */
#define KPTA_PUT_MAP_ERROR						(KPTA_BASE_ERROR + 16)	/* Hash map put error */
#define KPTA_SLOT_NOT_FOUND_ERROR				(KPTA_BASE_ERROR + 17)	/* Could not find slot name */
#define KPTA_CERTIFICATES_NOT_FOUND				(KPTA_BASE_ERROR + 18)	/* Could not find certificates in PKCS #7 */
#define KPTA_CERT_DN_NOT_FOUND					(KPTA_BASE_ERROR + 19)	/* Selected certificate DN not found in repository */
#define KPTA_UNSUPPORTED_MECHANISM				(KPTA_BASE_ERROR + 20)	/* Specified cryptographic mechanism is not supported */
#define KPTA_JSON_PRINTER_ERROR					(KPTA_BASE_ERROR + 21)	/* JSON builder facility error indicator (use G_SYSERROR for details) */
#define KPTA_JSON_PRINTER_END					(KPTA_BASE_ERROR + 22)	/* JSON builder facility has ended writing */

#define KPTA_WINDOWS_API_ERROR					(KPTA_BASE_ERROR + 64)	/* Windows API error (use G_SYSERROR for details) */
#define KPTA_WINREGISTRY_ERROR					(KPTA_BASE_ERROR + 65)	/* Windows Registry access error */
#define KPTA_LOADDLL_ERROR						(KPTA_BASE_ERROR + 66)	/* Load Windows DLL error */
#define KPTA_CRYPTO_ENROLL_ERROR				(KPTA_BASE_ERROR + 67)	/* Windows Cryptographic API error under enrollment context */
#define KPTA_CRYPTO_ENUM_CERTS_ERROR			(KPTA_BASE_ERROR + 68)	/* Windows Cryptographic API error under enumerate certificates context */
#define KPTA_CRYPTO_SIGN_ERROR					(KPTA_BASE_ERROR + 69)	/* Windows Cryptographic API error under signing context */
#define KPTA_CRYPTO_VERIFY_ERROR				(KPTA_BASE_ERROR + 70)	/* Windows Cryptographic API error under verification context */
#define KPTA_TRY_NEXT_STORE						(KPTA_BASE_ERROR + 71)	/* Could not validate certificate in this store */

#define KPTA_LINUX_API_ERROR					(KPTA_BASE_ERROR + 128)	/* Linux system libraries error (use G_SYSERROR for details) */
#define KPTA_LOADSO_ERROR						(KPTA_BASE_ERROR + 129)	/* Load Linux shared object error */

#define KPTA_NHARU_ERROR						(KPTA_BASE_ERROR + 192)	/* Nharu Library error indicator */
#define KPTA_PARSE_CERT_ERROR					(KPTA_BASE_ERROR + 193)	/* Certificate parser error */
#define KPTA_ENCODE_CMS_ERROR					(KPTA_BASE_ERROR + 194)	/* CMS encoder error */
#define KPTA_CMS_SD_NOECONTENT_ERROR			(KPTA_BASE_ERROR + 195)	/* Unattached CMS error */

#define KP_SUCCESS(rv)							((rv) == KPTA_OK)
#define KP_FAIL(rv)								((rv))
#define SET_SYSERROR(err)						(err << 8)
#define KP_SET_ERROR(n, rv)						(SET_SYSERROR(n) | rv)


/**
 * Logger facility
 */
#if (defined(_WIN32))

	#define EVENT_LOG_SOURCE					"kryptonite-%i.log"
	#define LOG_SOURCE_MAX_SIZE					0X100000
	#define MAX__TIME64_T						0x793406fffi64
	
	#define KP_LOG_DEBUG						EVENTLOG_SUCCESS
	#define KP_LOG_INFO							EVENTLOG_INFORMATION_TYPE
	#define KP_LOG_WARN							EVENTLOG_WARNING_TYPE
	#define KP_LOG_ERROR						EVENTLOG_ERROR_TYPE

	typedef DWORD								KP_ULONG;
#else

	#include <syslog.h>
	#define EVENT_LOG_SOURCE					"Kryptonite"
	#define KP_LOG_DEBUG						LOG_DEBUG
	#define KP_LOG_INFO							LOG_NOTICE
	#define KP_LOG_WARN							LOG_WARNING
	#define KP_LOG_ERROR						LOG_ERR

	typedef unsigned long						KP_ULONG;
#endif

#define KP_LOG_DEBUG_LBL						"<debug>"
#define KP_LOG_INFO_LBL							"<info> "
#define KP_LOG_WARN_LBL							"<warn> "
#define KP_LOG_ERROR_LBL						"<error>"

/* Log categorias */
#define ENVIRONMENT_CATEGORY					0
#define CONFIGURATION_CATEGORY					1
#define ENROLLMENT_CATEGORY						2
#define SIGNATURE_CATEGORY						3
#define VALIDATION_CATEGORY						4
#define DEVELOPMENT_CATEGORY					5
#define CATEGORY_WATERLEVEL						6

/* Log messages */
#define KPL_OBJECT_CREATED						0
#define KPL_OBJECT_CREATION_FAILURE				1
#define KPL_OPERATION_FAILURE					2
#define KPL_OPERATION_SUCCEEDED					3
#define KPL_STDIO_SETMODE_ERROR					4
#define KPL_OP_VALUE_SUCCEEDED					5
#define KPL_OP_VALUE_FAILURE					6
#define KPL_OP_INIT								7
#define KPL_SLOT_ENUMERATED						8
#define KPL_CSR_PARAMS							9
#define KPL_CACERT_INSTALL_FAILURE				10
#define KPL_CERT_ENUMERATED						11
#define KPL_SIGNER_ID							12
#define KPL_SIGNER_CERT							13
#define KPL_ECONTENT							14
#define KPL_CMD_EXECUTED						15
#define KPL_RESPONSE_EXECUTED					16
#define KPL_REQUEST_SUCCEEDED					17
#define KPL_REQUEST_FAILED						18
#define KPL_HOST_INIT							19
#define KPL_HOST_END							20
#define KPL_INIT_FINISH							21
#define KPL_MESSAGE_RECEIVED					22
#define KPL_APP_FINISHED_BY_ERROR				23
#define KPL_GET_COMP_FAILURE					24
#define MESSAGE_WATERLEVEL						25

/* Messages replacements */
#define INSTALL_DIR_OP							"obter o diretório de instalação"
#define GET_REGVALUE_OP							"obter a chave de registro do produto"
#define CONFIG_JSON_OP							"obter o arquivo JSON de configuração"
#define ENVIRONMENT_OP							"criar o ambiente de execução"
#define GET_COMPONENT_OP						"obter componente instalado"
#define ENUMERATE_SLOTS_OP						"enumerar slots criptográficos"
#define GENERATE_CSR_OP							"gerar requisição de assinatura de certificado"
#define INSTALL_CERTIFICATE_OP					"instalar certificado"
#define ENUMERATE_CERTS_OP						"enumerar certificados de assinatura"
#define SIGN_MESSAGE_OP							"assinar conteúdo"
#define GET_SIGNER_ID_OP						"obter identificação do assinante"
#define GET_SIGNER_CERT_OP						"obter certificado do assinante"
#define TRUSTED_CERT_OP							"verificar a confiabilidade do certificado"
#define VERIFY_SIGNATURE_OP						"verificar a assinatura do documento"
#define VERIFY_ALL_OP							"verificar assinatura e validar atributos"
#define GET_CONTENT_OP							"obter conteúdo assinado"
#define ENROLL_CMD_OP							"executar comando de registro"
#define SIGN_CMD_OP								"executar comando de assinatura"
#define CM_CMD_OP								"executar comando de configuração"


typedef struct KP_LOG_STR						KP_LOG_STR;
typedef KP_METHOD(KP_RV, SET_LEVEL)				/* Sets log level according to JS configuration 	*/
(
	KP_INOUT KP_LOG_STR*,						/* This object */
	KP_IN int									/* Log level, where 3 = KLOG_ERROR, 4 = KLOG_WARN, 5 = KLOG_INFO and 7 = KLOG_DEBUG */
);
typedef KP_METHOD(KP_RV, MANAGE_LOG)			/* Initializes or finalizes log according to platform. Windows: event system; Unix: syslog */
(
	KP_INOUT KP_LOG_STR*						/* This object */
);
typedef KP_METHOD(void, CMD_LOG)				/* Logs in proper level */
(
	KP_IN KP_LOG_STR*,							/* This object */
	KP_IN unsigned int,							/* Event category */
	KP_IN KP_ULONG,								/* Message id */
	...											/* Message replacements */
);
typedef KP_METHOD(KP_RV, LOG_FILEPATH)(KP_IN KP_LOG_STR*, KP_OUT KP_STRING*);
struct KP_LOG_STR
{
	FILE*			stream;						/* Log stream (for Windows only) */
	unsigned int	level;						/* Current log level */

	SET_LEVEL		set_level;					/* (KP_INOUT KP_LOG_STR *self, KP_IN int cfgLevel) */
	MANAGE_LOG		open;						/* (KP_INOUT KP_LOG_STR *self) */
	MANAGE_LOG		close;						/* (KP_INOUT KP_LOG_STR *self) */
	CMD_LOG			debug;						/* (KP_IN KP_LOG_STR *self, KP_IN unsigned int category, KP_IN KPULONG id, ...) */
	CMD_LOG			info;						/* (KP_IN KP_LOG_STR *self, KP_IN unsigned int category, KP_IN KPULONG id, ..) */
	CMD_LOG			warn;						/* (KP_IN KP_LOG_STR *self, KP_IN unsigned int category, KP_IN KPULONG id, ..) */
	CMD_LOG			error;						/* (KP_IN KP_LOG_STR *self, KP_IN unsigned int category, KP_IN KPULONG id, ..) */
	LOG_FILEPATH	file_path;
};
typedef struct KP_LOG_STR*						KP_LOG;


/**
 * JSON documents manager facility
 */
typedef struct KP_JSON_STR						KP_JSON_STR;
typedef KP_METHOD(KP_RV, LOAD_FROM_FILE)		/* Load JSON document from a file */
(
	KP_INOUT KP_JSON_STR*,						/* This object */
	KP_IN KP_STRING								/* Complete path to file */
);
typedef KP_METHOD(KP_RV, LOAD_FROM_STDIN)		/* Load JSON command from stdin */
(
	KP_INOUT KP_JSON_STR*						/* This object */
);
typedef KP_METHOD(KP_RV, LOAD_FROM_TEMPLATE)	/* Load JSON from a template and replaces specified occurrences */
(
	KP_INOUT KP_JSON_STR*,						/* This object */
	KP_IN KP_STRING,							/* Template */
	...											/* Template replacement list */
);
typedef KP_METHOD(KP_RV, UNLADE_TO_DISK)		/* Writes JSON document to a file */
(
	KP_IN KP_JSON_STR*,							/* This object */
	KP_IN KP_STRING								/* Complete path to file */
);
typedef KP_METHOD(KP_RV, PARSE_JSON)			/* Parse current JSON document */
(
	KP_INOUT KP_JSON_STR*						/* This object */
);
typedef KP_METHOD(const kson_node_t*, QUERY_BY_PATH)(KP_IN KP_JSON_STR*, KP_IN int, ...);
typedef KP_METHOD(const kson_node_t*, QUERY_BY_KEY)(KP_IN kson_node_t*, KP_IN KP_STRING);
typedef KP_METHOD(const kson_node_t*, QUERY_BY_INDEX)(KP_IN kson_node_t*, KP_IN long);

struct KP_JSON_STR
{
	kson_t*				hParser;				/* JSON parser handle */
	KP_STRING			raw;					/* Raw JSON document */

	LOAD_FROM_FILE		from_file;				/* (KP_INOUT KP_JSON_STR *self, KP_IN STRING szFilename) */
	LOAD_FROM_STDIN		from_stdin;				/* (KP_INOUT KP_JSON_STR *self) */
	LOAD_FROM_TEMPLATE	from_template;			/* (KP_INOUT KP_JSON_STR *self, KP_IN KP_STRING szTempĺate, ...) */
	UNLADE_TO_DISK		unlade_disk;			/* (KP_IN KP_JSON_STR *self, KP_IN KP_STRING szFilename) */
	PARSE_JSON			parse;					/* (KP_INOUT KP_JSON_STR *self) */
	QUERY_BY_PATH		by_path;
	QUERY_BY_KEY		by_key;
	QUERY_BY_INDEX		by_index;
};
typedef struct KP_JSON_STR*						KP_JSON;

/**
 * JSON builder facility
 */
typedef struct KP_JSON_BUILDER_STR				KP_JSON_BUILDER_STR;
typedef KP_METHOD(KP_RV, JSON_BOUND)(KP_INOUT KP_JSON_BUILDER_STR*);
typedef KP_METHOD(void, JSON_END)(KP_INOUT KP_JSON_BUILDER_STR*);
typedef KP_METHOD(KP_RV, JSON_NAMEDBOUND)(KP_INOUT KP_JSON_BUILDER_STR*, KP_IN KP_STRING);
typedef KP_METHOD(KP_RV, JSON_STRMEMBER)(KP_INOUT KP_JSON_BUILDER_STR*, KP_IN KP_STRING, KP_IN KP_STRING);
typedef KP_METHOD(KP_RV, JSON_INTMEMBER)(KP_INOUT KP_JSON_BUILDER_STR*, KP_IN KP_STRING, KP_IN int);
typedef KP_METHOD(KP_RV, UNLADE_TO_STDOUT)(KP_IN KP_JSON_BUILDER_STR*);
typedef KP_METHOD(KP_RV, EMBED_JSON)(KP_INOUT KP_JSON_BUILDER_STR*, KP_IN kson_node_t*, KP_IN int);

struct KP_JSON_BUILDER_STR
{
	KP_BYTE*			_buffer;				/* Write stream */
	size_t				_size;					/* Size of _buffer */
	size_t				_i;						/* Current writing position */
	json_printer*		_printer;				/* JSON facility handler */
	KP_RV				_rv;					/* libjson exception handling (or lack of) workaround*/

	JSON_BOUND			begin_write;			/* Must be called before creating any object (KP_INOUT KP_JSON_BUILDER_STR *self) */
	JSON_END			end_write;				/* Must be called when finished (KP_INOUT KP_JSON_BUILDER_STR *self) */
	JSON_NAMEDBOUND		begin_object;			/*szName may be NULL if anonymous. (KP_INOUT KP_JSON_BUILDER_STR *self, KP_IN KP_STRING szName) */
	JSON_BOUND			end_object;				/* (KP_INOUT KP_JSON_BUILDER_STR *self) */
	JSON_NAMEDBOUND		begin_array;			/*szName may be NULL if anonymous. (KP_INOUT KP_JSON_BUILDER_STR *self, KP_IN KP_STRING szName) */
	JSON_BOUND			end_array;				/* (KP_INOUT KP_JSON_BUILDER_STR *self) */
	JSON_STRMEMBER		put_string;				/* szName may be NULL, if anonymous (KP_INOUT KP_JSON_BUILDER_STR *self, KP_IN KP_STRING szName, KP_IN KP_STRING szValue) */
	JSON_STRMEMBER		put_number;				/* szName may be NULL, if anonymous (KP_INOUT KP_JSON_BUILDER_STR *self, KP_IN KP_STRING szName, KP_IN int iValue) */
	JSON_END			reset;					/* Restores object state to default (but _size remains the same) */
	UNLADE_TO_STDOUT	unlade_stdout;			/* (KP_IN KP_JSON_BUILDER_STR *self) */
	JSON_INTMEMBER		put_boolean;
	EMBED_JSON			embed;
};
typedef struct KP_JSON_BUILDER_STR*				KP_JSON_BUILDER;


/**
 * Configuration facility
 */
typedef struct KP_CONFIG_STR					KP_CONFIG_STR;
typedef KP_METHOD(KP_RV, LOAD)					/* Load configuration from specified location */
(	
	KP_INOUT KP_CONFIG_STR*,					/* This object */
	KP_IN KP_STRING								/* config.json file location (without its filename) as a NULL terminated STRING. It sets szLocation property. */
);	
typedef KP_METHOD(KP_RV, STORE)					/* Store current configuration state in its original location */
(
	KP_IN KP_CONFIG_STR*						/* This object */
);
typedef KP_METHOD(KP_RV, REPLACE)				/* Replace config.json definition by a new one. Requires that szConfigFile has been setted by load */
(
	KP_INOUT KP_CONFIG_STR*,					/* This object */
	KP_IN KP_STRING								/* New raw json */
);
typedef KP_METHOD(KP_RV, GET_ULONGPROP)			/* Get numeric property value */
(
	KP_IN KP_CONFIG_STR*,						/* This object */
	KP_OUT KP_ULONG*							/* Property value as an unsigned long */
);
typedef KP_METHOD(KP_RV, GET_STRVALUE)			/* Get specified (by object name) string member, if available */
(
	KP_IN KP_CONFIG_STR*,						/* This object */
	KP_IN KP_STRING,							/* Object name */
	KP_OUT KP_STRING*							/* Member value. Must be freed when no more needed */
);
typedef KP_METHOD(KP_RV, GET_STRPROP)			/* Get specific string member */
(
	KP_IN KP_CONFIG_STR*,						/* This object */
	KP_OUT KP_STRING*							/* Member value. Must be freed after use */
);
struct KP_CONFIG_STR							/* Configuration handler (for config.json, that must exists) */
{
	KP_STRING		szConfigFile;				/* Path to config.json file as a NULL terminated STRING */
	KP_JSON			hJson;						/* Configuration JSON document */

	LOAD			load;						/* (KP_INOUT KP_CONFIG_STR *self, KP_IN KP_STRING szLocation) */
	STORE			store;						/* (KP_IN KP_CONFIG_STR *self) */
	REPLACE			replace;					/* (KP_INOUT KP_CONFIG_STR *self, KP_IN KP_STRING *raw) */
	GET_ULONGPROP	get_responsesize;			/* Get maxResponseSize config value (KP_IN KP_CONFIG_STR *self, KP_OUT KP_ULONG *ulValue) */
	GET_STRVALUE	get_component;				/* Get component name (KP_IN KP_CONFIG_STR *self, KP_IN STRING szID, KP_OUT STRING *szFilename) */
	GET_ULONGPROP	get_keysize;				/* Get key size (KP_IN KP_CONFIG_STR *self, KP_OUT unsigned long *ulValue) */
	GET_STRPROP		get_sigalgOID;				/* Get signatyure algorithm (KP_IN KP_CONFIG_STR *self, KP_OUT KP_STRING *szOID) */
	GET_ULONGPROP	get_sign_alg;				/* Get signature policy hash (KP_IN KP_CONFIG_STR *self, KP_OUT unsigned long *ulSize) */
	GET_STRPROP		get_sign_policy;			/* Get signature policy name (acording to RFC 5126) (KP_IN KP_CONFIG_STR *self, KP_OUT KP_STRING *szPolicy) */
};
typedef struct KP_CONFIG_STR*					KP_CONFIG;



/**
 * Base64 conversion facility
 */
typedef struct KP_BASE64_STR					KP_BASE64_STR;
typedef KP_METHOD(KP_RV, KP_ENCODE_B64)			/* Base64/PEM encodes */
(
	KP_IN KP_BYTE*,								/* Data buffer */
	KP_IN size_t,								/* Size of buffer */
	KP_OUT KP_STRING*							/* NULL terminated string output. It must be freed */
);
typedef KP_METHOD(KP_RV, KP_DECODE_B64)			/* Base64/PEM decodes */
(
	KP_IN KP_STRING,							/* NULL terminated string buffer */
	KP_OUT KP_BYTE**,							/* Decoded buffer. It must be freed */
	KP_OUT size_t*								/* Size of buffer */
);
struct KP_BASE64_STR
{
	KP_ENCODE_B64	encode;						/* Convert buffer to Base64 NULL terminated string. */
	KP_DECODE_B64	decode;						/* Convert a base64 NULL terminated string to binary */
	KP_ENCODE_B64	to_pkcs10;					/* Encodes a PKCS #10 in PEM format */
	KP_DECODE_B64	from_pkcs10;				/* Decodes PKCS #10 from PEM format */
	KP_ENCODE_B64	to_pkcs7;					/* Encodes PKCS # in PEM format */
	KP_DECODE_B64	from_pkcs7;					/* Decodes PKCS #10 from PEM format */
	KP_ENCODE_B64	to_cert;					/* Encodes a certificate in PEM format */
	KP_DECODE_B64	from_cert;					/* Decodes a certificate from PEM format */
};
typedef struct KP_BASE64_STR*					KP_BASE64;

/**
 * Cryptographic slots
 */
typedef struct KP_SLOT_STR
{
	KP_STRING		szName;						/* PKCS #11 slot name (or MS cryptographic provider name) */
#if defined (_WIN_API)
	BOOL			isLegacy;					/* TRUE if szName points to a legacy provider */
	DWORD			dwProvType;					/* MS legacy provider type. If isLegacy is FALSE, this value is ignored */
#else
	/* TODO: KP_SLOT under NSS API */
#endif
} KP_SLOT_STR, *KP_SLOT;
KHASH_MAP_INIT_STR(KP_SLOTS, KP_SLOT)			/* Slots/providers hash map */

/**
 * Command processor
 */
typedef struct KP_PROCESSOR_STR					KP_PROCESSOR_STR;
typedef KP_METHOD(KP_RV, KP_INIT_PROCESSOR)(KP_INOUT KP_PROCESSOR_STR*, KP_IN KP_JSON);
typedef KP_METHOD(const kson_node_t*, KP_PROCESSOR_CMD)(KP_IN KP_PROCESSOR_STR*);
typedef KP_METHOD(KP_RV, FINISH_PROCESSOR)(KP_IN KP_PROCESSOR_STR*, KP_IN KP_RV);
struct KP_PROCESSOR_STR
{
	KP_JSON				hInput;
	KP_JSON_BUILDER		hOutput;
	KP_INIT_PROCESSOR	init;
	KP_PROCESSOR_CMD	command;
	FINISH_PROCESSOR	finish;
};
typedef struct KP_PROCESSOR_STR					*KP_PROCESSOR;


/**
 * Environment and components invocation
 */
#if (defined(_WIN32))
	#define KP_LIBHANDLE						HMODULE
#else
	#define KP_LIBHANDLE						void*
#endif
typedef struct KP_ENVIRONMENT_STR				KP_ENVIRONMENT_STR;
typedef KP_CALLBACK(KP_RV, KP_FUNCTION)			/* Component entry point */
(
	KP_IN KP_ENVIRONMENT_STR*,					/* Environment */
	KP_IN KP_JSON,								/* Arguments */
	KP_OUT KP_JSON_BUILDER*						/* Response */
);
typedef struct KP_COMPONENT_STR					/* Loaded component entry */
{
	KP_STRING				szID;				/* Component ID */
	KP_LIBHANDLE			hModule;			/* Shared object system identifier */
	KP_FUNCTION				hProc;				/* Function address */
	int						bUnloaded;			/* Unloaded indicator */

} KP_COMPONENT_STR, 							*KP_COMPONENT;
KHASH_MAP_INIT_STR(KP_COMPONENTS, KP_COMPONENT)	/* Installed (and already invoked) components map */
typedef KP_METHOD(KP_COMPONENT, GET_CALLBACK)	/* Get specified component executor address */
(
	KP_INOUT KP_ENVIRONMENT_STR*,				/* This object */
	KP_IN KP_STRING								/* Component ID */
);
typedef KP_METHOD(KP_RV, NEW_JSON)				/* Creates a JSON object */
(
	KP_OUT KP_JSON*								/* The stuff */
);
typedef KP_METHOD(void, DELETE_JSON)			/* Releases a JSON object */
(
	KP_INOUT KP_JSON							/* the stuff */
);
typedef KP_METHOD(void, FREE_STRINGARRAY)		/* Releases string array memory */
(
	KP_INOUT KP_STRING*,						/* String array */
	KP_IN int									/* Array count */
);
typedef KP_METHOD(KP_RV, NEW_JSON_BUILDER)		/* Creates a new JSON builder */
(
	KP_OUT KP_JSON_BUILDER*						/* Created stuff */
);
typedef KP_METHOD(void, DELETE_JSON_BUILDER)	/* Releases a JSON builder object */
(
	KP_INOUT KP_JSON_BUILDER					/* the stuff */
);
typedef KP_METHOD(KP_RV, NEW_PROCESSOR)(KP_OUT KP_PROCESSOR*);
typedef KP_METHOD(VOID, DELETE_PROCESSOR)(KP_INOUT KP_PROCESSOR);
typedef KP_METHOD(KP_RV, KP_CP_PATH)(KP_IN KP_ENVIRONMENT_STR*, KP_IN KP_STRING, KP_OUT KP_STRING*);
typedef KP_METHOD(KP_RV, MANAGE_COMPONENT)(KP_INOUT KP_ENVIRONMENT_STR*, KP_IN KP_STRING);
struct KP_ENVIRONMENT_STR
{
	KP_STRING				szLocation;			/* Complete path to application home */
	KP_LOG					hLogger;			/* Logging facility */
	KP_CONFIG				hConfig;			/* Configuration facility */
	khash_t(KP_COMPONENTS)*	kMap;				/* Installed components */
	KP_BASE64				hEncoder;			/* Base64/PEM encoder/decoder */
	khash_t(KP_SLOTS)*		kSlots;				/* Enumerated (by enroll component) slots */

	GET_CALLBACK			get_component;		/* (KP_INOUT KP_COMPONENT_STR *self, KP_IN KP_STRING szID) */
	NEW_JSON				new_json;			/* (KP_OUT KP_JSON *hJSON) */
	DELETE_JSON				delete_json;		/* (KP_IN KP_JSON hJSON) */
	FREE_STRINGARRAY		free_buffer;		/* (KP_INOUT KP_STRING *buffer, KP_IN int count) */
	NEW_JSON_BUILDER		new_json_builder;	/* (KP_OUT KP_JSON_BUILDER *self) */
	DELETE_JSON_BUILDER		delete_json_builder;/* (KP_INOUT KP_JSON_BUILDER self) */
	NEW_PROCESSOR			new_processor;
	DELETE_PROCESSOR		delete_processor;
	KP_CP_PATH				get_component_path;
	MANAGE_COMPONENT		stop_component;
	MANAGE_COMPONENT		start_component;
};
typedef struct KP_ENVIRONMENT_STR*				KP_ENV;
#define KP_COMPONENT_EXECUTOR_NAME				"KP_execute_command"


#if defined(__cplusplus)
KP_EXTERN {
#endif

/**
 * Logger
 */
KP_EXTERN char* szCategories[];
KP_EXTERN char* szMessages[];
KP_NEW(new_logger)(KP_OUT KP_LOG*);
KP_DELETE(delete_logger)(KP_INOUT KP_LOG);

/**
 * JSON parser
 */
KP_NEW(new_json)(KP_OUT KP_JSON*);
KP_DELETE(delete_json)(KP_INOUT KP_JSON);
KP_NEW(new_json_builder)(KP_OUT KP_JSON_BUILDER*);
KP_DELETE(delete_json_builder)(KP_INOUT KP_JSON_BUILDER);

/**
 * Config manager
 */
KP_NEW(new_config)(KP_OUT KP_CONFIG*);
KP_DELETE(delete_config)(KP_INOUT KP_CONFIG);

KP_NEW(new_base64)(KP_OUT KP_BASE64*);
KP_DELETE(delete_base64)(KP_INOUT KP_BASE64);

/**
 * Environment manager
 */
KP_NEW(new_environment)(KP_OUT KP_ENV*);
KP_DELETE(delete_environment)(KP_INOUT KP_ENV);

#if defined(__cplusplus)
}
#endif

#endif /* __KRYPTONITE_H__ */