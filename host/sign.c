/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Signing component
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "sign.h"
#include <string.h>

KP_IMPLEMENTATION(KP_RV, __put_string)(KP_IN NH_ASN1_PNODE node, KP_IN KP_STRING szField, KP_INOUT KP_JSON_BUILDER hBuilder)
{
	KP_RV rv;
	KP_STRING szValue;
	if (KP_SUCCESS(rv = (szValue = (KP_STRING) malloc(node->valuelen + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR))
	{
		memset(szValue, 0, node->valuelen + 1);
		memcpy(szValue, node->value, node->valuelen);
		rv = hBuilder->put_string(hBuilder, szField, szValue);
		free(szValue);
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __parse_pki_br)(KP_IN unsigned char *buffer, KP_IN size_t size, KP_INOUT KP_JSON_BUILDER hBuilder)
{
	KP_RV rv;
	NH_CERTIFICATE_HANDLER hCert;
	NH_ASN1_PNODE node = NULL;
	NH_PKIBR_EXTENSION hExt;
	if (KP_SUCCESS(rv = NH_parse_certificate(buffer, size, &hCert)))
	{
		if
		(
			KP_SUCCESS(rv = hCert->subject_alt_names(hCert, &node)) && (node) &&
			KP_SUCCESS(rv = NH_parse_pkibr_extension(node->identifier, node->size + node->contents - node->identifier, &hExt))
		)
		{
			if (hExt->company_id) rv = __put_string(hExt->company_id, "company_id", hBuilder);
			if (KP_SUCCESS(rv) && hExt->sponsor_id) rv = __put_string(hExt->sponsor_id, "sponsor_id", hBuilder);
			if (KP_SUCCESS(rv) && hExt->subject_id) rv = __put_string(hExt->subject_id, "subject_id", hBuilder);
			NH_release_pkibr_extension(hExt);
		}
		NH_release_certificate(hCert);
	}
	return rv;
}

#if defined (_WIN_API)

#define SELECT_CERT_FLAGS				(CRYPT_ACQUIRE_PREFER_NCRYPT_KEY_FLAG)
#define BIN_TO_HEX_FLAGS				(CRYPT_STRING_HEX | CRYPT_STRING_NOCRLF)
#define CERT_ENCODING_FLAGS				(X509_ASN_ENCODING | PKCS_7_ASN_ENCODING)

KP_IMPLEMENTATION(KP_RV, __get_name)(KP_IN PCCERT_CONTEXT hCert, KP_IN DWORD dwFlags, KP_OUT KP_STRING *szOut)
{
	KP_RV rv;
	DWORD dwParam = CERT_X500_NAME_STR, cbName;
	WCHAR *szName = NULL;
	size_t len;
	KP_STRING szNameA = NULL;
	if
	(
		KP_SUCCESS(rv = (cbName = CertGetNameString(hCert, CERT_NAME_RDN_TYPE, dwFlags, &dwParam, NULL, 0)) > 1 ? KPTA_OK : KPTA_CRYPTO_ENUM_CERTS_ERROR) &&
		KP_SUCCESS(rv = (szName = (WCHAR*) malloc(cbName * sizeof(WCHAR))) ? KPTA_OK : KPTA_MEMORY_ERROR) &&
		KP_SUCCESS(rv = (cbName = CertGetNameString(hCert, CERT_NAME_RDN_TYPE, dwFlags, &dwParam, szName, cbName)) > 1 ? KPTA_OK : KPTA_CRYPTO_ENUM_CERTS_ERROR) &&
		KP_SUCCESS(rv = (len = wcstombs(NULL, szName, 0)) > 0 ? KPTA_OK : KPTA_MEMORY_ERROR) &&
		KP_SUCCESS(rv = (szNameA = (KP_STRING) malloc((len + 1) * sizeof(char))) ? KPTA_OK : KPTA_MEMORY_ERROR) &&
		KP_SUCCESS(rv = wcstombs(szNameA, szName, len + 1) > 0 ? KPTA_OK : KPTA_MEMORY_ERROR)
	)	*szOut = szNameA;
	if (szName) free(szName);
	if (KP_FAIL(rv) && szNameA) free(szNameA);
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __to_hex)(KP_IN PCRYPT_INTEGER_BLOB pBlob, KP_OUT KP_STRING *szOut)
{
	KP_RV rv;
	KP_STRING out;
	size_t i;

	if (KP_SUCCESS(rv = (out = (KP_STRING) malloc(pBlob->cbData * 2 + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR))
	{
		for (i = 0; i < pBlob->cbData; i++)
		{
			out[i * 2    ] = "0123456789ABCDEF"[pBlob->pbData[i] >> 4  ];
			out[i * 2 + 1] = "0123456789ABCDEF"[pBlob->pbData[i] & 0x0F];
		}
		out[pBlob->cbData * 2] = '\0';
		*szOut = out;
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __win_enum_certs)(KP_IN KP_ENV env, KP_OUT KP_JSON_BUILDER *hObject)
{
	KP_RV rv;
	KP_JSON_BUILDER hBuilder = NULL;
	HCERTSTORE hStore = NULL;
	PCCERT_CONTEXT hCert = NULL;
	KP_STRING szSubject = NULL, szIssuer = NULL, szSerial = NULL;
	HCRYPTPROV_OR_NCRYPT_KEY_HANDLE hKey = NULL;
	DWORD dwKeySpec = 0;
	BOOL mustFree = FALSE;

	if
	(
		KP_SUCCESS(rv = env->new_json_builder(&hBuilder)) &&
		KP_SUCCESS(rv = (hStore = CertOpenSystemStore(NULL, L"MY")) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENUM_CERTS_ERROR)) &&
		KP_SUCCESS(rv = hBuilder->begin_write(hBuilder)) &&
		KP_SUCCESS(rv = hBuilder->begin_array(hBuilder, "certificates"))
	)
	{
		while (KP_SUCCESS(rv) && (hCert = CertFindCertificateInStore(hStore, CERT_ENCODING_FLAGS, 0, CERT_FIND_HAS_PRIVATE_KEY, NULL, hCert)))
		{
			if
			(
				!CertVerifyTimeValidity(NULL, hCert->pCertInfo) &&
				CryptAcquireCertificatePrivateKey(hCert, SELECT_CERT_FLAGS, NULL, &hKey, &dwKeySpec, &mustFree) &&
				KP_SUCCESS(rv = __get_name(hCert, 0, &szSubject)) &&
				KP_SUCCESS(rv = __get_name(hCert, CERT_NAME_ISSUER_FLAG, &szIssuer)) &&
				KP_SUCCESS(rv = __to_hex(&hCert->pCertInfo->SerialNumber, &szSerial)) &&
				KP_SUCCESS(rv = hBuilder->begin_object(hBuilder, NULL)) &&
				KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "subject", szSubject)) &&
				KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "issuer", szIssuer)) &&
				KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "serial", szSerial)) &&
				KP_SUCCESS(rv = __parse_pki_br(hCert->pbCertEncoded, hCert->cbCertEncoded, hBuilder))
			)	rv = hBuilder->end_object(hBuilder);
			if (mustFree)
			{
				if (dwKeySpec == CERT_NCRYPT_KEY_SPEC) NCryptFreeObject(hKey);
				else CryptReleaseContext(hKey, 0);
			}
			if (szSubject) free(szSubject);
			if (szIssuer) free(szIssuer);
			if (szSerial) free(szSerial);
			szSubject = NULL;
			szIssuer = NULL;
			szSerial = NULL;
		}
		if (KP_SUCCESS(rv) && KP_SUCCESS(rv = hBuilder->end_array(hBuilder)))
		{
			hBuilder->end_write(hBuilder);
			*hObject = hBuilder;
		}
	}
	if (KP_FAIL(rv)) env->delete_json_builder(hBuilder);
	if (hCert) CertFreeCertificateContext(hCert);
	if (hStore) CertCloseStore(hStore, 0);
	return rv;
}
#define __platform_enum_certs					__win_enum_certs

KP_IMPLEMENTATION(KP_RV, __win_add_signing_cert)(KP_INOUT NH_CMS_SD_ENCODER hCMS, KP_IN KP_JSON hJson, KP_OUT KP_CERTIFICATE_HANDLER *hHandle)
{
	KP_RV rv;
	HCERTSTORE hStore = NULL;
	BOOL found = FALSE;
	PCCERT_CONTEXT hCert = NULL;
	KP_STRING szSubject = NULL, szIssuer = NULL, szSerial = NULL;
	const kson_node_t *p;
	HCRYPTPROV_OR_NCRYPT_KEY_HANDLE hKey = NULL;
	DWORD dwKeySpec = 0;
	BOOL mustFree = FALSE;
	NH_CERTIFICATE_HANDLER hNharu = NULL;
	KP_CERTIFICATE_HANDLER out;

	rv = (hStore = CertOpenSystemStore(NULL, L"MY")) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENUM_CERTS_ERROR);
	while
	(
		KP_SUCCESS(rv) &&
		!found &&
		(hCert = CertFindCertificateInStore(hStore, CERT_ENCODING_FLAGS, 0, CERT_FIND_HAS_PRIVATE_KEY, NULL, hCert))
	)
	{
		if
		(
			!CertVerifyTimeValidity(NULL, hCert->pCertInfo) &&
			KP_SUCCESS(rv = __get_name(hCert, 0, &szSubject)) &&
			KP_SUCCESS(rv = __get_name(hCert, CERT_NAME_ISSUER_FLAG, &szIssuer)) &&
			KP_SUCCESS(rv = __to_hex(&hCert->pCertInfo->SerialNumber, &szSerial))
		)
		{
			if
			(
				(p = hJson->by_path(hJson, 1, "subject")) &&
				strcmp(p->v.str, szSubject) == 0 &&
				(p = hJson->by_path(hJson, 1, "issuer")) &&
				strcmp(p->v.str, szIssuer) == 0 &&
				(p = hJson->by_path(hJson, 1, "serial")) &&
				strcmp(p->v.str, szSerial) == 0 &&
				CryptAcquireCertificatePrivateKey(hCert, SELECT_CERT_FLAGS, NULL, &hKey, &dwKeySpec, &mustFree)
			)
			{
				found = TRUE;
				if (mustFree)
				{
					if (dwKeySpec == CERT_NCRYPT_KEY_SPEC) NCryptFreeObject(hKey);
					else CryptReleaseContext(hKey, 0);
				}
			}
		}
		if (szSubject) free(szSubject);
		if (szIssuer) free(szIssuer);
		if (szSerial) free(szSerial);
		szSubject = NULL;
		szIssuer = NULL;
		szSerial = NULL;
	}
	if (found)
	{
		if
		(
			KP_SUCCESS(rv = KP_SUCCESS(rv = NH_parse_certificate(hCert->pbCertEncoded, hCert->cbCertEncoded, &hNharu)) ? KPTA_OK : KP_SET_ERROR(rv, KPTA_PARSE_CERT_ERROR)) &&
			KP_SUCCESS(rv = KP_SUCCESS(rv = hCMS->add_cert(hCMS, hNharu)) ? KPTA_OK :  KP_SET_ERROR(rv, KPTA_PARSE_CERT_ERROR)) &&
			KP_SUCCESS(rv = (out = (KP_CERTIFICATE_HANDLER) malloc(sizeof(KP_CERT_HANDLER_STR))) ? KPTA_OK : KPTA_MEMORY_ERROR)
		)
		{
			out->hNative = hCert;
			out->hNharu = hNharu;
			*hHandle = out;
		}
	}
	else rv = KPTA_CERT_DN_NOT_FOUND;
	if (KP_FAIL(rv))
	{
		if (hCert) CertFreeCertificateContext(hCert);
		if (hNharu) NH_release_certificate(hNharu);
	}
	if (hStore) CertCloseStore(hStore, 0);
	return rv;
}
#define __platform_add_signing_cert				__win_add_signing_cert

KP_IMPLEMENTATION(void, __win_release_cert)(KP_INOUT KP_CERTIFICATE_HANDLER hHandler)
{
	if (hHandler)
	{
		if (hHandler->hNative) CertFreeCertificateContext(hHandler->hNative);
		if (hHandler->hNharu) NH_release_certificate(hHandler->hNharu);
		free(hHandler);
	}
}
#define __platform_release_cert					__win_release_cert

KP_IMPLEMENTATION(NH_RV, __win_sign_callback)(KP_IN KP_BLOB *data, KP_IN CK_MECHANISM_TYPE mechanism, KP_IN void *params, KP_OUT KP_BYTE *signature, KP_INOUT size_t *sigSize)
{
	KP_RV rv;
	HCRYPTPROV_OR_NCRYPT_KEY_HANDLE hKey = NULL;
	DWORD dwKeySpec = 0, cbResult = 0;
	BOOL mustFree = NULL;
	KP_CERTIFICATE_HANDLER hHandler = (KP_CERTIFICATE_HANDLER) params;
	BCRYPT_PKCS1_PADDING_INFO paddingInfo = { NULL };
	SECURITY_STATUS stat;
	ALG_ID algID;
	HCRYPTHASH hHash = NULL;

	switch (mechanism)
	{
	case CKM_SHA1_RSA_PKCS:
		paddingInfo.pszAlgId = NCRYPT_SHA1_ALGORITHM;
		algID = CALG_SHA1;
		break;
	case CKM_SHA256_RSA_PKCS:
		paddingInfo.pszAlgId = NCRYPT_SHA256_ALGORITHM;
		algID = CALG_SHA_256;
		break;
	case CKM_SHA384_RSA_PKCS:
		paddingInfo.pszAlgId = NCRYPT_SHA384_ALGORITHM;
		algID = CALG_SHA_384;
		break;
	case CKM_SHA512_RSA_PKCS:
		paddingInfo.pszAlgId = NCRYPT_SHA512_ALGORITHM;
		algID = CALG_SHA_512;
		break;
	default: return KPTA_UNSUPPORTED_MECHANISM;
	}
	if (KP_SUCCESS(rv = CryptAcquireCertificatePrivateKey(hHandler->hNative, SELECT_CERT_FLAGS, NULL, &hKey, &dwKeySpec, &mustFree) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_SIGN_ERROR)))
	{
		if (dwKeySpec == CERT_NCRYPT_KEY_SPEC)
		{
			if (KP_SUCCESS(rv = (stat = NCryptSignHash(hKey, &paddingInfo, data->data, data->length, NULL, 0, &cbResult, NCRYPT_PAD_PKCS1_FLAG)) == ERROR_SUCCESS ? KPTA_OK : KP_SET_ERROR(stat, KPTA_CRYPTO_SIGN_ERROR)))
			{
				if (!signature) *sigSize = cbResult;
				else
				{
					if (KP_SUCCESS(rv = (*sigSize >= cbResult) ? KPTA_OK : KPTA_BUFFER_TOO_SMALL))
						rv = (stat = NCryptSignHash(hKey, &paddingInfo, data->data, data->length, signature, *sigSize, &cbResult, NCRYPT_PAD_PKCS1_FLAG)) == ERROR_SUCCESS ? KPTA_OK : KP_SET_ERROR(stat, KPTA_CRYPTO_SIGN_ERROR);
				}
			}
		}
		else
		{
			if (KP_SUCCESS(rv = CryptCreateHash(hKey, algID, 0, 0, &hHash) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_SIGN_ERROR)))
			{
				if
				(
					KP_SUCCESS(rv = CryptSetHashParam(hHash, HP_HASHVAL, data->data, 0) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_SIGN_ERROR)) &&
					KP_SUCCESS(rv = CryptSignHash(hHash, AT_SIGNATURE, NULL, 0, NULL, &cbResult) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_SIGN_ERROR))
				)
				{
					if (!signature) *sigSize = cbResult;
					else
					{
						if (KP_SUCCESS(rv = (*sigSize >= cbResult) ? KPTA_OK : KPTA_BUFFER_TOO_SMALL))
							rv = CryptSignHash(hHash, AT_SIGNATURE, NULL, 0, signature, &cbResult) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_SIGN_ERROR);
					}
				}
				CryptDestroyHash(hHash);
			}
		}
		if (mustFree)
		{
			if (dwKeySpec == CERT_NCRYPT_KEY_SPEC) NCryptFreeObject(hKey);
			else CryptReleaseContext(hKey, 0);
		}
	}
	return (NH_RV) rv;
}
#define __platform_sign_callback				__win_sign_callback

#else
 /**
  * TODO:
  */
#endif

KP_UTILITY(KP_RV, KPTA_enumerate_certs)(KP_IN KP_ENV env, KP_OUT KP_JSON_BUILDER *hCerts)
{
	KP_RV rv;
	env->hLogger->debug(env->hLogger, SIGNATURE_CATEGORY, KPL_OP_INIT, ENUMERATE_CERTS_OP, "");
	if (KP_SUCCESS(rv = __platform_enum_certs(env, hCerts)))
	{
		env->hLogger->debug(env->hLogger, SIGNATURE_CATEGORY, KPL_CERT_ENUMERATED, (char*)(*hCerts)->_buffer);
		env->hLogger->info(env->hLogger, SIGNATURE_CATEGORY, KPL_OPERATION_SUCCEEDED, ENUMERATE_CERTS_OP);
	}
	else env->hLogger->error(env->hLogger, SIGNATURE_CATEGORY, KPL_OPERATION_FAILURE, rv, ENUMERATE_CERTS_OP);
	return rv;
}

KP_UTILITY(KP_RV, KPTA_sign)(KP_IN KP_ENV env, KP_IN KP_STRING szSelected, KP_IN KP_STRING szData, KP_IN int isBase64, KP_IN int attach, KP_OUT KP_STRING *szCMS)
{
	KP_RV rv;
	KP_STRING szParams;
	KP_JSON hJson = NULL;
	KP_ULONG ulSigAlg = 0;
	KP_STRING szPolicy = NULL;
	size_t cbSize = 0, cbParams;
	KP_BLOB blob = { NULL, 0 };
	NH_CMS_SD_ENCODER hCMS = NULL;
	KP_CERTIFICATE_HANDLER hCertHandler = NULL;
	NH_CMS_ISSUER_SERIAL_STR sid = { NULL, NULL, NULL };
	KP_BYTE *pEncoding = NULL;

	if
	(
		env->hLogger->level == KP_LOG_DEBUG &&
		(cbParams = strlen(szSelected) + strlen(szData) + 512) &&
		(szParams = (KP_STRING) malloc(cbParams))
	)
	{
		snprintf(szParams, cbParams, "szSelected=%s, szData=%s, isBase64=%d, attach=%d\n", szSelected, szData, isBase64, attach);
		env->hLogger->debug(env->hLogger, SIGNATURE_CATEGORY, KPL_OP_INIT, SIGN_MESSAGE_OP, szParams);
		free(szParams);
	}
	
	if
	(
		KP_SUCCESS(rv = env->new_json(&hJson)) &&
		KP_SUCCESS(rv = hJson->from_template(hJson, szSelected)) &&
		KP_SUCCESS(rv = hJson->parse(hJson)) &&
		KP_SUCCESS(rv = env->hConfig->get_sign_alg(env->hConfig, &ulSigAlg)) &&
		KP_SUCCESS(rv = env->hConfig->get_sign_policy(env->hConfig, &szPolicy))
	)
	{
		if (isBase64) rv = env->hEncoder->decode(szData, &blob.data, &blob.length);
		else
		{
			blob.data = (KP_BYTE*) szData;
			blob.length = strlen(szData);
		}
	}
	if
	(
		KP_SUCCESS(rv) &&
		KP_SUCCESS(rv = KP_SUCCESS(rv = NH_cms_encode_signed_data(&blob, &hCMS)) ? KPTA_OK : KP_SET_ERROR(rv, KPTA_ENCODE_CMS_ERROR)) &&
		KP_SUCCESS(rv = KP_SUCCESS(hCMS->data_ctype(hCMS, (CK_BBOOL) attach)) ? KPTA_OK : KP_SET_ERROR(rv, KPTA_ENCODE_CMS_ERROR)) &&
		KP_SUCCESS(rv = __platform_add_signing_cert(hCMS, hJson, &hCertHandler))
	)
	{
			sid.name = hCertHandler->hNharu->issuer;
			sid.serial = hCertHandler->hNharu->serialNumber;
			if (strcmp(szPolicy, CADES_BES_POLICY) == 0) rv = KP_SUCCESS(rv = hCMS->sign_cades_bes(hCMS, hCertHandler->hNharu, ulSigAlg, __platform_sign_callback, hCertHandler)) ? KPTA_OK : KP_SET_ERROR(rv, KPTA_ENCODE_CMS_ERROR);
			else rv = KP_SUCCESS(rv = hCMS->sign(hCMS, &sid, ulSigAlg, __platform_sign_callback, hCertHandler)) ? KPTA_OK : KP_SET_ERROR(rv, KPTA_ENCODE_CMS_ERROR);
	}
	if
	(
		KP_SUCCESS(rv) &&
		KP_SUCCESS(rv = (cbSize = hCMS->hEncoder->encoded_size(hCMS->hEncoder, hCMS->hEncoder->root)) > 0 ? KPTA_OK : KPTA_ENCODE_CMS_ERROR) &&
		KP_SUCCESS(rv = (pEncoding = (KP_BYTE*) malloc(cbSize)) ? KPTA_OK : KPTA_MEMORY_ERROR) &&
		KP_SUCCESS(rv = KP_SUCCESS(rv = hCMS->hEncoder->encode(hCMS->hEncoder, hCMS->hEncoder->root, pEncoding)) ? KPTA_OK : KP_SET_ERROR(rv, KPTA_ENCODE_CMS_ERROR))
	)	rv = env->hEncoder->to_pkcs7(pEncoding, cbSize, szCMS);

	if (hJson) env->delete_json(hJson);
	if (szPolicy) free(szPolicy);
	if (isBase64 && blob.data) free(blob.data);
	if (hCertHandler) __platform_release_cert(hCertHandler);
	if (hCMS) NH_cms_release_sd_encoder(hCMS);
	if (pEncoding) free(pEncoding);
	if (KP_SUCCESS(rv)) env->hLogger->info(env->hLogger, SIGNATURE_CATEGORY, KPL_OPERATION_SUCCEEDED, SIGN_MESSAGE_OP);
	else env->hLogger->error(env->hLogger, SIGNATURE_CATEGORY, KPL_OPERATION_FAILURE, rv, SIGN_MESSAGE_OP);
	return rv;
}