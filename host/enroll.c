/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Enroll component implementation
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "enroll.h"
#include <string.h>

KP_IMPLEMENTATION(void, __release_slots)(KP_INOUT khash_t(KP_SLOTS) *kSlots)
{
	khiter_t k;
	KP_SLOT pSlot;
	for (k = kh_begin(kSlots); k != kh_end(kSlots); ++k) if (kh_exist(kSlots, k) && (pSlot = kh_value(kSlots, k)))
	{
		if (pSlot->szName) free(pSlot->szName);
#if defined (_NSS_API)
		/* TODO: release NSS slot */
#endif
		free(pSlot);
	}
	kh_clear(KP_SLOTS, kSlots);
}

/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *	Windows CryptoAPI/CNG implmentation
 *	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#if defined(_WIN_API)
#include <wincrypt.h>
KP_IMPLEMENTATION(KP_RV, __enum_legacy_providers)(KP_IN KP_ENV env, KP_IN DWORD dwType, KP_OUT int *icount, KP_OUT KP_STRING *pList[])
{
	KP_RV rv = KPTA_OK;
	DWORD dwIndex = 0, cbProvName, dwError, dwProvType;
	int count = 0, i;
	LPWSTR szProvName = NULL;
	KP_STRING *items = NULL;
	size_t len;
	while (CryptEnumProviders(dwIndex++, NULL, 0, &dwProvType, NULL, &cbProvName)) if (dwProvType == dwType) count++;
	if ((dwError = GetLastError()) == ERROR_NO_MORE_ITEMS)
	{
		if (count > 0 && KP_SUCCESS(rv = (items = (KP_STRING*)malloc(count * sizeof(KP_STRING))) ? KPTA_OK : KPTA_MEMORY_ERROR))
		{
			memset(items, 0, count * sizeof(KP_STRING));
			dwIndex = 0;
			i = 0;
			while (KP_SUCCESS(rv) && CryptEnumProviders(dwIndex, NULL, 0, &dwProvType, NULL, &cbProvName))
			{
				if ((dwProvType == dwType) && KP_SUCCESS(rv = (szProvName = (LPWSTR) malloc(cbProvName + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR))
				{
					if
					(
						KP_SUCCESS(rv = CryptEnumProviders(dwIndex, NULL, 0, &dwProvType, szProvName, &cbProvName) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR)) &&
						KP_SUCCESS(rv = (len = wcstombs(NULL, szProvName, 0)) > 0 ? KPTA_OK : KPTA_MEMORY_ERROR) &&
						KP_SUCCESS(rv = (items[i] = (KP_STRING)malloc((len + 1) * sizeof(char))) ? KPTA_OK : KPTA_MEMORY_ERROR) &&
						KP_SUCCESS(rv = wcstombs(items[i], szProvName, len + 1) > 0 ? KPTA_OK : KPTA_MEMORY_ERROR)
					)	i++;
					free(szProvName);
				}
				dwIndex++;
			}
		}
	}
	else rv = KP_SET_ERROR(dwError, KPTA_CRYPTO_ENROLL_ERROR);
	if (KP_SUCCESS(rv))
	{
		if ((dwError = GetLastError()) == ERROR_NO_MORE_ITEMS)
		{
			*icount = count;
			*pList = items;
		}
		else rv = KP_SET_ERROR(dwError, KPTA_CRYPTO_ENROLL_ERROR);
	}
	else if (rv == KPTA_CRYPTO_ENROLL_ERROR) rv = KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR);
	if (KP_FAIL(rv) && items) env->free_buffer(items, count);
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __enum_providers)(KP_IN KP_ENV env, KP_OUT int *icount, KP_OUT KP_STRING *pList[])
{
	KP_RV rv;
	SECURITY_STATUS ret;
	DWORD dwCount = 0, i = 0;
	NCryptProviderName *pProviderList = NULL;
	KP_STRING *items = NULL;
	size_t len;
	if (KP_SUCCESS(rv = (ret = NCryptEnumStorageProviders(&dwCount, &pProviderList, 0)) == ERROR_SUCCESS ? KPTA_OK : KP_SET_ERROR(ret, KPTA_CRYPTO_ENROLL_ERROR)))
	{
		if (KP_SUCCESS(rv = (items = (KP_STRING*) malloc(dwCount * sizeof(KP_STRING))) ? KPTA_OK : KPTA_MEMORY_ERROR)) memset(items, 0, dwCount * sizeof(KP_STRING));
		while (KP_SUCCESS(rv) && i < dwCount)
		{
			if
			(
				KP_SUCCESS(rv = (len = wcstombs(NULL, pProviderList[i].pszName, 0)) > 0 ? KPTA_OK : KPTA_MEMORY_ERROR) &&
				KP_SUCCESS(rv = (items[i] = (KP_STRING) malloc((len + 1) * sizeof(char))) ? KPTA_OK : KPTA_MEMORY_ERROR) &&
				KP_SUCCESS(rv = (len = wcstombs(items[i], pProviderList[i].pszName, len + 1)) > 0 ? KPTA_OK : KPTA_MEMORY_ERROR)
			)	i++;
		}
		NCryptFreeBuffer(pProviderList);
	}
	if (KP_SUCCESS(rv))
	{
		*icount = (int) dwCount;
		*pList = items;
	}
	else if (items) env->free_buffer(items, dwCount);
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __win_store_slots)(KP_IN KP_ENV env, KP_INOUT khash_t(KP_SLOTS) *kSlots, KP_IN BOOL isLegacy, KP_IN DWORD dwProvType, KP_IN KP_STRING *szProviders, KP_IN int count)
{
	KP_RV rv = KPTA_OK;
	int i = 0, ret;
	KP_SLOT pSlot;
	khiter_t k;
	while (KP_SUCCESS(rv) && i < count)
	{
		if
		(
			KP_SUCCESS(rv = (pSlot = (KP_SLOT) malloc(sizeof(KP_SLOT_STR))) ? KPTA_OK : KPTA_MEMORY_ERROR) &&
			KP_SUCCESS(rv = (pSlot->szName = (KP_STRING) malloc(strlen(szProviders[i]) + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR)
		)
		{
			strcpy(pSlot->szName, szProviders[i]);
			pSlot->isLegacy = isLegacy;
			pSlot->dwProvType = dwProvType;
			env->hLogger->debug(env->hLogger, ENROLLMENT_CATEGORY, KPL_SLOT_ENUMERATED, pSlot->szName);
			k = kh_put(KP_SLOTS, kSlots, pSlot->szName, &ret);
			switch (ret)
			{
			case -1:
				rv = KPTA_PUT_MAP_ERROR;
			case  0:
				free(pSlot->szName);
				free(pSlot);
				break;
			case  1:
			case  2:
				kh_val(kSlots, k) = pSlot;
				break;
			}
		}
		i++;
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __win_enum_providers)(KP_INOUT KP_ENV env)
{
	KP_RV rv;
	KP_STRING *szProviders;
	int iCount;
	if (KP_SUCCESS(rv = __enum_legacy_providers(env, PROV_RSA_FULL, &iCount, &szProviders)))
	{
		rv = __win_store_slots(env, env->kSlots, TRUE, PROV_RSA_FULL, szProviders, iCount);
		env->free_buffer(szProviders, iCount);
		iCount = 0;
		if
		(
			KP_SUCCESS(rv) &&
			KP_SUCCESS(rv = __enum_legacy_providers(env, PROV_RSA_AES, &iCount, &szProviders))
			
		)
		{
			rv = __win_store_slots(env, env->kSlots, TRUE, PROV_RSA_AES, szProviders, iCount);
			env->free_buffer(szProviders, iCount);
			iCount = 0;
			if (KP_SUCCESS(rv = __enum_providers(env, &iCount, &szProviders)))
			{
				rv = __win_store_slots(env, env->kSlots, FALSE, 0, szProviders, iCount);
				env->free_buffer(szProviders, iCount);
			}
		}
	}
	return rv;
}
#define __platform_enum_slots					__win_enum_providers

#define KPTA_KEY_CONTAIER						L"Kryptonite key container "
KP_IMPLEMENTATION(KP_RV, __win_new_legacy_container)(KP_IN LPWSTR szProvName, KP_IN DWORD dwProvType, KP_OUT HCRYPTPROV *hHandle, KP_OUT LPWSTR *szName)
{
	KP_RV rv = KPTA_OK;
	WCHAR szContainer[1024], szNumber[32], *ret;
	int i = 0;
	HCRYPTPROV hProv = NULL;
	DWORD dwError;

	while (KP_SUCCESS(rv))
	{
		_itow(i++, szNumber, 10);
		wcscpy(szContainer, KPTA_KEY_CONTAIER);
		wcscat(szContainer, szNumber);
		if (CryptAcquireContext(&hProv, szContainer, szProvName, dwProvType, CRYPT_NEWKEYSET)) break;
		else rv = ((dwError = GetLastError()) == NTE_EXISTS ? KPTA_OK : KP_SET_ERROR(dwError, KPTA_CRYPTO_ENROLL_ERROR));
	}
	if (KP_SUCCESS(rv) && hProv)
	{
		if (KP_SUCCESS(rv = (ret = (WCHAR*) malloc((wcslen(szContainer) + 1) * sizeof(WCHAR))) ? KPTA_OK : KPTA_MEMORY_ERROR))
		{
			wcscpy(ret, szContainer);
			*szName = ret;
			*hHandle = hProv;
		}
		else CryptReleaseContext(hProv, 0);
	}
	return rv;
}
#define KPTA_KEY_NAME							L"Kryptonite key "
KP_IMPLEMENTATION(KP_RV, __win_new_key_name)(KP_IN LPWSTR szProvName, KP_OUT NCRYPT_PROV_HANDLE *hHandle, KP_OUT NCRYPT_KEY_HANDLE *hRSAKey, KP_OUT LPWSTR *szKeyName)
{
	KP_RV rv;
	SECURITY_STATUS stat;
	NCRYPT_PROV_HANDLE hProv = NULL;
	int i = 0;
	WCHAR szName[1024], szNumber[32], *ret;
	NCRYPT_KEY_HANDLE hKey = NULL;
	
	rv = (stat = NCryptOpenStorageProvider(&hProv, szProvName, 0)) == ERROR_SUCCESS ? KPTA_OK : KP_SET_ERROR(stat, KPTA_CRYPTO_ENROLL_ERROR);
	while (KP_SUCCESS(rv))
	{
		_itow(i++, szNumber, 10);
		wcscpy(szName, KPTA_KEY_NAME);
		wcscat(szName, szNumber);
		if ((stat = NCryptCreatePersistedKey(hProv, &hKey, (LPWSTR) BCRYPT_RSA_ALGORITHM, szName, AT_SIGNATURE, 0)) == ERROR_SUCCESS) break;
		else rv = stat == NTE_EXISTS ? KPTA_OK : KP_SET_ERROR(stat, KPTA_CRYPTO_ENROLL_ERROR);
	}
	if (KP_SUCCESS(rv) && KP_SUCCESS(rv = (ret = (WCHAR*) malloc((wcslen(szName) + 1) * sizeof(WCHAR))) ? KPTA_OK : KPTA_MEMORY_ERROR))
	{
		wcscpy(ret, szName);
		*hHandle = hProv;
		*hRSAKey = hKey;
		*szKeyName = ret;
	} 
	else
	{
		if (hProv) NCryptFreeObject(hProv);
		if (hKey) NCryptDeleteKey(hKey, 0);
	}
	return rv;
}
#define REQUEST_ENCODING						(DWORD)(PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
KP_IMPLEMENTATION(KP_RV, __win_generate_legacy_keys)(KP_IN HCRYPTPROV hProv, KP_IN KP_ULONG ulKeyLen, KP_OUT HCRYPTKEY *hHandle, KP_OUT CERT_PUBLIC_KEY_INFO **pPubKey, KP_OUT DWORD *cbPubKey)
{
	KP_RV rv;
	HCRYPTKEY hKey = NULL;
	DWORD dwFlags = ((ulKeyLen << 16) | CRYPT_FORCE_KEY_PROTECTION_HIGH | CRYPT_USER_PROTECTED | CRYPT_EXPORTABLE), cbInfo = 0;
	CERT_PUBLIC_KEY_INFO *pInfo = NULL;
	if
	(
		KP_SUCCESS(rv = CryptGenKey(hProv, AT_SIGNATURE, dwFlags, &hKey) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR)) &&
		KP_SUCCESS(rv = CryptExportPublicKeyInfo(hProv, AT_SIGNATURE, REQUEST_ENCODING, NULL, &cbInfo) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR)) &&
		KP_SUCCESS(rv = (pInfo = (CERT_PUBLIC_KEY_INFO*)malloc(cbInfo)) ? KPTA_OK : KPTA_MEMORY_ERROR) &&
		KP_SUCCESS(rv = CryptExportPublicKeyInfo(hProv, AT_SIGNATURE, REQUEST_ENCODING, pInfo, &cbInfo) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR))
	)
	{
		*hHandle = hKey;
		*pPubKey = pInfo;
		*cbPubKey = cbInfo;
	}
	else
	{
		if (hKey) CryptDestroyKey(hKey);
		if (pInfo) free(pInfo);
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __win_generate_keys)(KP_IN NCRYPT_KEY_HANDLE hKey, KP_IN KP_ULONG ulKeySize, KP_OUT CERT_PUBLIC_KEY_INFO **pPubKey, KP_OUT DWORD *cbPubKey)
{
	KP_RV rv;
	SECURITY_STATUS stat;
	DWORD dwExport = NCRYPT_ALLOW_EXPORT_FLAG | NCRYPT_ALLOW_PLAINTEXT_EXPORT_FLAG, dwKeyUsage = NCRYPT_ALLOW_ALL_USAGES, cbInfo;
	NCRYPT_UI_POLICY pPolicy = { 1, NCRYPT_UI_PROTECT_KEY_FLAG | NCRYPT_UI_FORCE_HIGH_PROTECTION_FLAG, NULL, NULL, NULL };
	CERT_PUBLIC_KEY_INFO *pInfo = NULL;
	if
	(
		KP_SUCCESS(rv = (stat = NCryptSetProperty(hKey, NCRYPT_EXPORT_POLICY_PROPERTY, (BYTE*) &dwExport, sizeof(DWORD), NCRYPT_PERSIST_FLAG)) == ERROR_SUCCESS ? KPTA_OK : KP_SET_ERROR(stat, KPTA_CRYPTO_ENROLL_ERROR)) &&
		KP_SUCCESS(rv = (stat = NCryptSetProperty(hKey, NCRYPT_KEY_USAGE_PROPERTY, (BYTE*) &dwKeyUsage, sizeof(DWORD), NCRYPT_PERSIST_FLAG)) == ERROR_SUCCESS ? KPTA_OK : KP_SET_ERROR(stat, KPTA_CRYPTO_ENROLL_ERROR)) &&
		KP_SUCCESS(rv = (stat = NCryptSetProperty(hKey, NCRYPT_LENGTH_PROPERTY, (BYTE*)&ulKeySize, sizeof(DWORD), NCRYPT_PERSIST_FLAG)) == ERROR_SUCCESS ? KPTA_OK : KP_SET_ERROR(stat, KPTA_CRYPTO_ENROLL_ERROR)) &&
		KP_SUCCESS(rv = (stat = NCryptSetProperty(hKey, NCRYPT_UI_POLICY_PROPERTY, (BYTE*) &pPolicy, sizeof(NCRYPT_UI_POLICY), NCRYPT_PERSIST_FLAG)) == ERROR_SUCCESS ? KPTA_OK : KP_SET_ERROR(stat, KPTA_CRYPTO_ENROLL_ERROR)) &&
		KP_SUCCESS(rv = (stat = NCryptFinalizeKey(hKey, NCRYPT_WRITE_KEY_TO_LEGACY_STORE_FLAG)) == ERROR_SUCCESS ? KPTA_OK : KP_SET_ERROR(stat, KPTA_CRYPTO_ENROLL_ERROR)) &&
		KP_SUCCESS(rv = CryptExportPublicKeyInfo(hKey, AT_SIGNATURE, REQUEST_ENCODING, NULL, &cbInfo) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR)) &&
		KP_SUCCESS(rv = (pInfo = (CERT_PUBLIC_KEY_INFO*) malloc(cbInfo)) ? KPTA_OK : KPTA_MEMORY_ERROR) &&
		KP_SUCCESS(rv = CryptExportPublicKeyInfo(hKey, AT_SIGNATURE, REQUEST_ENCODING, pInfo, &cbInfo) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR))
	)
	{
		*pPubKey = pInfo;
		*cbPubKey = cbInfo;
	}
	else if (pInfo) free(pInfo);
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __win_generate_CSR)(KP_IN KP_SLOT pSlot, KP_IN KP_STRING userCName, KP_IN KP_ULONG ulKeySize, KP_IN KP_STRING szSigOID, KP_OUT KP_BYTE **out, KP_OUT KP_ULONG *ulOutSize)
{
	KP_RV rv;
	CERT_RDN_ATTR rgName[] = { (LPSTR) szOID_COMMON_NAME, CERT_RDN_PRINTABLE_STRING, 0, NULL };
	CERT_RDN rgRDN[] = { 1, NULL };
	CERT_NAME_INFO Name = { 1, NULL };
	BYTE *bName = NULL, *bEncoded = NULL;
	DWORD cbName = 0, cbInfo = 0, cbEncoded = 0;
	size_t len;
	WCHAR *szProvName = NULL, *szContainer = NULL;
	HCRYPTPROV hProv = NULL;
	HCRYPTKEY hKey = NULL;
	CERT_PUBLIC_KEY_INFO *pInfo = NULL;
	HCRYPTPROV_OR_NCRYPT_KEY_HANDLE hParam = NULL;
	NCRYPT_PROV_HANDLE hNProv = NULL;
	NCRYPT_KEY_HANDLE hNKey = NULL;
	CERT_NAME_BLOB subject;
	CERT_REQUEST_INFO request;
	CRYPT_ALGORITHM_IDENTIFIER hAlg;
	CRYPT_OBJID_BLOB parms = { 0, NULL };
	rgName[0].Value.cbData = strlen(userCName);
	rgName[0].Value.pbData = (BYTE*) userCName;
	rgRDN->rgRDNAttr = &rgName[0];
	Name.rgRDN = rgRDN;
	if
	(
		KP_SUCCESS(rv = CryptEncodeObject(REQUEST_ENCODING, ((LPCSTR) 7), &Name, NULL, &cbName) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR)) &&
		KP_SUCCESS(rv = (bName = (BYTE*) malloc(cbName)) ? KPTA_OK : KPTA_MEMORY_ERROR) &&
		KP_SUCCESS(rv = CryptEncodeObject(REQUEST_ENCODING, ((LPCSTR) 7), &Name, bName, &cbName) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR)) &&
		KP_SUCCESS(rv = (len = mbstowcs(NULL, pSlot->szName, 0)) > 0 ? KPTA_OK : KPTA_MEMORY_ERROR) &&
		KP_SUCCESS(rv = (szProvName = (WCHAR*) malloc((len + 1) * sizeof(WCHAR))) ? KPTA_OK : KPTA_MEMORY_ERROR) &&
		KP_SUCCESS(rv = (len = mbstowcs(szProvName, pSlot->szName, len + 1)) > 0 ? KPTA_OK : KPTA_MEMORY_ERROR)
	)
	{
		if (pSlot->isLegacy)
		{
			if
			(
				KP_SUCCESS(rv = __win_new_legacy_container(szProvName, pSlot->dwProvType, &hProv, &szContainer)) &&
				KP_SUCCESS(rv = __win_generate_legacy_keys(hProv, ulKeySize, &hKey, &pInfo, &cbInfo))
			)	hParam = hProv;
		}
		else
		{
			if
			(
				KP_SUCCESS(rv = __win_new_key_name(szProvName, &hNProv, &hNKey, &szContainer)) &&
				KP_SUCCESS(rv = __win_generate_keys(hNKey, ulKeySize, &pInfo, &cbInfo))
			)	hParam = hNKey;
		}
	}
	if (KP_SUCCESS(rv))
	{
		subject.cbData = cbName;
		subject.pbData = bName;
		request.Subject = subject;
		request.cAttribute = 0;
		request.rgAttribute = NULL;
		request.dwVersion = CERT_REQUEST_V1;
		request.SubjectPublicKeyInfo = *pInfo;
		hAlg.pszObjId = szSigOID;
		hAlg.Parameters = parms;
		if
		(
			KP_SUCCESS(rv = CryptSignAndEncodeCertificate(hParam, AT_SIGNATURE, REQUEST_ENCODING, X509_CERT_REQUEST_TO_BE_SIGNED, &request, &hAlg, NULL, NULL, &cbEncoded) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR)) &&
			KP_SUCCESS(rv = (bEncoded = (BYTE*) malloc(cbEncoded)) ? KPTA_OK : KPTA_MEMORY_ERROR) &&
			KP_SUCCESS(rv = CryptSignAndEncodeCertificate(hParam, AT_SIGNATURE, REQUEST_ENCODING, X509_CERT_REQUEST_TO_BE_SIGNED, &request, &hAlg, NULL, bEncoded, &cbEncoded) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR))
		)
		{
			*out = bEncoded;
			*ulOutSize = cbEncoded;
		}
	}
	else
	{
		if (hProv && szContainer && szProvName)
		{
			CryptAcquireContext(&hProv, szContainer, szProvName, PROV_RSA_FULL, CRYPT_DELETEKEYSET);
			CryptAcquireContext(&hProv, szContainer, szProvName, PROV_RSA_AES, CRYPT_DELETEKEYSET);
		}
	}
	
	if (bName) free(bName);
	if (szProvName) free(szProvName);
	if (hKey) CryptDestroyKey(hKey);
	if (hNKey) NCryptDeleteKey(hNKey, 0);
	if (hProv && KP_SUCCESS(rv)) CryptReleaseContext(hProv, 0);
	if (hNProv) NCryptFreeObject(hNProv);
	if (pInfo) free(pInfo);
	if (szContainer) free(szContainer);
	if (KP_FAIL(rv) && bEncoded) free(bEncoded);
	return rv;
}
#define __platform_generate_CSR					__win_generate_CSR

KP_IMPLEMENTATION(KP_RV, __win_install_certificate)(KP_IN KP_BYTE *bEncoded, KP_IN size_t cbEncoded)
{
	KP_RV rv;
	PCCERT_CONTEXT hCert = NULL;
	CRYPT_KEY_PROV_INFO *hInfo = NULL;
	HCRYPTPROV hProv = NULL;
	SECURITY_STATUS stat;
	NCRYPT_PROV_HANDLE hNProv = NULL;
	NCRYPT_KEY_HANDLE hNKey = NULL;
	CERT_KEY_CONTEXT hKey = { 0, NULL, 0 };
	DWORD cbData;
	HCERTSTORE hStore = NULL;
	if
	(
		KP_SUCCESS(rv = (hCert = CertCreateCertificateContext(REQUEST_ENCODING, bEncoded, cbEncoded)) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR)) &&
		KP_SUCCESS(rv = CryptFindCertificateKeyProvInfo(hCert, CRYPT_FIND_USER_KEYSET_FLAG, NULL) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR)) &&
		KP_SUCCESS(rv = CertGetCertificateContextProperty(hCert, CERT_KEY_PROV_INFO_PROP_ID, NULL, &cbData) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR)) &&
		KP_SUCCESS(rv = (hInfo = (CRYPT_KEY_PROV_INFO*)malloc(cbData)) ? KPTA_OK : KPTA_MEMORY_ERROR) &&
		KP_SUCCESS(rv = CertGetCertificateContextProperty(hCert, CERT_KEY_PROV_INFO_PROP_ID, hInfo, &cbData) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR))
	)
	{
		if (hInfo->dwProvType == 0)
		{
			if
			(
				KP_SUCCESS(rv = (stat = NCryptOpenStorageProvider(&hNProv, hInfo->pwszProvName, 0)) == ERROR_SUCCESS ? KPTA_OK : KP_SET_ERROR(stat, KPTA_CRYPTO_ENROLL_ERROR)) &&
				KP_SUCCESS(rv = (stat = NCryptOpenKey(hNProv, &hNKey, hInfo->pwszContainerName, AT_SIGNATURE, 0)) == ERROR_SUCCESS ? KPTA_OK : KP_SET_ERROR(stat, KPTA_CRYPTO_ENROLL_ERROR))
			)
			{
				hKey.hCryptProv = hNKey;
				hKey.dwKeySpec = CERT_NCRYPT_KEY_SPEC;
				hKey.cbSize = sizeof(hKey);
			}
		}
		else
		{
			if (KP_SUCCESS(rv = CryptAcquireContext(&hProv, hInfo->pwszContainerName, hInfo->pwszProvName, hInfo->dwProvType, 0) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR)))
			{
				hKey.hCryptProv = hProv;
				hKey.dwKeySpec = AT_SIGNATURE;
				hKey.cbSize = sizeof(hKey);
			}
		}
	}
	if
	(
		KP_SUCCESS(rv) &&
		KP_SUCCESS(rv = CertSetCertificateContextProperty(hCert, CERT_KEY_CONTEXT_PROP_ID, 0, &hKey) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR)) &&
		KP_SUCCESS(rv = (hStore = CertOpenSystemStore(NULL, L"MY")) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR))
	)	rv = CertAddCertificateContextToStore(hStore, hCert, CERT_STORE_ADD_NEW, NULL) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR);
	if (hCert) CertFreeCertificateContext(hCert);
	else if (hProv) CryptReleaseContext(hProv, 0);
	if (hStore) CertCloseStore(hStore, 0);
	if (hInfo) free(hInfo);
	if (hNKey) NCryptFreeObject(hNKey);
	if (hNProv) NCryptFreeObject(hNProv);
	return rv;
}
#define __platform_install_cert					__win_install_certificate

KP_IMPLEMENTATION(KP_RV, __win_install_ca_certificate)(KP_IN KP_BYTE *bEncoded, KP_IN size_t cbEncoded, KP_IN int isRoot)
{
	KP_RV rv;
	PCCERT_CONTEXT hCert = NULL;
	HCERTSTORE hStore = NULL;
	LPWSTR szSubsystem = isRoot ? L"Root" : L"CA";
	if
	(
		KP_SUCCESS(rv = (hCert = CertCreateCertificateContext(REQUEST_ENCODING, bEncoded, cbEncoded)) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR)) &&
		KP_SUCCESS(rv = (hStore = CertOpenStore(CERT_STORE_PROV_SYSTEM, X509_ASN_ENCODING | PKCS_7_ASN_ENCODING, NULL, CERT_SYSTEM_STORE_CURRENT_USER, szSubsystem)) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR))
	)	rv = CertAddCertificateContextToStore(hStore, hCert, CERT_STORE_ADD_NEW, NULL) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_ENROLL_ERROR);
	if (hCert) CertFreeCertificateContext(hCert);
	if (hStore) CertCloseStore(hStore, 0);
	return rv;
}
#define __platform_install_ca_cert				__win_install_ca_certificate


/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *	NSS API implementation
 *	* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
#else
KP_IMPLEMENTATION(KP_RV, __nss_enum_providers)(KP_INOUT KP_ENV env)
{
    /* TODO: Implement NSS slot enumeration */
}
#define __platform_enum_slots					__nss_enum_providers

KP_IMPLEMENTATION(KP_RV, __nss_generate_CSR)(KP_IN KP_SLOT pSlot, KP_IN KP_STRING userCName, KP_IN KP_ULONG ulKeySize, KP_IN KP_STRING szSigOID, KP_OUT KP_BYTE **out, KP_OUT KP_ULONG *ulOutSize)
{
    /* TODO: Implement NSS request generation */
}
#define __platform_generate_CSR					__nss_generate_CSR

KP_IMPLEMENTATION(KP_RV, __nss_install_certificate)(KP_IN KP_STRING szPEM)
{
	/* TODO: Implement NSS install certificate */
}
#define __platform_install_cert					__nss_install_certificate


#endif


/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *	Component interface
 *  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
KP_UTILITY(KP_RV, KPTA_enumerate_slots)(KP_INOUT KP_ENV env, KP_OUT KP_STRING **szSlots, KP_OUT int *count)
{
	KP_RV rv;
	KP_STRING *szNames;
	int i = 0;
	KP_SLOT pSlot;
	env->hLogger->debug(env->hLogger, ENROLLMENT_CATEGORY, KPL_OP_INIT, ENUMERATE_SLOTS_OP, "");
	__release_slots(env->kSlots);
	if (KP_SUCCESS(rv = __platform_enum_slots(env)))
	{
		if (KP_SUCCESS(rv = (szNames = (KP_STRING*) malloc(kh_size(env->kSlots) * sizeof(KP_STRING))) ? KPTA_OK : KPTA_MEMORY_ERROR))
		{
			memset(szNames, 0, kh_size(env->kSlots) * sizeof(KP_STRING));
			kh_foreach_value
			(
				env->kSlots,
				pSlot,
				{
					if (KP_SUCCESS(rv = (szNames[i] = (KP_STRING) malloc(strlen(pSlot->szName) + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR))
					{
						strcpy(szNames[i], pSlot->szName);
						i++;
					}
				}
			);
			if (KP_SUCCESS(rv))
			{
				*szSlots = szNames;
				*count = kh_size(env->kSlots);
				env->hLogger->info(env->hLogger, ENROLLMENT_CATEGORY, KPL_OPERATION_SUCCEEDED, ENUMERATE_SLOTS_OP);
			}
			else env->free_buffer(szNames, kh_size(env->kSlots));
		}
	}
	else __release_slots(env->kSlots);
	if (KP_FAIL(rv)) env->hLogger->error(env->hLogger, ENROLLMENT_CATEGORY, KPL_OPERATION_FAILURE, rv, ENUMERATE_SLOTS_OP);
	return rv;
}
KP_UTILITY(KP_RV, KPTA_generate_CSR)(KP_IN KP_ENV env, KP_IN KP_STRING szSlotName, KP_IN KP_STRING szUserCName, KP_OUT KP_STRING *szPEM)
{
	KP_RV rv = KPTA_OK;
	khiter_t k;
	KP_SLOT pSlot = NULL;
	KP_ULONG ulKeySize, ulDERSize;
	KP_STRING szAlgOID = NULL;
	KP_BYTE *bDER = NULL;
	env->hLogger->debug(env->hLogger, ENROLLMENT_CATEGORY, KPL_OP_INIT, GENERATE_CSR_OP, szSlotName);
	k = kh_get(KP_SLOTS, env->kSlots, szSlotName);
	if (k != kh_end(env->kSlots) && kh_exist(env->kSlots, k)) pSlot = kh_value(env->kSlots, k);
	else rv = KPTA_SLOT_NOT_FOUND_ERROR;
	if
	(
		KP_SUCCESS(rv) &&
		KP_SUCCESS(rv = env->hConfig->get_keysize(env->hConfig, &ulKeySize)) &&
		KP_SUCCESS(rv = env->hConfig->get_sigalgOID(env->hConfig, &szAlgOID))
	)
	{
		env->hLogger->debug(env->hLogger, ENROLLMENT_CATEGORY, KPL_CSR_PARAMS, ulKeySize, szAlgOID);
		if
		(
			KP_SUCCESS(rv = __platform_generate_CSR(pSlot, szUserCName, ulKeySize, szAlgOID, &bDER, &ulDERSize)) &&
			KP_SUCCESS(rv = env->hEncoder->to_pkcs10(bDER, ulDERSize, szPEM))
		)	env->hLogger->info(env->hLogger, ENROLLMENT_CATEGORY, KPL_OPERATION_SUCCEEDED, GENERATE_CSR_OP);
		else env->hLogger->error(env->hLogger, ENROLLMENT_CATEGORY, KPL_OPERATION_FAILURE, rv, GENERATE_CSR_OP);
	}
	else env->hLogger->error(env->hLogger, ENROLLMENT_CATEGORY, KPL_OPERATION_FAILURE, rv, GENERATE_CSR_OP);
	if (szAlgOID) free(szAlgOID);
	if (bDER) free(bDER);
	return rv;
}
KP_UTILITY(KP_RV, KPTA_install_certificate)(KP_IN KP_ENV env, KP_IN KP_STRING szPEM)
{
	KP_RV rv, ignored;
	KP_BYTE *bEncoded = NULL;
	size_t cbEncoded;
	NH_CMS_SD_PARSER hParser = NULL;
	NH_ASN1_PNODE node = NULL, eNode;
	NH_CERTIFICATE_HANDLER hCert = NULL;
	int isRoot;
	env->hLogger->debug(env->hLogger, ENROLLMENT_CATEGORY, KPL_OP_INIT, INSTALL_CERTIFICATE_OP, szPEM);
	if (KP_SUCCESS(rv = env->hEncoder->from_pkcs7(szPEM, &bEncoded, &cbEncoded)) && KP_SUCCESS(rv = NH_cms_parse_signed_data(bEncoded, cbEncoded, &hParser)))
	{
		rv = hParser->certificates && (node = hParser->certificates->child) ? KPTA_OK : KPTA_CERTIFICATES_NOT_FOUND;
		while (KP_SUCCESS(rv) && node)
		{
			if (KP_SUCCESS(rv = NH_parse_certificate(node->identifier, node->size + node->contents - node->identifier, &hCert)))
			{
				eNode = NULL;
				if (KP_SUCCESS(rv = hCert->basic_constraints(hCert, &eNode)))
				{
					if (eNode && (eNode = eNode->child) && ASN_IS_PRESENT(eNode) && *(unsigned char*) eNode->value)
					{
						isRoot = (strcmp(hCert->issuer->stringprep, hCert->subject->stringprep) == 0);
						if
						(
							KP_FAIL(ignored = __platform_install_ca_cert(node->identifier, node->size + node->contents - node->identifier, isRoot))
						)	env->hLogger->warn(env->hLogger, ENROLLMENT_CATEGORY, KPL_CACERT_INSTALL_FAILURE, hCert->subject->stringprep, ignored);
					}
					else rv = __platform_install_cert(node->identifier, node->size + node->contents - node->identifier);
				}
				NH_release_certificate(hCert);
			}
			node = node->next;
		}
		NH_cms_release_sd_parser(hParser);
	}
	if (bEncoded) free(bEncoded);
	if (KP_SUCCESS(rv)) env->hLogger->info(env->hLogger, ENROLLMENT_CATEGORY, KPL_OPERATION_SUCCEEDED, INSTALL_CERTIFICATE_OP);
	else env->hLogger->error(env->hLogger, ENROLLMENT_CATEGORY, KPL_OPERATION_FAILURE, rv, INSTALL_CERTIFICATE_OP);
	return rv;
}