/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Signature verification component
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "verify.h"
#include <string.h>

#if defined (_WIN_API)

#define REQUEST_ENCODING						(DWORD)(PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
KP_IMPLEMENTATION(KP_RV, __check_store)(KP_IN NH_CERTIFICATE_HANDLER hSigner, KP_IN PCCERT_CONTEXT hCtx, KP_IN LPCWSTR szSubsystemProtocol)
{
	KP_RV rv;
	BOOL verified = FALSE;
	HCERTSTORE hStore = NULL;
	PCCERT_CONTEXT hCA = NULL;
	NH_CERTIFICATE_HANDLER hCert;
	rv = (hStore = CertOpenSystemStore(0, szSubsystemProtocol)) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_VERIFY_ERROR);
	while (KP_SUCCESS(rv) && !verified && (hCA = CertFindCertificateInStore(hStore, REQUEST_ENCODING, 0, CERT_FIND_ISSUER_OF, hCtx, hCA)))
	{
		if (KP_SUCCESS(rv = NH_parse_certificate(hCA->pbCertEncoded, hCA->cbCertEncoded, &hCert)))
		{
			verified = KP_SUCCESS(hSigner->verify(hSigner, hCert->pubkey));
			NH_release_certificate(hCert);
		}
	}
	if (hStore) CertCloseStore(hStore, 0);
	if (hCA) CertFreeCertificateContext(hCA);
	if (KP_SUCCESS(rv) && !verified) rv = KPTA_TRY_NEXT_STORE;
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __win_trusted_cert)(KP_IN NH_CERTIFICATE_HANDLER hCert)
{
	KP_RV rv;
	PCCERT_CONTEXT hCtx = NULL;
	if
	(
		KP_SUCCESS(rv = (hCtx = CertCreateCertificateContext(REQUEST_ENCODING, hCert->hParser->encoding, hCert->hParser->length)) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_CRYPTO_VERIFY_ERROR)) &&
		(rv = __check_store(hCert, hCtx, L"CA")) == KPTA_TRY_NEXT_STORE
	)	rv = __check_store(hCert, hCtx, L"Root");
	if (hCtx) CertFreeCertificateContext(hCtx);
	return rv;
}

#define __platform_trusted_cert					__win_trusted_cert
#else
	/* TODO: */
#endif

KP_IMPLEMENTATION(KP_STRING, bin2hex)(KP_IN KP_BYTE *bin, KP_IN size_t len)
{
	KP_STRING out;
	size_t i;
	if (!bin || !len) return NULL;

	if ((out = malloc(len * 2 + 1)))
	{
		for (i = 0; i < len; i++)
		{
			out[i * 2    ] = "0123456789ABCDEF"[bin[i] >> 4  ];
			out[i * 2 + 1] = "0123456789ABCDEF"[bin[i] & 0x0F];
		}
		out[len * 2] = '\0';
	}
	return out;
}

KP_UTILITY(KP_RV, KPTA_signer_id)(KP_IN KP_ENV env, KP_IN KP_STRING szCMS, KP_OUT KP_JSON_BUILDER *hOut)
{
	KP_RV rv;
	KP_BYTE *pEncoding = NULL;
	size_t cbLen;
	NH_CMS_SD_PARSER hParser = NULL;
	NH_CMS_ISSUER_SERIAL sid;
	KP_JSON_BUILDER hBuilder = NULL;
	KP_STRING buffer = NULL;
	env->hLogger->debug(env->hLogger, VALIDATION_CATEGORY, KPL_OP_INIT, GET_SIGNER_ID_OP, szCMS);
	if
	(
		KP_SUCCESS(rv = env->hEncoder->from_pkcs7(szCMS, &pEncoding, &cbLen)) &&
		KP_SUCCESS(rv = NH_cms_parse_signed_data(pEncoding, cbLen, &hParser)) &&
		KP_SUCCESS(rv = hParser->get_sid(hParser, 0, &sid)) &&
		KP_SUCCESS(rv = env->new_json_builder(&hBuilder)) &&
		KP_SUCCESS(rv = hBuilder->begin_write(hBuilder))
	)
	{
		if
		(
			sid->name &&
			sid->serial &&
			KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "issuer", sid->name->stringprep)) &&
			KP_SUCCESS(rv = (buffer = bin2hex((KP_BYTE*) sid->serial->value, sid->serial->valuelen)) ? KPTA_OK : KPTA_MEMORY_ERROR)
		)	rv = hBuilder->put_string(hBuilder, "serial", buffer);
		else if (KP_SUCCESS(rv = (buffer = bin2hex((KP_BYTE*) sid->keyIdentifier->value, sid->keyIdentifier->valuelen)) ? KPTA_OK : KPTA_MEMORY_ERROR))
			rv = hBuilder->put_string(hBuilder, "subjectKeyIdentifier", buffer);
	}
	if (KP_SUCCESS(rv))
	{
		hBuilder->end_write(hBuilder);
		*hOut = hBuilder;
		env->hLogger->debug(env->hLogger, VALIDATION_CATEGORY, KPL_SIGNER_ID, (char*)hBuilder->_buffer);
		env->hLogger->info(env->hLogger, VALIDATION_CATEGORY, KPL_OPERATION_SUCCEEDED, GET_SIGNER_ID_OP);
	}
	else env->hLogger->error(env->hLogger, VALIDATION_CATEGORY, KPL_OPERATION_FAILURE, rv, GET_SIGNER_ID_OP);
	if (pEncoding) free(pEncoding);
	if (hParser) NH_cms_release_sd_parser(hParser);
	if (KP_FAIL(rv) && hBuilder) env->delete_json_builder(hBuilder);
	if (buffer) free(buffer);
	return rv;
}

KP_UTILITY(KP_RV, KPTA_signer_cert)(KP_IN KP_ENV env, KP_IN KP_STRING szCMS, KP_OUT KP_STRING *szOut)
{
	KP_RV rv;
	KP_BYTE *pEncoding = NULL;
	size_t cbSize;
	NH_CMS_SD_PARSER hCMS = NULL;
	NH_CMS_ISSUER_SERIAL sid;
	NH_CERTIFICATE_HANDLER hCert = NULL;
	env->hLogger->debug(env->hLogger, VALIDATION_CATEGORY, KPL_OP_INIT, GET_SIGNER_CERT_OP, szCMS);

	if
	(
		KP_SUCCESS(rv = env->hEncoder->from_pkcs7(szCMS, &pEncoding, &cbSize)) &&
		KP_SUCCESS(rv = NH_cms_parse_signed_data(pEncoding, cbSize, &hCMS)) &&
		KP_SUCCESS(rv = hCMS->get_sid(hCMS, 0, &sid)) &&
		KP_SUCCESS(rv = hCMS->get_cert(hCMS, sid, &hCert))
	)	rv = env->hEncoder->to_cert(hCert->hParser->encoding, hCert->hParser->length, szOut);
	if (KP_SUCCESS(rv))
	{
		env->hLogger->debug(env->hLogger, VALIDATION_CATEGORY, KPL_SIGNER_CERT, *szOut);
		env->hLogger->info(env->hLogger, VALIDATION_CATEGORY, KPL_OPERATION_SUCCEEDED, GET_SIGNER_CERT_OP);
	}
	else env->hLogger->error(env->hLogger, VALIDATION_CATEGORY, KPL_OPERATION_FAILURE, rv, GET_SIGNER_CERT_OP);
	if (pEncoding) free(pEncoding);
	if (hCMS) NH_cms_release_sd_parser(hCMS);
	if (hCert) NH_release_certificate(hCert);
	return rv;
}

KP_UTILITY(KP_RV, KPTA_content)(KP_IN KP_ENV env, KP_IN KP_STRING szCMS, KP_OUT KP_STRING *szOut)
{
	KP_RV rv;
	KP_BYTE *pEncoding = NULL;
	size_t cbSize;
	NH_CMS_SD_PARSER hCMS = NULL;
	NH_ASN1_PNODE eContent;
	KP_STRING szContent;
	env->hLogger->debug(env->hLogger, VALIDATION_CATEGORY, KPL_OP_INIT, GET_CONTENT_OP, szCMS);
	if
	(
		KP_SUCCESS(rv = env->hEncoder->from_pkcs7(szCMS, &pEncoding, &cbSize)) &&
		KP_SUCCESS(rv = NH_cms_parse_signed_data(pEncoding, cbSize, &hCMS)) &&
		KP_SUCCESS(rv = (eContent = hCMS->hParser->sail(hCMS->encapContentInfo, NH_ECONTENT_INFO_PATH)) ? KPTA_OK : KPTA_CMS_SD_NOECONTENT_ERROR) &&
		KP_SUCCESS(rv = env->hEncoder->encode(eContent->value, eContent->valuelen, &szContent))
	)
	{
		env->hLogger->info(env->hLogger, VALIDATION_CATEGORY, KPL_OPERATION_SUCCEEDED, GET_CONTENT_OP);
		env->hLogger->debug(env->hLogger, VALIDATION_CATEGORY, KPL_ECONTENT, szContent);
		*szOut = szContent;
	}
	else env->hLogger->error(env->hLogger, VALIDATION_CATEGORY, KPL_OPERATION_FAILURE, rv, GET_CONTENT_OP);
	if (pEncoding) free(pEncoding);
	if (hCMS) NH_cms_release_sd_parser(hCMS);
	return rv;
}

KP_UTILITY(KP_RV, KPTA_trusted)(KP_IN KP_ENV env, KP_IN KP_STRING szCert)
{
	KP_RV rv;
	KP_BYTE *pEncoding = NULL;
	size_t cbSize;
	NH_CERTIFICATE_HANDLER hCert = NULL;
	env->hLogger->debug(env->hLogger, VALIDATION_CATEGORY, KPL_OP_INIT, TRUSTED_CERT_OP, szCert);
	if
	(
		KP_SUCCESS(rv = env->hEncoder->from_cert(szCert, &pEncoding, &cbSize)) &&
		KP_SUCCESS(rv = NH_parse_certificate(pEncoding, cbSize, &hCert))
	)	rv = __platform_trusted_cert(hCert);
	if (KP_SUCCESS(rv)) env->hLogger->info(env->hLogger, VALIDATION_CATEGORY, KPL_OPERATION_SUCCEEDED, TRUSTED_CERT_OP);
	else env->hLogger->error(env->hLogger, VALIDATION_CATEGORY, KPL_OPERATION_FAILURE, rv, TRUSTED_CERT_OP); 
	if (pEncoding) free(pEncoding);
	if (hCert) NH_release_certificate(hCert);
	return rv;
}

KP_UTILITY(KP_RV, KPTA_verify)(KP_IN KP_ENV env, KP_IN KP_STRING szCMS, KP_IN KP_STRING szContent, KP_IN int isBase64, KP_IN KP_STRING szCert)
{
	KP_RV rv;
	KP_STRING szParams;
	KP_BYTE *pCMSEncoding = NULL, *pCertEncoding = NULL, *pContent = NULL;
	size_t cbCMSSize, cbCertSize, cbSize = 0, cbParams;
	NH_CMS_SD_PARSER hCMS = NULL;
	NH_CERTIFICATE_HANDLER hCert = NULL;
	if
	(
		env->hLogger->level == KP_LOG_DEBUG &&
		(cbParams = strlen(szCMS) + strlen(szContent) + strlen(szCert) + 512) &&
		(szParams = (KP_STRING) malloc(cbParams))
	)
	{
		snprintf(szParams, cbParams, "szCMS=%s, szContent=%s, isBase64=%d, szCert=%s\n", szCMS, szContent, isBase64, szCert);
		env->hLogger->debug(env->hLogger, VALIDATION_CATEGORY, KPL_OP_INIT, VERIFY_SIGNATURE_OP, szParams);
		free(szParams);
	}
	if
	(
		KP_SUCCESS(rv = env->hEncoder->from_pkcs7(szCMS, &pCMSEncoding, &cbCMSSize)) &&
		KP_SUCCESS(rv = NH_cms_parse_signed_data(pCMSEncoding, cbCMSSize, &hCMS)) &&
		KP_SUCCESS(rv = env->hEncoder->from_cert(szCert, &pCertEncoding, &cbCertSize)) &&
		KP_SUCCESS(rv = NH_parse_certificate(pCertEncoding, cbCertSize, &hCert))
	)
	{
		if (isBase64) rv = env->hEncoder->decode(szContent, &pContent, &cbSize);
		else
		{
			pContent = (KP_BYTE* ) szContent;
			cbSize = strlen(szContent);
		}
	}
	if (KP_SUCCESS(rv) && KP_SUCCESS(rv = hCMS->verify(hCMS, 0, hCert->pubkey))) rv = hCMS->validate(hCMS, pContent, cbSize);
	if (KP_SUCCESS(rv)) env->hLogger->info(env->hLogger, VALIDATION_CATEGORY, KPL_OPERATION_SUCCEEDED, VERIFY_SIGNATURE_OP);
	else env->hLogger->error(env->hLogger, VALIDATION_CATEGORY, KPL_OPERATION_FAILURE, rv, VERIFY_SIGNATURE_OP); 
	if (pCMSEncoding) free(pCMSEncoding);
	if (hCMS) NH_cms_release_sd_parser(hCMS);
	if (pCertEncoding) free(pCertEncoding);
	if (hCert) NH_release_certificate(hCert);
	if (isBase64 && pContent) free(pContent);
	return rv;
}

KP_UTILITY(KP_RV, KPTA_verify_all)(KP_IN KP_ENV env, KP_IN KP_STRING szCMS)
{
	KP_RV rv;
	KP_BYTE *pEncoding = NULL;
	size_t cbSize;
	NH_CMS_SD_PARSER hCMS = NULL;
	NH_CMS_ISSUER_SERIAL sid;
	NH_CERTIFICATE_HANDLER hCert = NULL;
	env->hLogger->debug(env->hLogger, VALIDATION_CATEGORY, KPL_OP_INIT, VERIFY_ALL_OP, szCMS);
	if
	(
		KP_SUCCESS(rv = env->hEncoder->from_pkcs7(szCMS, &pEncoding, &cbSize)) &&
		KP_SUCCESS(rv = NH_cms_parse_signed_data(pEncoding, cbSize, &hCMS)) &&
		KP_SUCCESS(rv = hCMS->get_sid(hCMS, 0, &sid)) &&
		KP_SUCCESS(rv = hCMS->get_cert(hCMS, sid, &hCert)) &&
		KP_SUCCESS(rv = __platform_trusted_cert(hCert)) &&
		KP_SUCCESS(rv = hCMS->verify(hCMS, 0, hCert->pubkey))
	)	rv = hCMS->validate_attached(hCMS);
	if (KP_SUCCESS(rv)) env->hLogger->info(env->hLogger, VALIDATION_CATEGORY, KPL_OPERATION_SUCCEEDED, VERIFY_ALL_OP);
	else env->hLogger->error(env->hLogger, VALIDATION_CATEGORY, KPL_OPERATION_FAILURE, rv, VERIFY_ALL_OP); 
	if (pEncoding) free(pEncoding);
	if (hCMS) NH_cms_release_sd_parser(hCMS);
	if (hCert) NH_release_certificate(hCert);
	return rv;
}