/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Environment object implementation
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "kryptonite.h"

KP_IMPLEMENTATION(KP_RV, __get_logger)(KP_OUT KP_LOG *out)
{
	KP_RV rv;
	KP_LOG hLogger;
	if (KP_SUCCESS(rv = new_logger(&hLogger)))
	{
		if (KP_SUCCESS(rv = hLogger->open(hLogger))) *out = hLogger;
		else delete_logger(hLogger);
	}
	return rv;
}
#if (defined(_WIN32))
	#define REGISTRY_KEY			"Software\\Crypthing\\Kryptonite"
	#define REGISTRY_VALUE_NAME		NULL
#else

	#include <unistd.h>
	#include <sys/types.h>
	#include <pwd.h>
	#include <string.h>
	#define REGISTRY_KEY			"/.config/kryptonite/config"

KP_IMPLEMENTATION(char*, __get_home_dir)()
{
	const char *homedir;
	if ((homedir = getenv("HOME")) == NULL) homedir = getpwuid(getuid())->pw_dir;
	return homedir;
}

#endif
KP_IMPLEMENTATION(KP_RV, __get_home)(KP_IN KP_LOG hLogger, KP_OUT KP_STRING *szLocation)
{
	KP_RV rv;
	KP_STRING szData = NULL;
#if defined(_WIN32)
	LSTATUS ret;
	DWORD cbData = 0;
	if
	(
		KP_SUCCESS
		(
			rv = (ret = RegGetValueA(HKEY_CURRENT_USER, REGISTRY_KEY, REGISTRY_VALUE_NAME, RRF_RT_REG_SZ, NULL, NULL, &cbData)) == ERROR_SUCCESS ?
				KPTA_OK :
				KP_SET_ERROR(ret, KPTA_WINREGISTRY_ERROR)
		) &&
		KP_SUCCESS(rv = (szData = (KP_STRING) malloc(cbData + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR)
	)
	{
		hLogger->debug(hLogger, ENVIRONMENT_CATEGORY, KPL_OPERATION_SUCCEEDED, GET_REGVALUE_OP);
		rv = (ret = RegGetValueA(HKEY_CURRENT_USER, REGISTRY_KEY, REGISTRY_VALUE_NAME, RRF_RT_REG_SZ, NULL, szData, &cbData)) == ERROR_SUCCESS ? KPTA_OK : KP_SET_ERROR(ret, KPTA_WINREGISTRY_ERROR);
		szData[cbData-1] = 0;
	}
#else
	char szConfigFile[MAX_PATH], buffer[MAX_PATH];
	FILE *in;
	size_t len;
	strcpy(szConfigFile, __get_home_dir());
	strcat(szConfigFile, REGISTRY_KEY);
	if (KP_SUCCESS(rv = (hLog = fopen(szConfigFile, "r")) ? KPTA_OK : KP_SET_ERROR(errno, KPTA_IO_FILE_ERROR)))
	{
		if (KP_SUCCESS(rv = fgets(buffer, MAX_PATH, in) ? KPTA_OK : KPTA_IO_FILE_ERROR))
		{
			len = strlen(buffer);
			if (buffer[len - 1] == '\n') --len;
			if (KP_SUCCESS(rv = (szData = (KP_STRING) malloc(len)) ? KPTA_OK : KPTA_MEMORY_ERROR))
			{
				memset(szData, 0, len);
				memcpy(szData, buffer, len - 1);
			}
		}
		fclose(in);
	}
#endif
	if (KP_SUCCESS(rv))
	{
		*szLocation = szData;
		hLogger->info(hLogger, ENVIRONMENT_CATEGORY, KPL_OPERATION_SUCCEEDED, INSTALL_DIR_OP);
	}
	else
	{
		hLogger->error(hLogger, ENVIRONMENT_CATEGORY, KPL_OPERATION_FAILURE, rv, INSTALL_DIR_OP);
		if (szData) free(szData);
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __get_config)(KP_IN KP_LOG hLogger, KP_IN KP_STRING szLocation, KP_OUT KP_CONFIG *out)
{
	KP_RV rv;
	KP_CONFIG hConfig;
	if (KP_SUCCESS(rv = new_config(&hConfig)))
	{
		if (KP_SUCCESS(rv = hConfig->load(hConfig, szLocation)))
		{
			hLogger->info(hLogger, ENVIRONMENT_CATEGORY, KPL_OPERATION_SUCCEEDED, CONFIG_JSON_OP);
			*out = hConfig;
		}
		else
		{
			hLogger->error(hLogger, ENVIRONMENT_CATEGORY, KPL_OPERATION_FAILURE, rv, CONFIG_JSON_OP);
			delete_config(hConfig);
		}
	}
	else hLogger->error(hLogger, ENVIRONMENT_CATEGORY, KPL_OBJECT_CREATION_FAILURE, rv, "KP_CONFIG");
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __load_executor)(KP_IN KP_STRING szComponent, KP_LIBHANDLE *hMOut, KP_FUNCTION *hPOut)
{
	KP_RV rv;
	KP_LIBHANDLE hModule = NULL;
	KP_FUNCTION hProc = NULL;
#if defined(_WIN32)
	if (KP_SUCCESS(rv = (hModule = LoadLibraryA(szComponent)) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_LOADDLL_ERROR)))
	{
		if (KP_SUCCESS(rv = (hProc = (KP_FUNCTION) GetProcAddress(hModule, KP_COMPONENT_EXECUTOR_NAME)) ? KPTA_OK : KP_SET_ERROR(GetLastError(), KPTA_LOADDLL_ERROR)))
		{
			*hMOut = hModule;
			*hPOut = hProc;
		}
		else FreeLibrary(hModule);
	}
#else
	if (KP_SUCCESS(rv = (hModule = dlopen(szComponent, RTLD_LAZY)) ? KPTA_OK : KPTA_LOADSO_ERROR))
	{
		if (KP_SUCCESS(rv = (hProc = (KP_FUNCTION) dlsym(hModule, KP_COMPONENT_EXECUTOR_NAME)) ? KPTA_OK : KPTA_LOADSO_ERROR))
		{
			*hMOut = hModule;
			*hPOut = hProc;
		}
		else dlclose(hModule);
	}
#endif
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __load_component)(KP_IN KP_STRING szID, KP_IN KP_STRING szComponent, KP_OUT KP_COMPONENT *out)
{
	KP_RV rv;
	KP_COMPONENT hComponent = NULL;
	KP_LIBHANDLE hModule = NULL;
	KP_FUNCTION hProc = NULL;
	if (KP_SUCCESS(rv = (hComponent = (KP_COMPONENT) malloc(sizeof(KP_COMPONENT_STR))) ? KPTA_OK : KPTA_MEMORY_ERROR))
	{
		memset(hComponent, 0, sizeof(KP_COMPONENT_STR));
		if (KP_SUCCESS(rv = (hComponent->szID = (KP_STRING) malloc(strlen(szID) + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR))
		{
			strcpy(hComponent->szID, szID);
			if (KP_SUCCESS(rv = __load_executor(szComponent, &hModule, &hProc)))
			{
				hComponent->hModule = hModule;
				hComponent->hProc = hProc;
				hComponent->bUnloaded = FALSE;
				*out = hComponent;
			}
		}
	}
	if (KP_SUCCESS(rv) && hComponent)
	{
		hComponent->hModule = hModule;
		hComponent->hProc = hProc;
		*out = hComponent;
	}
	else
	{
		if (hComponent->szID) free(hComponent->szID);
		free(hComponent);
	}
	return rv;
}
KP_IMPLEMENTATION(void, __unload_executor)(KP_INOUT KP_COMPONENT hComponent)
{
	if (hComponent)
	{
		if (hComponent->hModule)
		{
#if defined(_WIN32)
			FreeLibrary(hComponent->hModule);
#else
			dlclose(hComponent->hComponent);
#endif
		}
	}
}
KP_IMPLEMENTATION(void, __unload_component)(KP_INOUT KP_COMPONENT hComponent)
{
	__unload_executor(hComponent);
	if (hComponent)
	{
		if (hComponent->szID) free(hComponent->szID);
		free(hComponent);
	}
}
KP_IMPLEMENTATION(KP_COMPONENT, __get_component)(KP_INOUT KP_ENVIRONMENT_STR *self, KP_IN KP_STRING szID)
{
	khint_t k;
	KP_RV rv = KPTA_OK;
	KP_STRING szComponent = NULL;
	KP_COMPONENT hComponent = NULL;
	int ret;

	if ((k = kh_get(KP_COMPONENTS, self->kMap, szID)) != kh_end(self->kMap) && kh_exist(self->kMap, k)) hComponent = kh_val(self->kMap, k);
	else if (KP_SUCCESS(rv = self->get_component_path(self, szID, &szComponent)) && KP_SUCCESS(rv = __load_component(szID, szComponent, &hComponent)))
	{
		k = kh_put(KP_COMPONENTS, self->kMap, hComponent->szID, &ret);
		switch (ret)
		{
		case -1:
			rv = KPTA_PUT_MAP_ERROR;
			break;
		case  0:
			rv = KPTA_COMPONENT_MAP_ERROR;
			break;
		default:
			kh_val(self->kMap, k) = hComponent;
		}
	}
	if (KP_SUCCESS(rv)) self->hLogger->debug(self->hLogger, ENVIRONMENT_CATEGORY, KPL_OP_VALUE_SUCCEEDED, GET_COMPONENT_OP, szID);
	else
	{
		self->hLogger->error(self->hLogger, ENVIRONMENT_CATEGORY, KPL_OP_VALUE_FAILURE, GET_COMPONENT_OP, szID, rv);
		if (hComponent) __unload_component(hComponent);
	}
	if (szComponent) free(szComponent);
	if (hComponent->bUnloaded) hComponent = NULL;
	return hComponent;
}
KP_IMPLEMENTATION(KP_RV, __new_json)(KP_OUT KP_JSON *hJSON) { return new_json(hJSON); }
KP_IMPLEMENTATION(void, __delete_json)(KP_INOUT KP_JSON hJSON) { delete_json(hJSON); }
KP_IMPLEMENTATION(void, __free_buffer)(KP_INOUT KP_STRING *buffer, KP_IN int count)
{
	int i;
	if (buffer)
	{
		for (i = 0; i < count; i++) if (buffer[i]) free(buffer[i]);
		free(buffer);
	}
}
KP_IMPLEMENTATION(KP_RV, __new_json_builder)(KP_OUT KP_JSON_BUILDER *hBuilder) { return new_json_builder(hBuilder); }
KP_IMPLEMENTATION(void, __delete_json_builder)(KP_INOUT KP_JSON_BUILDER hBuilder) { delete_json_builder(hBuilder); }

KP_IMPLEMENTATION(KP_RV, __component_path)(KP_IN KP_ENVIRONMENT_STR *self, KP_IN KP_STRING szID, KP_OUT KP_STRING *szOut)
{
	KP_RV rv;
	KP_STRING szComponent, szPath;
	char szFilename[MAX_PATH];

	if (KP_SUCCESS(rv = self->hConfig->get_component(self->hConfig, szID, &szComponent)))
	{
		snprintf(szFilename, MAX_PATH, COMPONENT_LOCATION, self->szLocation, szComponent);
		free(szComponent);
		if (KP_SUCCESS(rv = (szPath = (KP_STRING) malloc(strlen(szFilename) + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR))
		{
			strcpy(szPath, szFilename);
			*szOut = szPath;
		}
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __stop_component)(KP_INOUT KP_ENVIRONMENT_STR *env, KP_IN KP_STRING szID)
{
	khint_t k;
	KP_COMPONENT hComponent;

	if ((k = kh_get(KP_COMPONENTS, env->kMap, szID)) != kh_end(env->kMap) && kh_exist(env->kMap, k)) 
	{
		hComponent = kh_val(env->kMap, k);
		hComponent->bUnloaded = TRUE;
		__unload_executor(hComponent);
	}
	return KPTA_OK;
}
KP_IMPLEMENTATION(KP_RV, __start_component)(KP_INOUT KP_ENVIRONMENT_STR *env, KP_IN KP_STRING szID)
{
	KP_RV rv = KPTA_OK;
	khint_t k;
	KP_COMPONENT hComponent;
	KP_STRING szComponent;

	if ((k = kh_get(KP_COMPONENTS, env->kMap, szID)) != kh_end(env->kMap) && kh_exist(env->kMap, k)) 
	{
		if (KP_SUCCESS(rv = __component_path(env, szID, &szComponent)))
		{
			hComponent = kh_val(env->kMap, k);
			if (KP_SUCCESS(rv = __load_executor(szComponent, &hComponent->hModule, &hComponent->hProc))) hComponent->bUnloaded = FALSE;
			free(szComponent);
		}
	}
	return rv;
}

/**
 * Command processor
 */
KP_IMPLEMENTATION(KP_RV, __init_processor)(KP_INOUT KP_PROCESSOR_STR *self, KP_IN KP_JSON hInput)
{
	KP_RV rv;
	KP_JSON_BUILDER output = NULL;
	if (KP_SUCCESS(rv = new_json_builder(&output)) && KP_SUCCESS(rv = output->begin_write(output)))
	{
		if (self->hOutput) delete_json_builder(self->hOutput);
		self->hInput = hInput;
		self->hOutput = output;
	}
	if (KP_FAIL(rv) && output) delete_json_builder(output);
	return rv;
}
KP_IMPLEMENTATION(const kson_node_t*, __command_processor)(KP_IN KP_PROCESSOR_STR *self)
{
	const kson_node_t *p = NULL;
	
	if (self->hInput) p = self->hInput->by_path(self->hInput, 3, "payload", "payload", "commandID");
	return p;
}
KP_IMPLEMENTATION(KP_RV, __finish_processor)(KP_IN KP_PROCESSOR_STR *self, KP_IN KP_RV reason)
{
	KP_RV rv;
	char _reason[24], *_result = KP_SUCCESS(reason) ? "0" : "1";
	sprintf(_reason, "%llu", reason);
	if (KP_SUCCESS(rv = self->hOutput->put_number(self->hOutput, "result", _result)) && KP_SUCCESS(rv = self->hOutput->put_number(self->hOutput, "reason", _reason))) self->hOutput->end_write(self->hOutput);
	return rv;
}
static const KP_PROCESSOR_STR default_processor =
{
	NULL,
	NULL,
	__init_processor,
	__command_processor,
	__finish_processor
};
KP_IMPLEMENTATION(KP_RV, __new_processor)(KP_OUT KP_PROCESSOR *self)
{
	KP_RV rv;
	KP_PROCESSOR out;
	if (KP_SUCCESS(rv = (out = (KP_PROCESSOR) malloc(sizeof(KP_PROCESSOR_STR))) ? KPTA_OK : KPTA_MEMORY_ERROR))
	{
		memcpy(out, &default_processor, sizeof(KP_PROCESSOR_STR));
		*self = out;
	}
	return rv;
}
KP_IMPLEMENTATION(void, __delete_processor)(KP_INOUT KP_PROCESSOR self)
{
	if (self) free(self);
}

static const KP_ENVIRONMENT_STR default_env =
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,

	__get_component,
	__new_json,
	__delete_json,
	__free_buffer,
	__new_json_builder,
	__delete_json_builder,
	__new_processor,
	__delete_processor,
	__component_path,
	__stop_component,
	__start_component
};
KP_NEW(new_environment)(KP_OUT KP_ENV *self)
{
	KP_RV rv;
	KP_LOG hLogger = NULL;
	KP_STRING szLocation = NULL;
	KP_CONFIG hConfig = NULL;
	KP_BASE64 hEncoder = NULL;
	KP_ENV env = NULL;
	if (KP_SUCCESS(rv = (env = (KP_ENV) malloc(sizeof(KP_ENVIRONMENT_STR))) ? KPTA_OK : KPTA_MEMORY_ERROR))
	{
		memcpy(env, &default_env, sizeof(KP_ENVIRONMENT_STR));
		if
		(
			KP_SUCCESS(rv = __get_logger(&hLogger)) &&
			KP_SUCCESS(rv = __get_home(hLogger, &szLocation)) &&
			KP_SUCCESS(rv = __get_config(hLogger, szLocation, &hConfig)) &&
			KP_SUCCESS(rv = (env->kMap = kh_init(KP_COMPONENTS)) ? KPTA_OK : KPTA_MEMORY_ERROR) &&
			KP_SUCCESS(rv = (env->kSlots = kh_init(KP_SLOTS)) ? KPTA_OK : KPTA_MEMORY_ERROR) &&
			KP_SUCCESS(rv = new_base64(&hEncoder))
		)
		{
			env->szLocation = szLocation;
			env->hLogger = hLogger;
			env->hConfig = hConfig;
			env->hEncoder = hEncoder;
			*self = env;
			hLogger->info(hLogger, ENVIRONMENT_CATEGORY, KPL_OPERATION_SUCCEEDED, ENVIRONMENT_OP);
		}
		else
		{
			if (hLogger) hLogger->error(hLogger, ENVIRONMENT_CATEGORY, KPL_OPERATION_FAILURE, rv, ENVIRONMENT_OP);
			delete_environment(env);
		}
	}
	return rv;
}
KP_DELETE(delete_environment)(KP_INOUT KP_ENV env)
{
	khiter_t k;
	KP_SLOT pSlot;
	if (env)
	{
		if (env->hLogger) delete_logger(env->hLogger);
		if (env->szLocation) free(env->szLocation);
		if (env->hConfig) delete_config(env->hConfig);
		if (env->kMap)
		{
	    	for (k = kh_begin(env->kMap); k != kh_end(env->kMap); ++k) if (kh_exist(env->kMap, k)) __unload_component(kh_value(env->kMap, k));
			kh_destroy(KP_COMPONENTS, env->kMap);
		}
		if (env->kSlots)
		{
	    	for (k = kh_begin(env->kSlots); k != kh_end(env->kSlots); ++k) if (kh_exist(env->kSlots, k))
			{
				if ((pSlot = kh_value(env->kSlots, k)))
				{
					if (pSlot->szName) free(pSlot->szName);
#if defined (_NSS_API)
					/* TODO: free NSS API resources */
#endif
					free(pSlot);
				}
			}
			kh_destroy(KP_SLOTS, env->kSlots);
		}
		if (env->hEncoder) delete_base64(env->hEncoder);
		free(env);
	}
}
