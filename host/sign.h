/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Signing component
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * 
 * See https://tools.ietf.org/html/rfc3852
 * See https://tools.ietf.org/html/rfc5126
 *  * * * * * * * * * * * * * * * * * * * * * * */
#ifndef __SIGN_H__
#define __SIGN_H__

#include "kryptonite.h"

#if defined(_WIN_API)
	#include <Windows.h>
	#include <wincrypt.h>
	typedef PCCERT_CONTEXT				KP_CERT_HANDLE;
#else
	#include <cert.h>
	typedef CERTCertificate*			KP_CERT_HANDLE;
#endif
typedef struct KP_CERT_HANDLER_STR		/* Internal handler to certificate */
{
	NH_CERTIFICATE_HANDLER	hNharu;		/* Nharu handler */
	KP_CERT_HANDLE			hNative;	/* Native handler */

} KP_CERT_HANDLER_STR,					*KP_CERTIFICATE_HANDLER;
#define CADES_BES_POLICY				"CAdES-BES"


#if defined(__cplusplus)
KP_EXTERN {
#endif

/**
 * enumerate_certs JSON response:
 * {
 * 		"certificates":
 * 	[
 * 		{
 * 			"subject": "CN=User of legacy provider 1",
 * 			"issuer": "C=BR, O=PKI Brazil, OU=PKI Ruler for All Cats, CN=Common Name for All Cats End User CA",
 * 			"serial": "04"
 * 		},
 * 		{
 * 			"subject": "CN=User of provider 1",
 * 			"issuer": "C=BR, O=PKI Brazil, OU=PKI Ruler for All Cats, CN=Common Name for All Cats End User CA",
 * 			"serial": "05"
 * 		}
 * 	]
 * }
 * 
 */
KP_UTILITY(KP_RV, KPTA_enumerate_certs)		/* Enumerate signed certificates installed in personal storage */
(
    KP_IN KP_ENV,                    	/* Environment */
    KP_OUT KP_JSON_BUILDER*				/* Certificate list */
);
KP_UTILITY(KP_RV, KPTA_sign)			/* Sign specified content */
(
	KP_IN KP_ENV,						/* Environment */
	KP_IN KP_STRING,					/* Signing certificate (one of those returned by enumerate_certs) */
	KP_IN KP_STRING,					/* Content to be signed */
	KP_IN int,							/* szData Base64 encoding indicator */
	KP_IN int,							/* Attach content indicator */
	KP_OUT KP_STRING*					/* PEM encoded CMS output. Must be freed after use */
);

#if defined(__cplusplus)
}
#endif



#endif /* __SIGN_H__ */