/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * JSON documents manager facility
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "kryptonite.h"
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdarg.h>
#include <string.h>

KP_IMPLEMENTATION(KP_RV, __escape)(KP_IN KP_STRING szIn, KP_OUT KP_STRING *szOut)
{
	KP_RV rv;
	size_t size = strlen(szIn);
	KP_STRING buffer;
	char *c, *d;
	if (KP_SUCCESS(rv = (buffer = (KP_STRING) malloc(size * 2 + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR))
	{
		c = szIn;
		d = buffer;
		while (*c)
		{
			switch (*c)
			{
			case 0x22: *d++ = '\\'; *d = '"';  break;
			case 0x5C: *d++ = '\\'; *d = '\\'; break;
			case 0x08: *d++ = '\\'; *d = 'b';  break;
			case 0x0C: *d++ = '\\'; *d = 'f';  break;
			case 0x0A: *d++ = '\\'; *d = 'n';  break;
			case 0x0D: *d++ = '\\'; *d = 'r';  break;
			case 0x09: *d++ = '\\'; *d = 't';  break;
			default: *d = *c;
			}
			c++; d++;
		}
		*d = 0;
		*szOut = buffer;
	}
	return rv;
}

#define ROUNDUP(x)	(--(x), (x)|=(x)>>1, (x)|=(x)>>2, (x)|=(x)>>4, (x)|=(x)>>8, (x)|=(x)>>16, ++(x))
KP_IMPLEMENTATION(KP_RV, __read_from_file)(KP_INOUT KP_JSON_STR *self, KP_IN KP_STRING szFilename)
{
	KP_RV rv;
	char buf[512], *json = NULL;
	int i, max = 0, len = 0;
	FILE *fp;
	if
	(
		KP_SUCCESS(rv = !self->raw ? KPTA_OK : KPTA_INVALID_STATE_OBJECT) &&
		KP_SUCCESS(rv = (fp = fopen(szFilename, "r")) ? KPTA_OK : KP_SET_ERROR(errno, KPTA_IO_FILE_ERROR))
	)
	{
		while ((i = fread(buf, 1, sizeof(buf), fp)))
		{
			if (len + i + 1 > max)
			{
				max = len + i + 1;
				ROUNDUP(max);
				json = (char*) realloc(json, max);
			}
			memcpy(json + len, buf, i);
			len += i;
		}
		if (feof(fp))
		{
			json[len] = 0;
			self->raw = json;
		}
		else
		{
			rv = KP_SET_ERROR(ferror(fp), KPTA_IO_FILE_ERROR);
			if (json) free(json);
		}
		fclose(fp);
	}
	return rv;
}

KP_IMPLEMENTATION(KP_RV, __read_from_stdin)(KP_INOUT KP_JSON_STR *self)
{
	KP_RV rv;
	unsigned int length = 0, i = 0;
	unsigned int read_char;
	KP_STRING json;

	rv = !self->raw ? KPTA_OK : KPTA_INVALID_STATE_OBJECT;
	while (KP_SUCCESS(rv) && i < 4)
	{
		length |= ((read_char = getchar()) << i * 8);
		i++;
	}
	if (KP_SUCCESS(rv) && KP_SUCCESS(rv = (json = (KP_STRING) malloc(length + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR))
	{
		for (i = 0; i < length; i++) json[i] = getchar();
		json[length] = 0;
		self->raw = json;
	}
	return rv;
}

KP_IMPLEMENTATION(KP_RV, __read_from_template)(KP_INOUT KP_JSON_STR *self, KP_IN KP_STRING szTemplate, ...)
{
	KP_RV rv;
	va_list argp;
	char *json;
	int len;
	size_t size = strlen(szTemplate) + 512;
	if
	(
		KP_SUCCESS(rv = (!self->raw ? KPTA_OK : KPTA_INVALID_STATE_OBJECT)) &&
		KP_SUCCESS(rv = (szTemplate ? KPTA_OK : KPTA_INVALID_ARG)) &&
		KP_SUCCESS(rv = (json = (char*) malloc(size)) ? KPTA_OK : KPTA_MEMORY_ERROR)
	)
	{
		va_start(argp, szTemplate);
		len = vsnprintf(json, size, szTemplate, argp);
		va_end(argp);
		if (KP_SUCCESS(rv = len > 0 ? KPTA_OK : KPTA_TEMPLATE_ERROR))
		{
			if ((int) size < len)
			{
				free(json);
				size = len + 1;
				if (KP_SUCCESS(rv = (json = (char*) malloc(size)) ? KPTA_OK : KPTA_MEMORY_ERROR))
				{
					va_start(argp, szTemplate);
					len = vsnprintf(json, size, szTemplate, argp);
					va_end(argp);
				}
			}
			if (len < 0 || (int) size < len)
			{
				free(json);
				rv = (KP_RV) KPTA_TEMPLATE_ERROR;
			}
			else self->raw = json;
		}
	}
	return rv;
}

KP_IMPLEMENTATION(KP_RV, __unlade_to_disk)(KP_IN KP_JSON_STR *self, KP_IN KP_STRING szFilename)
{
	KP_RV rv;
	FILE *fp;
	if
	(
		KP_SUCCESS(rv = (self->raw ? KPTA_OK : KPTA_INVALID_STATE_OBJECT)) &&
		KP_SUCCESS(rv = (szFilename ? KPTA_OK : KPTA_INVALID_ARG)) &&
		KP_SUCCESS(rv = (fp = fopen(szFilename, "w")) ? KPTA_OK : KP_SET_ERROR(errno, KPTA_IO_FILE_ERROR))
	)
	{
		rv = fwrite(self->raw, 1, strlen(self->raw), fp) == strlen(self->raw) ? KPTA_OK : KP_SET_ERROR(ferror(fp), KPTA_IO_FILE_ERROR);
		fclose(fp);
	}
	return rv;
}

KP_IMPLEMENTATION(KP_RV, __parse_json)(KP_INOUT KP_JSON_STR *self)
{
	KP_RV rv;
	if
	(
		KP_SUCCESS(rv = self->raw ? KPTA_OK : KPTA_INVALID_STATE_OBJECT) &&
		KP_SUCCESS(rv = !self->hParser ? KPTA_OK : KPTA_INVALID_STATE_OBJECT)
	)	rv = (self->hParser = kson_parse(self->raw)) ? KPTA_OK : KPTA_INVALID_JSON;
	return rv;
}
KP_IMPLEMENTATION(const kson_node_t*, __by_path)(KP_IN KP_JSON_STR *self, KP_IN int len, ...)
{
	va_list ap;
	const kson_node_t *ret;
	va_start(ap, len);
	ret = kson_by_vpath(self->hParser->root, len, ap);
	va_end(ap);
	return ret;
}
KP_IMPLEMENTATION(const kson_node_t*, __by_key)(KP_IN kson_node_t *node, KP_IN KP_STRING key){ return kson_by_key(node, key); }
KP_IMPLEMENTATION(const kson_node_t*, __by_index)(KP_IN kson_node_t *node, KP_IN long i) { return kson_by_index(node, i); }


const static KP_JSON_STR default_json =
{
	NULL,
	NULL,

	__read_from_file,
	__read_from_stdin,
	__read_from_template,
	__unlade_to_disk,
	__parse_json,
	__by_path,
	__by_key,
	__by_index
};
KP_NEW(new_json)(KP_OUT KP_JSON *self)
{
	KP_RV rv;
	KP_JSON out;
	if (KP_SUCCESS(rv = (out = (KP_JSON) malloc(sizeof(KP_JSON_STR))) ? KPTA_OK : KPTA_MEMORY_ERROR))
	{
		memcpy(out, &default_json, sizeof(KP_JSON_STR));
		*self = out;
	}
	return rv;
}
KP_DELETE(delete_json)(KP_INOUT KP_JSON self)
{
	if (self)
	{
		if (self->raw) free(self->raw);
		if (self->hParser) kson_destroy(self->hParser);
		free(self);
	}
}


KP_IMPLEMENTATION(KP_RV, __begin_write)(KP_INOUT KP_JSON_BUILDER_STR *self)
{
	if (KP_SUCCESS(self->_rv)) json_print_raw(self->_printer, JSON_OBJECT_BEGIN, NULL, 0);
	return self->_rv;
}
KP_IMPLEMENTATION(void, __end_write)(KP_INOUT KP_JSON_BUILDER_STR *self)
{
	if (KP_SUCCESS(self->_rv)) json_print_raw(self->_printer, JSON_OBJECT_END, NULL, 0);
	if (KP_SUCCESS(self->_rv))
	{
		self->_buffer[self->_i] = 0;
		self->_rv = KPTA_JSON_PRINTER_END;
	}
}
KP_IMPLEMENTATION(KP_RV, __begin_object)(KP_INOUT KP_JSON_BUILDER_STR *self, KP_IN KP_STRING szName)
{
	if (KP_SUCCESS(self->_rv) && szName) json_print_raw(self->_printer, JSON_KEY, szName, strlen(szName));
	return self->begin_write(self);
}
KP_IMPLEMENTATION(KP_RV, __end_object)(KP_INOUT KP_JSON_BUILDER_STR *self)
{
	if (KP_SUCCESS(self->_rv)) json_print_raw(self->_printer, JSON_OBJECT_END, NULL, 0);
	return self->_rv;
}
KP_IMPLEMENTATION(KP_RV, __begin_array)(KP_INOUT KP_JSON_BUILDER_STR *self, KP_IN KP_STRING szName)
{
	if (KP_SUCCESS(self->_rv)) json_print_raw(self->_printer, JSON_KEY, szName, strlen(szName));
	if (KP_SUCCESS(self->_rv)) json_print_raw(self->_printer, JSON_ARRAY_BEGIN, NULL, 0);
	return self->_rv;
}
KP_IMPLEMENTATION(KP_RV, __end_array)(KP_INOUT KP_JSON_BUILDER_STR *self)
{
	if (KP_SUCCESS(self->_rv)) json_print_raw(self->_printer, JSON_ARRAY_END, NULL, 0);
	return self->_rv;
}
KP_IMPLEMENTATION(KP_RV, __put_string)(KP_INOUT KP_JSON_BUILDER_STR *self, KP_IN KP_STRING szName, KP_IN KP_STRING szValue)
{
	if (KP_SUCCESS(self->_rv) && szName) json_print_raw(self->_printer, JSON_KEY, szName, strlen(szName));
	if (KP_SUCCESS(self->_rv)) json_print_raw(self->_printer, JSON_STRING, szValue, strlen(szValue));
	return self->_rv;
}
KP_IMPLEMENTATION(KP_RV, __put_number)(KP_INOUT KP_JSON_BUILDER_STR *self, KP_IN KP_STRING szName, KP_IN KP_STRING szValue)
{
	if (KP_SUCCESS(self->_rv) && szName) json_print_raw(self->_printer, JSON_KEY, szName, strlen(szName));
	if (KP_SUCCESS(self->_rv)) json_print_raw(self->_printer, JSON_INT, szValue, strlen(szValue));
	return self->_rv;
}
KP_IMPLEMENTATION(KP_RV, __put_boolean)(KP_INOUT KP_JSON_BUILDER_STR *self, KP_IN KP_STRING szName, KP_IN int bValue)
{
	json_type type = bValue ? JSON_TRUE : JSON_FALSE;
	if (KP_SUCCESS(self->_rv) && szName) json_print_raw(self->_printer, JSON_KEY, szName, strlen(szName));
	if (KP_SUCCESS(self->_rv)) json_print_raw(self->_printer, type, NULL, 0);
	return self->_rv;
}
KP_IMPLEMENTATION(int, __printer_callback)(void *userdata, const char *s, uint32_t length)
{
	KP_JSON_BUILDER hHandler = (KP_JSON_BUILDER) userdata;
	if (KP_SUCCESS(hHandler->_rv))
	{
		if (hHandler->_i + length + 1 > hHandler->_size)
		{
			if (hHandler->_size < 1024) hHandler->_size = hHandler->_i + length + 1024;
			else hHandler->_size = hHandler->_i + length + 1;
			ROUNDUP(hHandler->_size);
			hHandler->_rv = (hHandler->_buffer = (KP_BYTE*) realloc(hHandler->_buffer, hHandler->_size)) ? KPTA_OK : KPTA_MEMORY_ERROR;
		}
		if (!hHandler->_rv)
		{
			memcpy(hHandler->_buffer + hHandler->_i, s, length);
			hHandler->_i += length;
		}
	}
	return hHandler->_rv;
}
KP_IMPLEMENTATION(void, __reset)(KP_INOUT KP_JSON_BUILDER_STR *self)
{
	if (KP_SUCCESS(self->_rv))
	{
		self->_i = 0;
		if (self->_size > 0)
		{
			self->_buffer[0] = 0;
			json_print_free(self->_printer);
			json_print_init(self->_printer, __printer_callback, self);
		}
	}
}
KP_IMPLEMENTATION(KP_RV, __unlade_to_stdout)(KP_IN KP_JSON_BUILDER_STR *self)
{
	KP_RV rv;
	KP_INT32 len;
	size_t iSize;
	int iLen;
	if
	(
		KP_SUCCESS(rv = self->_buffer ? KPTA_OK : KPTA_INVALID_STATE_OBJECT) &&
		KP_SUCCESS(rv = (len = strlen((char*) self->_buffer)) < INT32_MAX ? KPTA_OK : KPTA_MSG_SIZE_ERROR) &&
		KP_SUCCESS(rv = (iSize = fwrite(&len, 1,sizeof(KP_INT32), stdout)) == 4 ? KPTA_OK : KPTA_STDIO_ERROR) &&
		KP_SUCCESS(rv = (iLen = (int) len) > 0 ? KPTA_OK : KPTA_MSG_SIZE_ERROR) &&
		KP_SUCCESS(rv = (iSize = fwrite(self->_buffer, 1, iLen, stdout)) == iLen ? KPTA_OK : KPTA_STDIO_ERROR)
	)	fflush(stdout);
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __embed)(KP_INOUT KP_JSON_BUILDER_STR *self, KP_IN kson_node_t *root, KP_IN int bPrintKey)
{
	KP_RV rv = KPTA_OK;
	long i;
	KP_STRING szBuffer;
	if
	(
		root->key &&
		bPrintKey &&
		KP_SUCCESS(rv = self->_printer->callback(self->_printer->userdata, "\"", 1)) &&
		KP_SUCCESS(rv = self->_printer->callback(self->_printer->userdata, root->key, strlen(root->key)))
	)	rv = self->_printer->callback(self->_printer->userdata, "\": ", 3);
	if (KP_SUCCESS(rv))
	{
		if (root->type == KSON_TYPE_BRACKET || root->type == KSON_TYPE_BRACE)
		{
			if (KP_SUCCESS(rv = self->_printer->callback(self->_printer->userdata, root->type == KSON_TYPE_BRACKET ? "[" : "{", sizeof(char))) && root->n)
			{
				for (i = 0; i < (long) root->n; ++i)
				{
					if (i) rv = self->_printer->callback(self->_printer->userdata, ",", sizeof(char));
					if (KP_SUCCESS(rv)) rv = __embed(self, root->v.child[i], TRUE);
					if (KP_FAIL(rv)) break;
				}
			}
			if (KP_SUCCESS(rv)) rv = self->_printer->callback(self->_printer->userdata, root->type == KSON_TYPE_BRACKET ? "]" : "}", sizeof(char));
		}
		else
		{
			if (root->type != KSON_TYPE_NO_QUOTE) rv = self->_printer->callback(self->_printer->userdata, root->type == KSON_TYPE_SGL_QUOTE ? "\'" : "\"", sizeof(char));
			if (KP_SUCCESS(rv) && KP_SUCCESS(rv = __escape(root->v.str, &szBuffer)))
			{
				rv = self->_printer->callback(self->_printer->userdata, szBuffer, strlen(szBuffer));
				free(szBuffer);
			}
			if (KP_SUCCESS(rv) && root->type != KSON_TYPE_NO_QUOTE) rv = self->_printer->callback(self->_printer->userdata, root->type == KSON_TYPE_SGL_QUOTE ? "\'" : "\"", sizeof(char));
		}
		if (KP_SUCCESS(rv) && !bPrintKey) self->_buffer[self->_i] = 0;
	}
	return rv;
}

const static KP_JSON_BUILDER_STR default_builder =
{
	NULL,			/* _buffer */
	0,				/* _size */
	0,				/* _i */
	NULL,			/* _printer */
	KPTA_OK,		/* _rv */
	__begin_write,
	__end_write,
	__begin_object,
	__end_object,
	__begin_array,
	__end_array,
	__put_string,
	__put_number,
	__reset,
	__unlade_to_stdout,
	__put_boolean,
	__embed
};
KP_NEW(new_json_builder)(KP_OUT KP_JSON_BUILDER *self)
{
	KP_RV rv;
	int ret;
	KP_JSON_BUILDER out = NULL;
	if (KP_SUCCESS(rv = (out = (KP_JSON_BUILDER) malloc(sizeof(KP_JSON_BUILDER_STR))) ? KPTA_OK : KPTA_MEMORY_ERROR))
	{
		memcpy(out, &default_builder, sizeof(KP_JSON_BUILDER_STR));
		if
		(
			KP_SUCCESS(rv = (out->_printer = (json_printer*) malloc(sizeof(json_printer))) ? KPTA_OK : KPTA_MEMORY_ERROR) &&
			KP_SUCCESS(rv = !(ret = json_print_init(out->_printer, __printer_callback, out)) ? KPTA_OK : KP_SET_ERROR(ret, KPTA_JSON_PRINTER_ERROR))
		)	*self = out;
		else delete_json_builder(out);
	}
	return rv;
}
KP_DELETE(delete_json_builder)(KP_INOUT KP_JSON_BUILDER self)
{
	if (self)
	{
		if (self->_buffer) free(self->_buffer);
		if (self->_printer)
		{
			json_print_free(self->_printer);
			free(self->_printer);
		}
		free(self);
	}
}