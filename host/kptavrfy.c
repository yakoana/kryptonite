/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Signature verification component
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "verify.h"

#if (defined(_WIN32))

#include <SDKDDKVer.h>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason, LPVOID lpReserved)
{
    UNREFERENCED_PARAMETER(hModule);
    UNREFERENCED_PARAMETER(lpReserved);
	switch (ul_reason)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

#else

/* TODO: Constructor required under Linux? */

#endif

#define NATIVE_GET_SIGNER_ID_CMD			"cf18e965-ddec-4a15-beb4-567744052cd7"
#define NATIVE_GET_SIGNER_CERT_CMD			"9fe3492a-5a85-45b9-b586-6ca535249fd5"
#define NATIVE_GET_CONTENT_CMD				"f2112590-0868-4872-b112-c0c00f339f78"
#define NATIVE_IS_TRUSTED_CMD				"2e420e7e-be8e-403a-9ced-674e0467b12f"
#define NATIVE_VERIFY_CMD					"308e30e3-cf68-48a1-9476-47319ca72560"
#define NATIVE_VERIFY_ALL_CMD				"83d9121c-7913-414e-a590-6794c5be2ff3"

KP_FUNCTION(KP_RV, KP_execute_command)(KP_IN KP_ENV env, KP_IN KP_JSON input, KP_OUT KP_JSON_BUILDER *output)
{
	KP_RV rv = KPTA_OK, ret;
	KP_PROCESSOR hProcessor = NULL;
	const kson_node_t *q, *p, *r, *s, *t;
	KP_JSON_BUILDER hBuilder;
	KP_JSON hParser;
	KP_STRING szRet;

	env->hLogger->debug(env->hLogger, SIGNATURE_CATEGORY, KPL_OP_INIT, SIGN_CMD_OP, input->raw);
	if
	(
		KP_SUCCESS(ret = env->new_processor(&hProcessor)) &&
		KP_SUCCESS(ret = hProcessor->init(hProcessor, input)) &&
		KP_SUCCESS(ret = (q = hProcessor->command(hProcessor)) ? KPTA_OK : KPTA_INVALID_ARG)
	)
	{
		if (strcmp(q->v.str, NATIVE_GET_SIGNER_ID_CMD) == 0)
		{
			if
			(
				KP_SUCCESS(rv = (p = input->by_path(input, 3, "payload", "payload", "payload")) ? KPTA_OK : KPTA_INVALID_ARG) &&
				KP_SUCCESS(rv = KPTA_signer_id(env, p->v.str, &hBuilder))
			)
			{
				if (KP_SUCCESS(ret = env->new_json(&hParser)))
				{
					if
					(
						KP_SUCCESS(ret = hParser->from_template(hParser, (KP_STRING) hBuilder->_buffer)) &&
						KP_SUCCESS(ret = hParser->parse(hParser)) &&
						KP_SUCCESS(ret = hProcessor->hOutput->begin_object(hProcessor->hOutput, "payload")) &&
						KP_SUCCESS(ret = hProcessor->hOutput->begin_object(hProcessor->hOutput, "signer-certificate"))
					)
					{
						if ((p = hParser->by_path(hParser, 1, "issuer")) && (q = hParser->by_path(hParser, 1, "serial")))
						{
							if
							(
								KP_SUCCESS(ret = hProcessor->hOutput->put_string(hProcessor->hOutput, "issuer", p->v.str))
							)	ret = hProcessor->hOutput->put_string(hProcessor->hOutput, "serial", q->v.str);
						}
						else if
						(
							KP_SUCCESS(ret = (p = hParser->by_path(hParser, 1, "subjectKeyIdentifier")) ? KPTA_OK : KPTA_INVALID_JSON)
						)	ret = hProcessor->hOutput->put_string(hProcessor->hOutput, "subjectKeyIdentifier", p->v.str);
						if (KP_SUCCESS(ret = hProcessor->hOutput->end_object(hProcessor->hOutput))) ret = hProcessor->hOutput->end_object(hProcessor->hOutput);
					}
					env->delete_json(hParser);
				}
				env->delete_json_builder(hBuilder);
			}
		}
		else if (strcmp(q->v.str, NATIVE_GET_SIGNER_CERT_CMD) == 0)
		{
			if
			(
				KP_SUCCESS(rv = (p = input->by_path(input, 3, "payload", "payload", "payload")) ? KPTA_OK : KPTA_INVALID_ARG) &&
				KP_SUCCESS(rv = KPTA_signer_cert(env, p->v.str, &szRet))
			)
			{
				ret = hProcessor->hOutput->put_string(hProcessor->hOutput, "payload", szRet);
				free(szRet);
			}
		}
		else if (strcmp(q->v.str, NATIVE_GET_CONTENT_CMD) == 0)
		{
			if
			(
				KP_SUCCESS(rv = (p = input->by_path(input, 3, "payload", "payload", "payload")) ? KPTA_OK : KPTA_INVALID_ARG) &&
				KP_SUCCESS(rv = KPTA_content(env, p->v.str, &szRet))
			)
			{
				ret = hProcessor->hOutput->put_string(hProcessor->hOutput, "payload", szRet);
				free(szRet);
			}
		}
		else if (strcmp(q->v.str, NATIVE_IS_TRUSTED_CMD) == 0) if (KP_SUCCESS(rv = (p = input->by_path(input, 3, "payload", "payload", "payload")) ? KPTA_OK : KPTA_INVALID_ARG)) rv = KPTA_trusted(env, p->v.str);
		else if (strcmp(q->v.str, NATIVE_VERIFY_CMD) == 0)
		{
			if
			(
				KP_SUCCESS(rv = (p = input->by_path(input, 3, "payload", "payload", "payload")) ? KPTA_OK : KPTA_INVALID_ARG) &&
				KP_SUCCESS(rv = (q = input->by_key(p, "cms")) ? KPTA_OK : KPTA_INVALID_ARG) &&
				KP_SUCCESS(rv = (r = input->by_key(p, "content")) ? KPTA_OK : KPTA_INVALID_ARG) &&
				KP_SUCCESS(rv = (s = input->by_key(p, "isBase64")) ? KPTA_OK : KPTA_INVALID_ARG) &&
				KP_SUCCESS(rv = (t = input->by_key(p, "certificate")) ? KPTA_OK : KPTA_INVALID_ARG)
			)	rv = KPTA_verify(env, q->v.str, r->v.str, strcmp(s->v.str, "true") == 0, t->v.str);
		}
		else if (strcmp(q->v.str, NATIVE_VERIFY_ALL_CMD) == 0)
		{
			if (KP_SUCCESS(rv = (p = input->by_path(input, 3, "payload", "payload", "payload")) ? KPTA_OK : KPTA_INVALID_ARG)) rv = KPTA_verify_all(env, p->v.str);
		}
		else rv = KPTA_INVALID_ARG;
	}
	if (KP_SUCCESS(ret) && KP_SUCCESS(ret = hProcessor->finish(hProcessor, rv)))
	{
		env->hLogger->info(env->hLogger, SIGNATURE_CATEGORY, KPL_OPERATION_SUCCEEDED, SIGN_CMD_OP);
		env->hLogger->debug(env->hLogger, SIGNATURE_CATEGORY, KPL_CMD_EXECUTED, (char*) hProcessor->hOutput->_buffer);
		*output = hProcessor->hOutput;
	}
	else
	{
		if (hProcessor->hOutput) env->delete_json_builder(hProcessor->hOutput);
		env->hLogger->error(env->hLogger, SIGNATURE_CATEGORY, KPL_OPERATION_FAILURE, ret, SIGN_CMD_OP);
	}
	if (hProcessor) env->delete_processor(hProcessor);
	return ret;
}
