/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Log device implementation
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "kryptonite.h"
#include <stdio.h>

#if (defined(_WIN32))
	#include <sys/stat.h>  
	#include <errno.h>
	#include <time.h>
	#include <io.h>
#else
	#include <sys/syscall.h>
#endif

char* szCategories[] =
{
	"Infraestrutura de produto",
	"Controle de versão",
	"Emissão de certificados",
	"Assinatura digital",
	"Validação de assinatura",
	"Desenvolvimento de produto"
};

char* szMessages[] =
{
	"Objeto %s criado com sucesso",
	"Ocorreu um erro código %llu na criação do objeto %s",
	"Ocorreu o erro %llu na execução da operação %s",
	"Operação %s realizada com sucesso",
	"Falha %d na redefinição da entrada e saída padrão",
	"Operação de %s com o valor %s realizada com sucesso",
	"Operação de %s com o valor %s foi mal sucedida pelo erro %llu",
	"Início de operação %s. Parâmetros recebidos: [%s]",
	"Slot [%s] enumerado",
	"Para a requisição do certificado será gerada chave de %lu bits e assinada com o algoritmo %s",
	"Não foi possível instalar o certificado da AC %s pelo motivo %llu",
	"Certificados [%s] enumerados",
	"Identificação do assinante obtida: [%s]",
	"Certificado do assinante obtido: [%s]",
	"Conteúdo assinado obtido: [%s]",
	"Comando executado com a saída [%s]",
	"Enviada a resposta [%s] ao navegador",
	"A requisição [%s] foi executada",
	"O processamento da requisição [%s] falhou com o erro %llu",
	"O aplicativo foi iniciado com o argumento [%s]",
	"O aplicativo está sendo finalizado",
	"Inicialização finalizada com sucesso",
	"Mensagem [%s] recebida para processamento",
	"Aplicativo finalizado em vista do erro %llu",
	"Falha na obtenção do componente de processamento: %llu"
};


KP_IMPLEMENTATION(KP_RV, __set_level)(KP_INOUT KP_LOG_STR *self, KP_IN int cfgLevel)
{
	switch (cfgLevel)
	{
	case KP_LOG_ERROR:
	case KP_LOG_WARN:
	case KP_LOG_INFO:
	case KP_LOG_DEBUG:
		self->level = cfgLevel;
		break;
	default: return (KP_RV) KPTA_INVALID_LOGLEVEL;
	}
	return KPTA_OK;
}

#if (defined(_WIN32))
KP_IMPLEMENTATION(KP_RV, __open_log_file)(KP_OUT FILE **out)
{
	KP_RV rv = 0;
	char szFileName[FILENAME_MAX];
	const char *szMode;
	struct _stat buf;
	int i = 0, oldI = 0;
	__time64_t oldT = MAX__TIME64_T;
	FILE *hLog;
	while (KP_SUCCESS(rv) && i < 3)
	{
		snprintf(szFileName, FILENAME_MAX, EVENT_LOG_SOURCE, i);
		if (_stat(szFileName, &buf))
		{
			if (errno == ENOENT) break;
			else rv = KP_SET_ERROR(errno, KPTA_LOGFILE_ERROR);
		}
		else
		{
			if (buf.st_size < LOG_SOURCE_MAX_SIZE) break;
			else
			{
				if (buf.st_mtime < oldT)
				{
					oldT = buf.st_mtime;
					oldI = i;
				}
			}
		}
		i++;
	}
	if (KP_SUCCESS(rv))
	{
		if (i < 3) szMode = "a+";
		else
		{
			i = oldI;
			szMode = "w";
		}
		snprintf(szFileName, FILENAME_MAX, EVENT_LOG_SOURCE, i);
		if
		(
			KP_SUCCESS(rv = (hLog = fopen(szFileName, szMode)) ? KPTA_OK : KP_SET_ERROR(errno, KPTA_LOGFILE_ERROR))
		)	*out = hLog;
	}
	return rv;
}

#endif
KP_IMPLEMENTATION(KP_RV, __open_log)(KP_INOUT KP_LOG_STR *self)
{
	KP_RV rv = KPTA_OK;
#if (defined(_WIN32))
	if (KP_SUCCESS(rv = !self->stream ? KPTA_OK : KPTA_INVALID_STATE_OBJECT)) rv = __open_log_file(&self->stream);
#else
	openlog(EVENT_LOG_SOURCE, LOG_PID, LOG_USER);
#endif
	return rv;
}

KP_IMPLEMENTATION(KP_RV, __close_log)(KP_INOUT KP_LOG_STR *self)
{
	KP_RV rv = KPTA_OK;
#if (defined(_WIN32))
	if (KP_SUCCESS(rv = self->stream ? KPTA_OK : KPTA_INVALID_STATE_OBJECT))
	{
		rv = !fclose(self->stream) ? KPTA_OK : KPTA_INVALID_STATE_OBJECT;
		self->stream = NULL;
	}
#else
	closelog();
#endif
	return rv;
}

#if (defined(_WIN32))
KP_IMPLEMENTATION(void, __win_time)(KP_IN KP_STRING buffer, KP_INOUT FILE *stream)
{
	time_t current;
	char winLine[4096], szTime[48];
	struct tm *p;
	current = time(NULL);
	p = localtime(&current);
	strftime(szTime, 48, "%c", p);
	snprintf(winLine, sizeof(winLine), "%s - %s", szTime, buffer);
	fputs(winLine, stream);
	fflush(stream);
}
#endif
KP_IMPLEMENTATION(void, __log)
(
	KP_IN KP_LOG_STR *self,
	KP_IN unsigned int level,		/* Log level. Platform value */
	KP_IN unsigned int category,	/* Log category. */
	KP_IN unsigned int mID ,		/* Multiplatform message ID */
	va_list argp					/* Message printf replacements values */
)
{
	char *lv, id[256], msg[4096], buffer[4096];
	KP_ULONG pid;
	if
	(
		((self->level == KP_LOG_DEBUG || self->level >= level)) &&
		category < CATEGORY_WATERLEVEL &&
		mID < MESSAGE_WATERLEVEL
	)
	{
#if (defined(_WIN32))
		if (!self->stream || ferror(self->stream)) return;
		pid = GetProcessId(GetCurrentProcess());
#else
		pid = syscall(SYS_gettid);
#endif
		switch (level)
		{
		case KP_LOG_DEBUG:	lv = KP_LOG_DEBUG_LBL;	break;
		case KP_LOG_INFO:	lv = KP_LOG_INFO_LBL;	break;
		case KP_LOG_WARN:	lv = KP_LOG_WARN_LBL;	break;
		case KP_LOG_ERROR:	lv = KP_LOG_ERROR_LBL;	break;
		default: return;
		}
		snprintf(id, sizeof(id), "%s %s[%d]", lv, szCategories[category], pid);
		vsnprintf(msg, sizeof(msg), szMessages[mID], argp);
		snprintf(buffer, sizeof(buffer), "%s: %s\n", id, msg);
#if (defined(_WIN32))
		__win_time(buffer, self->stream);
#else
		syslog(level, "%s", buffer);
#endif
	}
}
KP_IMPLEMENTATION(void, __debug)(KP_IN KP_LOG_STR *self, KP_IN unsigned int category, KP_IN KP_ULONG id , ...)
{
	va_list argp;
	va_start(argp, id);
	__log(self, KP_LOG_DEBUG, category, id, argp);
	va_end(argp);
}
KP_IMPLEMENTATION(void, __info)(KP_IN KP_LOG_STR *self, KP_IN unsigned int category, KP_IN KP_ULONG id , ...)
{
	va_list argp;
	va_start(argp, id);
	__log(self, KP_LOG_INFO, category, id, argp);
	va_end(argp);
}
KP_IMPLEMENTATION(void, __warn)(KP_IN KP_LOG_STR *self, KP_IN unsigned int category, KP_IN KP_ULONG id , ...)
{
	va_list argp;
	va_start(argp, id);
	__log(self, KP_LOG_WARN, category, id, argp);
	va_end(argp);
}
KP_IMPLEMENTATION(void, __error)(KP_IN KP_LOG_STR *self, KP_IN unsigned int category, KP_IN KP_ULONG id , ...)
{
	va_list argp;
	va_start(argp, id);
	__log(self, KP_LOG_ERROR, category, id, argp);
	va_end(argp);
}
KP_IMPLEMENTATION(KP_RV, __log_file_path)(KP_IN KP_LOG_STR *self, KP_OUT KP_STRING *szOut)
{
	KP_RV rv = KPTA_OK;
#if (defined(_WIN32))
	KP_STRING szPath;
	HANDLE hHandle;
	DWORD ret;
	if
	(
		KP_SUCCESS(rv = self->stream ? KPTA_OK : KPTA_INVALID_STATE_OBJECT) &&
		KP_SUCCESS(rv = (hHandle = (HANDLE) _get_osfhandle(_fileno(self->stream))) != INVALID_HANDLE_VALUE ? KPTA_OK : KPTA_WINDOWS_API_ERROR)
	)
	{
		ret = GetFinalPathNameByHandleA(hHandle, NULL, 0, 0);
		if (KP_SUCCESS(rv = (szPath = (KP_STRING) malloc(ret + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR))
		{
			GetFinalPathNameByHandleA(hHandle, szPath, ret + 1, 0);
			*szOut = szPath;
		}
	}
#else
	/* TODO: Return information about syslog */
#endif
	return rv;
}

const static KP_LOG_STR default_logger =
{
	NULL,
	KP_LOG_ERROR,

	__set_level,
	__open_log,
	__close_log,
	__debug,
	__info,
	__warn,
	__error,
	__log_file_path
};
KP_NEW(new_logger)(KP_OUT KP_LOG *self)
{
	KP_RV rv;
	KP_LOG out;
	if (KP_SUCCESS(rv = (out = (KP_LOG) malloc(sizeof(KP_LOG_STR))) ? KPTA_OK : KPTA_MEMORY_ERROR))
	{
		memcpy(out, &default_logger, sizeof(KP_LOG_STR));
		*self = out;
	}
	return rv;
}
KP_DELETE(delete_logger)(KP_INOUT KP_LOG self)
{
	if (self)
	{
		if (self->stream) self->close(self);
		free(self);
	}
}