/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Document signature test facility
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "test.h"
#include <stdio.h>

#define FORMAT			"%s\n%s\t%s\t%s\n"
#define CMD_LINE		"usage: sign_t <szLocation> <szCertDN> <szSlotName> <szFilename>, where:"
#define ARG_1			"szLocation: test directory. Must exist."
#define ARG_2           "iSelected: signing certificate index (in JSON object)."
#define ARG_3           "szFilename: PEM PKCS #7 file to generated at szLocation."
void check_params(int argc, char *argv[])
{
	if (argc != 4 || __ACCESS(argv[1], __F))
	{
		printf(FORMAT, CMD_LINE, ARG_1, ARG_2, ARG_3);
		exit(KP_TEST_ERRORLEVEL);
	}
}
int toFile(KP_IN KP_STRING szPEM, KP_IN KP_STRING szPath, KP_IN KP_STRING szFilename)
{
    char szTarget[MAX_PATH];
    FILE *out;
    int ret = 0;
    strcpy(szTarget, szPath);
    strcat(szTarget, PATH_SEPARATOR);
    strcat(szTarget, szFilename);
    if ((out = fopen(szTarget, "w")))
    {
        if (fwrite(szPEM, 1, strlen(szPEM), out) != strlen(szPEM)) ret = ferror(out);
        fclose(out);
    }
    else ret = errno;
    return ret;
}
static KP_RV __select(KP_IN KP_STRING szEnum, KP_IN long idx, KP_OUT KP_JSON_BUILDER *hOut)
{
	KP_RV rv;
	KP_JSON hParser = NULL;
	kson_node_t *p, *q;
	KP_JSON_BUILDER hBuilder = NULL;
	if
	(
		KP_SUCCESS(rv = new_json(&hParser)) &&
		KP_SUCCESS(rv = hParser->from_template(hParser, szEnum)) &&
		KP_SUCCESS(rv = hParser->parse(hParser)) &&
		KP_SUCCESS(rv = (p = (kson_node_t *)hParser->by_path(hParser, 1, "certificates")) ? KPTA_OK : KPTA_INVALID_JSON) &&
		KP_SUCCESS(rv = (p = (kson_node_t *)hParser->by_index(p, idx)) ? KPTA_OK : KPTA_INVALID_JSON)
	)
	{
		if
		(
			KP_SUCCESS(rv = new_json_builder(&hBuilder)) &&
			KP_SUCCESS(rv = hBuilder->begin_write(hBuilder)) &&
			KP_SUCCESS(rv = (q = (kson_node_t *) hParser->by_key(p, "subject")) ? KPTA_OK : KPTA_INVALID_JSON) &&
			KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "subject", q->v.str)) &&
			KP_SUCCESS(rv = (q = (kson_node_t *) hParser->by_key(p, "issuer")) ? KPTA_OK : KPTA_INVALID_JSON) &&
			KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "issuer", q->v.str)) &&
			KP_SUCCESS(rv = (q = (kson_node_t *) hParser->by_key(p, "serial")) ? KPTA_OK : KPTA_INVALID_JSON) &&
			KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "serial", q->v.str))
		)
		{
			hBuilder->end_write(hBuilder);
			*hOut = hBuilder;
		}
	}
	if (hParser) delete_json(hParser);
	if (KP_FAIL(rv) && hBuilder) delete_json_builder(hBuilder);
	return rv;
}
int main(int argc, char* argv[])
{
	int ret = KP_SUCCESS_ERRORLEVEL;
	KP_RV rv;
	KP_ENV env = NULL;
	KP_JSON_BUILDER hBuilder = NULL, hCert = NULL;
	KP_STRING szPEM;
	check_params(argc, argv);
    printf("%s", "Signature document test... ");
    if
	(
		KP_SUCCESS(rv = new_environment(&env)) &&
		KP_SUCCESS(rv = env->hLogger->set_level(env->hLogger, KP_LOG_DEBUG))
	)
	{
		if (KP_SUCCESS(rv = KPTA_enumerate_certs(env, &hBuilder)))
		{
			if (KP_SUCCESS(rv = __select((KP_STRING) hBuilder->_buffer, atoi(argv[2]), &hCert)))
			{
				if (KP_SUCCESS(rv = KPTA_sign(env, (KP_STRING) hCert->_buffer, "Signed transaction ", FALSE, TRUE, &szPEM)))
				{
					if (!(ret = toFile(szPEM, argv[1], argv[3]))) printf("%s\n", "succeeded!");
					else printf("%s with error code %d\n", "failed!", ret);
					free(szPEM);
				}
				else
				{
					printf("%s with error code %llu\n", "failed!", rv);
					ret = KP_SIGN_ERRORLEVEL;
				}
				env->delete_json_builder(hCert);
			}
			env->delete_json_builder(hBuilder);
		}
		else
		{
			printf("%s with error code %llu\n", "failed!", rv);
			ret = KP_SIGN_ERRORLEVEL;
		}
        delete_environment(env);
    }
    else
    {
        printf("%s with error code %llu\n", "failed!", rv);
        ret = KP_SIGN_ERRORLEVEL;
    }
    return ret;
}