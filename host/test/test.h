/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Regression test facility
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#ifndef __TEST_H__
#define __TEST_H__

#include "kryptonite.h"
#include "enroll.h"
#include "sign.h"

#define KP_SUCCESS_ERRORLEVEL       0
#define KP_TEST_ERRORLEVEL          (KP_SUCCESS_ERRORLEVEL + 1)
#define KP_LOGGER_ERRORLEVEL        (KP_SUCCESS_ERRORLEVEL + 2)
#define KP_JSON_ERRORLEVEL          (KP_SUCCESS_ERRORLEVEL + 3)
#define KP_CONFIG_ERRORLEVEL        (KP_SUCCESS_ERRORLEVEL + 4)
#define KP_ENV_ERRORLEVEL           (KP_SUCCESS_ERRORLEVEL + 5)
#define KP_ENUM_ERRORLEVEL          (KP_SUCCESS_ERRORLEVEL + 6)
#define KP_GENCSR_ERRORLEVEL        (KP_SUCCESS_ERRORLEVEL + 7)
#define KP_INSTALL_ERRORLEVEL       (KP_SUCCESS_ERRORLEVEL + 8)
#define KP_SIGN_ERRORLEVEL			(KP_SUCCESS_ERRORLEVEL + 9)
#define KP_MESSAGE_ERRORLEVEL		(KP_SUCCESS_ERRORLEVEL + 10)

int check_json(KP_LOG);
int check_config(KP_LOG, const char *, const char*);
int check_json_facility();


#if (defined(_WIN32))
	#include <io.h>
	#define __ACCESS		_access
	#define __RW			06
	#define __R				04
	#define __F				00
#else
	#include <unistd.h>
	#define __ACCESS		access
	#define __RW			R_OK|W_OK
	#define __R				R_OK
	#define __F				F_OK
#endif


#endif /* __TEST_H__ */