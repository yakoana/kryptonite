/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Infrastructure regression test
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "test.h"
#include <stdio.h>
#include <string.h>

static char CFG_TEMPLATE[] = "{\"logLevel\":%d,\"maxResponseSize\": 1024000,\"components\": {\"f258091d-bd65-49f0-ba7b-a18f182ce6e0\": \"kptaroll\",\"72499c1f-93ba-4b7d-8cb2-76048f8b31b5\": \"kptasign\",\"51d003c8-a028-450d-8c51-41b4ea3bafb0\": \"kptavrfy\"},\"enroll\": {\"blackList\": [],\"whiteList\": [],\"issueProfile\": {\"keySize\": 2048,\"signAlgOID\": \"1.2.840.113549.1.1.11\"}},\"sign\": {\"blackList\": [],\"whiteList\": [],\"signProfile\": {\"signAlg\": 64,\"policy\": \"CAdES-BES\"}}}";
static int LOG_LEVEL = 3;
static char JSON_FNAME[] = "tconfig.json";
int check_json(KP_LOG hLogger)
{
    int ret = KP_SUCCESS_ERRORLEVEL;
    KP_RV rv;
    KP_JSON hJson, hMedea;

    printf("%s", "JSON object basic test... ");
    if (KP_SUCCESS(rv = new_json(&hJson)))
    {
        hLogger->debug(hLogger, DEVELOPMENT_CATEGORY, KPL_OBJECT_CREATED, "KP_JSON");
        if (KP_SUCCESS(rv = hJson->from_template(hJson, CFG_TEMPLATE, LOG_LEVEL)))
        {
            hLogger->debug(hLogger, DEVELOPMENT_CATEGORY, KPL_OPERATION_SUCCEEDED, "KP_JSON->from_template");
            if (KP_SUCCESS(rv = hJson->unlade_disk(hJson, JSON_FNAME)))
            {
                hLogger->debug(hLogger, DEVELOPMENT_CATEGORY, KPL_OPERATION_SUCCEEDED, "KP_JSON->unlade_disk");
                if (KP_SUCCESS(rv = new_json(&hMedea)))
                {
                    if (KP_SUCCESS(rv = hMedea->from_file(hMedea, JSON_FNAME)))
                    {
                        hLogger->debug(hLogger, DEVELOPMENT_CATEGORY, KPL_OPERATION_SUCCEEDED, "KP_JSON->from_file");
                        if (KP_SUCCESS(rv = hMedea->parse(hMedea)))
						{
							hLogger->debug(hLogger, DEVELOPMENT_CATEGORY, KPL_OPERATION_SUCCEEDED, "KP_JSON->parse");
							printf("%s\n", "succeeded!");
						}
                        else
                        {
                            hLogger->error(hLogger, DEVELOPMENT_CATEGORY, KPL_OPERATION_FAILURE, rv, "KP_JSON->parse");
                            printf("failed! JSON facility failure. Error code %llu\n", rv);
                            ret = KP_JSON_ERRORLEVEL;
                        }
                    }
                    else
                    {
                        hLogger->error(hLogger, DEVELOPMENT_CATEGORY, KPL_OPERATION_FAILURE, rv, "KP_JSON->from_file");
                        printf("failed! JSON facility failure. Error code %llu\n", rv);
                        ret = KP_JSON_ERRORLEVEL;
                    }
                    delete_json(hMedea);
                }
                else
                {
                    hLogger->error(hLogger, DEVELOPMENT_CATEGORY, KPL_OBJECT_CREATION_FAILURE, rv, "KP_JSON");
                    printf("failed! JSON facility failure. Error code %llu\n", rv);
                    ret = KP_JSON_ERRORLEVEL;
                }
                remove(JSON_FNAME);
            }
            else
            {
                hLogger->error(hLogger, DEVELOPMENT_CATEGORY, KPL_OPERATION_FAILURE, rv, "KP_JSON->unlade_disk");
                printf("failed! JSON facility failure. Error code %llu\n", rv);
                ret = KP_JSON_ERRORLEVEL;
            }
        }
        else
        {
            hLogger->error(hLogger, DEVELOPMENT_CATEGORY, KPL_OPERATION_FAILURE, rv, "KP_JSON->from_template");
            printf("failed! JSON facility failure. Error code %llu\n", rv);
            ret = KP_JSON_ERRORLEVEL;
        }
        delete_json(hJson);
    }
    else
    {
        hLogger->error(hLogger, DEVELOPMENT_CATEGORY, KPL_OBJECT_CREATION_FAILURE, rv, "KP_JSON");
        printf("failed! JSON facility failure. Error code %llu\n", rv);
        ret = KP_JSON_ERRORLEVEL;
    }
    return ret;
}


#define CONFIG_JSON         ("config.json")
#define COMPONENT_ID        "f258091d-bd65-49f0-ba7b-a18f182ce6e0"
#define COMPONENT_VALUE     "kptaroll"
int check_config(KP_LOG hLogger, const char *szSource, const char *szLocation)
{
	int ret = KP_SUCCESS_ERRORLEVEL;
	FILE *in, *out;
	char szTarget[FILENAME_MAX], buffer[512];
	size_t size;
	KP_RV rv;
	KP_CONFIG hConfig;
	KP_STRING szComponent;


	*szTarget = 0;
	printf("%s", "Configuration object basic test... ");
	strcat(szTarget, szLocation);
	strcat(szTarget, CONFIG_JSON);
	if ((in = fopen(szSource, "r")))
	{
		if ((out = fopen(szTarget, "w")))
		{
			while ((size = fread(buffer, 1, 512, in)) > 0) fwrite(buffer, 1, size, out);
			fclose(out);
		}
		fclose(in);
	}


	if (KP_SUCCESS(rv = new_config(&hConfig)))
	{
		hLogger->debug(hLogger, DEVELOPMENT_CATEGORY, KPL_OBJECT_CREATED, "KP_CONFIG");
		if (KP_SUCCESS(rv = hConfig->load(hConfig, (KP_STRING) szLocation)))
		{
			hLogger->debug(hLogger, DEVELOPMENT_CATEGORY, KPL_OPERATION_SUCCEEDED, "KP_CONFIG->load");
			if (KP_SUCCESS(rv = hConfig->get_component(hConfig, COMPONENT_ID, &szComponent)))
			{
				if (strcmp(szComponent, COMPONENT_VALUE) == 0)
				{
					hLogger->debug(hLogger, DEVELOPMENT_CATEGORY, KPL_OPERATION_SUCCEEDED, "KP_CONFIG->get_component");
					printf("%s\n", "succeeded!");
				}
				else printf("failed! Unexpected name %s\n", szComponent);
				free(szComponent);
			}
			else
			{
				hLogger->error(hLogger, DEVELOPMENT_CATEGORY, KPL_OPERATION_FAILURE, rv, "KP_CONFIG->get_component");
				printf("failed! KP_CONFIG facility failure. Error code %llu\n", rv);
				ret = KP_CONFIG_ERRORLEVEL;
			}
		}
		delete_config(hConfig);
	}
	return ret;
}

int check_json_facility()
{
	int ret = KP_SUCCESS_ERRORLEVEL;
	KP_RV rv;
	KP_JSON_BUILDER hBuilder;
	KP_JSON hParser;

	printf("%s", "JSON facility validation test... ");
	if (KP_SUCCESS(rv = new_json_builder(&hBuilder)))
	{
		if
		(
			KP_SUCCESS(rv = hBuilder->begin_write(hBuilder)) &&
			KP_SUCCESS(rv = hBuilder->begin_array(hBuilder, "certificates")) &&
			KP_SUCCESS(rv = hBuilder->begin_object(hBuilder, NULL)) &&
			KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "subject", "Subject Name 1")) &&
			KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "issuer", "Issuer Name 1")) &&
			KP_SUCCESS(rv = hBuilder->put_number(hBuilder, "serial", "47")) &&
			KP_SUCCESS(rv = hBuilder->end_object(hBuilder)) &&
			KP_SUCCESS(rv = hBuilder->begin_object(hBuilder, NULL)) &&
			KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "subject", "Subject Name 2")) &&
			KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "issuer", "Issuer Name 2")) &&
			KP_SUCCESS(rv = hBuilder->put_number(hBuilder, "serial", "48")) &&
			KP_SUCCESS(rv = hBuilder->end_object(hBuilder)) &&
			KP_SUCCESS(rv = hBuilder->end_array(hBuilder))
		)
		{
			hBuilder->end_write(hBuilder);
			if (KP_SUCCESS(rv = new_json(&hParser)))
			{
				if
				(
					KP_SUCCESS(rv = hParser->from_template(hParser, (KP_STRING) hBuilder->_buffer)) &&
					KP_SUCCESS(rv = hParser->parse(hParser))
				)	printf("%s\n", "passed!");
				else printf("failed with error code %llu\n", rv);
				
				delete_json(hParser);
			}
		}
		delete_json_builder(hBuilder);
	}
	else printf("failed with error code %llu\n", rv);
	if (KP_FAIL(rv)) ret = KP_JSON_ERRORLEVEL;
	return ret;
}