/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Host app simulation test facility
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 * 
 * Under Windows, execute before test:
 * REG ADD "HKEY_CURRENT_USER\Software\Kryptonite" /v "InstallDir" /t REG_SZ /d "[path]" /f,
 * where path is the complete path to ./dist/debug directory
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "test.h"
#include <stdio.h>
#include <string.h>

#if (defined(_WIN32))
#include <Windows.h>
#include <fcntl.h>
#include <io.h>
#define IO_HANDLE					HANDLE
#else
#define IO_HANDLE					int
#endif

IO_HANDLE STDOUT_W = NULL;
IO_HANDLE STDOUT_R = NULL;

IO_HANDLE STDIN_R = NULL;
IO_HANDLE STDIN_W = NULL;

#if (defined(_WIN32))
KP_IMPLEMENTATION(KP_RV, __win_launch)(KP_IN KP_STRING szChild)
{
	KP_RV rv = KPTA_OK;
	SECURITY_ATTRIBUTES att;
	IO_HANDLE stdOutR = NULL, stdOut = NULL, stdIn = NULL, stdInW = NULL;
	PROCESS_INFORMATION pInfo; 
	STARTUPINFOA pStart;

	ZeroMemory(&pInfo, sizeof(PROCESS_INFORMATION));
	ZeroMemory(&pStart, sizeof(STARTUPINFO));
	att.nLength = sizeof(SECURITY_ATTRIBUTES);
	att.bInheritHandle = TRUE;
	att.lpSecurityDescriptor = NULL;

	stdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	rv = CreatePipe(&stdOutR, &STDOUT_W, &att, 0) ? KPTA_OK : GetLastError();
	if (KP_SUCCESS(rv)) rv = SetStdHandle(STD_OUTPUT_HANDLE, STDOUT_W) ? KPTA_OK : GetLastError();
	if (KP_SUCCESS(rv)) rv = DuplicateHandle(GetCurrentProcess(), stdOutR, GetCurrentProcess(), &STDOUT_R, 0, FALSE, DUPLICATE_SAME_ACCESS) ? KPTA_OK : GetLastError();
	if (KP_SUCCESS(rv))
	{
		CloseHandle(stdOutR);
		stdIn = GetStdHandle(STD_INPUT_HANDLE);
		rv = CreatePipe(&STDIN_R, &stdInW, &att, 0) ? KPTA_OK : GetLastError();
	}
	if (KP_SUCCESS(rv)) rv = SetStdHandle(STD_INPUT_HANDLE, STDIN_R) ? KPTA_OK : GetLastError();
	if (KP_SUCCESS(rv)) rv = DuplicateHandle(GetCurrentProcess(), stdInW, GetCurrentProcess(), &STDIN_W, 0, FALSE, DUPLICATE_SAME_ACCESS) ? KPTA_OK : GetLastError();
	if (KP_SUCCESS(rv))
	{
		CloseHandle(stdInW);
		pStart.cb = sizeof(STARTUPINFO); 
		rv = CreateProcessA(NULL, szChild, NULL, NULL, TRUE, 0, NULL, NULL, &pStart, &pInfo) ? KPTA_OK : GetLastError();
	}
	if (KP_SUCCESS(rv))
	{
		CloseHandle(pInfo.hThread);
		CloseHandle(pInfo.hProcess);
		rv = SetStdHandle(STD_INPUT_HANDLE, stdIn) ? KPTA_OK : GetLastError();
		if (KP_SUCCESS(rv)) rv = SetStdHandle(STD_OUTPUT_HANDLE, stdOut) ? KPTA_OK : GetLastError();
	}
	return rv;
}
#define __platform_launch			__win_launch
KP_IMPLEMENTATION(void, __win_release)()
{
	CloseHandle(STDIN_R);
	CloseHandle(STDIN_W);
	CloseHandle(STDOUT_R);
	CloseHandle(STDOUT_W);
}
#define __platform_release			__win_release
KP_IMPLEMENTATION(KP_RV, __win_write)(KP_IN char *szMessage)
{
	DWORD writen = 0;
	KP_INT32 size = (KP_INT32) strlen(szMessage);
	KP_RV rv = WriteFile(STDIN_W, &size, sizeof(KP_INT32), &writen, NULL) ? KPTA_OK : GetLastError();
	if (KP_SUCCESS(rv)) rv = WriteFile(STDIN_W, szMessage, size, &writen, NULL) ? KPTA_OK : GetLastError();
	return rv;
}
#define __platform_write			__win_write
KP_IMPLEMENTATION(KP_RV, __win_read)(KP_OUT KP_STRING *szOut)
{
	DWORD read = 0;
	KP_INT32 iSize = 0, remains;
	KP_STRING szMessage = NULL;
	KP_RV rv = ReadFile(STDOUT_R, &iSize, sizeof(KP_INT32), &read, NULL) ? KPTA_OK : GetLastError();
	if (KP_SUCCESS(rv)) rv = read == sizeof(KP_INT32) ? KPTA_OK : KPTA_STDIO_ERROR;
	if (KP_SUCCESS(rv)) rv = (szMessage = (KP_STRING) malloc(iSize + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR;
	read = 0;
	remains = iSize;
	while (KP_SUCCESS(rv) && (KP_INT32) remains > 0)
	{
		rv = ReadFile(STDOUT_R, szMessage + iSize - remains, remains, &read, NULL) ? KPTA_OK : GetLastError();
		remains -= read;
	}
	if (KP_SUCCESS(rv))
	{
		szMessage[iSize] = 0;
		*szOut = szMessage;
	}
	else if (szMessage) free(szMessage);
	return rv;
}
#define __platform_read				__win_read
KP_IMPLEMENTATION(KP_RV, __win_exec_cmdline)(KP_IN char *szCmdLine)
{
	KP_RV rv = KPTA_OK;
	PROCESS_INFORMATION pInfo; 
	STARTUPINFOA pStart;
	DWORD dwCode;
	ZeroMemory(&pInfo, sizeof(PROCESS_INFORMATION));
	ZeroMemory(&pStart, sizeof(STARTUPINFO));
	pStart.cb = sizeof(STARTUPINFO); 
	if (KP_SUCCESS(rv = CreateProcessA(NULL, (LPSTR) szCmdLine, NULL, NULL, FALSE, CREATE_NO_WINDOW|CREATE_DEFAULT_ERROR_MODE, NULL, NULL, &pStart, &pInfo) ? KPTA_OK : GetLastError()))
	{
		if
		(
			KP_SUCCESS(rv = WaitForSingleObject(pInfo.hProcess, INFINITE) != WAIT_FAILED ? KPTA_OK : GetLastError()) &&
			KP_SUCCESS(rv = GetExitCodeProcess(pInfo.hProcess, &dwCode) ? KPTA_OK : GetLastError())
		)	rv = dwCode;
		CloseHandle(pInfo.hThread);
		CloseHandle(pInfo.hProcess);
	}
	return rv;
}
#define __platform_exec_cmdline		__win_exec_cmdline
#else
/* TODO */
#endif

KP_IMPLEMENTATION(KP_RV, __rebuild)(KP_IN KP_STRING szPart, KP_OUT KP_STRING *szOut)
{
	KP_RV rv = KPTA_OK;
	KP_STRING szBuffer;
	size_t pos = *szOut ? strlen(*szOut) : 0, len = strlen(szPart);
	if (KP_SUCCESS(rv = (szBuffer = (KP_STRING) realloc(*szOut, len + pos + 1)) ? KPTA_OK : KPTA_MEMORY_ERROR))
	{
		memcpy(szBuffer + pos, szPart, len);
		szBuffer[pos + len] = 0;
		*szOut = szBuffer;
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __receive)(KP_OUT KP_JSON *hOut)
{
	KP_RV rv = KPTA_OK;
	KP_STRING szReceived, szMessage = NULL;
	KP_JSON hEnvelope, hResponse = NULL;
	const kson_node_t *p;
	int part, parts = 1, received = 0;
	KP_JSON_BUILDER hBuilder;
	KP_BASE64 hEncoder;
	KP_BYTE *bDecoded;
	size_t size;
	
	while (KP_SUCCESS(rv) && received < parts && KP_SUCCESS(rv = __platform_read(&szReceived)))
	{
		received++;
		if (KP_SUCCESS(rv = new_json(&hEnvelope)))
		{
			if
			(
				KP_SUCCESS(rv = hEnvelope->from_template(hEnvelope, szReceived)) &&
				KP_SUCCESS(rv = hEnvelope->parse(hEnvelope)) &&
				KP_SUCCESS(rv = (p = hEnvelope->by_key(hEnvelope->hParser->root, "part")) ? KPTA_OK : KPTA_INVALID_RESPONSE) &&
				KP_SUCCESS(rv = (part = atoi(p->v.str)) && part == received ? KPTA_OK : KPTA_INVALID_RESPONSE) &&
				KP_SUCCESS(rv = (p = hEnvelope->by_key(hEnvelope->hParser->root, "parts")) ? KPTA_OK : KPTA_INVALID_RESPONSE) &&
				KP_SUCCESS(rv = (parts = atoi(p->v.str)) ? KPTA_OK : KPTA_INVALID_RESPONSE) &&
				KP_SUCCESS(rv = (p = hEnvelope->by_key(hEnvelope->hParser->root, "payload")) ? KPTA_OK : KPTA_INVALID_RESPONSE)
			)
			{
				if (parts > 1) rv = __rebuild(p->v.str, &szMessage);
				else
				{
					if (KP_SUCCESS(rv = new_json_builder(&hBuilder)))
					{
						if (KP_SUCCESS(rv = hBuilder->embed(hBuilder, p, FALSE))) rv = __rebuild((KP_STRING) hBuilder->_buffer, &szMessage);
						delete_json_builder(hBuilder);
					}
				}
			}
			delete_json(hEnvelope);
		}
		free(szReceived);
	}
	if (KP_SUCCESS(rv) && szMessage)
	{
		if (parts > 1 && KP_SUCCESS(rv = new_base64(&hEncoder)))
		{
			if (KP_SUCCESS(rv = hEncoder->decode(szMessage, &bDecoded, &size))) bDecoded[size] = 0;
			delete_base64(hEncoder);
		}
		else bDecoded = (KP_BYTE*) szMessage;
		if
		(
			KP_SUCCESS(rv) &&
			KP_SUCCESS(rv = new_json(&hResponse)) &&
			KP_SUCCESS(rv = hResponse->from_template(hResponse, (KP_STRING) bDecoded)) &&
			KP_SUCCESS(rv = hResponse->parse(hResponse))
		)	*hOut = hResponse;
		if (KP_FAIL(rv) && hResponse) delete_json(hResponse);
		if (parts > 1) free(bDecoded);
	}
	if (szMessage) free(szMessage);
	return rv;
}

#define FORMAT		"%s\n\t%s\n\t%s\n\t%s\n\t%s\n\t%s\n"
#define CMD_LINE		"Usage: host_t <szLocation> <szHostProcess> <szIssue> <szCN> <szVersion>, where:"
#define ARG_1		"szLocation: test directory. Must exist."
#define ARG_2		"szHostProcess: Host application command line. Must exist."
#define ARG_3		"szIssue: OpenSSL certificate issue command line. Must exist"
#define ARG_4		"szSSL: OpenSSL location"
#define ARG_5		"szCN: user common name"
#define ARG_6		"szVersion: complete path to update component file"
KP_IMPLEMENTATION(void, __check_params)(int argc, char *argv[])
{
	if (__ACCESS(argv[2], __R))
	{
		printf(FORMAT, CMD_LINE, ARG_1, ARG_2, ARG_3, ARG_4, ARG_5);
		exit(KP_TEST_ERRORLEVEL);
	}
}
KP_IMPLEMENTATION(int, __write_file)(KP_IN char *szTarget, KP_IN char *szContent)
{
    FILE *out;
    int ret = 0;
    if ((out = fopen(szTarget, "w")))
    {
        if (fwrite(szContent, 1, strlen(szContent), out) != strlen(szContent)) ret = ferror(out);
        fclose(out);
    }
    else ret = errno;
    return ret;
}
#define ROUNDUP(x)	(--(x), (x)|=(x)>>1, (x)|=(x)>>2, (x)|=(x)>>4, (x)|=(x)>>8, (x)|=(x)>>16, ++(x))
KP_IMPLEMENTATION(int, __read_file)(KP_IN char *szTarget, KP_OUT char **szPEM)
{
    FILE *in;
    int ret = 0, i, max = 0, len = 0;
    char buf[512], *szData = NULL;
    if ((in = fopen(szTarget, "r")))
    {
        while ((i = fread(buf, 1, sizeof(buf), in)))
        {
            if (len + i + 1 > max)
            {
                max = len + i + 1;
                ROUNDUP(max);
                szData = (char*) realloc(szData, max);
            }
            memcpy(szData + len, buf, i);
            len += i;
        }
        if (feof(in))
        {
            szData[len] = 0;
            *szPEM = szData;
        }
        else
        {
            ret = ferror(in);
            if (szData) free(szData);
        }
        fclose(in);
    }
    else ret = errno;
    return ret;
}
KP_IMPLEMENTATION(int, __unescape)(char *szJSON)
{
	int j = 0, i = 0;
	char r, w;

	while ((r = szJSON[i]))
	{
		if (r == '\\')
		{
			r = szJSON[++i];
			switch (r)
			{
			case '"':  w = 0x22; break;
			case '\\': w = 0x5C; break;
			case 'b':  w = 0x08; break;
			case 'f':  w = 0x0C; break;
			case 'n':  w = 0x0A; break;
			case 'r':  w = 0x0D; break;
			case 't':  w = 0x09; break;
			default:   w = szJSON[--i];
			}
		}
		else w = r;
		szJSON[j] = w;
		j++; i++;
	}
	szJSON[j] = 0x00;
	return j;
}

static const char update_cmd_template[] = "{\"nonce\":1,\"payload\":{\"commandID\": \"e203869a-9a1c-4958-afe1-68c8315bc3fd\",\"payload\": {\"commandID\": \"0b8578e3-a69a-4d98-a097-5d8edabd5c7f\",\"payload\": {\"component\": \"%s\",\"hash\": \"%s\",\"file\": \"%s\"}}}}";
static const char requires_update_cmd_template[] = "{\"nonce\":1,\"payload\":{\"commandID\": \"e203869a-9a1c-4958-afe1-68c8315bc3fd\",\"payload\": {\"commandID\": \"7093fe99-0f69-46e5-ba87-5418db398ecf\",\"payload\": {\"component\": \"%s\",\"hash\": \"%s\"}}}}";
static const char genCSR_cmd_template[] = "{\"nonce\":1,\"payload\":{\"commandID\": \"f258091d-bd65-49f0-ba7b-a18f182ce6e0\",\"payload\":{\"commandID\": \"6c17f198-385c-4ba6-af2b-59b40c2950f9\",\"payload\":{\"device\": \"%s\",\"commonName\": \"%s\"}}}}";
static const char sign_cmd_template[] = "{\"nonce\": 1,\"payload\":{\"commandID\": \"72499c1f-93ba-4b7d-8cb2-76048f8b31b5\",\"payload\":{\"commandID\": \"e14f5034-b3a8-4400-bdae-715c0981df2a\",\"payload\":{\"certificate\":%s,\"transaction\": \"%s\",\"isBase64\": false,\"attach\": true}}}}";
static const char config_cmd_template[] = "{\"nonce\":1,\"payload\":{\"commandID\": \"e203869a-9a1c-4958-afe1-68c8315bc3fd\",\"payload\":{\"commandID\": \"7b040131-6ba2-42e4-b886-b134d8090ed7\"}}}";
static const char set_cfg_cmd_template[] = "{\"nonce\":1,\"payload\":{\"commandID\": \"e203869a-9a1c-4958-afe1-68c8315bc3fd\",\"payload\":{\"commandID\": \"dda968c3-8683-4d2e-b944-6cdfeb1f17c3\",\"payload\":{\"logLevel\": 3,\"maxResponseSize\": 1024000,\"components\": {\"f258091d-bd65-49f0-ba7b-a18f182ce6e0\": \"kptaroll\",\"72499c1f-93ba-4b7d-8cb2-76048f8b31b5\": \"kptasign\",\"51d003c8-a028-450d-8c51-41b4ea3bafb0\": \"kptavrfy\"},\"enroll\": {\"blackList\": [],\"whiteList\": [],\"issueProfile\": {\"keySize\": 2048,\"signAlgOID\": \"1.2.840.113549.1.1.11\"}},\"sign\": {\"blackList\": [\"http://localhost/malware/\"],\"whiteList\": [],\"signProfile\": {\"signAlg\": 64,\"policy\": \"CAdES-BES\"}}}}}}";

static const char enum_devices_cmd[] = "{\"nonce\":1,\"payload\":{\"commandID\": \"f258091d-bd65-49f0-ba7b-a18f182ce6e0\",\"payload\":{\"commandID\": \"aae1b8ce-9c85-4b52-a80b-578e36a3a775\"}}}";
static const char exit_cmd[] = "{\"nonce\":1,\"payload\":{\"commandID\": \"exit\"}}";
static char genCSR_cmd[2048];
static char install_cert_cmd[4096];
static const char enum_certs_cmd[] = "{\"nonce\":1,\"payload\":{\"payload\":{\"commandID\":\"a35bc7b0-9fca-4350-8f6f-b915bf23be85\"},\"commandID\":\"72499c1f-93ba-4b7d-8cb2-76048f8b31b5\"}}";
static char sign_cmd[24576];
static char signer_id_cmd[24576];
static char signer_cert_cmd[24576];
static char content_cmd[24576];
static char is_trusted_cmd[24576];
static char verify_cmd[24576];
static char verify_all_cmd[24576];
static char update_cmd[2359296];

static char requires_update_cmd[1024];
static char szCMSSigned[24576];
static char szCert[4096];
static char szContent[4096];
KP_IMPLEMENTATION(KP_RV, __send_cmd)(KP_IN char* szCmd, KP_OUT KP_JSON *hOut)
{
	KP_JSON hJSON = NULL;
	KP_RV rv = KPTA_OK;
	if (KP_SUCCESS(rv = __platform_write(szCmd)) && KP_SUCCESS(rv = __receive(&hJSON))) *hOut = hJSON;
	return rv;
}

KP_IMPLEMENTATION(int, __succeeded)(KP_IN KP_JSON hResponse, KP_IN KP_STRING cmd, KP_OUT KP_RV *resultCode)
{
	long result = -1, reason = 0;
	const kson_node_t *p;
	printf("%s ", cmd);
	if ((p = hResponse->by_path(hResponse, 1, "result"))) result = atol(p->v.str);
	else printf("%s\n", "Invalid response!");
	if ((p = hResponse->by_path(hResponse, 1, "reason"))) reason = atol(p->v.str);
	else printf("%s\n", "Invalid response!");
	if (KP_SUCCESS(result)) printf("%s\n", "succeeded!");
	else printf("failed with result %lu and reason %lu\n", result, reason);
	*resultCode = result;
	return !result;
}
#define SELECTED_DEVICE			"Microsoft Enhanced RSA and AES Cryptographic Provider"
KP_IMPLEMENTATION(KP_RV, __process_enum_devices)(KP_IN KP_JSON hResponse, KP_IN char *szCN)
{
	KP_RV rv = KPTA_OK;
	const kson_node_t *p, *q;
	long i = 0;
	int found = FALSE;
	
	if
	(
		__succeeded(hResponse,"EnumerateDevices command validation...", &rv) &&
		KP_SUCCESS(rv = (p = hResponse->by_path(hResponse, 1, "payload")) ? KPTA_OK : KPTA_INVALID_RESPONSE)
	)
	{
		while (!found && (q = hResponse->by_index(p, i)))
		{
			found = strcmp(SELECTED_DEVICE, q->v.str) == 0;
			i++;
		}
		if (found) sprintf(genCSR_cmd, genCSR_cmd_template, SELECTED_DEVICE, szCN);
		else rv = KP_MESSAGE_ERRORLEVEL;
	}
	return rv;
}
#define CERTIFICATE_REQUEST_FILE		"__new_usr_"
KP_IMPLEMENTATION(KP_RV, __process_genCSR)(KP_IN KP_JSON hResponse, KP_IN char *szLocation, KP_IN char *szIssue, KP_IN char *szSSl)
{
    char szFilename[MAX_PATH], szCmdLine[MAX_PATH], szCMS[MAX_PATH], szTemp[MAX_PATH], szPayload[4096], *szPEM = NULL;
	KP_RV rv = KPTA_OK;
	const kson_node_t *p = NULL;
	KP_JSON_BUILDER hJSON;
    strcpy(szFilename, szLocation);
    strcat(szFilename, PATH_SEPARATOR);
    strcat(szFilename, CERTIFICATE_REQUEST_FILE);
	strcat(szFilename, ".req");
    strcpy(szTemp, szLocation);
    strcat(szTemp, PATH_SEPARATOR);
    strcat(szTemp, CERTIFICATE_REQUEST_FILE);
	strcat(szTemp, ".pem");
	strcpy(szCMS, szLocation);
	strcat(szCMS, PATH_SEPARATOR);
	strcat(szCMS, CERTIFICATE_REQUEST_FILE);
	strcat(szCMS, ".p7b");
	strcpy(szCmdLine, szIssue);
	strcat(szCmdLine, " ");
	strcat(szCmdLine, szFilename);
	strcat(szCmdLine, " ");
	strcat(szCmdLine, szSSl);
	
	if
	(
		__succeeded(hResponse, "GenerateCSR command validation...", &rv) &&
		KP_SUCCESS(rv = (p = hResponse->by_path(hResponse, 1, "payload")) ? KPTA_OK : KPTA_INVALID_RESPONSE)
	)
	{
		strcpy(szPayload, p->v.str);
		__unescape(szPayload);
		rv = __write_file(szFilename, szPayload);
	}
	if (KP_SUCCESS(rv)) rv = __platform_exec_cmdline(szCmdLine);
	if (KP_SUCCESS(rv)) rv = __read_file(szCMS, &szPEM);
	if (KP_SUCCESS(rv) && KP_SUCCESS(rv = new_json_builder(&hJSON)))
	{
		rv = hJSON->begin_write(hJSON);
		if (KP_SUCCESS(rv)) rv = hJSON->put_number(hJSON, "nonce", "0.7047747624699197");
		if (KP_SUCCESS(rv)) rv = hJSON->begin_object(hJSON, "payload");
		if (KP_SUCCESS(rv)) rv = hJSON->put_string(hJSON, "commandID", "f258091d-bd65-49f0-ba7b-a18f182ce6e0");
		if (KP_SUCCESS(rv)) rv = hJSON->begin_object(hJSON, "payload");
		if (KP_SUCCESS(rv)) rv = hJSON->put_string(hJSON, "commandID", "8565fa47-b184-417f-a7a7-62b145a3b260");
		if (KP_SUCCESS(rv)) rv = hJSON->put_string(hJSON, "payload", szPEM);
		if (KP_SUCCESS(rv)) rv = hJSON->end_object(hJSON);
		if (KP_SUCCESS(rv)) rv = hJSON->end_object(hJSON);
		if (KP_SUCCESS(rv))
		{
			hJSON->end_write(hJSON);
			strcpy(install_cert_cmd, (char*) hJSON->_buffer);
		}
		remove(szFilename);
		remove(szCMS);
		remove(szTemp);
		free(szPEM);
		delete_json_builder(hJSON);
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __process_install_cert)(KP_IN KP_JSON hResponse)
{
	KP_RV rv = KPTA_OK;
	__succeeded(hResponse, "InstallCertificate command validation...", &rv);
	return rv;
}
#define TRANSACTION					"This is a document to sign"
KP_IMPLEMENTATION(KP_RV, __process_enum_certs)(KP_IN KP_JSON hResponse, KP_IN char *szUserName)
{
	const kson_node_t *p = NULL, *q, *r;
	KP_JSON_BUILDER hBuilder;
	long idx = 0;
	int found = FALSE;
	char szCN[256], *szTransaction = "01234567";
	KP_RV rv = KPTA_OK;
	if (__succeeded(hResponse, "EnumerateCerts command validation...", &rv))
	{
		strcpy(szCN, "CN=");
		strcat(szCN, szUserName);
		rv = (p = hResponse->by_path(hResponse, 2, "payload", "certificates")) ? KPTA_OK : KPTA_INVALID_RESPONSE;
	}
	while (KP_SUCCESS(rv) && !found && (q = hResponse->by_index(p, idx++)))
	{
		if
		(
			KP_SUCCESS(rv = (r = hResponse->by_key(q, "subject")) ? KPTA_OK : KPTA_INVALID_RESPONSE) &&
			(found = strcmp(szCN, r->v.str) == 0) &&
			KP_SUCCESS(rv = new_json_builder(&hBuilder))
		)
		{
			if
			(
				KP_SUCCESS(rv = hBuilder->begin_write(hBuilder)) &&
				KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "subject", r->v.str)) &&
				KP_SUCCESS(rv = (r = hResponse->by_key(q, "issuer")) ? KPTA_OK : KPTA_INVALID_RESPONSE) &&
				KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "issuer", r->v.str)) &&
				KP_SUCCESS(rv = (r = hResponse->by_key(q, "serial")) ? KPTA_OK : KPTA_INVALID_RESPONSE) &&
				KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "serial", r->v.str))
			)
			{
				hBuilder->end_write(hBuilder);
				sprintf(sign_cmd, sign_cmd_template, (char*) hBuilder->_buffer, szTransaction);
			}
			delete_json_builder(hBuilder);
		}
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __build_verify_cmd)(KP_IN KP_STRING szCmd, KP_IN KP_STRING szPayload, KP_OUT KP_JSON_BUILDER *hOut)
{
	KP_RV rv = KPTA_OK;
	KP_JSON_BUILDER hBuilder = NULL;
	if
	(
		KP_SUCCESS(rv = new_json_builder(&hBuilder)) &&
		KP_SUCCESS(rv = hBuilder->begin_write(hBuilder)) &&
		KP_SUCCESS(rv = hBuilder->put_number(hBuilder, "nonce", "0.7047747624699197")) &&
		KP_SUCCESS(rv = hBuilder->begin_object(hBuilder, "payload")) &&
		KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "commandID", "51d003c8-a028-450d-8c51-41b4ea3bafb0")) &&
		KP_SUCCESS(rv = hBuilder->begin_object(hBuilder, "payload")) &&
		KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "commandID", szCmd)) &&
		KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "payload", szPayload)) &&
		KP_SUCCESS(rv = hBuilder->end_object(hBuilder)) &&
		KP_SUCCESS(rv = hBuilder->end_object(hBuilder))
	)
	{
		hBuilder->end_write(hBuilder);
		*hOut = hBuilder;
	}
	if (KP_FAIL(rv) && hBuilder) delete_json_builder(hBuilder);
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __process_sign)(KP_IN KP_JSON hResponse)
{
	const kson_node_t *p = NULL;
	KP_JSON_BUILDER hBuilder;
	KP_RV rv = KPTA_OK;
	
	if (__succeeded(hResponse, "Sign command validation...", &rv)) rv = (p = hResponse->by_path(hResponse, 1, "payload")) ? KPTA_OK : KPTA_INVALID_RESPONSE;
	if (KP_SUCCESS(rv))
	{
		strcpy(szCMSSigned, p->v.str);
		if (KP_SUCCESS(rv = __build_verify_cmd("cf18e965-ddec-4a15-beb4-567744052cd7", szCMSSigned, &hBuilder)))
		{
			strcpy(signer_id_cmd, (char*) hBuilder->_buffer);
			delete_json_builder(hBuilder);
		}
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __process_signer_id)(KP_IN KP_JSON hResponse)
{
	const kson_node_t *p = NULL, *q;
	KP_JSON_BUILDER hBuilder;
	KP_RV rv = KPTA_OK;

	if (__succeeded(hResponse, "GetSignerID command validation...", &rv)) rv = (p = hResponse->by_path(hResponse, 2, "payload", "signer-certificate")) ? KPTA_OK : KPTA_INVALID_RESPONSE;
	if (KP_SUCCESS(rv)) rv = (q = hResponse->by_key(p, "issuer")) ? KPTA_OK : KPTA_INVALID_RESPONSE;
	if
	(
		KP_SUCCESS(rv) &&
		KP_SUCCESS(rv = (q = hResponse->by_key(p, "serial")) ? KPTA_OK : KPTA_INVALID_RESPONSE) &&
		KP_SUCCESS(rv = __build_verify_cmd("9fe3492a-5a85-45b9-b586-6ca535249fd5", szCMSSigned, &hBuilder))
	)
	{
		strcpy(signer_cert_cmd, (char*) hBuilder->_buffer);
		delete_json_builder(hBuilder);
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __process_signer_cert)(KP_IN KP_JSON hResponse)
{
	const kson_node_t *p;
	KP_JSON_BUILDER hBuilder;
	KP_RV rv = KPTA_OK;
	
	if (__succeeded(hResponse, "GetSignerCert command validation...", &rv) && KP_SUCCESS(rv = (p = hResponse->by_path(hResponse, 1, "payload")) ? KPTA_OK : KPTA_INVALID_RESPONSE))
	{
		strcpy(szCert, p->v.str);
		if (KP_SUCCESS(rv = __build_verify_cmd("f2112590-0868-4872-b112-c0c00f339f78", szCMSSigned, &hBuilder)))
		{
			strcpy(content_cmd, (char*) hBuilder->_buffer);
			delete_json_builder(hBuilder);
		}
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __process_content)(KP_IN KP_JSON hResponse)
{
	const kson_node_t *p;
	KP_JSON_BUILDER hBuilder;
	KP_RV rv = KPTA_OK;
	
	if (__succeeded(hResponse, "GetContent command validation...", &rv) && KP_SUCCESS(rv = (p = hResponse->by_path(hResponse, 1, "payload")) ? KPTA_OK : KPTA_INVALID_RESPONSE))
	{
		strcpy(szContent, p->v.str);
		if (KP_SUCCESS(rv = __build_verify_cmd("2e420e7e-be8e-403a-9ced-674e0467b12f", szCert, &hBuilder)))
		{
			strcpy(is_trusted_cmd, (char*) hBuilder->_buffer);
			delete_json_builder(hBuilder);
		}
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __process_trusted)(KP_IN KP_JSON hResponse)
{
	KP_JSON_BUILDER hBuilder;
	KP_RV rv = KPTA_OK;
	
	if (__succeeded(hResponse, "IsTrusted command validation...", &rv) && KP_SUCCESS(rv = new_json_builder(&hBuilder)))
	{
		if
		(
			KP_SUCCESS(rv = hBuilder->begin_write(hBuilder)) &&
			KP_SUCCESS(rv = hBuilder->put_number(hBuilder, "nonce", "0.7047747624699197")) &&
			KP_SUCCESS(rv = hBuilder->begin_object(hBuilder, "payload")) &&
			KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "commandID", "51d003c8-a028-450d-8c51-41b4ea3bafb0")) &&
			KP_SUCCESS(rv = hBuilder->begin_object(hBuilder, "payload")) &&
			KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "commandID", "308e30e3-cf68-48a1-9476-47319ca72560")) &&
			KP_SUCCESS(rv = hBuilder->begin_object(hBuilder, "payload")) &&
			KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "cms", szCMSSigned)) &&
			KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "payload", szContent)) &&
			KP_SUCCESS(rv = hBuilder->put_boolean(hBuilder, "isBase64", TRUE)) &&
			KP_SUCCESS(rv = hBuilder->put_string(hBuilder, "certificate", szCert)) &&
			KP_SUCCESS(rv = hBuilder->end_object(hBuilder)) &&
			KP_SUCCESS(rv = hBuilder->end_object(hBuilder)) &&
			KP_SUCCESS(rv = hBuilder->end_object(hBuilder))
		)
		{
			hBuilder->end_write(hBuilder);
			strcpy(verify_cmd, (char*) hBuilder->_buffer);
		}
		delete_json_builder(hBuilder);
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __process_verify)(KP_IN KP_JSON hResponse)
{
	KP_JSON_BUILDER hBuilder;
	KP_RV rv = KPTA_OK;
	
	if (__succeeded(hResponse, "Verify command validation...", &rv) && KP_SUCCESS(rv = __build_verify_cmd("83d9121c-7913-414e-a590-6794c5be2ff3", szCMSSigned, &hBuilder)))
	{
		strcpy(verify_all_cmd, (char*) hBuilder->_buffer);
		delete_json_builder(hBuilder);
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __process_verify_all)(KP_IN KP_JSON hResponse)
{
	KP_RV rv = KPTA_OK;
	__succeeded(hResponse, "VerifyAll command validation...", &rv);
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __process_get_config)(KP_IN KP_JSON hResponse)
{
	KP_RV rv = KPTA_OK;
	__succeeded(hResponse, "GetConfig command validation...", &rv);
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __prepare_version_test)(KP_IN char *szVersion)
{
	KP_RV rv = KPTA_OK;
	KP_JSON hJSON;
	char szFile[MAX_PATH], *szDat;
	const kson_node_t *p, *q, *r, *s;

	strcpy(szFile, szVersion);
	strcat(szFile, PATH_SEPARATOR);
	strcat(szFile, "current-version.json");
	if (KP_SUCCESS(rv = new_json(&hJSON)))
	{
		if
		(
			KP_SUCCESS(rv = hJSON->from_file(hJSON, szFile)) &&
			KP_SUCCESS(rv = hJSON->parse(hJSON)) &&
			KP_SUCCESS(rv = (p = hJSON->by_key(hJSON->hParser->root, "components")) ? KPTA_OK : KPTA_INVALID_ARG) &&
			KP_SUCCESS(rv = (q = hJSON->by_index(p, 0)) ? KPTA_OK : KPTA_INVALID_ARG) &&
			KP_SUCCESS(rv = (r = hJSON->by_key(q, "component")) ? KPTA_OK : KPTA_INVALID_ARG) &&
			KP_SUCCESS(rv = (s = hJSON->by_key(q, "hash")) ? KPTA_OK : KPTA_INVALID_ARG)
		)
		{
			sprintf(requires_update_cmd, requires_update_cmd_template, r->v.str, s->v.str);
			if (KP_SUCCESS(rv = (p = hJSON->by_key(q, "file")) ? KPTA_OK : KPTA_INVALID_ARG))
			{
				strcpy(szFile, szVersion);
				strcat(szFile, PATH_SEPARATOR);
				strcat(szFile, p->v.str);
				if (KP_SUCCESS(rv = __read_file(szFile, &szDat)))
				{
					sprintf(update_cmd, update_cmd_template, r->v.str, s->v.str, szDat);
					free(szDat);
				}
			}
		}
		delete_json(hJSON);
	}
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __process_set_config)(KP_IN KP_JSON hResponse, KP_IN char *szVersion)
{
	KP_RV rv = KPTA_OK;
	__succeeded(hResponse, "SetConfig command validation...", &rv);
	/* if (__succeeded(hResponse, "SetConfig command validation...", &rv)) rv = __prepare_version_test(szVersion); */
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __process_requires_update)(KP_IN KP_JSON hResponse)
{
	KP_RV rv = KPTA_OK;
	const kson_node_t *p;
	int requires = FALSE;
	if (__succeeded(hResponse, "RequiresUpdate command validation...", &rv) && KP_SUCCESS(rv = (p = hResponse->by_path(hResponse, 1, "payload")) ? KPTA_OK : KPTA_INVALID_RESPONSE))
	{
		requires = strcmp(p->v.str, "true") == 0;
		printf("%s: %d\n", "Required update", requires);
	}
	if (KP_FAIL(rv) || !requires) update_cmd[0] = 0;
	return rv;
}
KP_IMPLEMENTATION(KP_RV, __process_update)(KP_IN KP_JSON hResponse)
{
	KP_RV rv = KPTA_OK;
	__succeeded(hResponse, "Update command validation...", &rv);
	return rv;
}
int main(int argc, char* argv[])
{
	KP_RV rv = KPTA_OK;
	KP_JSON hJSON = NULL;
	__check_params(argc, argv);
	printf("%s\n", "Testing commands...");
	memset(genCSR_cmd, 0, 2048);
	memset(install_cert_cmd, 0, 4096);
	memset(sign_cmd, 0, 24576);
	memset(update_cmd, 0, 2359296);
	if (KP_SUCCESS(rv = __platform_launch(argv[2])))
	{
		if (KP_SUCCESS(rv = __send_cmd(enum_devices_cmd, &hJSON)))
		{
			rv = __process_enum_devices(hJSON, argv[5]);
			delete_json(hJSON);
			hJSON = NULL;
		}
		if (KP_SUCCESS(rv) && KP_SUCCESS(rv = __send_cmd(genCSR_cmd, &hJSON)))
		{
			rv = __process_genCSR(hJSON, argv[1], argv[3], argv[4]);
			delete_json(hJSON);
			hJSON = NULL;
		}
		if (KP_SUCCESS(rv) && KP_SUCCESS(rv = __send_cmd(install_cert_cmd, &hJSON)))
		{
			rv = __process_install_cert(hJSON);
			delete_json(hJSON);
			hJSON = NULL;
		}
		if (KP_SUCCESS(rv) && KP_SUCCESS(rv = __send_cmd(enum_certs_cmd, &hJSON)))
		{
			rv = __process_enum_certs(hJSON, argv[5]);
			delete_json(hJSON);
			hJSON = NULL;
		}
		if (KP_SUCCESS(rv) && KP_SUCCESS(rv = __send_cmd(sign_cmd, &hJSON)))
		{
			rv = __process_sign(hJSON);
			delete_json(hJSON);
			hJSON = NULL;
		}
		if (KP_SUCCESS(rv) && KP_SUCCESS(rv = __send_cmd(signer_id_cmd, &hJSON)))
		{
			rv = __process_signer_id(hJSON);
			delete_json(hJSON);
			hJSON = NULL;
		}
		if (KP_SUCCESS(rv) && KP_SUCCESS(rv = __send_cmd(signer_cert_cmd, &hJSON)))
		{
			rv = __process_signer_cert(hJSON);
			delete_json(hJSON);
			hJSON = NULL;
		}
		if (KP_SUCCESS(rv) && KP_SUCCESS(rv = __send_cmd(content_cmd, &hJSON)))
		{
			rv = __process_content(hJSON);
			delete_json(hJSON);
			hJSON = NULL;
		}
		if (KP_SUCCESS(rv) && KP_SUCCESS(rv = __send_cmd(is_trusted_cmd, &hJSON)))
		{
			rv = __process_trusted(hJSON);
			delete_json(hJSON);
			hJSON = NULL;
		}
		if (KP_SUCCESS(rv) && KP_SUCCESS(rv = __send_cmd(verify_cmd, &hJSON)))
		{
			rv = __process_verify(hJSON);
			delete_json(hJSON);
			hJSON = NULL;
		}
		if (KP_SUCCESS(rv) && KP_SUCCESS(rv = __send_cmd(verify_all_cmd, &hJSON)))
		{
			rv = __process_verify_all(hJSON);
			delete_json(hJSON);
			hJSON = NULL;
		}
		if (KP_SUCCESS(rv) && KP_SUCCESS(rv = __send_cmd(config_cmd_template, &hJSON)))
		{
			rv = __process_get_config(hJSON);
			delete_json(hJSON);
			hJSON = NULL;
		}
		if (KP_SUCCESS(rv) && KP_SUCCESS(rv = __send_cmd(set_cfg_cmd_template, &hJSON)))
		{
			rv = __process_set_config(hJSON, NULL);
			delete_json(hJSON);
			hJSON = NULL;
		}
/*		if (KP_SUCCESS(rv) && KP_SUCCESS(rv = __send_cmd(requires_update_cmd, &hJSON)))
		{
			rv = __process_requires_update(hJSON);
			delete_json(hJSON);
			hJSON = NULL;
		}
		if (KP_SUCCESS(rv) && update_cmd && KP_SUCCESS(rv = __send_cmd(update_cmd, &hJSON)))
		{
			rv = __process_update(hJSON);
			delete_json(hJSON);
			hJSON = NULL;
		} */
		__platform_write(exit_cmd);
		__platform_release();
	}
	return (int) rv;
}