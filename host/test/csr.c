/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * CSR generation test facility
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "test.h"

#include <stdio.h>
#include <string.h>
#include <errno.h>

#define FORMAT		"%s\n\t%s\n\t%s\n\t%s\n\t%s\n"
#define CMD_LINE		"usage: csr_t <szLocation> <szCommonName> <szSlotName> <szFilename>, where:"
#define ARG_1		"szLocation: test directory. Must exist."
#define ARG_2           "szCommonName: certificate request subject Common Name."
#define ARG_3           "szSlotName: cryptographic slot to be used."
#define ARG_4           "szFilename: PEM request file to generated at szLocation."
void check_params(int argc, char *argv[])
{
	if (argc < 5)
	{
		printf(FORMAT, CMD_LINE, ARG_1, ARG_2, ARG_3, ARG_4);
		exit(KP_TEST_ERRORLEVEL);
	}
}
int toFile(KP_IN KP_STRING szPEM, KP_IN KP_STRING szPath, KP_IN KP_STRING szFilename)
{
    char szTarget[MAX_PATH];
    FILE *out;
    int ret = 0;
    strcpy(szTarget, szPath);
    strcat(szTarget, PATH_SEPARATOR);
    strcat(szTarget, szFilename);
    if ((out = fopen(szTarget, "w")))
    {
        if (fwrite(szPEM, 1, strlen(szPEM), out) != strlen(szPEM)) ret = ferror(out);
        fclose(out);
    }
    else ret = errno;
    return ret;
}
int main(int argc, char* argv[])
{
	int ret = KP_SUCCESS_ERRORLEVEL, count = 0, found = FALSE, i = 0;
	KP_RV rv;
	KP_ENV env = NULL;
	KP_STRING *szSlots = NULL, szPEM = NULL;

	check_params(argc, argv);
	printf("%s", "Generate CSR test... ");
	if
	(
		KP_SUCCESS(rv = new_environment(&env)) &&
		KP_SUCCESS(rv = env->hLogger->set_level(env->hLogger, KP_LOG_DEBUG))
	)
	{
	if (KP_SUCCESS(rv = KPTA_enumerate_slots(env, &szSlots, &count)))
	{
		while (i < count && !found) found = strcmp(szSlots[i++], argv[3]) == 0;
		if (found)
		{
			if (KP_SUCCESS(rv = KPTA_generate_CSR(env, argv[3], argv[2], &szPEM)))
			{
				if ((ret = toFile(szPEM, argv[1], argv[4]))) printf("%s with error code %d\n", "failed!", ret);
				else printf("%s\n", "succeeded!");
				free(szPEM);
			}
			else
			{
				printf("%s with error code %llu\n", "failed!", rv);
				ret = KP_GENCSR_ERRORLEVEL;
			}
		}
		else
		{
			printf("%s\n", "failed! Cryptographic slot not found");
			ret = KP_TEST_ERRORLEVEL;
		}
		env->free_buffer(szSlots, count);
	}
	else
	{
		printf("%s with error code %llu\n", "failed!", rv);
		ret = KP_ENUM_ERRORLEVEL;
	}
	delete_environment(env);
	}
    else
    {
        printf("%s with error code %llu\n", "failed!", rv);
        ret = KP_ENV_ERRORLEVEL;
    }
    return ret;
}
