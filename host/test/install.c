/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * CSR generation test facility
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "test.h"

#include <stdio.h>
#include <string.h>
#include <errno.h>

#define FORMAT			"%s\n\t%s\n"
#define CMD_LINE		"usage: install_t <szFilename>, where:"
#define ARG_1           "szFilename: complete path to PEM encoded PKCS #7 file."
void check_params(int argc, char *argv[])
{
	if (argc != 2 || __ACCESS(argv[1], __RW))
	{
		printf(FORMAT, CMD_LINE, ARG_1);
		exit(KP_TEST_ERRORLEVEL);
	}
}
#define ROUNDUP(x)	(--(x), (x)|=(x)>>1, (x)|=(x)>>2, (x)|=(x)>>4, (x)|=(x)>>8, (x)|=(x)>>16, ++(x))
static int fromFile(KP_IN KP_STRING szFilename, KP_OUT KP_STRING *szPEM)
{
    FILE *in;
    int ret = 0, i, max = 0, len = 0;
    char buf[512], *szData = NULL;
    if ((in = fopen(szFilename, "r")))
    {
        while ((i = fread(buf, 1, sizeof(buf), in)))
        {
            if (len + i + 1 > max)
            {
                max = len + i + 1;
                ROUNDUP(max);
                szData = (char*) realloc(szData, max);
            }
            memcpy(szData + len, buf, i);
            len += i;
        }
        if (feof(in))
        {
            szData[len] = 0;
            *szPEM = szData;
        }
        else
        {
            ret = ferror(in);
            if (szData) free(szData);
        }
        fclose(in);
    }
    else ret = errno;
    return ret;
}
int main(int argc, char* argv[])
{
    int ret = KP_SUCCESS_ERRORLEVEL;
    KP_RV rv;
    KP_ENV env;
    KP_STRING szPEM;
    check_params(argc, argv);
    printf("%s", "Install signed certificate test... ");
    if
	(
		KP_SUCCESS(rv = new_environment(&env)) &&
		KP_SUCCESS(rv = env->hLogger->set_level(env->hLogger, KP_LOG_DEBUG))
	)
    {
        if (!(ret = fromFile(argv[1], &szPEM)))
        {
            if (KP_SUCCESS(rv = KPTA_install_certificate(env, szPEM))) printf("%s\n", "succeeded!");
            else
            {
                printf("%s with error code %llu\n", "failed!", rv);
                ret = KP_INSTALL_ERRORLEVEL;
            }
            free(szPEM);
        }
        else printf("%s with error code %d\n", "failed!", ret);
        delete_environment(env);
    }
    else
    {
        printf("%s with error code %llu\n", "failed!", rv);
        ret = KP_ENV_ERRORLEVEL;
    }
    return ret;
}