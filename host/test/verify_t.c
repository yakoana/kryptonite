/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Signature verification test facility
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "test.h"
#include "verify.h"
#include <stdio.h>
#include <string.h>

#define FORMAT		"%s\n%s\t%s\n"
#define CMD_LINE		"Usage: verify_t <szLocation> <szFilename>, where:"
#define ARG_1		"szLocation: test directory. Must exist."
#define ARG_2           "szFilename: PEM PKCS #7 file to read at szLocation."
void check_params(int argc, char *argv[])
{
	if (argc != 3)
	{
		printf(FORMAT, CMD_LINE, ARG_1, ARG_2);
		exit(KP_TEST_ERRORLEVEL);
	}
}
#define ROUNDUP(x)	(--(x), (x)|=(x)>>1, (x)|=(x)>>2, (x)|=(x)>>4, (x)|=(x)>>8, (x)|=(x)>>16, ++(x))
static int fromFile(KP_IN KP_STRING szFilename, KP_OUT KP_STRING *szPEM)
{
    FILE *in;
    int ret = 0, i, max = 0, len = 0;
    char buf[512], *szData = NULL;
    if ((in = fopen(szFilename, "r")))
    {
        while ((i = fread(buf, 1, sizeof(buf), in)))
        {
            if (len + i + 1 > max)
            {
                max = len + i + 1;
                ROUNDUP(max);
                szData = (char*) realloc(szData, max);
            }
            memcpy(szData + len, buf, i);
            len += i;
        }
        if (feof(in))
        {
            szData[len] = 0;
            *szPEM = szData;
        }
        else
        {
            ret = ferror(in);
            if (szData) free(szData);
        }
        fclose(in);
    }
    else ret = errno;
    return ret;
}

int main(int argc, char* argv[])
{
	KP_RV rv;
	int ret = KP_SUCCESS_ERRORLEVEL;
	KP_ENV env;
	char szFilename[MAX_PATH];
	KP_STRING szPEM, szCert = NULL, szContent = NULL;
	KP_JSON_BUILDER hBuilder = NULL;
	check_params(argc, argv);
	strcpy(szFilename, argv[1]);
	strcat(szFilename, PATH_SEPARATOR);
	strcat(szFilename, argv[2]);
	printf("%s", "Verify signature test... ");
	if
	(
		KP_SUCCESS(rv = new_environment(&env)) &&
		KP_SUCCESS(rv = env->hLogger->set_level(env->hLogger, KP_LOG_DEBUG))
	)
	{
		if (!(ret = fromFile(szFilename, &szPEM)))
		{
			if
			(
				KP_SUCCESS(rv = KPTA_verify_all(env, szPEM)) &&
				KP_SUCCESS(rv = KPTA_signer_id(env, szPEM, &hBuilder)) &&
				KP_SUCCESS(rv = KPTA_signer_cert(env, szPEM, &szCert)) &&
				KP_SUCCESS(rv = KPTA_trusted(env, szCert)) &&
				KP_SUCCESS(rv = KPTA_content(env, szPEM, &szContent)) &&
				KP_SUCCESS(rv = KPTA_verify(env, szPEM, szContent, TRUE, szCert))
			)	printf("%s\n", "succeeded!");
			else
			{
				printf("%s with error code %llu\n", "failed!", rv);
				ret = KP_INSTALL_ERRORLEVEL;
			}
			free(szPEM);
		}
		else printf("%s with error code %d\n", "failed!", ret);
		if (hBuilder) env->delete_json_builder(hBuilder);
		if (szCert) free(szCert);
		if (szContent) free(szContent);
		delete_environment(env);
	}
	else
	{
		printf("%s with error code %llu\n", "failed!", rv);
		ret = KP_ENV_ERRORLEVEL;
	}
    return ret;
}