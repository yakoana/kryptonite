/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Infrastructure regression test facility
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "test.h"
#include <stdlib.h>
#include <stdio.h>

#define FORMAT		"%s\n\t%s\n\t%s\n"
#define CMD_LINE		"usage: infra_t <szSource> <szLocation>, where:"
#define ARG_1		"szSource: complete path to config.json file to be used during test and"
#define ARG_2		"szLocation: test directory, with trailing slash. Both must exist."
void check_params(int argc, char *argv[])
{
	if (argc != 3 || __ACCESS(argv[1], __RW))
	{
		printf(FORMAT, CMD_LINE, ARG_1, ARG_2);
		exit(KP_TEST_ERRORLEVEL);
	}
}
int main(int argc, char* argv[])
{
	int ret = KP_SUCCESS_ERRORLEVEL;
	KP_RV rv;
	KP_LOG hLogger;

	check_params(argc, argv);
	printf("%s\n%s\n", "* * * * * * * * * * * * * * *", "Initializing infrastructure regression test");
	if (KP_SUCCESS(rv = new_logger(&hLogger)))
	{
		if
		(
			KP_SUCCESS(rv = hLogger->set_level(hLogger, KP_LOG_DEBUG)) &&
			KP_SUCCESS(rv = hLogger->open(hLogger))
		)
		{
			hLogger->debug(hLogger, DEVELOPMENT_CATEGORY, KPL_OBJECT_CREATED, "KP_LOG");
			if (KP_SUCCESS((ret = check_json(hLogger))) && KP_SUCCESS((ret = check_config(hLogger, argv[1], argv[2]))) && KP_SUCCESS((ret = check_json_facility()))) printf("%s\n%s\n", "Infrastructure regression test finished", "* * * * * * * * * * * * * * *");
			hLogger->close(hLogger);
		}
		else
		{
			printf("failed! KP_LOG facility failure. Error code %llu\n", rv);
			ret = KP_LOGGER_ERRORLEVEL;
		}
		delete_logger(hLogger);
	}
	else
	{
		printf("failed! KP_LOG facility failure. Error code %llu\n", rv);
		ret = KP_LOGGER_ERRORLEVEL;
	}

	return ret;
}
