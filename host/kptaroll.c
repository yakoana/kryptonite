/** * * * * * * * * * * * * * * * * * * * * * * *
 * Kryptonite
 * Cryptographic extension for Internet browsers
 * Enrollment shared pobject implementation
 * Copyleft (C) 2018 The Crypthing Initiative
 * Authors:
 * 		diego.sohsten@gmail.com
 * 		yorick.flannagan@gmail.com
 *  * * * * * * * * * * * * * * * * * * * * * * */
#include "enroll.h"
#include <string.h>
#include <stdio.h>

#if (defined(_WIN32))

#include <SDKDDKVer.h>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

BOOL APIENTRY DllMain(HMODULE hModule, DWORD  ul_reason, LPVOID lpReserved)
{
    UNREFERENCED_PARAMETER(hModule);
    UNREFERENCED_PARAMETER(lpReserved);
	switch (ul_reason)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

#endif


#define NATIVE_ENUM_DEVICES_CMD			"aae1b8ce-9c85-4b52-a80b-578e36a3a775"
#define NATIVE_GEN_CSR_CMD				"6c17f198-385c-4ba6-af2b-59b40c2950f9"
#define NATIVE_INSTALL_CERT_CMD			"8565fa47-b184-417f-a7a7-62b145a3b260"

KP_FUNCTION(KP_RV, KP_execute_command)(KP_IN KP_ENV env, KP_IN KP_JSON input, KP_OUT KP_JSON_BUILDER *output)
{
	KP_RV rv = KPTA_OK, ret;
	KP_PROCESSOR hProcessor = NULL;
	const kson_node_t *q, *p;

	int count = 0, i = 0;
	char **szSlots = NULL, szDevice[4096], szCN[4096], *szCSR = NULL;

	env->hLogger->debug(env->hLogger, ENROLLMENT_CATEGORY, KPL_OP_INIT, ENROLL_CMD_OP, input->raw);
	if
	(
		KP_SUCCESS(ret = env->new_processor(&hProcessor)) &&
		KP_SUCCESS(ret = hProcessor->init(hProcessor, input)) &&
		KP_SUCCESS(ret = (q = hProcessor->command(hProcessor)) ? KPTA_OK : KPTA_INVALID_ARG)
	)
	{
		if (strcmp(q->v.str, NATIVE_ENUM_DEVICES_CMD) == 0)
		{
			if
			(
				KP_SUCCESS(rv = KPTA_enumerate_slots(env, &szSlots, &count)) &&
				KP_SUCCESS(ret = hProcessor->hOutput->begin_array(hProcessor->hOutput, "payload"))
			)
			{
				while (KP_SUCCESS(ret) && i < count) ret = hProcessor->hOutput->put_string(hProcessor->hOutput, NULL, szSlots[i++]);
				if (KP_SUCCESS(ret)) ret = hProcessor->hOutput->end_array(hProcessor->hOutput);
			}
			env->free_buffer(szSlots, count);
		}
		else if (strcmp(q->v.str, NATIVE_GEN_CSR_CMD) == 0)
		{
			if
			(
				KP_SUCCESS(rv = (p = input->by_path(input, 3, "payload", "payload", "payload")) ? KPTA_OK : KPTA_INVALID_ARG) &&
				KP_SUCCESS(rv = (q = input->by_key(p, "device")) ? KPTA_OK : KPTA_INVALID_ARG) &&
				strcpy(szDevice, q->v.str) &&
				KP_SUCCESS(rv = (q = input->by_key(p, "commonName")) ? KPTA_OK : KPTA_INVALID_ARG) &&
				strcpy(szCN, q->v.str) &&
				KP_SUCCESS(rv = KPTA_generate_CSR(env, szDevice, szCN, &szCSR))
			)	ret = hProcessor->hOutput->put_string(hProcessor->hOutput, "payload", szCSR);
			if (szCSR) free(szCSR);
		}
		else if (strcmp(q->v.str, NATIVE_INSTALL_CERT_CMD) == 0) if (KP_SUCCESS(rv = (p = input->by_path(input, 3, "payload", "payload", "payload")) ? KPTA_OK : KPTA_INVALID_ARG)) rv = KPTA_install_certificate(env, p->v.str);
		else rv = KPTA_INVALID_ARG;
	}
	if (KP_SUCCESS(ret) && KP_SUCCESS(ret = hProcessor->finish(hProcessor, rv)))
	{
		env->hLogger->info(env->hLogger, ENROLLMENT_CATEGORY, KPL_OPERATION_SUCCEEDED, ENROLL_CMD_OP);
		env->hLogger->debug(env->hLogger, ENROLLMENT_CATEGORY, KPL_CMD_EXECUTED, (char*) hProcessor->hOutput->_buffer);
		*output = hProcessor->hOutput;
	}
	else
	{
		if (hProcessor->hOutput) env->delete_json_builder(hProcessor->hOutput);
		env->hLogger->error(env->hLogger, ENROLLMENT_CATEGORY, KPL_OPERATION_FAILURE, ret, ENROLL_CMD_OP);
	}
	if (hProcessor) env->delete_processor(hProcessor);
	return ret;
}
