/// <reference types="chrome" />

////////////////////
// Global object
////////////////////
interface Window
{
    browser: typeof chrome;
    msBrowser: typeof chrome;
    host: typeof chrome;
}

declare namespace chrome.runtime
{
    /** The reason that this event is being dispatched. */
    enum OnInstalledReason
    {
        install = "install",
        update = "update",
        browser_update = "browser_update",
        chrome_update = "chrome_update",
        shared_module_update = "shared_module_update"
    }
}
