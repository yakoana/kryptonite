﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Json;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Listener
{
	  public class Uninstall : POSTProcessor
	  {
		    protected string _arguments;
		    public override void Init(HttpListenerRequest request)
		    {
				base.Init(request);
				if (_body.ContainsKey("uninstallString")) _arguments = _body["uninstallString"].ToString().Replace("MsiExec.exe ", "").Replace("/ I", "/x") + " /passive";
		    }
		    public override Response Execute()
		    {
				Response ret = new Response();
				Process p = new Process();
				int reason = 0;
				try
				{
					  p.StartInfo.FileName = "MsiExec.exe";
					  p.StartInfo.Arguments = _arguments;
					  p.StartInfo.UseShellExecute = false;
					  p.StartInfo.CreateNoWindow = false;
					  p.Start();
					  p.WaitForExit();
					  reason = p.ExitCode;
				}
				catch (Exception e)
				{
					  Console.WriteLine(e.Message);
					  reason = (int)ReasonCode.INTERNAL_ERROR;
				}
				finally { p.Close(); }
				JsonObject json = (JsonObject)JsonValue.Parse("{}");
				json.Add("success", new JsonPrimitive(reason == 0));
				json.Add("reason", new JsonPrimitive(reason));
				ret.Result = _encoding.GetBytes(json.ToString());
				return ret;
		    }
	  }
	  public class Repair : Uninstall
	  {
		    public override void Init(HttpListenerRequest request)
		    {
				base.Init(request);
				if (_body.ContainsKey("modifyPath")) _arguments = _body["modifyPath"].ToString().Replace("MsiExec.exe ", "") + "ADDLOCAL=ALL /passive";
		    }
	  }
	  public class DownloadInstaller : AsyncProcessor
	  {
		    private static readonly HttpClient _client = new HttpClient() { Timeout = TimeSpan.FromMinutes(4) };
		    private string _uri;
		    private FileInfo _target;
		    private FileInfo GetNewTarget()
		    {
				FileInfo ret = null;
				while (ret == null)
				{
					  ret = new FileInfo(Directory.GetCurrentDirectory() + "\\" + Path.GetRandomFileName());
					  if (ret.Exists) ret = null;
				}
				return ret;
		    }
		    public override void Init(HttpListenerRequest request)
		    {
				base.Init(request);
				_uri = Resources.INSTALLER_URI;
				_target = GetNewTarget();
		    }
		    delegate void ProgressChangedHandler(long? fileSize, long downloaded, bool isDone);
		    event ProgressChangedHandler ProgressChanged;
		    async Task<bool> Start()
		    {
				HttpResponseMessage response = await _client.GetAsync(_uri, HttpCompletionOption.ResponseHeadersRead);
				try
				{
					  response.EnsureSuccessStatusCode();
					  long? total = response.Content.Headers.ContentLength;
					  Stream content = await response.Content.ReadAsStreamAsync();
					  try { return await ProcessContentStream(total, content); }
					  finally { content.Dispose(); }
				}
				finally { response.Dispose();  }
		    }
		    private async Task<bool> ProcessContentStream(long? fileSize, Stream content)
		    {
				long total = 0L;
				long count = 0L;
				bool hasMore = true;
				byte[] buffer = new byte[8192];
				FileStream fs = new FileStream(_target.FullName, FileMode.Create, FileAccess.Write, FileShare.None, 8192, true);
				try
				{
					  do
					  {
						    int read = await content.ReadAsync(buffer, 0, buffer.Length);
						    if (read == 0)
						    {
								hasMore = false;
								TriggerProgressChanged(fileSize, total, true);
						    }
						    else
						    {
								await fs.WriteAsync(buffer, 0, read);
								total += read;
								count += 1;
								if (count % 100 == 0) TriggerProgressChanged(fileSize, total, false);
						    }
					  }
					  while (hasMore);
				}
				finally { fs.Close();  }
				return true;
		    }
		    private void TriggerProgressChanged(long? fileSize, long read, bool isDone)
		    {
				if (ProgressChanged == null) return;
				ProgressChanged(fileSize, read, isDone);
		    }
		    public override void ExecuteAsync(HttpListenerResponse response)
		    {
				try
				{
					  response.ProtocolVersion = new Version("1.1");
					  response.StatusCode = (int)HttpStatusCode.OK;
					  response.StatusDescription = "OK";
					  response.KeepAlive = true;
					  response.ContentType = "text/event-stream";
					  Stream stream = response.OutputStream;
					  try
					  {
						    stream.Flush();
						    Task<bool> ret = Start();
						    Exception ex = null;
						    ProgressChanged += (long? fileSize, long downloaded, bool isDone) =>
						    {
								JsonObject json = (JsonObject)JsonValue.Parse("{}");
								json.Add("success", new JsonPrimitive(ex == null));
								if (ex != null) json.Add("reason", new JsonPrimitive(ex.Message));
								if (isDone) json.Add("token", new JsonPrimitive(_target.Name));
								if (fileSize.HasValue) json.Add("fileSize", new JsonPrimitive(fileSize.Value));
								json.Add("downloaded", new JsonPrimitive(downloaded));
								byte[] data = UTF8Encoding.UTF8.GetBytes("data: " + json.ToString() + "\n\n");
								stream.Write(data, 0, data.Length);
								stream.Flush();
						    };
						    try { ret.Wait(); }
						    catch (Exception e) { Console.WriteLine(e.Message); ex = e; }
					  }
					  finally { stream.Close(); }
				}
				finally { response.Close(); }
		    }
	  }
	  public class EnableExtensions : POSTProcessor
	  {
		    private List<string> _extensions;
		    public override void Init(HttpListenerRequest request)
		    {
				base.Init(request);
				_extensions = new List<string>();
				JsonArray param = (JsonArray)_body["correct"];
				for (int i = 0; i < param.Count; i++) _extensions.Add(param[i]);
		    }
		    public override Response Execute()
		    {
				Response ret = new Response();
				ReasonCode reason = ReasonCode.SUCCESS;
				try
				{
					  JsonValue prefs = ExtensionsEnableCheck.GetSecurePreferences();
					  JsonObject settings = (JsonObject)prefs["extensions"]["settings"];
					  for (int i = 0; i < _extensions.Count; i++)
					  {
						    if (settings.ContainsKey(_extensions[i]))
						    {
								JsonObject ext = (JsonObject)settings[_extensions[i]];
								if (ext["state"] == 0)
								{
									  if (!ext.ContainsKey("disable_reasons")) throw new ApplicationException("Unexpected value in Secure Preferences file");
									  if (ext["disable_reasons"] == 8192 && ext.ContainsKey("ack_prompt_count")) { ext["ack_prompt_count"] = 0; }
									  else if (ext["disable_reasons"] == 1) { ext["disable_reasons"] = 8192; }
								}
								else if (ext["state"] == 2) { ext["state"] = 0; }
						    }
					  }
					  ExtensionsEnableCheck.SaveSecurePreferences(prefs);
				}
				catch (Exception e)
				{
					  Console.WriteLine(e.Message);
					  Console.WriteLine(e.StackTrace);
					  reason = ReasonCode.INTERNAL_ERROR;
				}
				JsonObject json = (JsonObject)JsonValue.Parse("{}");
				json.Add("success", new JsonPrimitive(reason == ReasonCode.SUCCESS));
				json.Add("reason", new JsonPrimitive((int)reason));
				ret.Result = _encoding.GetBytes(json.ToString());
				return ret;
		    }
	  }
}