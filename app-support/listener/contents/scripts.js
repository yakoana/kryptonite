"use strict";


function prepare()
{
	let acc = document.getElementsByClassName("accordion");
	let i;
	for (i = 0; i < acc.length; i++) acc[i].addEventListener
	(
		"click",
		function()
		{
			this.classList.toggle("active");
			let panel = this.nextElementSibling;
			if (panel.style.maxHeight) panel.style.maxHeight = null;
			else panel.style.maxHeight = panel.scrollHeight + "px";
		}
	);
	let bar = document.getElementById("bar");
	if (bar)
	{
		let tabs = document.getElementsByClassName("tab");
		let i;
		for (i = 0; i < tabs.length; i++)
		{
			if (!tabs[i].classList.contains("tabc"))
			{
				let span = document.createElement("SPAN");
				span.classList.add("step");
				bar.appendChild(span);
			}
		}
	}
	navigate(1);
}
function wait()
{
	let div = document.createElement("DIV");
	div.classList.add("circle");
	let parent = document.getElementById("client");
	if (parent)
	{
		div.style.top = parent.clientHeight / 2 + "px";
		div.style.left = parent.clientWidth / 2 + "px";
		parent.appendChild(div);
	}
	return div;
}
function stopWait(div)
{
	let parent = document.getElementById("client");
	if (parent) parent.removeChild(div);
}
function showCheckLabel(tab, n, success)
{
	let tick, char, label;
	if (success)
	{
		tick = "tick-good";
		char = "&#10003;";
		label = "Verificação bem sucedida";
	}
	else
	{
		tick = "tick-bad";
		char = "&#9747;";
		label = "Verificação mal sucedida";
	}
	let td = tab.getElementsByClassName("tick");
	let button = tab.getElementsByClassName("accordion");
	if (td[n] && button[n])
	{
		let span = document.createElement("SPAN");
		span.classList.add(tick);
		span.innerHTML = char;
		td[n].appendChild(span);
		button[n].innerHTML = label;
	}
}
function showResult(tab, n, item, ...subitems)
{
	let div = tab.getElementsByClassName("panel");
	if (div[n])
	{
		let pitem = document.createElement("P");
		pitem.classList.add("item");
		pitem.innerHTML = item;
		div[n].appendChild(pitem);
		if (subitems) subitems.forEach(function(value)
		{
			if (value)
			{
				let p = document.createElement("P");
				p.classList.add("subitem");
				p.innerHTML = value;
				div[n].appendChild(p);
			}
		});
	}
}
function showCommFailure(tab, n, msg)
{
	showCheckLabel(tab, n, false);
	showResult(tab, n, "Falha na comunicação com a aplicação de diagnóstico:", msg);
}
function showErr(tab, n, subi, subj)
{
	showCheckLabel(tab, n, false);
	showResult(tab, n, "Motivo", subi);
	showResult(tab, n, "Correção sugerida", subj);
}
function showSuccess(tab, n, item, ...subitems)
{
	showCheckLabel(tab, n, true);
	showResult(tab, n, item, ...subitems);
}


const CONNECTIVITY_CMD = "/kryptonite/check-connectivity";
const REGISTRY_CMD = "/kryptonite/check-registry";
const FILESYS_CMD = "/kryptonite/check-filesys";
const ENABLED_CMD = "/kryptonite/check-enable";
const STORE_URI = "https://clients2.google.com/service/update2/crx";

const SUCCESS = 0;  
const SECURITY_ERROR = 1;
const REGISTRY_ERROR = 2;
const INTERNAL_ERROR = 3;
const CONNECTIVITY_ERROR = 4;
const RESPONSE_ERROR = 5;
const HOME_NOT_FOUND_ERROR = 6;
const EXT_NOT_FOUND_ERROR = 7;
const FILES_NOT_FOUND_ERROR = 8;
const MF_NOT_FOUND_ERROR = 9;
const NOT_LISTED_EXT_ERROR = 10;
const NOT_ENABLED_EXT_ERROR = 11;
const DISABLED_EXT_ERROR = 12;
const REMOVED_EXT_ERROR = 13;

const HTML_ELEMENT_ID = "kryptonite_install_signal";
const ZIP_EXTENSION_ID = "zip@kryptonite";
const ENROLL_EXTENSION_ID = "enroll@kryptonite";
const SIGN_EXTENSION_ID = "sign@kryptonite";
const VERIFY_EXTENSION_ID = "verify@kryptonite";

const TAB_ERROR = "A aplicação de diagnóstico não está funcionando apropriadamente. Tab de exibição não encontrada";
const INTERNAL_ERR_MSG = "Erro interno da aplicação de diagnóstico";
const UNESPECTED_ERROR = "Erro não catalogado";
// Connectivity step
const CONNECTIVITY_ERROR_MESSAGE =
	"A aplicação foi incapaz de acessar o endereço da Loja de extensões. " +
	"Isso pode ter sido causado tanto por indisponibilidade de acesso à Internet " +
	"como por restrição de acesso ao endereço " + STORE_URI + ", restrição que, " +
	"se aplicada por administrador da rede, é impeditiva para o uso das extensões. " +
	"O aplicativo de diagnóstico não tem como determinar a causa desse tipo de problema.";
// Registry step
const REG_PERMISSION_ERROR = "Falta de permissão de acesso ao registro do Windows";
const REG_ACCESS_ERROR = "Falha no acesso ao banco de dados de registro de Windows";
const INSTALL_ERROR = "Registro da instalação da Kryptonita não encontrado";
const FOLDERS_ERROR = "Chaves de Registro incompatíveis entre si";
const ZIP_ERROR = "Extensão não instalada";
const EXT_ERROR = "Extensões não instaladas";
// File system step
const FILE_NOT_FOUND_ERROR = "Arquivo [_FILE_] não encontrado no diretório de instalação";
const FILE_NAME = "[_FILE_]";
// Extensions enable step
const EXT_NOT_INSTALLED = "Extensão não instalada";
const EXT_ENABLED = "Extensão habilitada";
const NOT_LISTED_MESSAGE = "Extensão não listada no arquivo Secure Preferences do navegador";
const NOT_ENABLED_MESSAGE = "Extensão não habilitada no navegador";
const DISABLED_MESSAGE = "Extensão desabilitada no navegador";
const REMOVED_MESSAGE = "Extensão removida do navegador";
// Extensions capabilities check step
const CHECK_SUCCESS = "Verificação de capacidade bem sucedida";
const CHECK_FAILED = "Ocorreu o erro número [_REASON_] durante a verificação da extensão [_EXT_]";
const REASON = "[_REASON_]";
const EXT = "[_EXT_]";
const PROVIDER = "Microsoft Enhanced RSA and AES Cryptographic Provider";
const NO_PROVIDER = "Para emitir certificados é necessário a instalação do provider " + PROVIDER + "no sistema operacional";
const NO_CERTIFICATES = "Para assinar um documento é necessário solicitar e instalar um certificado digital";

const ADM_CORRECTION = "Consulte os administradores de rede";
const INSTALL_CORRECTION = "Obter a versão mais recente do instalador<br>Instalar novamente o aplicativo";
const REINSTALL_CORRECTION = "Desinstalar o aplicativo<br>" + INSTALL_CORRECTION;
const ALT_CORRECTION = "Alterar a instalação corrente, adicionando pelo menos uma extensão";
const PREFS_CORRECTION = "Restaurar os valores do arquivo de preferências do navegador para o original";


function init(n) { }
function connectivityCheck(n)
{
	let tabs = document.getElementsByClassName("tab");
	if (!tabs[n]) { console.error(TAB_ERROR); return; }
	let div = wait();
	let client = new XMLHttpRequest();
	client.addEventListener("load", function()
	{
		try
		{
			if (client.status != 200) throw new Error("HTTP Status Code: " + client.status);
			let response = JSON.parse(client.responseText);
			if (response.success) { showSuccess(tabs[n], 0, "Loja acessada", STORE_URI); }
			else
			{
				_tasks[n].error = true;
				showErr(tabs[n], 0, CONNECTIVITY_ERROR_MESSAGE, ADM_CORRECTION);
			}
		}
		catch (e)
		{
			let msg = e ? (typeof (e) === "object" ? JSON.stringify(e) : e) : "Erro inesperado";
			_tasks[n].error = true;
			showCommFailure(tabs[n], 0, msg);
		}
		_tasks[n].done = true;
		stopWait(div);
	});
	client.addEventListener("error", function(e)
	{
		let msg = e ? (typeof (e) === "object" ? JSON.stringify(e) : e) : "Erro inesperado";
		_tasks[n].error = true;
		_tasks[n].done = true;
		showCommFailure(tabs[n], 0, msg);
		stopWait(div);
	});
	client.open("POST", CONNECTIVITY_CMD);
	client.setRequestHeader("Content-Type", "text/plain");
	client.send("{}");
}
function installCheck(n)
{
	const INSTALL_STEP = 0;
	const REGISTRY_STEP = 1;
	const ZIP_STEP = 2;
	const OPTS_STEP = 3;

	let tabs = document.getElementsByClassName("tab");
	if (!tabs[n]) { console.error(TAB_ERROR); return; }
	let i;
	let div = wait();
	let client = new XMLHttpRequest();
	let param = {};
	client.addEventListener("load", function()
	{
		try
		{
			let motive, correction;
			if (client.status != 200) throw new Error("HTTP Status Code: " + client.status);
			let response = JSON.parse(client.responseText);
			if (!response.success)
			{
				switch (response.reason)
				{
				case SECURITY_ERROR:
					motive = REG_PERMISSION_ERROR;
					break;
				case REGISTRY_ERROR:
					motive = REG_ACCESS_ERROR;
					break;
				case INTERNAL_ERROR:
					motive = INTERNAL_ERR_MSG;
					break;
				default:
					motive = UNESPECTED_ERROR;
				}
				for (i = INSTALL_STEP; i <= OPTS_STEP; i++) showErr(tabs[n], i, motive, ADM_CORRECTION);
				stopWait(div);
				return;
			}
			if (response.details.install.installed)
			{
				showSuccess
				(
					tabs[n],
					INSTALL_STEP,
					"Comandos para o instalador:",
					"Alterar: " + response.details.install.modifyPath,
					"Remover: " + response.details.install.uninstallString
				);
			}
			else
			{
				for (i = INSTALL_STEP; i <= OPTS_STEP; i++) showErr(tabs[n], i, INSTALL_ERROR, INSTALL_CORRECTION);
				_tasks[n].error = true;
				param.installer = INSTALLER_URI;
			}
			if (response.details.folders.consistent)
			{
				showSuccess
				(
					tabs[n],
					REGISTRY_STEP,
					"Elementos registrados:",
					"Diretório de instalação: " + response.details.folders.home,
					"Manifesto do Chrome: " + response.details.folders.manifest
				);
			}
			else
			{
				showErr(tabs[n], REGISTRY_STEP, FOLDERS_ERROR, REINSTALL_CORRECTION);
				param.uninstallString = response.details.install.uninstallString;
				param.installer = INSTALLER_URI;
				_tasks[n].error = true;
			}
			if (response.details.zip.installed) showSuccess(tabs[n], ZIP_STEP, "Identificador da extensão:", response.details.zip.id);
			else
			{
				showErr(tabs[n], ZIP_STEP, ZIP_ERROR, REINSTALL_CORRECTION);
				param.uninstallString = response.details.install.uninstallString;
				param.installer = INSTALLER_URI;
				_tasks[n].error = true;
			}
			if (response.details.extensions.consistent)
			{
				showSuccess
				(
					tabs[n],
					OPTS_STEP,
					"Extensões instaladas:",
					response.details.extensions.sign,
					response.details.extensions.verify,
					response.details.extensions.enroll
				);
			}
			else
			{
				showErr(tabs[n], OPTS_STEP, EXT_ERROR, ALT_CORRECTION);
				param.modifyPath = response.details.install.modifyPath;
				_tasks[n].error = true;
			}
			if (_tasks[n].error)
			{
				param.tab = _tasks[n].tab;
				_tasks[n].params = param;
				document.getElementById("next").defaultValue = "Corrigir";
			}
			else
			{
				_configuration.install.home = response.details.folders.home;
				_configuration.install.modifyPath = response.details.install.modifyPath;
				_configuration.install.uninstallString = response.details.install.uninstallString;
				_configuration.extensions.zip = response.details.zip.id;
				_configuration.extensions.sign = response.details.extensions.sign;
				_configuration.extensions.verify = response.details.extensions.verify;
				_configuration.extensions.enroll = response.details.extensions.enroll;
			}
		}
		catch (e)
		{
			let msg = e ? (typeof (e) === "object" ? JSON.stringify(e) : e) : "Erro inesperado";
			for (i = 0; i < 3; i++) showCommFailure(tabs[n], i, msg);
			_tasks[n].error = true;
			_tasks[n].correct = null;
		}
		_tasks[n].done = true;
		stopWait(div);
	});
	client.addEventListener("error", function(e)
	{
		let msg = e ? (typeof (e) === "object" ? JSON.stringify(e) : e) : "Erro inesperado";
		let i;
		for (i = 0; i < 3; i++) showCommFailure(tabs[n], i, msg);
		_tasks[n].error = true;
		_tasks[n].done = true;
		_tasks[n].correct = null;
		stopWait(div);
	});
	client.open("POST", REGISTRY_CMD);
	client.setRequestHeader("Content-Type", "text/plain");
	client.send("{}");
}
function fileSystemCheck(n)
{
	const FOLDER_STEP = 0;
	const NATIVE_STEP = 1;
	const MANIFEST_STEP = 2;
	const CONFIG_STEP = 3;
	const COMPONENTS_STEP = 4;
	let tabs = document.getElementsByClassName("tab");
	if (!tabs[n]) { console.error(TAB_ERROR); return; }
	let div = wait();
	let client = new XMLHttpRequest();
	let param = {}, request = { "home": "", "extensions": {} };
	let i;
	request.home = _configuration.install.home;
	if (_configuration.extensions.sign)		request.extensions.sign   = _configuration.extensions.sign;
	if (_configuration.extensions.verify)	request.extensions.verify = _configuration.extensions.verify;
	if (_configuration.extensions.enroll)	request.extensions.enroll = _configuration.extensions.enroll;
	client.addEventListener("load", function()
	{
		try
		{
			if (client.status != 200) throw new Error("HTTP Status Code: " + client.status);
			let response = JSON.parse(client.responseText);
			if (response.success && response.files)
			{
				let err = false;
				if (response.files.home) showSuccess(tabs[n], FOLDER_STEP, "Diretório de instalação localizado");
				else { err = true; showErr(tabs[n], FOLDER_STEP, "Diretório de instalação não localizado", REINSTALL_CORRECTION); }
				if (response.files["host.exe"]) showSuccess(tabs[n], NATIVE_STEP, "Executável nativo localizado");
				else { err = true; showErr(tabs[n], NATIVE_STEP, FILE_NOT_FOUND_ERROR.replace(FILE_NAME, "host.exe", REINSTALL_CORRECTION)); }
				if (response.files["manifest.json"]) showSuccess(tabs[n], MANIFEST_STEP, "Manifesto para o navegador localizado");
				else { err = true; showErr(tabs[n], MANIFEST_STEP, FILE_NOT_FOUND_ERROR.replace(FILE_NAME, "manifest.json", REINSTALL_CORRECTION)); }
				if (response.files["config.json"]) showSuccess(tabs[n], CONFIG_STEP, "Arquivo de configuração localizado");
				else { err = true; showErr(tabs[n], CONFIG_STEP, FILE_NOT_FOUND_ERROR.replace(FILE_NAME, "config.json", REINSTALL_CORRECTION)); }
				let components = [];
				if (response.files.hasOwnProperty("kptasign.dll"))
				{
					if (response.files["kptasign.dll"]) components.push("Arquivo localizado: kptasign.dll");
					else { err = true; components.push(FILE_NOT_FOUND_ERROR.replace(FILE_NAME, "kptasign.dll")); }
				}
				if (response.files.hasOwnProperty("kptavrfy.dll"))
				{
					if (response.files["kptavrfy.dll"]) components.push("Arquivo localizado: kptavrfy.dll");
					else { err = true; components.push(FILE_NOT_FOUND_ERROR.replace(FILE_NAME, "kptavrfy.dll")); }
				}
				if (response.files.hasOwnProperty("kptaroll.dll"))
				{
					if (response.files["kptaroll.dll"]) components.push("Arquivo localizado: kptaroll.dll");
					else { err = true; components.push(FILE_NOT_FOUND_ERROR.replace(FILE_NAME, "kptaroll.dll")); }
				}
				let p = "";
				components.forEach((value) => { p += value + "<br>"; });
				if (!err) showSuccess(tabs[n], COMPONENTS_STEP, "Todos os componentes localizados", p);
				else
				{
					showErr(tabs[n], COMPONENTS_STEP, p, REINSTALL_CORRECTION);
					_tasks[n].error = true;
					param.installer = INSTALLER_URI;
					param.tab = _tasks[n].tab;
					_tasks[n].params = param;
				}
			}
			else
			{
				for (i = FOLDER_STEP; i <= COMPONENTS_STEP; i++) showErr(tabs[n], i, INTERNAL_ERR_MSG)
				_tasks[n].error = true;
				_tasks[n].correct = null;
			}
			_tasks[n].done = true;
			if (_tasks[n].error) document.getElementById("next").defaultValue = "Corrigir";
			else _tasks[n].response = response;
		}
		catch (e)
		{
			let msg = e ? (typeof (e) === "object" ? JSON.stringify(e) : e) : "Erro inesperado";
			for (i = FOLDER_STEP; i <= COMPONENTS_STEP; i++) showCommFailure(tabs[n], i, msg);
			_tasks[n].error = true;
			_tasks[n].correct = null;
		}
		_tasks[n].done = true;
		stopWait(div);
	});
	client.addEventListener("error", function(e)
	{
		let msg = e ? (typeof (e) === "object" ? JSON.stringify(e) : e) : "Erro inesperado";
		for (i = FOLDER_STEP; i <= COMPONENTS_STEP; i++) showCommFailure(tabs[n], i, msg);
		_tasks[n].error = true;
		_tasks[n].done = true;
		_tasks[n].correct = null;
		stopWait(div);
	});
	client.open("POST", FILESYS_CMD);
	client.setRequestHeader("Content-Type", "text/plain");
	client.send(JSON.stringify(request));
}
function extensionsCheck(n)
{
	const ZIP_STEP = 0;
	const SIGN_STEP = 1;
	const VERIFY_STEP = 2;
	const ENROLL_STEP = 3;

	let tabs = document.getElementsByClassName("tab");
	if (!tabs[n]) { console.error(TAB_ERROR); return; }
	let div = wait();
	let request = { "check": [] };
	let signal = document.getElementById(HTML_ELEMENT_ID);
	let buttons = tabs[n].getElementsByClassName("accordion");
	function declare(property, value)
	{
		let ret = {};
		ret[property] = value;
		return ret;
	}
	if (signal.textContent.includes(ZIP_EXTENSION_ID)) showSuccess(tabs[n], ZIP_STEP, EXT_ENABLED);
	else request.check.push(declare(_configuration.extensions.zip, ZIP_STEP));
	if (_configuration.extensions.sign)
	{
		if (signal.textContent.includes(SIGN_EXTENSION_ID)) showSuccess(tabs[n], SIGN_STEP, EXT_ENABLED);
		else request.check.push(declare(_configuration.extensions.sign, SIGN_STEP));
	}
	else buttons[SIGN_STEP].innerHTML = EXT_NOT_INSTALLED;
	if (_configuration.extensions.verify)
	{
		if (signal.textContent.includes(VERIFY_EXTENSION_ID)) showSuccess(tabs[n], VERIFY_STEP, EXT_ENABLED);
		else request.check.push(declare(_configuration.extensions.verify, VERIFY_STEP));
	}
	else buttons[VERIFY_STEP].innerHTML = EXT_NOT_INSTALLED;
	if (_configuration.extensions.enroll)
	{
		if (signal.textContent.includes(ENROLL_EXTENSION_ID)) showSuccess(tabs[n], ENROLL_STEP, EXT_ENABLED);
		else request.check.push(declare(_configuration.extensions.enroll, ENROLL_STEP));
	}
	else buttons[ENROLL_STEP].innerHTML = EXT_NOT_INSTALLED;
	if (request.check.length == 0) { stopWait(div); _tasks[n].done = true; return; }

	function _show_error(msg)
	{
		if (!signal.textContent.includes(ZIP_EXTENSION_ID))
		{
			showCheckLabel(tabs[n], ZIP_STEP, false);
			showResult(tabs[n], ZIP_STEP, msg);
		}
		if (_configuration.extensions.sign && !signal.textContent.includes(SIGN_EXTENSION_ID))
		{
			showCheckLabel(tabs[n], SIGN_STEP, false);
			showResult(tabs[n], SIGN_STEP, msg);
		}
		if (_configuration.extensions.verify && !signal.textContent.includes(VERIFY_EXTENSION_ID))
		{
			showCheckLabel(tabs[n], VERIFY_STEP, false);
			showResult(tabs[n], VERIFY_STEP, msg);
		}
		if (_configuration.extensions.enroll && !signal.textContent.includes(ENROLL_EXTENSION_ID))
		{
			showCheckLabel(tabs[n], ENROLL_STEP, false);
			showResult(tabs[n], ENROLL_STEP, msg);
		}
	}
	let param = { correct: [] };
	let client = new XMLHttpRequest();
	client.addEventListener("load", function()
	{
		try
		{
			if (client.status != 200) throw new Error("HTTP Status Code: " + client.status);
			let response = JSON.parse(client.responseText);
			if (response.success)
			{
				response.checked.forEach((value) =>
				{
					let sent = Object.getOwnPropertyNames(value.object);
					let step = value.object[sent[0]];
					let msg;
					switch (value.reason)
					{
					case SUCCESS:
						msg = EXT_ENABLED;
						break;
					case NOT_LISTED_EXT_ERROR:
						msg = NOT_LISTED_MESSAGE;
						param.correct.push(sent[0]);
						break;
					case NOT_ENABLED_EXT_ERROR:
						msg = NOT_ENABLED_MESSAGE;
						param.correct.push(sent[0]);
						break;
					case DISABLED_EXT_ERROR:
						msg = DISABLED_MESSAGE;
						param.correct.push(sent[0]);
						break;
					case REMOVED_EXT_ERROR:
						msg = REMOVED_MESSAGE;
						param.correct.push(sent[0]);
						break;
					default:
						msg = UNESPECTED_ERROR;
					}
					if (value.reason == SUCCESS) showSuccess(tabs[n], step, msg);
					else
					{
						showErr(tabs[n], step, msg, PREFS_CORRECTION);
						_tasks[n].error = true;
						param.tab = _tasks[n].tab;
						_tasks[n].params = param;
					}
				});
			}
			else
			{
				_show_error(INTERNAL_ERR_MSG);
				_tasks[n].error = true;
				_tasks[n].correct = null;
			}
			if (_tasks[n].error) document.getElementById("next").defaultValue = "Corrigir";
		}
		catch (e)
		{
			let msg = e ? (typeof (e) === "object" ? JSON.stringify(e) : e) : "Erro inesperado";
			_show_error(msg);
			_tasks[n].error = true;
			_tasks[n].correct = null;
		}
		_tasks[n].done = true;
		stopWait(div);
	});
	client.addEventListener("error", function(e)
	{
		let msg = e ? (typeof (e) === "object" ? JSON.stringify(e) : e) : "Erro inesperado";
		_show_error(msg);
		_tasks[n].error = true;
		_tasks[n].done = true;
		_tasks[n].correct = null;
		stopWait(div);
	});
	client.open("POST", ENABLED_CMD);
	client.setRequestHeader("Content-Type", "text/plain");
	client.send(JSON.stringify(request));
}
function capabilitiesCheck(n)
{
	const ZIP_STEP = 0;
	const ENROLL_STEP = 1;
	const SIGN_STEP = 2;

	let tabs = document.getElementsByClassName("tab");
	if (!tabs[n]) { console.error(TAB_ERROR); return; }
	let div = wait();
	let buttons = tabs[n].getElementsByClassName("accordion");
	let signal = document.getElementById(HTML_ELEMENT_ID);

	let data = new Uint8Array([75, 114, 121, 112, 116, 111, 110, 105, 116, 97]);
	let handle;
	let zip = new kptazip.Deflate();
	zip.create().then((value) =>
	{
		if (value.reason != kptazip.KPTAError.KPTA_OK) { throw value.reason; }
		handle = value.payload;
		return zip.add(handle, data, "My entry");
	})
	.then((response) =>
	{
		if (response.reason != kptazip.KPTAError.KPTA_OK) { throw response.reason; }
		return Promise.resolve();
	})
	.then(() => { return zip.close(handle); })
	.then((ret) => {
		if (ret.reason != kptazip.KPTAError.KPTA_OK) { throw ret.reason; }
		showSuccess(tabs[n], ZIP_STEP, CHECK_SUCCESS);
	})
	.catch((e) => { showErr(tabs[n], ZIP_STEP, CHECK_FAILED.replace(REASON, e).replace(EXT, "Kryptonita Zip")); });
	
	if (signal.textContent.includes(ENROLL_EXTENSION_ID))
	{
		let enroll = new kptaenroll.Enroll();
		enroll.enumerateDevices().then((value) =>
		{
			if (value.reason != kptaenroll.KPTAError.KPTA_OK) { throw value.reason; }
			let found = value.payload.find((dev) => { return dev === PROVIDER; });
			if (found) showSuccess(tabs[n], ENROLL_STEP, CHECK_SUCCESS);
			else showErr(tabs[n], ENROLL_STEP, NO_PROVIDER);
		})
		.catch((e) => { showErr(tabs[n], ENROLL_STEP, CHECK_FAILED.replace(REASON, e).replace(EXT, "Kryptonita Emissão")); });
	}
	else buttons[ENROLL_STEP].innerHTML = EXT_NOT_INSTALLED;

	if (signal.textContent.includes(SIGN_EXTENSION_ID))
	{
		let sign = new kptasign.Sign();
		sign.enumerateCerts().then((value) => {
			if (value.reason != kptasign.KPTAError.KPTA_OK) { throw value.reason; }
			if (value.payload.certificates.length > 0) showSuccess(tabs[n], SIGN_STEP, CHECK_SUCCESS);
			else showErr(tabs[n], SIGN_STEP, NO_CERTIFICATES);
		})
		.catch((e) => { showErr(tabs[n], SIGN_STEP, CHECK_FAILED.replace(REASON, e).replace(EXT, "Kryptonita Assinatura")); });
	}
	else buttons[SIGN_STEP].innerHTML = EXT_NOT_INSTALLED;
	_tasks[n].done = true;
	stopWait(div);
}
function finish(n)
{
	// TODO: implement finish()
}

function installCorrect(params)
{
	const UNINSTALL_STEP = 0;
	const DOWNLOAD_STEP = 1;
	const INSTALL_STEP = 2;
	// TODO: implement installCorrect()
	let n = param.tab;
	let tabs = document.getElementsByClassName("tabc");
	if (!tabs[n]) { console.error(TAB_ERROR); return; }
	let buttons = tabs[n].getElementsByClassName("accordion");

	let div = wait();
	if (param.uninstallString)
	{

	}
	setTimeout(() => { stopWait(div); window.location.reload(); }, 5000);
}
function extensionsCorrect(params)
{
	// TODO: implement extensionsCorrect()
	let div = wait();
	setTimeout(() => { stopWait(div); window.location.reload(); }, 5000);
}
let _configuration = {
	"install": {
		"home": "",
		"modifyPath": "",
		"uninstallString": ""
	},
	"extensions": {
		"zip": "",
		"sign": null,
		"verify": null,
		"enroll": null
	}
};
let _tasks = [
	{
		"callback": init,		// Step executor
		"done": false,		// Step done
		"error": false,		// Step error state
		"correct": null,		// Correction executor (if error is true)
		"tab": -1,			// Correction step tab (if any)
		"params": null		// Correction executor parameters
	},
	{
		"callback": connectivityCheck,
		"done": false,
		"error": false,
		"correct": null,
		"tab": -1,
		"params": null
	},
	{
		"callback": installCheck,
		"done": false,
		"error": false,
		"correct": installCorrect,
		"tab": 0,
		"params": null
	},
	{
		"callback": fileSystemCheck,
		"done": false,
		"error": false,
		"correct": installCorrect,
		"tab": 0,
		"params": null
	},
	{
		"callback": extensionsCheck,
		"done": false,
		"error": false,
		"correct": extensionsCorrect,
		"tab": -1,
		"params": null
	},
	{
		"callback": capabilitiesCheck,
		"done": false,
		"error": false,
		"correct": null,
		"tab": -1,
		"params": null
	},
	{
		"callback": finish,
		"done": false,
		"error": false,
		"correct": null,
		"tab": -1,
		"params": null
	}
];
let _ntab = -1;
function navigate(n)
{
	let x = document.getElementsByClassName("tab");
	let previous = _ntab, next = _ntab + n;
	if (n > 0 && previous > 0 && _tasks[previous].error)
	{
		if (_tasks[previous].correct)
		{
			document.getElementsByClassName("tabc")[_tasks[previous].tab].style.left = 0;
			document.getElementsByClassName("tab")[previous].style.left = "-100%";
			document.getElementById("prev").style.display = "none";
			document.getElementById("next").defaultValue = "Final";
			_tasks[previous].correct(_tasks[previous].params);
		}
		return;
	}
	if (next > -1 && next < x.length)
	{
		if (next == 0)
		{
			if (previous > 0) x[previous].style.left = "-100%";
		}
		else x[previous].style.left = n + "00%";
		x[next].style.left = 0;
	}
	else return;
	if (next == x.length - 1)
	{
		document.getElementById("prev").style.display = "inline";
		document.getElementById("next").defaultValue = "Final";
	}
	else
	{
		document.getElementById("prev").style.display = next == 0 ? "none" : "inline";
		document.getElementById("next").defaultValue = _tasks[next].error ? "Corrigir" : "Próximo";
	}
	let i, y = document.getElementsByClassName("step");
	for (i = 0; i < y.length; i++) y[i].className = y[i].className.replace(" active", "");
	if (next > -1) y[next].className += " active";
	_ntab = next;
	if (n > 0 && !_tasks[next].done) _tasks[next].callback(next);
}
