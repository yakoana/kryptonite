﻿
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Json;
using System.Linq;
using System.Net;
using System.Security;
using System.Security.Principal;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace Listener
{
	  public class ConnectivityCheck : POSTProcessor
	  {
		    public override Response Execute()
		    {
				Response ret = new Response();
				ReasonCode reason = ReasonCode.SUCCESS;
				WebClient client = new WebClient();
				try
				{
					  string response = client.DownloadString(Resources.STORE_URI);
					  XDocument doc = XDocument.Parse(response);
					  if (doc.Root.GetDefaultNamespace() != Resources.STORE_RESPONSE) throw new XmlException();
				}
				catch (Exception e)
				{
					  if (e is WebException) reason = ReasonCode.CONNECTIVITY_ERROR;
					  else if (e is XmlException) reason = ReasonCode.RESPONSE_ERROR;
					  else reason = ReasonCode.INTERNAL_ERROR;
				}
				JsonObject json = (JsonObject)JsonValue.Parse("{}");
				if (reason == ReasonCode.SUCCESS) json.Add("success", new JsonPrimitive(true));
				else json.Add("success", new JsonPrimitive(false));
				json.Add("reason", new JsonPrimitive((int)reason));
				ret.Result = _encoding.GetBytes(json.ToString());
				return ret;
		    }
	  }
	  public class RegistryCheck : POSTProcessor
	  {
		    private RegistryKey FindProduct()
		    {
				RegistryKey ret = null, pKey;
				string productsKey = Resources.PRODUCTS_KEY.Replace(Resources.SID, WindowsIdentity.GetCurrent().User.ToString());
				if ((pKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64).OpenSubKey(productsKey, false)) != null)
				{
					  try
					  {
						    string[] products = pKey.GetSubKeyNames();
						    int i = 0;
						    while (ret == null && i < products.Length)
						    {
								RegistryKey key;
								if ((key = pKey.OpenSubKey(products[i] + @"\InstallProperties")) != null)
								{
									  string name;
									  if ((name = (string)key.GetValue("DisplayName")) != null)
									  {
										    if (name.CompareTo("Kryptonita") == 0) ret = key;
										    else key.Close();
									  }
								}
								i++;
						    }
					  }
					  finally { pKey.Close(); }
				}
				return ret;
		    }
		    private string GetValue(string regkey)
		    {
				String ret = null;
				RegistryKey key;
				Object value;
				if ((key = Registry.CurrentUser.OpenSubKey(regkey)) != null)
				{
					  try { if ((value = key.GetValue(null)) != null) ret = value.ToString(); }
					  finally { key.Close(); }
				}
				return ret;
		    }
		    private List<string> GetExtensions()
		    {
				List<string> list = new List<string>();
				RegistryKey ext, key;
				if ((ext = Registry.CurrentUser.OpenSubKey(Resources.EXTENSIONS_KEY)) != null)
				{
					  if ((key = ext.OpenSubKey(Resources.ZIP_ID)) != null)
					  {
						    list.Add(Resources.ZIP_ID);
						    key.Close();
					  }
					  if ((key = ext.OpenSubKey(Resources.SIGN_ID)) != null)
					  {
						    list.Add(Resources.SIGN_ID);
						    key.Close();
					  }
					  if ((key = ext.OpenSubKey(Resources.VERIFY_ID)) != null)
					  {
						    list.Add(Resources.VERIFY_ID);
						    key.Close();
					  }
					  if ((key = ext.OpenSubKey(Resources.ENROLL_ID)) != null)
					  {
						    list.Add(Resources.ENROLL_ID);
						    key.Close();
					  }
				}
				return list;
		    }
		    public override Response Execute()
		    {
				Response ret = new Response();
				ReasonCode reason = ReasonCode.SUCCESS;
				RegistryKey installKey;
				string value;
				JsonObject json = (JsonObject)JsonValue.Parse("{}");
				JsonObject details = (JsonObject)JsonValue.Parse("{}");
				try
				{
					  JsonObject install = (JsonObject)JsonValue.Parse("{}");
					  JsonObject folders = (JsonObject)JsonValue.Parse("{}");
					  JsonObject zip = (JsonObject)JsonValue.Parse("{}");
					  JsonObject extensions = (JsonObject)JsonValue.Parse("{}");
					  if ((installKey = FindProduct()) != null)
					  {
						    try
						    {
								install.Add("installed", new JsonPrimitive(true));
								if ((value = (string)installKey.GetValue("ModifyPath")) == null) throw new IOException();
								install.Add("modifyPath", new JsonPrimitive(value));
								if ((value = (string)installKey.GetValue("UninstallString")) == null) throw new IOException();
								install.Add("uninstallString", new JsonPrimitive(value));
						    }
						    finally { installKey.Close(); }
						    string home = GetValue(Resources.KRYPTO_KEY), manifest = GetValue(Resources.GOOGLE_NATIVE_KEY);
						    if (home != null && manifest != null && manifest.ToLower().StartsWith(home.ToLower())) folders.Add("consistent", new JsonPrimitive(true));
						    else folders.Add("consistent", new JsonPrimitive(false));
						    if (home != null) folders.Add("home", new JsonPrimitive(home));
						    if (manifest != null) folders.Add("manifest", new JsonPrimitive(manifest));
						    List<string> exts = GetExtensions();
						    if (exts.Count > 0 && exts.Contains(Resources.ZIP_ID))
						    {
								zip.Add("installed", new JsonPrimitive(true));
								zip.Add("id", Resources.ZIP_ID);
						    }
						    else zip.Add("installed", new JsonPrimitive(false));
						    if (exts.Count > 1 && exts.Contains(Resources.ZIP_ID))
						    {
								extensions.Add("consistent", new JsonPrimitive(true));
								if (exts.Contains(Resources.SIGN_ID)) extensions.Add("sign", new JsonPrimitive(Resources.SIGN_ID));
								if (exts.Contains(Resources.VERIFY_ID)) extensions.Add("verify", new JsonPrimitive(Resources.VERIFY_ID));
								if (exts.Contains(Resources.ENROLL_ID)) extensions.Add("enroll", new JsonPrimitive(Resources.ENROLL_ID));
						    }
						    else extensions.Add("consistent", new JsonPrimitive(false));
					  }
					  else install.Add("installed", new JsonPrimitive(false));
					  details.Add("install", install);
					  details.Add("folders", folders);
					  details.Add("zip", zip);
					  details.Add("extensions", extensions);
				}
				catch (Exception e)
				{
					  if (e is SecurityException || e is UnauthorizedAccessException) reason = ReasonCode.SECURITY_ERROR;
					  else if (e is IOException || e is InvalidOperationException) reason = ReasonCode.REGISTRY_ERROR;
					  else reason = ReasonCode.INTERNAL_ERROR;
				}
				if (reason == ReasonCode.SUCCESS) json.Add("success", new JsonPrimitive(true));
				else json.Add("success", new JsonPrimitive(false));
				json.Add("reason", new JsonPrimitive((int)reason));
				json.Add("details", details);
				ret.Result = _encoding.GetBytes(json.ToString());
				return ret;
		    }
	  }
	  public class FileSystemCheck : POSTProcessor
	  {
		    private string _home;
		    private List<string> _files;
		    public override void Init(HttpListenerRequest request)
		    {
				base.Init(request);
				_files = new List<string>();
				_home = _body["home"];
				_files.Add("host.exe");
				_files.Add("manifest.json");
				_files.Add("config.json");
				if (_body["extensions"].ContainsKey("sign")) _files.Add("kptasign.dll");
				if (_body["extensions"].ContainsKey("verify")) _files.Add("kptavrfy.dll");
				if (_body["extensions"].ContainsKey("enroll")) _files.Add("kptaroll.dll");
		    }
		    public override Response Execute()
		    {
				Response ret = new Response();
				ReasonCode reason = ReasonCode.SUCCESS;
				JsonObject jFiles = (JsonObject)JsonValue.Parse("{}");
				try
				{
					  jFiles.Add("home", new JsonPrimitive((new DirectoryInfo(_home)).Exists));
					  _files.ForEach(delegate (string name)
					  {
	  					  jFiles.Add(name, new JsonPrimitive((new FileInfo(_home + name)).Exists));
					  });
				}
				catch (Exception e)
				{
					  Console.WriteLine(e.Message);
					  Console.WriteLine(e.StackTrace);
					  reason = ReasonCode.INTERNAL_ERROR;
				}
				JsonObject json = (JsonObject)JsonValue.Parse("{}");
				json.Add("success", new JsonPrimitive(reason == ReasonCode.SUCCESS));
				json.Add("reason", new JsonPrimitive((int)reason));
				json.Add("files", jFiles);
				ret.Result = _encoding.GetBytes(json.ToString());
				return ret;
		    }
	  }
	  public class ExtensionsEnableCheck : POSTProcessor
	  {
		    private readonly static DirectoryInfo _profile = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + Resources.PROFILE_FOLDER);
		    public static DirectoryInfo Profile => _profile;
		    static ExtensionsEnableCheck() { if (!_profile.Exists) throw new ApplicationException("Could not find Google default profile folder"); }
		    public static JsonObject GetSecurePreferences()
		    {
				FileInfo prefs = new FileInfo(Profile.FullName + @"\Secure Preferences");
				if (!prefs.Exists) throw new ApplicationException("Could not find Google profile Secure Preferences file");
				FileStream input = prefs.OpenRead();
				try
				{
					  StreamReader reader = new StreamReader(input, Encoding.UTF8);
					  return (JsonObject)JsonValue.Parse(reader.ReadToEnd());
				}
				finally { input.Close(); }
		    }
		    public static void SaveSecurePreferences(JsonValue file)
		    {
				FileInfo prefs = new FileInfo(Profile.FullName + @"\Secure Preferences");
				if (!prefs.Exists) throw new ApplicationException("Could not find Google profile Secure Preferences file");
				FileStream output = prefs.OpenWrite();
				try { file.Save(output); }
				finally { output.Close(); }
		    }

		    private Dictionary<string, int> _check;
		    public override void Init(HttpListenerRequest request)
		    {
				base.Init(request);
				_check = new Dictionary<string, int>();
				JsonArray param = (JsonArray)_body["check"];
				for (int i = 0; i < param.Count; i++)
				{
					  JsonObject item = (JsonObject) param[i];
					  _check.Add(item.Keys.ElementAt(0), item[item.Keys.ElementAt(0)]);
				}
		    }
		    public override Response Execute()
		    {
				Response ret = new Response();
				ReasonCode reason = ReasonCode.SUCCESS;
				JsonArray check = (JsonArray)JsonValue.Parse("[]");
				try
				{
					  JsonObject settings = (JsonObject)GetSecurePreferences()["extensions"]["settings"];
					  Dictionary<string, int>.Enumerator e = _check.GetEnumerator();
					  while (e.MoveNext())
					  {
						    ReasonCode status = ReasonCode.SUCCESS;
						    if (settings.ContainsKey(e.Current.Key))
						    {
								JsonObject ext = (JsonObject)settings[e.Current.Key];
								if (ext["state"] == 0)
								{
									  if (!ext.ContainsKey("disable_reasons")) throw new ApplicationException("Unexpected value in Secure Preferences file");
									  if (ext["disable_reasons"] == 8192) { status = ReasonCode.NOT_ENABLED_EXT_ERROR; }
									  else if (ext["disable_reasons"] == 1) { status = ReasonCode.DISABLED_EXT_ERROR; }
									  else throw new ApplicationException("Unexpected value in Secure Preferences file");
								}
								else if (ext["state"] == 1) { }
								else if (ext["state"] == 2) { status = ReasonCode.REMOVED_EXT_ERROR; }
								else throw new ApplicationException("Unexpected value in Secure Preferences file");
						    }
						    else { status = ReasonCode.NOT_LISTED_EXT_ERROR; }
						    JsonObject given = (JsonObject)JsonValue.Parse("{}");
						    given.Add(e.Current.Key, new JsonPrimitive(e.Current.Value));
						    JsonObject item = (JsonObject)JsonValue.Parse("{}");
						    item.Add("object", given);
						    item.Add("reason", new JsonPrimitive((int)status));
						    check.Add(item);
					  }

				}
				catch (Exception e)
				{
					  Console.WriteLine(e.Message);
					  Console.WriteLine(e.StackTrace);
					  reason = ReasonCode.INTERNAL_ERROR;
				}
				JsonObject json = (JsonObject)JsonValue.Parse("{}");
				json.Add("success", new JsonPrimitive(reason == ReasonCode.SUCCESS));
				json.Add("reason", new JsonPrimitive((int)reason));
				json.Add("checked", check);
				ret.Result = _encoding.GetBytes(json.ToString());
				return ret;
		    }
	  }
}