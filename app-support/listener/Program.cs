﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Json;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;

namespace Listener
{
	  class Resources
	  {
		    public const string DEFAULT_RESOURCE = @"\index.html";
		    public const string RESPONSE_TEMPLATE =
				"<?xml version=\"1.0\" ?>" +
				"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">" +
				"<html xmlns=\"http://www.w3.org/1999/xhtml\">" +
				"<head>" +
				"<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />" +
				"<title>Kryptonita &#8212; Aplica&#184;&#227;o de diagn&#243;stico</title></head>" +
				"<body>[__BODY__]</body>" +
				"</html>";
		    public const string BODY = "[__BODY__]";
		    public const string CONTEXT = "/kryptonite";
		    public const string PREFIX = "http://localhost:8080" + CONTEXT + "/";
		    public const string FORBIDDEN = "<div style=\"margin: 2em;\" ><p style=\"font-size: 200%;\">Forbidden</p></div>";
		    public const string NOT_ALLOWED = "<div style=\"margin: 2em;\" ><p style=\"font-size: 200%;\">HTTP method not allowed</p></div>";
		    public const string NOT_FOUND = "<div style=\"margin: 2em;\" ><p style=\"font-size: 200%;\">Not found</p></div>";
		    public const string INTERNAL_ERROR = "<div style=\"margin: 2em;\" ><p style=\"font-size: 200%;\">Internal Server Error</p></div>";

		    // TODO: Replace INSTALLER_URI
		    public const string INSTALLER_URI = "http://localhost:8888/kryptonita.msi";
		    public const string STORE_URI = "https://clients2.google.com/service/update2/crx";
		    public const string STORE_RESPONSE = "http://www.google.com/update2/response";
		    public const string PRODUCTS_KEY = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Installer\UserData\[_SID_]\Products";
		    public const string SID = "[_SID_]";
		    public const string KRYPTO_KEY = @"Software\Crypthing\Kryptonite";
		    public const string GOOGLE_NATIVE_KEY = @"Software\Google\Chrome\NativeMessagingHosts\org.crypthing.kryptonite";
		    public const string EXTENSIONS_KEY = @"Software\Google\Chrome\Extensions";
		    public const string PROFILE_FOLDER = @"\Google\Chrome\User Data\Default";

		    public const string ZIP_ID = "fbefajnakmfifehnaneljnhojeijccna";
		    public const string SIGN_ID = "kfkoineenohoofaigpoafaadfigmabld";
		    public const string VERIFY_ID = "akaofigjbabehheajkgnkbmeihnnhfjj";
		    public const string ENROLL_ID = "lcopjikfdpjdhmjkjgolonjhffjlldlc";

		    public const string BYE_CMD = CONTEXT + "/bye-bye";
		    public const string CONNECTIVITY_CMD = CONTEXT + "/check-connectivity";
		    public const string REGISTRY_CMD = CONTEXT + "/check-registry";
		    public const string FILESYS_CMD = CONTEXT + "/check-filesys";
		    public const string ENABLE_CMD = CONTEXT + "/check-enable";
		    public const string DOWLOAD_CMD = CONTEXT + "/get-installer";
		    public const string REMOVE_CMD = CONTEXT + "/remove";
		    public const string REPAIR_CMD = CONTEXT + "/repair";
		    public const string ENABLE_EXT_CMD = CONTEXT + "/enable";
	  }
	  public enum ReasonCode
	  {
		    SUCCESS = 0,		    // Success
		    SECURITY_ERROR,	    // User not authorized 
		    REGISTRY_ERROR,	    // Registry access error
		    INTERNAL_ERROR,	    // Internal server error

		    CONNECTIVITY_ERROR,	    // Connectivity failure
		    RESPONSE_ERROR,	    // Invalid store response (possibly invalid Chrome update URL)

		    HOME_NOT_FOUND_ERROR,   // Home directory not found
		    EXT_NOT_FOUND_ERROR,    // Extension implementation not found
		    FILES_NOT_FOUND_ERROR,  // Critical files not found
		    MF_NOT_FOUND_ERROR,	    // Chrome manifest not found

		    NOT_LISTED_EXT_ERROR,   // Extension not listed in Secure Preferences file
		    NOT_ENABLED_EXT_ERROR,  // Extension not enabled
		    DISABLED_EXT_ERROR,	    // Extension disabled by user
		    REMOVED_EXT_ERROR	    // Extension removed by user
	  }
	  public class Response
	  {
		    public int StatusCode { get; set; }
		    public string StatusDescription { get; set; }
		    public Boolean KeepAlive { get; set; }
		    public byte[] Result { get; set; }
		    public string ContentType { get; set; }
		    public Response()
		    {
				StatusCode = (int)HttpStatusCode.OK;
				StatusDescription = "OK";
				KeepAlive = true;
				ContentType = "application/octet-stream";
		    }
	  }
	  public abstract class Processor
	  {
		    public abstract void Init(HttpListenerRequest request);
		    public abstract Response Execute();
	  }
	  public class MethodNotAllowedException : ProtocolViolationException { }
	  public abstract class GETProcessor : Processor
	  {
		    override public void Init(HttpListenerRequest request)
		    {
				if (request.HttpMethod.CompareTo("GET") != 0) throw new MethodNotAllowedException();
		    }
	  }
	  public abstract class POSTProcessor : Processor
	  {
		    protected JsonValue _body;
		    protected Encoding _encoding;
		    override public void Init(HttpListenerRequest request)
		    {
				if (request.HttpMethod.CompareTo("POST") != 0) throw new MethodNotAllowedException();
				if (request.HasEntityBody)
				{
					  Encoding encoding = request.ContentEncoding;
					  if (encoding == null) encoding = UTF8Encoding.UTF8;
					  Stream body = request.InputStream;
					  try
					  {
						    StreamReader reader = new StreamReader(body, encoding);
						    try { _body = JsonValue.Parse(reader.ReadToEnd()); }
						    finally { reader.Close(); }
					  }
					  finally { body.Close(); }
				}
				else _body = JsonValue.Parse("{}");
				_encoding = request.ContentEncoding;
				if (_encoding == null) _encoding = UTF8Encoding.UTF8;
		    }
	  }
	  public abstract class AsyncProcessor : GETProcessor
	  {
		    public override Response Execute() { throw new NotImplementedException();  }
		    public abstract void ExecuteAsync(HttpListenerResponse response);
	  }
	  class ResourceLoader : GETProcessor
	  {
		    private readonly string _contents;
		    private FileInfo _res;
		    public ResourceLoader(string contents)
		    {
				if (!Directory.Exists(contents)) throw new ArgumentException("Contents directory must exist");
				if (contents.EndsWith("\\")) _contents = contents.Substring(0, contents.Length - 1);
				else _contents = contents;
		    }
		    public override void Init(HttpListenerRequest request)
		    {
				base.Init(request);
				string target = request.RawUrl.Replace(Resources.CONTEXT, "").Replace("/", "\\");
				if (String.IsNullOrEmpty(target) || String.Compare(target, "\\") == 0) target = Resources.DEFAULT_RESOURCE;
				_res = new FileInfo(_contents + target);
				if (!_res.Exists) throw new FileNotFoundException(_res.Name);
		    }
		    public override Response Execute()
		    {
				Response ret = new Response { Result = new byte[_res.Length] };
				try
				{
					  FileStream stream = _res.OpenRead();
					  try { stream.Read(ret.Result, 0, ret.Result.Length); }
					  finally { stream.Close(); }
					  ret.ContentType = MimeMapping.GetMimeMapping(_res.Name);
				}
				catch (IOException)
				{
					  ret.StatusCode = (int)HttpStatusCode.InternalServerError;
					  ret.StatusDescription = "Internal Server Error";
					  ret.KeepAlive = false;
					  ret.Result = UTF8Encoding.UTF8.GetBytes(Resources.RESPONSE_TEMPLATE.Replace(Resources.BODY, Resources.INTERNAL_ERROR));
				}
				return ret;
		    }
	  }
	  class HttpServer
	  {
		    private readonly Dictionary<string, Processor> _catalog;
		    private HttpListener _listener;
		    public HttpServer(string contents)
		    {
				ResourceLoader loader = new ResourceLoader(contents);
				_catalog = new Dictionary<string, Processor>()
				{
					  { Resources.CONTEXT, loader },
					  { Resources.CONTEXT + "/", loader },
					  { Resources.CONNECTIVITY_CMD, new ConnectivityCheck() },
					  { Resources.REGISTRY_CMD, new RegistryCheck() },
					  { Resources.FILESYS_CMD, new FileSystemCheck() },
					  { Resources.ENABLE_CMD, new ExtensionsEnableCheck() },
					  { Resources.DOWLOAD_CMD, new DownloadInstaller() },
					  { Resources.REMOVE_CMD, new Uninstall() },
					  { Resources.REPAIR_CMD, new Repair() },
					  { Resources.ENABLE_EXT_CMD, new EnableExtensions() }
				};
				EndPoint = Resources.PREFIX;
				_listener = new HttpListener();
				_listener.Prefixes.Add(EndPoint);
		    }
		    public string EndPoint { get; }
		    public void Listen()
		    {
				try
				{
					  _listener.Start();
					  while (_listener.IsListening)
					  {
						    HttpListenerContext ctx = _listener.GetContext();
						    Console.WriteLine(ctx.Request.HttpMethod + " command received with context " + ctx.Request.RawUrl);
						    if (!ctx.Request.RawUrl.Contains(Resources.BYE_CMD))
						    {
								Thread t = new Thread(Deal){ IsBackground = true };
								t.Start(ctx);
						    }
						    else _listener.Stop();
					  }
					  _listener.Close();
				}
				catch (HttpListenerException e)
				{
					  Console.WriteLine("Failed to listen to incoming requests");
					  Console.WriteLine(e.Message);
					  Console.WriteLine(e.StackTrace);
				}
		    }
		    private void Deal(Object param)
		    {
				try
				{
					  HttpListenerContext ctx = (HttpListenerContext)param;
					  Processor processor;
					  if (_catalog.ContainsKey(ctx.Request.RawUrl))  processor =  _catalog[ctx.Request.RawUrl];
					  else processor = _catalog[Resources.CONTEXT];
					  Response output = null;
					  try
					  {
						    processor.Init(ctx.Request);
						    if (processor is AsyncProcessor e) e.ExecuteAsync(ctx.Response);
						    else output = processor.Execute();
					  }
					  catch (MethodNotAllowedException)
					  {
						    output = new Response
						    {
								StatusCode = (int)HttpStatusCode.MethodNotAllowed,
								StatusDescription = "Method Not Allowed",
								KeepAlive = false,
								Result = UTF8Encoding.UTF8.GetBytes(Resources.RESPONSE_TEMPLATE.Replace(Resources.BODY, Resources.NOT_ALLOWED))
						    };
					  }
					  catch (ApplicationException e)
					  {
						    output = new Response
						    {
								StatusCode = (int)HttpStatusCode.InternalServerError,
								StatusDescription = "Internal server error",
								KeepAlive = false,
								Result = UTF8Encoding.UTF8.GetBytes(Resources.RESPONSE_TEMPLATE.Replace(Resources.BODY, Resources.INTERNAL_ERROR))
						    };
						    Console.WriteLine(e.Message);
						    Console.WriteLine(e.StackTrace);
					  }
					  if (output != null)
					  {
						    HttpListenerResponse response = ctx.Response;
						    try
						    {
								response.ProtocolVersion = new Version("1.1");
								response.StatusCode = output.StatusCode;
								response.StatusDescription = output.StatusDescription;
								response.KeepAlive = output.KeepAlive;
								response.ContentType = output.ContentType;
								response.ContentLength64 = output.Result.Length;
								Stream stream = response.OutputStream;
								try { stream.Write(output.Result, 0, output.Result.Length); }
								finally { stream.Close(); }
						    }
						    finally { response.Close(); }
					  }
				}
				catch (IOException e)
				{
					  Console.WriteLine("Failed to send response to client");
					  Console.WriteLine(e.Message);
					  Console.WriteLine(e.StackTrace);
				}
				catch (Exception e)
				{
					  Console.WriteLine("Unexpected error");
					  Console.WriteLine(e.Message);
					  Console.WriteLine(e.StackTrace);
				}
		    }
	  }
	  class Program
	  {
		    static void Main(string[] args)
		    {
				if (!HttpListener.IsSupported)
				{
					  Console.WriteLine("Cannot start HTTP server");
					  Environment.Exit(1);
				}
				string contents = args.Length == 0 ? AppDomain.CurrentDomain.BaseDirectory : args[0];
				if (contents.EndsWith("\\")) contents = contents.Substring(0, contents.Length - 1);
				if (!File.Exists(contents + Resources.DEFAULT_RESOURCE))
				{
					  Console.WriteLine("Argument must be contents directory");
					  Environment.Exit(2);
				}
				HttpServer _server = new HttpServer(contents);
				Console.WriteLine("Listening at " + _server.EndPoint + "...");
				_server.Listen();
				Console.WriteLine("Stop message received.");
		    }
	  }
}
