// Type definitions for Kryptonite Project
// Definitions by: yorick.flannagan@gmail.com

declare namespace kptazip 
{
	class KPTAError extends Error									// Error and reason codes
	{
		/*
		* Operation results
		*/
		static readonly KPTA_OK							:number;	// Successful operation
		static readonly KPTA_FAILED						:number;	// Operation failure
		static readonly KPTA_UNKNOWN					:number;	// Unsupported operation
	
		/*
		* Reason codes
		*/
		static readonly KPTA_NONE						:number;	// Success
		static readonly KPTA_INVALID_ARG				:number;	// Invalid arguments
		static readonly KPTA_INVALID_RESPONSE			:number;	// Invalid response from extensions
		static readonly KPTA_BASE_ERROR					:number;
		static readonly KPTA_MEMORY_ERROR				:number;	// Out of memory error
		static readonly KPTA_MSG_SIZE_ERROR				:number;	// JSON message size error
		static readonly KPTA_IO_FILE_ERROR				:number;	// JSON file I/O error
		static readonly KPTA_STDIO_ERROR				:number;	// JSON document I/O error
		static readonly KPTA_INVALID_JSON				:number;	// JSON file parsing error
		static readonly KPTA_TEMPLATE_ERROR				:number;	// JSON template error
		static readonly KPTA_INVALID_STATE_OBJECT		:number;	// Cannot execute method due to object invalid state
		static readonly KPTA_COMPONENT_NOT_FOUND		:number;	// Required component not found in configuration
		static readonly KPTA_BUFFER_TOO_SMALL			:number;	// Output buffer too small
		static readonly KPTA_PROPERTY_NOT_FOUND			:number;	// Requested property not found in configuration
		static readonly KPTA_INVALID_PROPERTY			:number;	// Requested property has another type
		static readonly KPTA_INVALID_LOGLEVEL			:number;	// JS log level value invalid
		static readonly KPTA_LOGFILE_ERROR				:number;	// Windows log file opening error
		static readonly KPTA_JSON_ERROR					:number;	// Invalid JSON document
		static readonly KPTA_COMPONENT_MAP_ERROR		:number;	// Components hash map access unexpected error
		static readonly KPTA_PUT_MAP_ERROR				:number;	// Hash map put error
		static readonly KPTA_SLOT_NOT_FOUND_ERROR		:number;	// Could not find slot name
		static readonly KPTA_CERTIFICATES_NOT_FOUND		:number;	// Could not find certificates in PKCS #7
		static readonly KPTA_CERT_DN_NOT_FOUND			:number;	// Selected certificate DN not found in repository
		static readonly KPTA_UNSUPPORTED_MECHANISM		:number;	// Specified cryptographic mechanism is not supported
		static readonly KPTA_JSON_PRINTER_ERROR			:number;	// JSON builder facility error indicator (use G_SYSERROR for details)
		static readonly KPTA_JSON_PRINTER_END			:number;	// JSON builder facility has ended writing
		static readonly KPTA_WINDOWS_API_ERROR			:number;	// Windows API error (use G_SYSERROR for details)
		static readonly KPTA_WINREGISTRY_ERROR			:number;	// Windows Registry access error
		static readonly KPTA_LOADDLL_ERROR				:number;	// Load Windows DLL error
		static readonly KPTA_CRYPTO_ENROLL_ERROR		:number;	// Windows Cryptographic API error under enrollment context
		static readonly KPTA_CRYPTO_ENUM_CERTS_ERROR	:number;	// Windows Cryptographic API error under enumerate certificates context
		static readonly KPTA_CRYPTO_SIGN_ERROR			:number;	// Windows Cryptographic API error under signing context
		static readonly KPTA_CRYPTO_VERIFY_ERROR		:number;	// Windows Cryptographic API error under verification context
		static readonly KPTA_TRY_NEXT_STORE				:number;	// Could not validate certificate in this store
		static readonly KPTA_LINUX_API_ERROR			:number;	// Linux system libraries error (use G_SYSERROR for details)
		static readonly KPTA_LOADSO_ERROR				:number;	// Load Linux shared object error
		static readonly KPTA_NHARU_ERROR				:number;	// Nharu Library error indicator
		static readonly KPTA_PARSE_CERT_ERROR			:number;	// Certificate parser error
		static readonly KPTA_ENCODE_CMS_ERROR			:number;	// CMS encoder error
		static readonly KPTA_CMS_SD_NOECONTENT_ERROR	:number;	// Unattached CMS error
		static readonly KPTA_SCRIPT_ERROR				:number;
		static readonly KPTA_UNEXPECTED_ERROR			:number;	// Unknown error
		static readonly KPTA_INVALID_HANDLE_ERROR		:number;	// Invalid zip handle
		static readonly KPTA_ADD_ZIP_ERROR				:number;	// Zip add error
		static readonly KPTA_UNZIP_ENTRY_ERROR			:number;	// Unzip entry error
		static readonly KPTA_REJECTED_BY_USER			:number;	// Command rejected by user
	}
	interface Protocol												// Protocol message
	{
		payload: any | undefined;
	}
	interface Response extends Protocol								// Protocol response
	{
		result: number;												// Comand result status
		reason: number;												// Reason code if failure
	}
	
	class Deflate													// Compress files in PKWare format
	{
		create														// Initializes a new zip file
		():	Promise<Response>;										// Payload returns a handle to new zip
		add															// Adds a new zip entry to current zip file
		(
			handle: number,											// Handle to zip file (returned by create)
			entry: ArrayBuffer | Uint8Array,						// Entry to zip
			name: string,											// Entry name
			date?: number,											// Entry date. Default value: current timestamp
			compress?: boolean										// Compression level indicator. Default value: true (level 8)
		):	Promise<Response>;
		close														// Finishes zipping
		(
			handle: number,											// Handle to zip file (returned by create)
			preserve?: boolean										// False if response payload must be converted to base 64
		):	Promise<Response>;										// Payload returns an Uint8Array with zip file.
	}
	class Inflate													// Uncompress files in PKWare format
	{
		open														// Opens a zip file
		(
			zip: ArrayBuffer | Uint8Array							// Zip file
		): Promise<Response>;										// Payload returns a handle to zip file
		list														// List all entries of zip files
		(
			handle: number											// Handle to zip file (returned by open)
		): Promise<Response>;										// Payload returns an Array of string
		inflate														// Gets zip entry specified by its name
		(
			handle: number,											// Handle to zip file (returned by open)
			name: string,											// Entry name (returned by list)
			preserve?: boolean										// False if response payload must be converted to base 64
		): Promise<Response>;										// Payload contais an Uint8Array
		close														// Closes zip entry
		(
			handle: number											// Handle to zip file (returned by open)
		): Promise<Response>;
	}
	class Base64													// Base64 conversion facility
	{
		static btoa(bytes: Uint8Array) : string;					// Encodes specified binary data
		static atob(base64: string): Uint8Array;					// Decodes specified Base64 value
	}
}
declare namespace kptasign
{
	class KPTAError extends kptazip.KPTAError {}
	interface Protocol extends kptazip.Protocol {}
	interface Response extends kptazip.Response {}
	class Certificate												// Refers to an installed user certificate
	{
		subject: string;											// Certificate subject common name
		issuer: string;												// Certificate issuer common name
		serial: string;												// Certificate serial number as an hexadecimal string
		company_id: string | undefined;								// OID ICP-Brasil 2.16.76.1.3.3 e conteúdo nas 14 (quatorze) posições o número do CNPJ da pessoa jurídica titular do certificado
		sponsor_id: string | undefined;								// OID ICP-Brasil 2.16.76.1.3.4 e conteúdo nas primeiras 8 (oito) posições, a data de nascimento do responsável pelo certificado, no formato ddmmaaaa; nas 11 (onze) posições subsequentes, o Cadastro de Pessoa Física (CPF) do responsável; etc
		subject_id: string | undefined;								// OID ICP-Brasil 2.16.76.1.3.1 e conteúdo nas primeiras 8 (oito) posições, a data de nascimento do titular,  no formato ddmmaaaa; nas 11 (onze) posições subsequentes, o Cadastro de Pessoa Física (CPF) do  titular; etc.
	}
	class Sign														// Sign documents and transactions
	{
		enumerateCerts												// Enumerates installed certificates which have private key
		(): Promise<Response>;										// Payload returns a (possibly empty) array of certificate objects.
		sign														// Signs specified document
		(
			certificate: Certificate,								// Signing certificate
			document: string,										// Document (or transaction) to sign
			attach: boolean,										// True if transaction must be attached to generated CMS Signed Data
			preserve?: boolean										// False if response payload must be converted to base 64
		): Promise<Response>;										// Payload should return CMS Signed data envelope as an Uint8Array object
		sign														// Signs specified document
		(
			certificate: Certificate,								// Signing certificate
			document: ArrayBuffer,									// Document to sign
			attach: boolean,										// True if document must be attached to generated CMS Signed Data
			preserve?: boolean										// False if response payload must be converted to base 64
		): Promise<Response>;										// Payload should return CMS Signed data envelope as an Uint8Array object
	}
	class Base64													// Base64 conversion facility
	{
		static btoa(bytes: Uint8Array) : string;					// Encodes specified binary data
		static atob(base64: string): Uint8Array;					// Decodes specified Base64 value
	}
}
declare namespace kptaverify
{
	class KPTAError extends kptazip.KPTAError {}
	interface Protocol extends kptazip.Protocol {}
	interface Response extends kptazip.Response {}
	class SignerIdentifier											// SignerIdentifier CMS field
	{
		issser: string | undefined;									// Signer certificate issuer common name
		serial: string | undefined;									// Hexadecimal representation of signer certificate serial number
		subjectKeyIdentifier: string | undefined;					// Hexadecimal representation of Subject Key Identifier extension value
	}
	class Verify
	{
		getSignerId													// Gets signer identifier CMS field value
		(
			cms: string												// PEM encoded CMS Signed Data document
		): Promise<Response>;										// Response.payload must return a SignerIdentifier object
		getSignerId													// Gets signer identifier CMS field value
		(
			cms: ArrayBuffer										// DER encoded CMS Signed Data document
		): Promise<Response>;										// Response.payload must return a SignerIdentifier object
		getSignerCert												// Gets signer certificate
		(
			cms: string												// PEM encoded CMS Signed Data document
		): Promise<Response>;										// Response.payload should return a PEM encoded certificate, if it is present
		getSignerCert												// Gets signer certificate
		(
			cms: ArrayBuffer										// DER encoded CMS Signed Data document
		): Promise<Response>;										// Response.payload should return a PEM encoded certificate, if it is present
		getSignerCert												// Gets signer certificate
		(
			cms: string,											// PEM encoded CMS Signed Data document
			preserve: boolean										// If true, Response.payload should return a DER encoded certificate, if it is present; otherwise, a PEM encoded certificate should be returned
		): Promise<Response>;										// Response.payload should return the Signer certificate
		getSignerCert												// Gets signer certificate
		(
			cms: ArrayBuffer,										// DER encoded CMS Signed Data document
			preserve: boolean										// If true, Response.payload should return a DER encoded certificate, if it is present; otherwise, a PEM encoded certificate should be returned
		): Promise<Response>;										// Response.payload should return the Signer certificate
		getContent													// Gets signed content info field value, if it is present
		(
			cms: string												// PEM encoded CMS Signed Data document
		): Promise<Response>;										// Response.payload should return a base64 encoded signed content info, if it is present
		getContent													// Gets signed content info field value, if it is present
		(
			cms: ArrayBuffer										// DER encoded CMS Signed Data document
		): Promise<Response>;										// Response.payload should return a base64 encoded signed content info, if it is present
		getContent													// Gets signed content info field value, if it is present
		(
			cms: string,											// PEM encoded CMS Signed Data document
			preserve: boolean										// If true, Response.payload should return signed content as is, if it is present; otherwise, a base64 encoded signed content should be returned
		): Promise<Response>;										// Response.payload should return signed content, if it is present
		getContent													// Gets signed content info field value, if it is present
		(
			cms: ArrayBuffer,										// DER encoded CMS Signed Data document
			preserve: boolean										// If true, Response.payload should return signed content as is, if it is present; otherwise, a base64 encoded signed content should be returned
		): Promise<Response>;										// Response.payload should return signed content, if it is present
		isTrusted													// Checks if specified certificate is trusted
		(
			certificate: string										// PEM encoded certificate
		): Promise<Response>;
		isTrusted													// Checks if specified certificate is trusted
		(
			certificate: ArrayBuffer								// DER encoded certificate
		): Promise<Response>;
		verify														// Verify specified CMS Signed Data document
		(
			cms: string,											// PEM encoded CMS Signed Data document
			content: string,										// Signed content
			certificate: string										// PEM encoded signer certififcate
		): Promise<Response>;
		verify														// Verify specified CMS Signed Data document
		(
			cms: ArrayBuffer,										// DER encoded CMS Signed Data document
			content: ArrayBuffer,									// Signed content
			certificate: ArrayBuffer								// DER encoded signer certificate
		): Promise<Response>;
		verify														// Verify specified CMS Signed Data document. Both signed content and signer certificate must be embbeded
		(
			cms: string												// PEM encoded CMS Signed Data document
		): Promise<Response>;
		verify														// Verify specified CMS Signed Data document. Both signed content and signer certificate must be embbeded
		(
			cms: ArrayBuffer										// DER encoded CMS Signed Data document
		): Promise<Response>;
	}
	class Base64													// Base64 conversion facility
	{
		static btoa(bytes: Uint8Array) : string;					// Encodes specified binary data
		static atob(base64: string): Uint8Array;					// Decodes specified Base64 value
	}
}

declare namespace kptaenroll
{
	class KPTAError extends kptazip.KPTAError {}
	interface Protocol extends kptazip.Protocol {}
	interface Response extends kptazip.Response {}
	class Enroll
	{
		enumerateDevices											// Enumerates installed cryptographic devices
		(): Promise<Response>;										// Response.payload should return an array of strings
		generateCSR													// Generates a certificate signing request
		(
			dev: string,											// Cryptographic device that must generate RSA pair. Must be returned by enumerateDevices()
			cn: string												// User Common Name
		): Promise<Response>;										// Response.payload should return generated CSR
		generateCSR													// Generates a certificate signing request
		(
			dev: string,											// Cryptographic device that must generate RSA pair. Must be returned by enumerateDevices()
			cn: string,												// User Common Name
			preserve: boolean										// If true, Response.payload should return a DER encoded CSR; otherwise, a PEM encoded CSR should be returned
		): Promise<Response>;										// Response.payload should return generated CSR
		installCertificate											// Installs signed certificate (and its CA chain, if possible)
		(
			cms: string												// PEM encoded CMS Signed Data with embedded chain
		): Promise<Response>;
		installCertificate											// Installs signed certificate (and its CA chain, if possible)
		(
			cms: ArrayBuffer										// DER encoded CMS Signed Data with embedded chain
		): Promise<Response>;
	}
	class Base64													// Base64 conversion facility
	{
		static btoa(bytes: Uint8Array) : string;					// Encodes specified binary data
		static atob(base64: string): Uint8Array;					// Decodes specified Base64 value
	}
}