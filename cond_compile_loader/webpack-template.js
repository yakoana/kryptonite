const path = require('path');
	module.exports =
	{
		mode: 'development',
		entry: path.resolve(__dirname, 'teste.ts'),
		devtool: 'inline-source-map',
		module:
		{
			rules:
			[
				{
					test: /\.tsx?$/,
					use: [{loader:'ts-loader'}, {loader: path.resolve(__dirname, 'loader.js'), options: { DEBUG : false , OUTRO: true, EOUTRO: false }}],
					exclude: /node_modules/
				}
			]
		},
		node:
		{
			fs: 'empty',
			Buffer : true
		},
		resolve:
		{
			extensions: [ '.tsx', '.ts', '.js' ]
		},
		output:
		{
			filename: 'outro.js',
			path: path.resolve(__dirname, 'dist')
		}
	};