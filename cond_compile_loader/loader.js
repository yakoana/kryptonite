const getOptions =  require("loader-utils").getOptions;

var ifelse =  /\/\/IFDEF\((.*?)\)([\s\S]*?)\/\/ELSE([\s\S]*?)\/\/ENDIF/gm;
var ifonly =  /\/\/IFDEF\((.*?)\)([\s\S]*?)\/\/ENDIF/gm;

module.exports=function(source) {
	const options = getOptions(this);
	new_source = source;
	match = ifelse.exec(source);
	while (match != null) {
		if(options[match[1]])
			new_source =  new_source.replace(match[0], match[2]);
		else
			new_source = new_source.replace(match[0], match[3]);
		ifelse.lastIndex=0;
		match = ifelse.exec(new_source);
	}
	
	
	match = ifonly.exec(source);
	while (match != null) {
		if(options[match[1]])
			new_source =  new_source.replace(match[0], match[2]);
		else
			new_source = new_source.replace(match[0], "");
		ifonly.lastIndex=0;
		match = ifonly.exec(new_source);
	}
	
	return new_source;
	
}